package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.stux.R;
import com.stux.Utils.CircleImageView;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class LostFoundItemViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView img_loading;
    CircleImageView cimg_item;
    TextView txt_c_time, txt_c_name, txt_c_description, txt_c_location;
    LinearLayout ll_c_main;

    public LostFoundItemViewHolders(View convertView) {
        super(convertView);
        convertView.setOnClickListener(this);

        cimg_item = (CircleImageView) convertView.findViewById(R.id.cimg_item);
        img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
        txt_c_time = (TextView) convertView.findViewById(R.id.txt_c_time);
        txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
        txt_c_description = (TextView) convertView.findViewById(R.id.txt_c_description);
        txt_c_location = (TextView) convertView.findViewById(R.id.txt_c_location);
        ll_c_main = (LinearLayout) convertView.findViewById(R.id.ll_c_main);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
