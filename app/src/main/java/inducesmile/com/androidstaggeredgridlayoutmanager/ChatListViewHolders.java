package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.rockerhieu.emojicon.EmojiconTextView;
import com.stux.R;
import com.stux.Utils.CircleImageView;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class ChatListViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    CircleImageView cimg_user_sender, cimg_user_receiver;
    ImageView img_loading_sender, img_loading_receiver;
    RelativeLayout rl_c_sender, rl_c_receiver;
    EmojiconTextView etxt_name_sender, etxt_name_receiver;
    TextView txt_c_time_sender, txt_c_time_receiver;
    RelativeLayout rl_c_main;

    public ChatListViewHolders(View convertView) {
        super(convertView);
//        convertView.setOnClickListener(this);

        cimg_user_sender = (CircleImageView) convertView.findViewById(R.id.cimg_user_sender);
        cimg_user_sender.setImageDrawable(null);
        cimg_user_receiver = (CircleImageView) convertView.findViewById(R.id.cimg_user_receiver);
        cimg_user_receiver.setImageDrawable(null);

        img_loading_sender = (ImageView) convertView.findViewById(R.id.img_loading_sender);
        img_loading_receiver = (ImageView) convertView.findViewById(R.id.img_loading_receiver);

        rl_c_sender = (RelativeLayout) convertView.findViewById(R.id.rl_c_sender);
        rl_c_receiver = (RelativeLayout) convertView.findViewById(R.id.rl_c_receiver);

        etxt_name_sender = (EmojiconTextView) convertView.findViewById(R.id.etxt_name_sender);
        etxt_name_receiver = (EmojiconTextView) convertView.findViewById(R.id.etxt_name_receiver);

        txt_c_time_sender = (TextView) convertView.findViewById(R.id.txt_c_time_sender);
        txt_c_time_receiver = (TextView) convertView.findViewById(R.id.txt_c_time_receiver);
        rl_c_main = (RelativeLayout) convertView.findViewById(R.id.rl_c_main);
    }

    @Override
    public void onClick(View view) {
//        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
