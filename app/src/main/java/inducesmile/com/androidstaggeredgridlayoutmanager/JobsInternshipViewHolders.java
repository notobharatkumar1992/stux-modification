package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.stux.R;
import com.stux.Utils.CircleImageView;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class JobsInternshipViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    CircleImageView cimg_user;
    TextView txt_c_condition, txt_c_jobs_name, txt_c_company_name, txt_c_address, txt_c_time;
    ImageView img_loading;

    LinearLayout ll_c_main;
    RelativeLayout rl_c_main;

    public JobsInternshipViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        ll_c_main = (LinearLayout) itemView.findViewById(R.id.ll_c_main);
        rl_c_main = (RelativeLayout) itemView.findViewById(R.id.rl_c_main);
        cimg_user = (CircleImageView) itemView.findViewById(R.id.cimg_user);
        cimg_user.setImageDrawable(null);

        img_loading = (ImageView) itemView.findViewById(R.id.img_loading);

        txt_c_condition = (TextView) itemView.findViewById(R.id.txt_c_condition);
        txt_c_jobs_name = (TextView) itemView.findViewById(R.id.txt_c_jobs_name);
        txt_c_company_name = (TextView) itemView.findViewById(R.id.txt_c_company_name);
        txt_c_address = (TextView) itemView.findViewById(R.id.txt_c_address);
        txt_c_time = (TextView) itemView.findViewById(R.id.txt_c_time);
    }

    @Override
    public void onClick(View view) {
//        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
