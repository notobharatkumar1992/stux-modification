package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.stux.AppDelegate;
import com.stux.Models.EventModel;
import com.stux.R;
import com.stux.Utils.TransitionHelper;
import com.stux.activities.EventDetailActivity;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class EventRecyclerViewAdapter extends RecyclerView.Adapter<EventViewHolders> {

    private ArrayList<EventModel> eventArray;
    private Activity mContext;
    private OnListItemClickListener itemClickListener;
    private String tagValue = "";
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    public EventRecyclerViewAdapter(Activity mContext, ArrayList<EventModel> eventArray, OnListItemClickListener itemClickListener, String tagValue) {
        this.eventArray = eventArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
        this.tagValue = tagValue;
        imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
    }

    @Override
    public EventViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_item, null);
        EventViewHolders rcv = new EventViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final EventViewHolders holder, final int position) {
        try {
            String time = eventArray.get(position).created.substring(0, eventArray.get(position).created.lastIndexOf("+"));
//        2016-06-27T06:32
            try {
                time = new SimpleDateFormat("dd MMM, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }
            holder.txt_c_time.setText(time);
            holder.txt_c_name.setText(eventArray.get(position).event_name);

            if (eventArray.get(position).gate_fees_type == 0) {
                holder.txt_c_free_entry.setVisibility(View.VISIBLE);
                holder.txt_c_ticket.setVisibility(View.GONE);
            } else {
                holder.txt_c_free_entry.setVisibility(View.GONE);
                holder.txt_c_ticket.setVisibility(View.VISIBLE);
                holder.txt_c_ticket.setText("Ticket : " + eventArray.get(position).gate_fees);
            }

            holder.txt_c_view.setText(eventArray.get(position).total_event_views);

            holder.txt_c_description.setText(eventArray.get(position).details);
            holder.img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
            frameAnimation.setCallback(holder.img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
            imageLoader.loadImage(eventArray.get(position).banner_image_thumb, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.img_events.setImageBitmap(loadedImage);
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });

//            Picasso.with(mContext).load(eventArray.get(position).banner_image_thumb).into(holder.img_events, new Callback() {
//                @Override
//                public void onSuccess() {
//                    holder.img_loading.setVisibility(View.GONE);
//                }
//
//                @Override
//                public void onError() {
//
//                }
//            });
            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Tags.EVENT, eventArray.get(position));
                        if (AppDelegate.isValidString(tagValue))
                            bundle.putString(Tags.FROM, tagValue);
                        Intent intent = new Intent(mContext, EventDetailActivity.class);
                        intent.putExtras(bundle);
                        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(mContext, false, new Pair<>(holder.img_events, "image"));
                        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(mContext, pairs);
                        mContext.startActivity(intent/*, transitionActivityOptions.toBundle()*/);
//                        itemClickListener.setOnListItemClickListener(AppDelegate.isValidString(tagValue) ? tagValue : Tags.event_name, position);
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.eventArray.size();
    }
}
