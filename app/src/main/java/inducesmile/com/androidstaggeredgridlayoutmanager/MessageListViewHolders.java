package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.stux.R;
import com.stux.Utils.CircleImageView;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class MessageListViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    CircleImageView cimg_user;
    ImageView img_loading;
    TextView txt_c_name, txt_c_description, txt_c_message, txt_c_time, txt_c_message_count;
    LinearLayout ll_c_main;

    public MessageListViewHolders(View convertView) {
        super(convertView);
        convertView.setOnClickListener(this);

        cimg_user = (CircleImageView) convertView.findViewById(R.id.cimg_user);
        cimg_user.setImageDrawable(null);
        img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
        txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
        txt_c_description = (TextView) convertView.findViewById(R.id.txt_c_description);
        txt_c_message = (TextView) convertView.findViewById(R.id.txt_c_message);
        txt_c_time = (TextView) convertView.findViewById(R.id.txt_c_time);
        txt_c_message_count = (TextView) convertView.findViewById(R.id.txt_c_message_count);
        ll_c_main = (LinearLayout) convertView.findViewById(R.id.ll_c_main);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
