package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.stux.AppDelegate;
import com.stux.Models.MoviesModel;
import com.stux.R;
import com.stux.Utils.TransitionHelper;
import com.stux.activities.MoviesDetailsActivity;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class MoviesRecyclerViewAdapter extends RecyclerView.Adapter<MoviesViewHolders> {
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private ArrayList<MoviesModel> moviesArray;
    private Activity mContext;
    private OnListItemClickListener itemClickListener;

    public MoviesRecyclerViewAdapter(Activity mContext, ArrayList<MoviesModel> moviesArray, OnListItemClickListener itemClickListener) {
        this.moviesArray = moviesArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
        imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
    }

    @Override
    public MoviesViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movies_list_item, null);
        MoviesViewHolders rcv = new MoviesViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final MoviesViewHolders holder, final int position) {
        try {
            holder.txt_c_name.setText(moviesArray.get(position).title);

            holder.img_loading.setVisibility(View.VISIBLE);
            if (holder.img_loading != null) {
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
            }

            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
//                        itemClickListener.setOnListItemClickListener(Tags.movies, position);
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Tags.movies, moviesArray.get(position));
                        Intent intent = new Intent(mContext, MoviesDetailsActivity.class);
                        intent.putExtras(bundle);
                        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(mContext, false, new Pair<>(holder.img_content, "image"));
                        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(mContext, pairs);
                        mContext.startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                    }
                }
            });
            holder.img_content.setImageDrawable(null);
            imageLoader.loadImage(moviesArray.get(position).banner_image, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    try {
                    } catch (Exception e) {
                    }
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    holder.img_content.mWidth = bitmap.getWidth();
                    holder.img_content.mHeight = bitmap.getHeight();
                    holder.img_content.setImageBitmap(bitmap);
                    holder.img_loading.setVisibility(View.GONE);
                    try {
                    } catch (Exception e) {
                    }
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });
//            Picasso.with(mContext).load(moviesArray.get(position).banner_image).into(new Target() {
//                @Override
//                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                    holder.img_content.mWidth = bitmap.getWidth();
//                    holder.img_content.mHeight = bitmap.getHeight();
//                    holder.img_content.setImageBitmap(bitmap);
//                    holder.img_loading.setVisibility(View.GONE);
//                    try {
//                        notifyDataSetChanged();
//                    } catch (Exception e) {
//                    }
//                }
//
//                @Override
//                public void onBitmapFailed(Drawable errorDrawable) {
//                    try {
//                        notifyDataSetChanged();
//                    } catch (Exception e) {
//                    }
//                }
//
//                @Override
//                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                }
//            });

//
//            Target target = new Target() {
//
//                @Override
//                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                    targets.remove(this);
//                    AppDelegate.LogT("onBitmapLoaded: called " + position);
//                    holder.img_content.mWidth = bitmap.getWidth();
//                    holder.img_content.mHeight = bitmap.getHeight();
//                    holder.img_content.setImageBitmap(bitmap);
//                    holder.img_loading.setVisibility(View.GONE);
//                }
//
//                @Override
//                public void onBitmapFailed(Drawable errorDrawable) {
//                    targets.remove(this);
//                    AppDelegate.LogT("onBitmapFailed: called " + position);
//                }
//
//                @Override
//                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    AppDelegate.LogT("Preparing: called " + position);
//                }
//            };
//            targets.add(target);
//            Picasso.with(mContext)
//                    .load(moviesArray.get(position).banner_image) // Start loading the current target
//                    .into(target);

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }


    @Override
    public int getItemCount() {
        return this.moviesArray.size();
    }
}
