package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bulletnoid.android.widget.StaggeredGridViewDemo.STGVImageView;
import com.lid.lib.LabelImageView;
import com.stux.R;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;

public class ProductViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public LinearLayout ll_c_main;
    public RelativeLayout rl_c_main;
    public ImageView img_product, img_star_1, img_star_2, img_star_3, img_star_4, img_star_5, img_loading;
    public LabelImageView label_image;
    public carbon.widget.TextView txt_c_product_name, txt_c_price, txt_c_condition;
    public carbon.widget.ImageView img_c_like;
    public STGVImageView img_content;

    public ProductViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        ll_c_main = (LinearLayout) itemView.findViewById(R.id.ll_c_main);
        rl_c_main = (RelativeLayout) itemView.findViewById(R.id.rl_c_main);
        img_product = (ImageView) itemView.findViewById(R.id.img_product);
        img_loading = (ImageView) itemView.findViewById(R.id.img_loading);
        img_content = (STGVImageView) itemView.findViewById(R.id.img_content);
        img_content.setImageDrawable(null);

        img_star_1 = (ImageView) itemView.findViewById(R.id.img_star_1);
        img_star_2 = (ImageView) itemView.findViewById(R.id.img_star_2);
        img_star_3 = (ImageView) itemView.findViewById(R.id.img_star_3);
        img_star_4 = (ImageView) itemView.findViewById(R.id.img_star_4);
        img_star_5 = (ImageView) itemView.findViewById(R.id.img_star_5);

        img_c_like = (carbon.widget.ImageView) itemView.findViewById(R.id.img_c_like);

        label_image = (LabelImageView) itemView.findViewById(R.id.label_image);
        txt_c_product_name = (carbon.widget.TextView) itemView.findViewById(R.id.txt_c_product_name);
        txt_c_price = (carbon.widget.TextView) itemView.findViewById(R.id.txt_c_price);
        txt_c_condition = (carbon.widget.TextView) itemView.findViewById(R.id.txt_c_condition);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
