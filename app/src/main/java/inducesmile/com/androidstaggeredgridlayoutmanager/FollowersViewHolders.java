package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.stux.R;
import com.stux.Utils.CircleImageView;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class FollowersViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txt_c_name, txt_c_description, txt_c_follow;
    public ImageView img_loading;
    public carbon.widget.ImageView img_c_follow;
    public LinearLayout ll_c_following;
    public CircleImageView cimg_user;
    public RelativeLayout rl_c_main;

    public FollowersViewHolders(View convertView) {
        super(convertView);
        convertView.setOnClickListener(this);

        txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
        txt_c_description = (TextView) convertView.findViewById(R.id.txt_c_description);
        txt_c_follow = (TextView) convertView.findViewById(R.id.txt_c_follow);
        ll_c_following = (LinearLayout) convertView.findViewById(R.id.ll_c_following);
        img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
        img_c_follow = (carbon.widget.ImageView) convertView.findViewById(R.id.img_c_follow);
        cimg_user = (CircleImageView) convertView.findViewById(R.id.cimg_user);
        rl_c_main = (RelativeLayout) convertView.findViewById(R.id.rl_c_main);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
