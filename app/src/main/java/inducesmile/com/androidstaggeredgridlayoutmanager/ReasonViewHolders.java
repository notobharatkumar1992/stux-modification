package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.stux.R;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class ReasonViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    LinearLayout ll_c_main;
    RelativeLayout rl_c_main;
    ImageView img_reason;
    TextView txt_c_name;

    public ReasonViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        ll_c_main = (LinearLayout) itemView.findViewById(R.id.ll_c_main);
        rl_c_main = (RelativeLayout) itemView.findViewById(R.id.rl_c_main);
        img_reason = (ImageView) itemView.findViewById(R.id.img_reason);
        img_reason.setImageDrawable(null);
        txt_c_name = (TextView) itemView.findViewById(R.id.txt_c_name);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
