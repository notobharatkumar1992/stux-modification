package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.stux.R;
import com.stux.Utils.CircleImageView;

import carbon.widget.TextView;

public class NotificationViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txt_c_name, txt_c_time;
    public ImageView img_loading, img_product;
    public CircleImageView cimg_user, cimg_user_ph;
    public RelativeLayout rl_c_following, rl_user_img;

    public NotificationViewHolders(View convertView) {
        super(convertView);
        convertView.setOnClickListener(this);

        txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
        txt_c_time = (TextView) convertView.findViewById(R.id.txt_c_time);
        img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
        img_product = (ImageView) convertView.findViewById(R.id.img_product);
        cimg_user = (CircleImageView) convertView.findViewById(R.id.cimg_user);
        cimg_user_ph = (CircleImageView) convertView.findViewById(R.id.cimg_user_ph);
        rl_c_following = (RelativeLayout) convertView.findViewById(R.id.rl_c_following);
        rl_user_img = (RelativeLayout) convertView.findViewById(R.id.rl_user_img);

//        title = "This is <font color='blue'>simple</font>.";
//        holder.txt_c_name.setText(Html.fromHtml(title), TextView.BufferType.SPANNABLE);
    }

    @Override
    public void onClick(View view) {
//        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
