package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.stux.AppDelegate;
import com.stux.Models.ItemFoundModel;
import com.stux.R;
import com.stux.Utils.TransitionHelper;
import com.stux.activities.LostFoundDetailActivity;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class LostFoundItemRecyclerViewAdapter extends RecyclerView.Adapter<LostFoundItemViewHolders> {
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private ArrayList<ItemFoundModel> itemFoundArray;
    private Activity mContext;
    private OnListItemClickListener itemClickListener;
    private String tagValue = "";

    public LostFoundItemRecyclerViewAdapter(Activity mContext, ArrayList<ItemFoundModel> itemFoundArray, OnListItemClickListener itemClickListener) {
        this.itemFoundArray = itemFoundArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    public LostFoundItemRecyclerViewAdapter(Activity mContext, ArrayList<ItemFoundModel> itemFoundArray, OnListItemClickListener itemClickListener, String tagValue) {
        this.itemFoundArray = itemFoundArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
        this.tagValue = tagValue;
        imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));

    }

    @Override
    public LostFoundItemViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lost_found_list_item, null);
        LostFoundItemViewHolders rcv = new LostFoundItemViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final LostFoundItemViewHolders holder, final int position) {
        try {
            String time = itemFoundArray.get(position).created.substring(0, itemFoundArray.get(position).created.lastIndexOf("+"));
            try {
                time = new SimpleDateFormat("MMM dd yyyy, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }
            holder.txt_c_time.setText(time);
            holder.txt_c_name.setText(itemFoundArray.get(position).item_name);
            holder.txt_c_description.setText(itemFoundArray.get(position).item_description);
            holder.txt_c_location.setText(AppDelegate.isValidString(itemFoundArray.get(position).location) ? itemFoundArray.get(position).location : "");

            holder.img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
            frameAnimation.setCallback(holder.img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Tags.FOUND, itemFoundArray.get(position));
                        Intent intent = new Intent(mContext, LostFoundDetailActivity.class);
                        intent.putExtras(bundle);
                        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(mContext, false, new Pair<>(holder.txt_c_name, "name"), new Pair<>(holder.cimg_item, "image"));
                        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(mContext, pairs);
                        mContext.startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                    }
                }
            });

            imageLoader.loadImage(itemFoundArray.get(position).image_thumb_1, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    holder.cimg_item.setImageBitmap(bitmap);
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });
//            Picasso.with(mContext).load(itemFoundArray.get(position).image_thumb_1).into(holder.cimg_item, new Callback() {
//                @Override
//                public void onSuccess() {
//                    holder.img_loading.setVisibility(View.GONE);
//                    notifyDataSetChanged();
//                }
//
//                @Override
//                public void onError() {
//
//                }
//            });
        } catch (Exception e) {
            AppDelegate.LogE(e, 1);
        }
    }

    @Override
    public int getItemCount() {
        return this.itemFoundArray.size();
    }
}
