package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.stux.AppDelegate;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

public class FollowersRecyclerViewAdapter extends RecyclerView.Adapter<FollowersViewHolders> {
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private Context mContext;
    private ArrayList<UserDataModel> arrayUser;
    private OnListItemClickListener clickListener;
    private Prefs prefs;
    private UserDataModel dataModel;

    public FollowersRecyclerViewAdapter(Context mContext, ArrayList<UserDataModel> arrayUser, OnListItemClickListener clickListener) {
        this.arrayUser = arrayUser;
        this.mContext = mContext;
        this.clickListener = clickListener;
        this.prefs = new Prefs(mContext);
        this.dataModel = prefs.getUserdata();
        imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
    }

    @Override
    public FollowersViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.followers_list_item, null);
        FollowersViewHolders rcv = new FollowersViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final FollowersViewHolders holder, final int position) {
        try {
            holder.txt_c_name.setText(arrayUser.get(position).first_name + " " + arrayUser.get(position).last_name);
            holder.txt_c_description.setText(arrayUser.get(position).institutionModel.institution_name + " - " + arrayUser.get(position).institutionModel.department_name);

            holder.rl_c_main.setVisibility(View.VISIBLE);
            holder.img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
            frameAnimation.setCallback(holder.img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            if (AppDelegate.isValidString(arrayUser.get(position).image)){
                imageLoader.loadImage(arrayUser.get(position).image, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        holder.img_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        holder.cimg_user.setImageBitmap(loadedImage);
                        try {
                            holder.img_loading.setVisibility(View.GONE);
                        } catch (Exception e) {
//                            AppDelegate.LogE(e);
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });
            }

//                Picasso.with(mContext).load(arrayUser.get(position).image).into(holder.cimg_user, new Callback() {
//                    @Override
//                    public void onSuccess() {
//                        try {
//                            holder.img_loading.setVisibility(View.GONE);
//                            notifyDataSetChanged();
//                        } catch (Exception e) {
////                            AppDelegate.LogE(e);
//                        }
//                    }
//
//                    @Override
//                    public void onError() {
//
//                    }
//                });

            try {
                if (arrayUser.get(position).userId.equalsIgnoreCase(dataModel.userId)) {
                    holder.ll_c_following.setVisibility(View.GONE);
                } else {
                    holder.ll_c_following.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            holder.ll_c_following.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null)
                        clickListener.setOnListItemClickListener(Tags.follow_status, position);
                }
            });

            holder.rl_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null)
                        clickListener.setOnListItemClickListener(Tags.follower_id, position);
                }
            });

            if (arrayUser.get(position).follow_status == 1) {
                holder.ll_c_following.setSelected(true);
                holder.img_c_follow.setVisibility(View.VISIBLE);
                holder.img_c_follow.setImageResource(R.drawable.following);
                holder.txt_c_follow.setText("Following");
                holder.txt_c_follow.setTextColor(Color.WHITE);
            } else {
                holder.ll_c_following.setSelected(false);
                holder.img_c_follow.setVisibility(View.GONE);
                holder.txt_c_follow.setText("+ Follow");
                holder.txt_c_follow.setTextColor(mContext.getResources().getColor(R.color.hint_color));
            }

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.arrayUser.size();
    }
}
