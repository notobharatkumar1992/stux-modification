package com.stux;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by Bharat on 08/15/2016.
 */
public class PreviewActivity extends FragmentActivity implements View.OnClickListener {

    private ImageView img_captured, img_dustbin, img_text, img_icon, img_download, img_attach, img_send;

    private int type = 0;
    private String filePath = "";
    private File file;

    private VideoView videoView;

    private boolean fileDelete = true;
    private boolean fileSaved = false;

    public static final int FILE_TYPE_IMAGE = 0, FILE_TYPE_VIDEO = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.image_preview_activity);
        type = getIntent().getExtras().getInt("from");
        filePath = getIntent().getExtras().getString("file");
        file = new File(filePath);
        initView();
        setValues();
    }

    private void setValues() {
        if (type == FILE_TYPE_IMAGE) {
            img_captured.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.GONE);
            img_captured.setImageBitmap(VideoRecordingPreLollipopActivity.decodeFile(file));
        } else {
            img_captured.setVisibility(View.GONE);
            videoView.setVisibility(View.VISIBLE);
            videoView.setMediaController(new MediaController(this));
            videoView.setVideoURI(Uri.fromFile(file));
            videoView.requestFocus();
            videoView.start();
        }
    }

    private void initView() {
        img_captured = (ImageView) findViewById(R.id.img_captured);
        videoView = (VideoView) findViewById(R.id.videoView);

        img_dustbin = (ImageView) findViewById(R.id.img_dustbin);
        img_dustbin.setOnClickListener(this);
        img_text = (ImageView) findViewById(R.id.img_text);
        img_text.setOnClickListener(this);
        img_icon = (ImageView) findViewById(R.id.img_icon);
        img_icon.setOnClickListener(this);
        img_download = (ImageView) findViewById(R.id.img_download);
        img_download.setOnClickListener(this);
        img_attach = (ImageView) findViewById(R.id.img_attach);
        img_attach.setOnClickListener(this);
        img_send = (ImageView) findViewById(R.id.img_send);
        img_send.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_dustbin:
                try {
                    if (file != null)
                        file.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
                break;

            case R.id.img_text:
                break;

            case R.id.img_icon:
                break;

            case R.id.img_download:
                if (!fileSaved) {
                    fileSaved = true;
                    fileDelete = false;
                    Toast.makeText(PreviewActivity.this, type == FILE_TYPE_IMAGE ? "Image" : "Video" + " saved at location : " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(PreviewActivity.this, type == FILE_TYPE_IMAGE ? "Image" : "Video" + " already saved.", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.img_attach:
                break;

            case R.id.img_send:
                // Send code,
                break;
        }
    }

    @Override
    protected void onDestroy() {
        try {
            if (fileDelete) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }

    public static Bitmap getBitmapAndWriteFile(Context mContext, String path) {
        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 500000; // 0.5MP
            in = mContext.getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
                scale++;
            }
            Bitmap b = null;
            in = mContext.getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);
                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();

                double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();
            Log.d("test", "getBitmapAndWriteFile => File saved successfully : ");

            storeImage(path, b);
            return b;
        } catch (IOException e) {
            AppDelegate.LogE(e);
            return null;
        }
    }

    public static void storeImage(String filePath, Bitmap image) {
        File pictureFile = new File(filePath);
        if (pictureFile == null) {
            Log.d("test", "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 0, fos);
            fos.close();
            Log.d("test", "storeImage => File saved successfully : " + filePath);
        } catch (FileNotFoundException e) {
            Log.d("test", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("test", "Error accessing file: " + e.getMessage());
        }
    }
}
