package com.stux.Async;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.stux.AppDelegate;
import com.stux.constants.Tags;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationAddress {

    // public static final String OPEN_MAP_KEY =
    // "Fmjtd|luu82q6rnu%2Cb2%3Do5-94tnl6";
    public static final String OPEN_MAP_KEY = "Fmjtd|luu82q6rnu,b2=o5-94tnl6";

    public static String placeName = null, placeAdd = null, result = null,
            geocodeQuality = null, street = null, adminArea1 = null,
            adminArea3 = null, adminArea4 = null, adminArea5 = null, city = null,
            country = null, postalCode = null;

    public static void setNullValue() {
        placeName = null;
        placeAdd = null;
        result = null;
        geocodeQuality = null;
        street = null;
        adminArea3 = null;
        adminArea4 = null;
        adminArea5 = null;
    }

    public static void getAddressFromLocation(final double latitude,
                                              final double longitude, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    setNullValue();
                    execute_Geocoder(latitude, longitude, context, handler);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        };
        thread.start();
    }

    public static void execute_Geocoder(final double latitude,
                                        final double longitude, final Context context, final Handler handler) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocation(latitude,
                    longitude, 1);
            AppDelegate.LogT("addressList latitude = " + latitude
                    + ", longitude = " + longitude + ", addressList size = "
                    + addressList.size());
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                AppDelegate.LogUR("addressList = " + address);
                StringBuilder sb = new StringBuilder();
                placeName = address.getAddressLine(0);
                if (address.getAddressLine(1) != null && !address.getAddressLine(1).equalsIgnoreCase("null") && address.getAddressLine(0).length() > 0) {
                    placeName = placeName + ", " + address.getAddressLine(1);
                    placeName = placeName.replace("null,", " ");
                    placeName = placeName.replace("null", " ");
                }
                if (placeName != null && placeName.contains("null")) {
                    placeName = placeName.replace("null,", " ");
                    placeName = placeName.replace("null", " ");
                }
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append(", ");
                }

                sb.append(address.getLocality()).append(", ");
                sb.append(address.getCountryName());

                if (address.getLocality() != null
                        && !address.getLocality().equalsIgnoreCase("null")) {
                    placeAdd = address.getLocality() + ", "
                            + address.getCountryName();
                } else {
                    placeAdd = address.getCountryName();
                }
                if (placeAdd != null && placeAdd.contains("null")) {
                    placeAdd = placeAdd.replace("null,", " ");
                    placeAdd = placeAdd.replace("null", " ");
                }
                if (placeAdd.contains(placeName)) {
                    placeAdd.replace(placeName, "");
                } else if (placeName.contains(placeAdd)) {
                    placeName.replace(placeAdd, "");
                }

                city = address.getSubAdminArea();
                country = address.getCountryName();
                postalCode = address.getPostalCode();

                result = sb.toString();
            }
        } catch (IOException e) {
            AppDelegate.LogE(e);
        } finally {
            AppDelegate.LogE("Finally called for LocationAddress");
            Message message = Message.obtain();
            message.setTarget(handler);

            message.what = 2;
            Bundle bundle = new Bundle();
            bundle.putDouble(Tags.LAT, latitude);
            bundle.putDouble(Tags.LNG, longitude);
            bundle.putString(Tags.PLACE_NAME, placeName);
            bundle.putString(Tags.PLACE_ADD, placeAdd);
            bundle.putString(Tags.ADDRESS, result);
            bundle.putString(Tags.city_param, city);
            bundle.putString(Tags.country_param, country);
            bundle.putString(Tags.postalCode, postalCode);
            message.setData(bundle);
            message.sendToTarget();

        }
    }
}
