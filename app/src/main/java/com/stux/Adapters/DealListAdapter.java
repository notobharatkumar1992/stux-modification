package com.stux.Adapters;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.stux.AppDelegate;
import com.stux.Models.DealModel;
import com.stux.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.TextView;

public class DealListAdapter extends ArrayAdapter<String> {

    static class ViewHolder {
        ImageView img_deals, img_loading;
        TextView txt_c_name, txt_c_description, txt_c_price, txt_c_price_old, txt_c_percent, txt_c_time;
    }

    private final LayoutInflater mLayoutInflater;
    private Context mContext;
    private ArrayList<DealModel> dealArray;

    public DealListAdapter(final Context context, final int textViewResourceId, ArrayList<DealModel> dealArray) {
        super(context, textViewResourceId);
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.dealArray = dealArray;
    }

    @Override
    public int getCount() {
        return dealArray.size();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.deals_list_item, parent, false);
            holder = new ViewHolder();
            holder.img_deals = (ImageView) convertView.findViewById(R.id.img_deals);
            holder.img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
            holder.txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
            holder.txt_c_description = (TextView) convertView.findViewById(R.id.txt_c_description);
            holder.txt_c_price = (TextView) convertView.findViewById(R.id.txt_c_price);
            holder.txt_c_price_old = (TextView) convertView.findViewById(R.id.txt_c_price_old);
            holder.txt_c_percent = (TextView) convertView.findViewById(R.id.txt_c_percent);
            holder.txt_c_time = (TextView) convertView.findViewById(R.id.txt_c_time);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            holder.txt_c_name.setText(dealArray.get(position).title);
            holder.txt_c_description.setText(dealArray.get(position).details);
            holder.txt_c_price_old.setText(" N" + dealArray.get(position).price + " ");
            holder.txt_c_price_old.setPaintFlags(holder.txt_c_price_old.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.txt_c_price.setText("N" + (Integer.parseInt(dealArray.get(position).price) - Integer.parseInt(dealArray.get(position).discount_price)));
            holder.txt_c_percent.setText(dealArray.get(position).discount + "%");

            String time = dealArray.get(position).created.substring(0, dealArray.get(position).created.lastIndexOf("+"));
            try {
//            //        2016-06-27T06:32 AppDelegate.LogT("time before = " + time);
                time = new SimpleDateFormat("dd MMM, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.txt_c_time.setText(time);
//        Picasso.with(mContext).load(dealArray.get(position).image_1).into(holder.img_product);
            holder.img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
            frameAnimation.setCallback(holder.img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
//
//            Picasso.with(mContext).load(dealArray.get(position).image_1_thumb).into(holder.img_deals, new Callback() {
//                @Override
//                public void onSuccess() {
//                    holder.img_loading.setVisibility(View.GONE);
//                }
//
//                @Override
//                public void onError() {
//
//                }
//            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return convertView;
    }

}