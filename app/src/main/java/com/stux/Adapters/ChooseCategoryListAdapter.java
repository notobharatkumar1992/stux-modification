package com.stux.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.stux.Models.ProductCategoryModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

import carbon.widget.RelativeLayout;


public class ChooseCategoryListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<ProductCategoryModel> arrayCategory;
    private OnListItemClickListener clickListener;

    public ChooseCategoryListAdapter(Context mContext, ArrayList<ProductCategoryModel> arrayCategory, OnListItemClickListener clickListener) {
        this.mContext = mContext;
        this.arrayCategory = arrayCategory;
        this.clickListener = clickListener;
    }

    @Override
    public int getCount() {
        return arrayCategory.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.choose_category_list_item, null);
            holder = new Holder();
            holder.txt_c_category = (TextView) convertView.findViewById(R.id.txt_c_category);
            holder.rl_c_main = (RelativeLayout) convertView.findViewById(R.id.rl_c_main);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.txt_c_category.setText(arrayCategory.get(position).cat_name);

        holder.rl_c_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.setOnListItemClickListener(Tags.category, position);
            }
        });
        return convertView;
    }

    class Holder {
        public TextView txt_c_category;
        public RelativeLayout rl_c_main;
    }

}
