package com.stux.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.stux.AppDelegate;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;


public class FollowersListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<UserDataModel> arrayUser;
    private OnListItemClickListener clickListener;

    public FollowersListAdapter(Context mContext, ArrayList<UserDataModel> arrayUser, OnListItemClickListener clickListener) {
        this.mContext = mContext;
        this.arrayUser = arrayUser;
        this.clickListener = clickListener;
    }

    @Override
    public int getCount() {
        return arrayUser.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.followers_list_item, null);
            holder = new Holder();
            holder.txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
            holder.txt_c_description = (TextView) convertView.findViewById(R.id.txt_c_description);
            holder.txt_c_follow = (TextView) convertView.findViewById(R.id.txt_c_follow);
            holder.ll_c_following = (LinearLayout) convertView.findViewById(R.id.ll_c_following);
            holder.img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
            holder.img_c_follow = (carbon.widget.ImageView) convertView.findViewById(R.id.img_c_follow);
            holder.cimg_user = (CircleImageView) convertView.findViewById(R.id.cimg_user);
            holder.rl_c_main = (RelativeLayout) convertView.findViewById(R.id.rl_c_main);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.txt_c_name.setText(arrayUser.get(position).first_name + " " + arrayUser.get(position).last_name);
        holder.txt_c_description.setText(arrayUser.get(position).institutionModel.institution_name + " - " + arrayUser.get(position).institutionModel.department_name);

        holder.rl_c_main.setVisibility(View.VISIBLE);
        holder.img_loading.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
        frameAnimation.setCallback(holder.img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();

        if (AppDelegate.isValidString(arrayUser.get(position).image))
//            Picasso.with(mContext).load(arrayUser.get(position).image).into(holder.cimg_user, new Callback() {
//                @Override
//                public void onSuccess() {
//                    try {
//                        holder.img_loading.setVisibility(View.GONE);
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
//                }
//
//                @Override
//                public void onError() {
//
//                }
//            });

        holder.ll_c_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.setOnListItemClickListener(Tags.follow_status, position);
            }
        });

        holder.rl_c_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.setOnListItemClickListener(Tags.follower_id, position);
            }
        });

        if (arrayUser.get(position).follow_status == 1) {
            holder.ll_c_following.setSelected(true);
            holder.img_c_follow.setVisibility(View.VISIBLE);
            holder.img_c_follow.setImageResource(R.drawable.following);
            holder.txt_c_follow.setText("Following");
            holder.txt_c_follow.setTextColor(Color.WHITE);
        } else {
            holder.ll_c_following.setSelected(false);
            holder.img_c_follow.setVisibility(View.GONE);
            holder.txt_c_follow.setText("+ Follow");
            holder.txt_c_follow.setTextColor(mContext.getResources().getColor(R.color.hint_color));
        }

        return convertView;
    }

    class Holder {
        public TextView txt_c_name, txt_c_description, txt_c_follow;
        public ImageView img_loading;
        public carbon.widget.ImageView img_c_follow;
        public LinearLayout ll_c_following;
        public CircleImageView cimg_user;
        public RelativeLayout rl_c_main;
    }

}
