package com.stux.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.stux.AppDelegate;
import com.stux.Models.MovieTime;
import com.stux.R;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class MoviesDetailListAdapter extends ArrayAdapter<String> {
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();

    private final LayoutInflater mLayoutInflater;
    private Context mContext;
    private ArrayList<MovieTime> movieArray;
    private OnListItemClickListener itemClickListener;


    public MoviesDetailListAdapter(final Activity context, final int textViewResourceId, ArrayList<MovieTime> movieArray, OnListItemClickListener itemClickListener) {
        super(context, textViewResourceId);
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.movieArray = movieArray;
        this.itemClickListener = itemClickListener;
        imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
    }

    @Override
    public int getCount() {
        return movieArray.size();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.movies_detail_list_item, parent, false);
            holder = new ViewHolder();
            holder.ll_c_main = (LinearLayout) convertView.findViewById(R.id.ll_c_main);
            holder.txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
            holder.img_trailer = (ImageView) convertView.findViewById(R.id.img_trailer);
            holder.img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
            holder.txt_c_description_1 = (TextView) convertView.findViewById(R.id.txt_c_description_1);
            holder.txt_c_description_2 = (TextView) convertView.findViewById(R.id.txt_c_description_2);
            holder.txt_c_description_3 = (TextView) convertView.findViewById(R.id.txt_c_description_3);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            holder.txt_c_name.setText(movieArray.get(position).cinema_type);
            holder.txt_c_description_1.setText(movieArray.get(position).cinema_time);
            holder.txt_c_description_1.setVisibility(View.VISIBLE);
            holder.txt_c_description_2.setVisibility(View.GONE);
            holder.txt_c_description_3.setVisibility(View.GONE);


            holder.img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
            frameAnimation.setCallback(holder.img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            if (AppDelegate.isValidString(movieArray.get(position).image)) {
                imageLoader.displayImage(movieArray.get(position).image, holder.img_trailer);
            } else {
                holder.img_trailer.setImageDrawable(mContext.getResources().getDrawable(R.drawable.no_image_found));
                frameAnimation.stop();
                holder.img_loading.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        return convertView;
    }

    static class ViewHolder {
        LinearLayout ll_c_main;
        ImageView img_trailer, img_loading;
        TextView txt_c_name, txt_c_description_1, txt_c_description_2, txt_c_description_3;
    }

}