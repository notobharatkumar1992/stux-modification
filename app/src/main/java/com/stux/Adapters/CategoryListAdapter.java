package com.stux.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.stux.Models.ProductCategoryModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

import carbon.widget.RelativeLayout;


public class CategoryListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<ProductCategoryModel> arrayCategory;
    private OnListItemClickListener clickListener;

    public CategoryListAdapter(Context mContext,
                               ArrayList<ProductCategoryModel> arrayCategory, OnListItemClickListener clickListener) {
        this.mContext = mContext;
        this.arrayCategory = arrayCategory;
        this.clickListener = clickListener;
    }

    @Override
    public int getCount() {
        return arrayCategory.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.refine_search_category_list_item, null);
            holder = new Holder();
            holder.txt_c_category = (TextView) convertView.findViewById(R.id.txt_c_category);
            holder.cb_category = (CheckBox) convertView.findViewById(R.id.cb_category);
            holder.rl_c_category = (RelativeLayout) convertView.findViewById(R.id.rl_c_category);
            holder.view = (View) convertView.findViewById(R.id.view);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.txt_c_category.setText(arrayCategory.get(position).cat_name);
        holder.cb_category.setChecked(arrayCategory.get(position).selected != 0);

        holder.rl_c_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.setOnListItemClickListener(Tags.category, position);
            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.setOnListItemClickListener(Tags.category, position);
            }
        });
        return convertView;
    }

    class Holder {
        public TextView txt_c_category;
        public CheckBox cb_category;
        public RelativeLayout rl_c_category;
        public View view;
    }

}
