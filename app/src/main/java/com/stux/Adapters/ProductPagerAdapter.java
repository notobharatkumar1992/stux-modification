package com.stux.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.stux.AppDelegate;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

import carbon.widget.RelativeLayout;

public class ProductPagerAdapter extends PagerAdapter {

    public ArrayList<String> arrayImages;
    public Context mContext;
    private OnListItemClickListener onListItemClickListener;

    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    public ProductPagerAdapter(Context mContext, ArrayList<String> arrayImages, OnListItemClickListener onListItemClickListener) {
        this.mContext = mContext;
        this.arrayImages = arrayImages;
        this.onListItemClickListener = onListItemClickListener;
        imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
    }

    @Override
    public int getCount() {
        return arrayImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        RelativeLayout relativeLayout = new RelativeLayout(view.getContext());
        view.addView(relativeLayout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        final ImageView img_loading = new ImageView(view.getContext());
        img_loading.setImageDrawable(view.getContext().getResources().getDrawable(R.drawable.black_spinner));
        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
        frameAnimation.setCallback(img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();

        relativeLayout.addView(img_loading, AppDelegate.dpToPix(view.getContext(), 40), AppDelegate.dpToPix(view.getContext(), 40));

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) img_loading.getLayoutParams();
        if (params != null) {
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            img_loading.setLayoutParams(params);
            AppDelegate.LogT("Banner adapter params != null");
        }

        final ImageView imageView = new ImageView(view.getContext());
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListItemClickListener != null)
                    onListItemClickListener.setOnListItemClickListener(Tags.LIST_ITEM_TRENDING, position);
            }
        });

//


        imageLoader.loadImage(arrayImages.get(position), options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                AppDelegate.LogT("bitmap => " + bitmap.getWidth() + ", " + bitmap.getHeight());
                if (bitmap.getWidth() >= bitmap.getHeight()) {
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                } else {
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
//
//        Picasso.with(view.getContext()).load(arrayImages.get(position)).into(new Target() {
//            @Override
//            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                AppDelegate.LogT("bitmap => " + bitmap.getWidth() + ", " + bitmap.getHeight());
//                if (bitmap.getWidth() >= bitmap.getHeight()) {
//                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
//                } else {
//                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//                }
//                imageView.setImageBitmap(bitmap);
//            }
//
//            @Override
//            public void onBitmapFailed(Drawable errorDrawable) {
//
//            }
//
//            @Override
//            public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//            }
//        });
//        Picasso.with(view.getContext()).load(arrayImages.get(position)).into(imageView, new Callback() {
//            @Override
//            public void onSuccess() {
//                img_loading.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onError() {
//
//            }
//        });
        relativeLayout.addView(imageView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        return relativeLayout;
    }

}