package com.stux.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.stux.Models.ProductCategoryModel;
import com.stux.R;

import java.util.ArrayList;

public class SpinnerProductCategoryAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<ProductCategoryModel> arrayList;

    public SpinnerProductCategoryAdapter(Context context, ArrayList<ProductCategoryModel> arrayList) {
        this.mContext = context;
        this.arrayList = arrayList;
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        convertView = ((LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.spinner_dropdown_text_fav_category, parent, false);
        holder = new Holder();
        holder.txt_spinner = (TextView) convertView.findViewById(R.id.text1);
        if (position == 0)
            holder.txt_spinner.setTextColor(mContext.getResources().getColor(R.color.hint_color));
        else
            holder.txt_spinner.setTextColor(mContext.getResources().getColor(R.color.profile_desc));
        // holder.txt_spinner.setBackgroundColor(Color.WHITE);
        holder.txt_spinner.setText(arrayList.get(position).cat_name);
        return convertView;
    }

    // It gets a View that displays in the drop down popup the data at the
    // specified position
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // It gets a View that displays the data at the specified position
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    class Holder {
        public TextView txt_spinner;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}