package com.stux.Adapters;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.stux.AppDelegate;
import com.stux.Models.ReviewModel;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class ReviewListAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mLayoutInflater;
    private Context mContext;
    private ArrayList<ReviewModel> reviewArray;
    private OnListItemClickListener itemClickListener;

    public ReviewListAdapter(final Context context, final int textViewResourceId, ArrayList<ReviewModel> reviewArray, OnListItemClickListener itemClickListener) {
        super(context, textViewResourceId);
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.reviewArray = reviewArray;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getCount() {
        return reviewArray.size();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.review_list_item, parent, false);
            holder = new ViewHolder();
            holder.ll_c_main = (LinearLayout) convertView.findViewById(R.id.ll_c_main);

            holder.cimg_user = (CircleImageView) convertView.findViewById(R.id.cimg_user);

            holder.img_star_1 = (ImageView) convertView.findViewById(R.id.img_star_1);
            holder.img_star_2 = (ImageView) convertView.findViewById(R.id.img_star_2);
            holder.img_star_3 = (ImageView) convertView.findViewById(R.id.img_star_3);
            holder.img_star_4 = (ImageView) convertView.findViewById(R.id.img_star_4);
            holder.img_star_5 = (ImageView) convertView.findViewById(R.id.img_star_5);
            holder.img_loading = (ImageView) convertView.findViewById(R.id.img_loading);

            holder.txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
            holder.txt_c_description = (TextView) convertView.findViewById(R.id.txt_c_description);
            holder.txt_c_time = (TextView) convertView.findViewById(R.id.txt_c_time);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            holder.txt_c_name.setText(reviewArray.get(position).userDataModel.first_name + " " + reviewArray.get(position).userDataModel.last_name);
            holder.txt_c_description.setText(reviewArray.get(position).reviews);
            String time = reviewArray.get(position).created.substring(0, reviewArray.get(position).created.lastIndexOf("+"));

            try {
//            AppDelegate.LogT("time before = " + time); //        2016-06-27T06:32
                time = new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
                holder.txt_c_time.setText("Posted " + time);
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }


            holder.img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
            frameAnimation.setCallback(holder.img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
//            Picasso.with(mContext).load(reviewArray.get(position).userDataModel.image).into(holder.cimg_user, new Callback() {
//                @Override
//                public void onSuccess() {
//                    holder.img_loading.setVisibility(View.GONE);
//                }
//
//                @Override
//                public void onError() {
//                    holder.img_loading.setVisibility(View.GONE);
//                }
//            });

            holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));

            switch (Integer.parseInt(reviewArray.get(position).rating)) {
                case 0:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 1:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 2:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 3:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 4:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 5:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    break;
            }

            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.product, position);
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        return convertView;
    }

    static class ViewHolder {
        LinearLayout ll_c_main;
        CircleImageView cimg_user;
        ImageView img_star_1, img_star_2, img_star_3, img_star_4, img_star_5, img_loading;
        TextView txt_c_name, txt_c_description, txt_c_time;
    }

}