package com.stux.Adapters;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.stux.AppDelegate;
import com.stux.Models.EventModel;
import com.stux.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.TextView;

public class EventListAdapter extends ArrayAdapter<String> {

    static class ViewHolder {
        ImageView img_events, img_loading;
        TextView txt_c_time, txt_c_name, txt_c_description, txt_c_free_entry, txt_c_ticket, txt_c_ticket_sold_out;
    }

    private final LayoutInflater mLayoutInflater;
    private Context mContext;
    private ArrayList<EventModel> productArray;

    public EventListAdapter(final Context context, final int textViewResourceId, ArrayList<EventModel> productArray) {
        super(context, textViewResourceId);
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.productArray = productArray;
    }

    @Override
    public int getCount() {
        return productArray.size();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.event_list_item, parent, false);
            holder = new ViewHolder();
            holder.img_events = (ImageView) convertView.findViewById(R.id.img_events);
            holder.img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
            holder.txt_c_time = (TextView) convertView.findViewById(R.id.txt_c_time);
            holder.txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
            holder.txt_c_description = (TextView) convertView.findViewById(R.id.txt_c_description);
            holder.txt_c_free_entry = (TextView) convertView.findViewById(R.id.txt_c_free_entry);
            holder.txt_c_ticket = (TextView) convertView.findViewById(R.id.txt_c_ticket);
            holder.txt_c_ticket_sold_out = (TextView) convertView.findViewById(R.id.txt_c_ticket_sold_out);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String time = productArray.get(position).created.substring(0, productArray.get(position).created.lastIndexOf("+"));
//        2016-06-27T06:32
        try {
//            AppDelegate.LogT("time before = " + time);
            time = new SimpleDateFormat("dd MMM, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
        } catch (ParseException e) {
            AppDelegate.LogE(e);
        }
        holder.txt_c_time.setText(time);
        holder.txt_c_name.setText(productArray.get(position).event_name);
        holder.txt_c_name.setText(productArray.get(position).details);

        if (productArray.get(position).gate_fees_type == 0) {
            holder.txt_c_free_entry.setVisibility(View.VISIBLE);
            holder.txt_c_ticket.setVisibility(View.GONE);
        } else {
            holder.txt_c_free_entry.setVisibility(View.GONE);
            holder.txt_c_ticket.setVisibility(View.VISIBLE);
        }
//        holder.txt_c_description.setText("N" + productArray.get(position).price);
        holder.img_loading.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
        frameAnimation.setCallback(holder.img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();

//        Picasso.with(mContext).load(productArray.get(position).banner_image_thumb).into(holder.img_events, new Callback() {
//            @Override
//            public void onSuccess() {
//                holder.img_loading.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onError() {
//
//            }
//        });
        return convertView;
    }

}