package com.stux.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.stux.Adapters.CategoryListAdapter;
import com.stux.Adapters.SpinnerArrayStringAdapter;
import com.stux.Adapters.SpinnerInstitutionDetailAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.InstitutionDetailModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductCategoryModel;
import com.stux.Models.RefineSearchModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by Bharat on 07/13/2016.
 */
public class RefineSearchFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse, OnListItemClickListener {

    private Prefs prefs;
    private UserDataModel dataModel;
    private ArrayList<String> arrayCondition = new ArrayList<>();
    private SpinnerArrayStringAdapter adapterCondition;
    private EditText et_search;
    private Spinner spn_campus_list, spn_condition;
    private ImageView img_c_checkbox, img_c_checkbox_upload;
    private TextView txt_c_selected_range;
    private SeekBar seekBar;
    private int rangeProgress = 100;

    public Handler mHandler;
    public ArrayList<InstitutionDetailModel> arrayInstitutionDetailModels = new ArrayList<>();
    private SpinnerInstitutionDetailAdapter adapterInstitutionName;

    private ListView list;
    private CategoryListAdapter categoryListAdapter;
    public ArrayList<ProductCategoryModel> arrayProductCategory = new ArrayList<>();

    public RefineSearchModel refineSearchModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.refine_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        initView(view);
        setHandler();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (arrayInstitutionDetailModels.size() == 0) {
            execute_getInstitutionNameApi(prefs.getInstitutionModel().institution_state_id);
        } else {
            mHandler.sendEmptyMessage(2);
            if (arrayProductCategory.size() == 0) {
                callGetProductTypeAsync();
            } else {
                mHandler.sendEmptyMessage(1);
            }
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {
                    categoryListAdapter.notifyDataSetChanged();
                    list.invalidate();
                    setListViewHeight(getActivity(), list, categoryListAdapter);
                } else if (msg.what == 2) {
                    adapterInstitutionName.notifyDataSetChanged();
                    spn_campus_list.invalidate();
                }
            }
        };
    }

    private void initView(View view) {
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Refine Search");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((TextView) view.findViewById(R.id.txt_c_right)).setText("Search");
        view.findViewById(R.id.txt_c_right).setVisibility(View.VISIBLE);
        view.findViewById(R.id.txt_c_right).setOnClickListener(this);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);

        et_search = (EditText) view.findViewById(R.id.et_search);
        et_search.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));

        if (arrayCondition.size() == 0) {
            arrayCondition.add("Select Condition");
            arrayCondition.add("New");
            arrayCondition.add("Almost New");
            arrayCondition.add("Used");
        }

        spn_condition = (Spinner) view.findViewById(R.id.spn_condition);
        adapterCondition = new SpinnerArrayStringAdapter(getActivity(), arrayCondition);
        spn_condition.setAdapter(adapterCondition);

        spn_campus_list = (Spinner) view.findViewById(R.id.spn_campus_list);
        adapterInstitutionName = new SpinnerInstitutionDetailAdapter(getActivity(), arrayInstitutionDetailModels);
        spn_campus_list.setAdapter(adapterInstitutionName);


        img_c_checkbox = (ImageView) view.findViewById(R.id.img_c_checkbox);
        img_c_checkbox.setOnClickListener(this);
        img_c_checkbox_upload = (ImageView) view.findViewById(R.id.img_c_checkbox_upload);
        img_c_checkbox_upload.setOnClickListener(this);

        txt_c_selected_range = (TextView) view.findViewById(R.id.txt_c_selected_range);

        seekBar = (SeekBar) view.findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress < 10) {
                    seekBar.setProgress(11);
                    return;
                }
                txt_c_selected_range.setText(progress + "KM");
                rangeProgress = progress;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBar.setProgress(100);

        categoryListAdapter = new CategoryListAdapter(getActivity(), arrayProductCategory, this);
        list = (ListView) view.findViewById(R.id.list);
        list.setAdapter(categoryListAdapter);
        setListViewHeight(getActivity(), list, categoryListAdapter);
    }


    public static void setListViewHeight(Context mContext, ListView listView, ListAdapter gridAdapter) {
        try {
            if (gridAdapter == null) {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = 80;
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;
            int itemCount = gridAdapter.getCount();

            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            AppDelegate.LogT("itemCount = " + itemCount);
            for (int i = 0; i < itemCount; i++) {
                View listItem = gridAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }
            totalHeight += AppDelegate.dpToPix(mContext, 10);
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void callGetProductTypeAsync() {
        if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        } else {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_PRODUCT_CATEGORY,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }

    private void execute_getInstitutionNameApi(String value) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.state_id, value + "");
            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    RefineSearchFragment.this, ServerRequestConstants.GET_INSTITUTION_NAME,
                    mPostArrayList, RefineSearchFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void execute_searchProduct() {
//        rangeProgress *= rangeProgress;
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, "1");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_name, et_search.getText().toString() + "");
            try {
                if (!arrayInstitutionDetailModels.get(spn_campus_list.getSelectedItemPosition()).id.equalsIgnoreCase("Search by Campus Name"))
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.campus_id, arrayInstitutionDetailModels.get(spn_campus_list.getSelectedItemPosition()).id + "");
                else
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.campus_id, "");
            } catch (Exception e) {
                AppDelegate.LogE(e);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.campus_id, "");
            }
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.item_condition, (spn_condition.getSelectedItemPosition() == 0 ? "" : spn_condition.getSelectedItemPosition()) + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.cat_id, getSelectedCategoryIds() + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.price, img_c_checkbox.isSelected() ? "asc" : "desc" + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.upload, img_c_checkbox_upload.isSelected() ? "asc" : "desc" + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.range, rangeProgress + "");

            refineSearchModel = new RefineSearchModel();
            refineSearchModel.logged_in_User_id = dataModel.userId;
            refineSearchModel.record = "10";
            refineSearchModel.page = 1;
            refineSearchModel.product_name = et_search.getText().toString();
            if (arrayInstitutionDetailModels.get(spn_campus_list.getSelectedItemPosition()).id.equalsIgnoreCase("Search by Campus Name")) {
                refineSearchModel.campus_id = "";
            } else
                refineSearchModel.campus_id = arrayInstitutionDetailModels.get(spn_campus_list.getSelectedItemPosition()).id;
            refineSearchModel.item_condition = (spn_condition.getSelectedItemPosition() == 0 ? "" : spn_condition.getSelectedItemPosition()) + "";
            refineSearchModel.cat_id = getSelectedCategoryIds();
            refineSearchModel.price = img_c_checkbox.isSelected() ? "asc" : "desc" + "";
            refineSearchModel.upload = img_c_checkbox_upload.isSelected() ? "asc" : "desc" + "";
            refineSearchModel.rangeProgress = rangeProgress + "";

            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    RefineSearchFragment.this, ServerRequestConstants.REFINE_PRODUCTS,
                    mPostArrayList, RefineSearchFragment.this);

            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private String getSelectedCategoryIds() {
        StringBuilder stringBuilder = null;
        for (int i = 0; i < arrayProductCategory.size(); i++) {
            if (arrayProductCategory.get(i).selected == 1) {
                if (stringBuilder == null) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append(arrayProductCategory.get(i).id + "");
                } else {
                    stringBuilder.append("," + arrayProductCategory.get(i).id);
                }
            }
        }
        return stringBuilder != null ? stringBuilder.toString() : "";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                getFragmentManager().popBackStack();
                break;
            case R.id.txt_c_right:
                execute_searchProduct();
                break;
            case R.id.img_c_checkbox:
                img_c_checkbox.setSelected(!img_c_checkbox.isSelected());
                break;
            case R.id.img_c_checkbox_upload:
                img_c_checkbox_upload.setSelected(!img_c_checkbox_upload.isSelected());
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_INSTITUTION_NAME)) {
            parseInstitutionName(result);
            if (arrayProductCategory.size() == 0) {
                callGetProductTypeAsync();
            } else {
                mHandler.sendEmptyMessage(1);
            }
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_PRODUCT_CATEGORY)) {
            parseProductCategoryResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.REFINE_PRODUCTS)) {
            parseRefineProduct(result);
        }
    }

    private void parseRefineProduct(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                Bundle bundle = new Bundle();
                bundle.putString(Tags.product, result);
                bundle.putParcelable(Tags.refine, refineSearchModel);
                Fragment fragment = new RefineProductFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.MESSAGE));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseProductCategoryResult(String result) {
        try {
            JSONObject object = new JSONObject(result);
            if (object.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = object.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ProductCategoryModel productCategoryModel = new ProductCategoryModel();
                    productCategoryModel.id = jsonObject.getString(Tags.id);
                    productCategoryModel.cat_name = jsonObject.getString(Tags.cat_name);
                    productCategoryModel.status = jsonObject.getString(Tags.id);
                    arrayProductCategory.add(productCategoryModel);
                }
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showAlert(getActivity(), object.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseInstitutionName(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                arrayInstitutionDetailModels.clear();
                arrayInstitutionDetailModels.add(new InstitutionDetailModel("Search by Campus Name"));

                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        InstitutionDetailModel institutionNameModel = new InstitutionDetailModel();
                        institutionNameModel.id = object.getString(Tags.ID);
                        institutionNameModel.institute_name = object.getString(Tags.institute_name);
                        institutionNameModel.state_id = object.getString(Tags.state_id);
                        institutionNameModel.created = object.getString(Tags.created);
                        institutionNameModel.status = object.getString(Tags.status);

                        arrayInstitutionDetailModels.add(institutionNameModel);
                    }
                }
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
            arrayInstitutionDetailModels.add(new InstitutionDetailModel("Other"));
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(2);
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.category)) {
            arrayProductCategory.get(position).selected = arrayProductCategory.get(position).selected != 0 ? 0 : 1;
            mHandler.sendEmptyMessage(1);
        }
    }
}
