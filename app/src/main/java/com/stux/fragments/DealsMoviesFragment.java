package com.stux.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.Adapters.PagerLoopAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.DealModel;
import com.stux.Models.MoviesModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.SliderModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.MyViewPager;
import com.stux.Utils.Prefs;
import com.stux.Utils.SpacesItemDecoration;
import com.stux.activities.MainActivity;
import com.stux.activities.MoviesDetailsActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.DealRecyclerViewAdapter;
import inducesmile.com.androidstaggeredgridlayoutmanager.MoviesRecyclerViewAdapter;

/**
 * Created by NOTO on 5/27/2016.
 */
public class DealsMoviesFragment extends Fragment implements OnClickListener, OnReciveServerResponse, OnListItemClickListener {

    public ArrayList<ProductModel> productArray = new ArrayList<>();
    private MyViewPager view_pager_banner;
    private PagerLoopAdapter bannerPagerAdapter;
    private LinearLayout ll_c_top_deals, ll_c_movies;
    private ArrayList<Fragment> bannerFragment = new ArrayList<>();
    private ArrayList<SliderModel> sliderArray = new ArrayList<>();
    private RelativeLayout rl_c_banner;
    private Handler mHandler;
    private ProgressBar progressbar;
    private android.widget.LinearLayout pager_indicator;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private ImageView[] dots;
    private TextView txt_c_shop_now;
    private Prefs prefs;
    private UserDataModel dataModel;

    private RecyclerView recyclerView;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private SpacesItemDecoration dealItemDecoration;
    private SpacesItemDecoration moviesItemDecoration;

    // Deal list
    public static ArrayList<DealModel> dealArray = new ArrayList<>();
    private DealRecyclerViewAdapter dealAdapter;
    private LinearLayoutManager linearLayoutManager;
    public static int dealCounter = 1, dealTotalPage = -1;

    // Movies list
    private MoviesRecyclerViewAdapter moviesAdapter;
    public static ArrayList<MoviesModel> moviesArray = new ArrayList<>();
    public static int moviesCounter = 1, moviesTotalPage = -1;

    private PostAsync sliderAsync;

    private boolean dealAsyncExcecuting = false, moviesAsyncExcecuting = false;

    private SwipyRefreshLayout swipyrefreshlayout;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.deals_movies_page, container, false);
    }

    public View convertView;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        convertView = view;
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        sliderArray.clear();
        clearArrayData(4);
        setHandler();
        initView(view);
        ((MainActivity) getActivity()).callGetNotificationAsync();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                    txt_c_shop_now.setVisibility(View.GONE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 14) {
                } else if (msg.what == 15) {
//                    progressbar_1.setVisibility(View.GONE);
                } else if (msg.what == 1) {
                    AppDelegate.LogT("sliderArray = " + sliderArray.size());
                    bannerFragment.clear();
                    for (int i = 0; i < sliderArray.size(); i++) {
                        Fragment fragment = new BannerHomeFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Tags.slider_id, sliderArray.get(i));
                        fragment.setArguments(bundle);
                        bannerFragment.add(fragment);
                    }

                    bannerPagerAdapter = new PagerLoopAdapter(getChildFragmentManager(), bannerFragment);
                    view_pager_banner.setAdapter(bannerPagerAdapter);

                    setUiPageViewController();
                    if (sliderArray.size() == 0) {
                        txt_c_shop_now.setVisibility(View.VISIBLE);
                        if (selected_tab == 0) {
                            txt_c_shop_now.setText("Top Deals banner are not available.");
                        } else if (selected_tab == 1) {
                            txt_c_shop_now.setText("Top Movies banner are not available.");
                        }
                    } else {
                        txt_c_shop_now.setVisibility(View.GONE);
                        rl_c_banner.setVisibility(View.VISIBLE);
                        view_pager_banner.setVisibility(View.VISIBLE);
                    }
                    timeOutLoop();
                } else if (msg.what == 3) {
                    if (selected_tab == 1) {
                        dealAdapter.notifyDataSetChanged();
                        recyclerView.invalidate();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                dealAdapter.notifyDataSetChanged();
                                recyclerView.invalidate();
                            }
                        }, 1000);
                    }
                } else if (msg.what == 4) {
                    if (selected_tab == 0) {
                        moviesAdapter.notifyDataSetChanged();
                        recyclerView.invalidate();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                moviesAdapter.notifyDataSetChanged();
                                recyclerView.invalidate();
                            }
                        }, 1000);
                    }
                } else if (msg.what == 20) {
                    if (convertView != null)
                        convertView.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
                }
            }
        };
    }

    private void initView(View view) {
        rl_c_banner = (RelativeLayout) view.findViewById(R.id.rl_c_banner);

        android.widget.LinearLayout.LayoutParams layoutParams = (android.widget.LinearLayout.LayoutParams) rl_c_banner.getLayoutParams();
        layoutParams.height = AppDelegate.getDeviceWith(getActivity()) / 2 - AppDelegate.dpToPix(getActivity(), 10);
        rl_c_banner.setLayoutParams(layoutParams);

        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Deals / Movies");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });
        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        ll_c_top_deals = (LinearLayout) view.findViewById(R.id.ll_c_top_deals);
        ll_c_top_deals.setOnClickListener(this);
        ll_c_movies = (LinearLayout) view.findViewById(R.id.ll_c_movies);
        ll_c_movies.setOnClickListener(this);

        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        txt_c_shop_now = (TextView) view.findViewById(R.id.txt_c_shop_now);

        pager_indicator = (android.widget.LinearLayout) view.findViewById(R.id.pager_indicator);
        view_pager_banner = (MyViewPager) view.findViewById(R.id.view_pager_banner);
        bannerPagerAdapter = new PagerLoopAdapter(getChildFragmentManager(), bannerFragment);
        view_pager_banner.setAdapter(bannerPagerAdapter);
        view_pager_banner.setVisibility(View.VISIBLE);
        setUiPageViewController();
        view_pager_banner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (bannerFragment.size() > 0)
                    switchBannerPage(position % bannerFragment.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        switchBannerPage(0);

        dealItemDecoration = new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 0), true);
        moviesItemDecoration = new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 5));

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
        recyclerView.setHasFixedSize(true);

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        gaggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);
        recyclerView.addItemDecoration(moviesItemDecoration);

        linearLayoutManager = new LinearLayoutManager(getActivity());

        // Movies list Data init
        moviesAdapter = new MoviesRecyclerViewAdapter(getActivity(), moviesArray, this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(moviesAdapter);

        // Deal list Data init

        dealAdapter = new DealRecyclerViewAdapter(getActivity(), dealArray, this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(dealAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (selected_tab == 1) {
                                 if (dealTotalPage != 0 && !dealAsyncExcecuting) {
                                     mHandler.sendEmptyMessage(2);
                                     callDealsListAsync();
                                     dealAsyncExcecuting = true;
                                 } else {
                                     swipyrefreshlayout.setRefreshing(false);
                                     AppDelegate.LogT("selected_tab = 1, " + dealTotalPage + ", " + dealAsyncExcecuting);
                                 }
                             } else {
                                 if (moviesTotalPage != 0 && !moviesAsyncExcecuting) {
                                     mHandler.sendEmptyMessage(4);
                                     callMoviesListAsync();
                                     moviesAsyncExcecuting = true;
                                 } else {
                                     swipyrefreshlayout.setRefreshing(false);
                                     AppDelegate.LogT("selected_tab = 2, " + moviesTotalPage + ", " + moviesAsyncExcecuting);
                                 }
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );

        switchPage(selected_tab);
    }

    public static void clearArrayData(int value) {
        AppDelegate.LogCh("clearArrayData called at HomeFragment => " + value);
        switch (value) {
            case 0:
                moviesCounter = 1;
                moviesTotalPage = -1;
                moviesArray.clear();
                break;

            case 1:
                dealCounter = 1;
                dealTotalPage = -1;
                dealArray.clear();
                break;

            default:
                moviesCounter = 1;
                moviesTotalPage = -1;
                moviesArray.clear();

                dealCounter = 1;
                dealTotalPage = -1;
                dealArray.clear();
                break;
        }
    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        if (isAdded()) {
                            int value = view_pager_banner.getCurrentItem();
                            if (value == bannerPagerAdapter.getCount() - 1) {
                                value = 0;
                            } else {
                                value++;
                            }
                            if (value < bannerPagerAdapter.getCount())
                                view_pager_banner.setCurrentItem(value, true);
                            if (isAdded()) {
                                mHandler.postDelayed(this, 3000);
                            } else {
                                mHandler.removeCallbacks(this);
                            }
                        } else if (mHandler != null) {
                            mHandler.removeCallbacks(this);
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }

    private void callDealsListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            try {
                if (AppDelegate.isValidString(prefs.getInstitutionModel().institution_name_id))
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.institution_id, prefs.getInstitutionModel().institution_name_id);
                else
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.institution_id, "other");
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, dealCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.DEALS_LIST,
                    mPostArrayList, null);
            if (dealCounter == 1)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callMoviesListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, moviesCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.MOVIES_LIST,
                    mPostArrayList, null);
            if (moviesCounter == 1)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void switchBannerPage(int position) {
        try {
            if (bannerFragment.size() > 0) {
                for (int i = 0; i < bannerFragment.size(); i++) {
                    if (dots != null && dots.length > i)
                        dots[i].setImageResource(R.drawable.white_radius_square);
                }
                if (dots != null && dots.length > position)
                    dots[position].setImageResource(R.drawable.orange_radius_square);
            }
            timeOutLoop();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = bannerFragment.size();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(getActivity());
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20, 20);
                if (getString(R.string.values_folder).equalsIgnoreCase("values-hdpi")) {
                    params = new LinearLayout.LayoutParams(12, 12);
                    params.setMargins(7, 0, 7, 0);
                } else
                    params.setMargins(10, 0, 10, 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.orange_radius_square));
        }
    }

    private void callClicksAsync(SliderModel sliderModel, int type) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.slider_id, sliderModel.id);
            if (type == 0)
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.click_status, "1");
            else
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.thumb_status, "1");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.device_id, AppDelegate.getUUID(getActivity()));
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.SLIDERS_CLICKS,
                    mPostArrayList, this);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callSliderAsync(String value) {
        try {
            if (sliderAsync != null) {
                sliderAsync.cancelAsync(false);
                sliderAsync.cancel(true);
            }
            sliderArray.clear();
            bannerFragment.clear();
            bannerPagerAdapter = new PagerLoopAdapter(getChildFragmentManager(), bannerFragment);
            view_pager_banner.setAdapter(bannerPagerAdapter);
            mHandler.sendEmptyMessage(1);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.slider_type, value);
            sliderAsync = new PostAsync(getActivity(),
                    this, ServerRequestConstants.SLIDERS_LIST,
                    mPostArrayList, this);
            txt_c_shop_now.setVisibility(View.GONE);
            mHandler.sendEmptyMessage(12);
            sliderAsync.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.SLIDERS_LIST)) {
            mHandler.sendEmptyMessage(13);
            parseSliderAsync(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.SLIDERS_CLICKS)) {
            mHandler.sendEmptyMessage(13);
            parseSlidersClick(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.DEALS_LIST)) {
            if (dealCounter > 1) {
                dealAsyncExcecuting = false;
            } else if (dealCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 1)
                swipyrefreshlayout.setRefreshing(false);
            parseDealListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.MOVIES_LIST)) {
            if (moviesCounter > 1) {
                moviesAsyncExcecuting = false;
            } else if (moviesCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 0)
                swipyrefreshlayout.setRefreshing(false);
            parseMoviesListResult(result);
        }
    }

    private void parseDealListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        DealModel dealModel = new DealModel();
                        dealModel.id = object.getString(Tags.id);
                        dealModel.user_id = object.getString(Tags.user_id);
                        dealModel.deal_area = object.getString(Tags.deal_area);
                        dealModel.deal_catid = object.getString(Tags.deal_catid);
                        dealModel.institute_id = object.getString(Tags.institute_id);
                        dealModel.title = object.getString(Tags.title);
                        dealModel.details = object.getString(Tags.details);
                        dealModel.venue = object.getString(Tags.venue);
                        dealModel.price = object.getString(Tags.price);
                        dealModel.discount = object.getString(Tags.discount);
                        dealModel.discount_price = object.getString(Tags.discount_price);
                        dealModel.emailid = JSONParser.getString(object, Tags.emailid);
                        dealModel.coupon_code = object.getString(Tags.coupon_code);
                        dealModel.coupon_no = object.optString(Tags.coupon_no);
                        dealModel.expiry_date = object.getString(Tags.expiry_date);

                        JSONObject dealObject = object.getJSONObject(Tags.deal_category);
                        dealModel.product_category = dealObject.getString(Tags.title);

                        dealModel.image_1 = object.getString(Tags.image_1);
                        dealModel.image_2 = object.getString(Tags.image_2);
                        dealModel.image_3 = object.getString(Tags.image_3);
                        dealModel.image_4 = object.getString(Tags.image_4);

                        dealModel.image_1_thumb = object.getString(Tags.image_1_thumb);
                        dealModel.image_2_thumb = object.getString(Tags.image_2_thumb);
                        dealModel.image_3_thumb = object.getString(Tags.image_3_thumb);
                        dealModel.image_4_thumb = object.getString(Tags.image_4_thumb);

                        dealModel.is_grabbed = object.getString(Tags.is_grabbed);

                        dealModel.total_deal_views = object.getString(Tags.total_deal_views);
                        dealModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);

                        dealModel.created = object.getString(Tags.created);
                        dealArray.add(dealModel);
                    }
                } else {
                    dealTotalPage = 0;
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                dealTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            dealCounter++;

            mHandler.sendEmptyMessage(3);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(3);
                }
            }, 2000);
        } catch (Exception e) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }


    private void parseMoviesListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    MoviesModel moviesModel = new MoviesModel();
                    moviesModel.id = object.getString(Tags.id);
                    moviesModel.user_id = object.getString(Tags.user_id);
                    moviesModel.title = object.getString(Tags.title);
                    moviesModel.banner_image = object.getString(Tags.banner_image);
                    moviesModel.banner_image_thumb = object.getString(Tags.banner_image_thumb);
                    moviesModel.sub_title = JSONParser.getString(object, Tags.sub_title);

                    moviesModel.imdb_rating = object.getString(Tags.imdb_rating);
                    moviesModel.synopsis = object.getString(Tags.synopsis);
                    moviesModel.run_time = object.getString(Tags.run_time);
                    moviesModel.starring = object.getString(Tags.starring);
                    moviesModel.trailer_link = object.getString(Tags.trailer_link);
                    moviesModel.genre = object.getString(Tags.genre);
                    moviesModel.rating = JSONParser.getString(object, Tags.rating);

                    moviesModel.cinema_type_with_time = object.getString(Tags.cinema_type_with_time);
                    moviesModel.status = object.getString(Tags.status);
                    moviesModel.is_view = object.getString(Tags.is_view);
                    moviesModel.total_movie_views = object.getString(Tags.total_movie_views);

                    moviesModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);
                    moviesModel.created = object.getString(Tags.created);

                    moviesModel.ticket_fee = object.getString(Tags.ticket_fee);
                    moviesModel.directed_by = object.getString(Tags.directed_by);

                    moviesArray.add(moviesModel);
                }
            } else {
                moviesTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            moviesCounter++;

            mHandler.sendEmptyMessage(4);
        } catch (Exception e) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    private void parseSlidersClick(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
//                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                if (click_type == 1) {
                    sliderArray.get(item_position).thump_clicked = 1;
                } else {
                    sliderArray.get(item_position).single_clicked = 1;
                }
            } else {
                if (click_type == 1) {
                    sliderArray.get(item_position).thump_clicked = 1;
                } else {
                    sliderArray.get(item_position).single_clicked = 1;
                }
            }
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }


    private void parseSliderAsync(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            sliderArray.clear();
            if (MainActivity.checkUserStatus(getActivity(), jsonObject)) {
                return;
            }
//            ((MainActivity) getActivity()).updateNotificationCount(JSONParser.getString(jsonObject, Tags.total_notifications));
            mHandler.sendEmptyMessage(20);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
//                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    SliderModel sliderModel = new SliderModel();
                    sliderModel.id = object.getString(Tags.id);
                    sliderModel.banner_image = object.getString(Tags.img);
//                    sliderModel.banner_thumb_image = object.getString(Tags.thumb);
                    sliderModel.banner_thumb_image = sliderModel.banner_image;
                    sliderModel.expiry_date = object.getString(Tags.expiry_date);
                    sliderModel.slider_category = object.getString(Tags.slider_category);
                    sliderModel.status = object.getString(Tags.status);
                    sliderModel.created = object.getString(Tags.created);
                    sliderModel.title = object.getString(Tags.title);
                    sliderModel.descriptions = object.getString(Tags.description);
                    sliderModel.url = object.getString(Tags.url);
                    sliderModel.banner_type = object.getInt(Tags.banner_type);
                    sliderModel.type_of_slider = selected_tab;
                    sliderModel.from_page = 1;

                    if (object.has(Tags.click_status) && object.optJSONObject(Tags.click_status) != null) {
                        sliderModel.single_clicked = Integer.parseInt(object.getJSONObject(Tags.click_status).getString(Tags.click_status));
                        sliderModel.thump_clicked = Integer.parseInt(object.getJSONObject(Tags.click_status).getString(Tags.thumb_status));
                    } else {
                        sliderModel.single_clicked = 0;
                        sliderModel.thump_clicked = 0;
                    }
                    sliderArray.add(sliderModel);
                }
            }
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) getActivity()).toggleSlider();
                    }
                }, 300);
                break;

            case R.id.img_c_right:
                break;

            case R.id.ll_c_top_deals:
                selected_tab = 1;
                switchPage(1);
                break;

            case R.id.ll_c_movies:
                selected_tab = 0;
                switchPage(0);
                break;

            case R.id.txt_c_shop_now:
                break;

            case R.id.view_background_banner:
                if (sliderArray.size() > 0) {
                    this.item_position = view_pager_banner.getCurrentItem();
                    if (sliderArray.get(item_position).single_clicked == 0) {
                        callClicksAsync(sliderArray.get(item_position), 0);
                    }
                    showBannerDialog(sliderArray.get(item_position));
                }
                break;
        }
    }

    private void switchPage(int i) {
        swipyrefreshlayout.setRefreshing(false);

        ll_c_movies.setSelected(false);
        ll_c_top_deals.setSelected(false);

        recyclerView.removeItemDecoration(moviesItemDecoration);
        recyclerView.removeItemDecoration(dealItemDecoration);

        switch (i) {
            case 0:
                ll_c_movies.setSelected(true);
                moviesAdapter = new MoviesRecyclerViewAdapter(getActivity(), moviesArray, this);
                recyclerView.setBackgroundColor(getResources().getColor(R.color.grey_font));
                recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
                recyclerView.setLayoutManager(gaggeredGridLayoutManager);
                recyclerView.addItemDecoration(moviesItemDecoration);
                recyclerView.setAdapter(moviesAdapter);
                callSliderAsync("4");
                if (moviesArray.size() == 0) {
                    callMoviesListAsync();
                } else {
                    mHandler.sendEmptyMessage(4);
                }
                break;

            case 1:
                ll_c_top_deals.setSelected(true);
                callSliderAsync("3");
                dealAdapter = new DealRecyclerViewAdapter(getActivity(), dealArray, this);
                recyclerView.setBackgroundColor(Color.WHITE);
                recyclerView.setPadding(0, 0, 0, 0);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.addItemDecoration(dealItemDecoration);
                recyclerView.setAdapter(dealAdapter);
                if (dealArray.size() == 0) {
                    callDealsListAsync();
                } else {
                    mHandler.sendEmptyMessage(3);
                }
                break;
        }
    }

    private void showBannerDialog(final SliderModel sliderModel) {
        Dialog bannerDialog = new Dialog(getActivity(), android.R.style.Theme_Light);
        bannerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bannerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        bannerDialog.setContentView(R.layout.dialog_deal_coupen);
        ImageView img_c_coupon = (ImageView) bannerDialog.findViewById(R.id.img_c_coupon);
        imageLoader.displayImage(sliderModel.banner_image, img_c_coupon);
//        Picasso.with(getActivity()).load(sliderModel.banner_image).into(img_c_coupon);
        TextView txt_c_coupon = (TextView) bannerDialog.findViewById(R.id.txt_c_coupon);
        TextView txt_coupon_time = (TextView) bannerDialog.findViewById(R.id.txt_coupon_time);
        TextView txt_c_save = (TextView) bannerDialog.findViewById(R.id.txt_c_save);
        txt_c_save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sliderModel.single_clicked == 0) {
                    callClicksAsync(sliderModel, 1);
                }
            }
        });
        bannerDialog.show();
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.deal)) {
//            Bundle bundle = new Bundle();
//            bundle.putParcelable(Tags.deal, dealArray.get(position));
//            Fragment fragment = new DealDetailsActivity();
//            fragment.setArguments(bundle);
//            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        } else if (name.equalsIgnoreCase(Tags.movies)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.movies, moviesArray.get(position));
            Intent intent = new Intent(getActivity(), MoviesDetailsActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }
}
