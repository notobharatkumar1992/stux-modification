package com.stux.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.CommentModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DividerItemDecoration;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.CommentRecyclerViewAdapter;

/**
 * Created by Bharat on 07/08/2016.
 */
public class CommentListingFragment extends Fragment implements OnReciveServerResponse, OnListItemClickListener {

    public static ArrayList<CommentModel> commentArray = new ArrayList<>();

    private Handler mHandler;
    private Prefs prefs;
    private UserDataModel userData;

    private ProgressBar progressbar;

    private TextView txt_c_no_list;
    public static int dealCounter = 1, dealTotalPage = -1;
    private boolean dealAsyncExcecuting = false;
    private int preLast;

    private SwipyRefreshLayout swipyrefreshlayout;
    private RecyclerView recyclerView;
    private CommentRecyclerViewAdapter rcAdapter;

    private ProductModel productModel;

    private EditText et_comment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.comment_listing, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        productModel = getArguments().getParcelable(Tags.product);
        commentArray.clear();
        dealCounter = 1;
        dealTotalPage = -1;
        if (commentArray.size() == 0) {
            dealAsyncExcecuting = true;
            callCampusListAsync(productModel.id);
        } else
            mHandler.sendEmptyMessage(2);
    }

    private void callCampusListAsync(String product_id) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, dealCounter + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, product_id + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, "1");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_COMMENT_LIST,
//                    user_id,product_id,status,page,record
                    mPostArrayList, null);
            if (dealCounter == 1)
                mHandler.sendEmptyMessage(12);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callPostCommentAsync(String product_id, String message) {
        if (message.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter comment before post.");
        } else if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, product_id + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.comment, message + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, "1");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.PRODUCT_COMMENTS,
//                    user_id,product_id,status,comment
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                    txt_c_no_list.setVisibility(View.GONE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    AppDelegate.LogT("mEventAdapter notified = " + commentArray.size());
                    txt_c_no_list.setVisibility(commentArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No comment available");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                    recyclerView.scrollToPosition(commentArray.size() - 1);
                } else if (msg.what == 3) {
                    txt_c_no_list.setVisibility(commentArray.size() > 0 ? View.GONE : View.VISIBLE);
                    et_comment.setText("");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                    recyclerView.scrollToPosition(commentArray.size() - 1);
                }
            }
        };
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Comments");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlider();
            }
        });
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });
        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        view.findViewById(R.id.img_c_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPostCommentAsync(productModel.id, et_comment.getText().toString());
            }
        });

        et_comment = (EditText) view.findViewById(R.id.et_comment);
        et_comment.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        rcAdapter = new CommentRecyclerViewAdapter(getActivity(), commentArray, this);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getActivity(), R.drawable.bg_divider));
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setPadding(0, 0, 0, 0);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.TOP) {
                             if (dealTotalPage != 0 && !dealAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 callCampusListAsync(productModel.id);
                                 dealAsyncExcecuting = true;
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + dealTotalPage + ", " + dealAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(13);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_COMMENT_LIST)) {
            dealAsyncExcecuting = false;
            swipyrefreshlayout.setRefreshing(false);
            parseEventListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.PRODUCT_COMMENTS)) {
            mHandler.sendEmptyMessage(11);
            parsePostCommentResult(result);
        }
    }

    private void parsePostCommentResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                CommentModel commentModel = new CommentModel();
                commentModel.id = jsonObject.getJSONObject(Tags.response).getString(Tags.ID);
                commentModel.user_id = jsonObject.getJSONObject(Tags.response).getString(Tags.user_id);

                commentModel.comment = jsonObject.getJSONObject(Tags.response).getString(Tags.comment);
                commentModel.status = jsonObject.getJSONObject(Tags.response).getString(Tags.status);
                commentModel.created = jsonObject.getJSONObject(Tags.response).getString(Tags.created);

                commentModel.user_first_name = userData.first_name;
                commentModel.user_last_name = userData.last_name;
                commentModel.user_image = userData.image;

                commentArray.add(commentModel);
                mHandler.sendEmptyMessage(3);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseEventListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1") && jsonObject.has(Tags.response)) {

                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    CommentModel commentModel = new CommentModel();
                    commentModel.id = object.getString(Tags.ID);
                    commentModel.user_id = object.getString(Tags.user_id);
                    commentModel.comment = object.getString(Tags.comment);
                    commentModel.status = object.getString(Tags.status);
                    commentModel.created = object.getString(Tags.created);

                    if (AppDelegate.isValidString(object.optString(Tags.user))) {
                        commentModel.user_first_name = JSONParser.getString(object.getJSONObject(Tags.user), Tags.first_name);
                        commentModel.user_last_name = JSONParser.getString(object.getJSONObject(Tags.user), Tags.last_name);
                        commentModel.user_image = JSONParser.getString(object.getJSONObject(Tags.user), Tags.image);
                        commentArray.add(commentModel);
                    }

                }
            } else {
                dealTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            dealCounter++;

            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.user)) {
            UserDataModel userDataModel = new UserDataModel();
            userDataModel.userId = commentArray.get(position).user_id;
            userDataModel.first_name = commentArray.get(position).user_first_name;
            userDataModel.last_name = commentArray.get(position).user_last_name;
            userDataModel.image = commentArray.get(position).user_id;
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.user, userDataModel);
            Fragment fragment = new SellersProfileFragment();
            if (userDataModel.userId.equalsIgnoreCase(userData.userId)) {
                fragment = new MyProfileFragment();
            } else {
                fragment = new SellersProfileFragment();
            }
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        }
    }
}
