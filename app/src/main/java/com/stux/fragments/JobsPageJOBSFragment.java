package com.stux.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.JobsModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.tweetcomposer.Card;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;
import com.wunderlist.slidinglayer.SlidingLayer;
import com.wunderlist.slidinglayer.transformer.AlphaTransformer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.JobsInternshipRecyclerViewAdapter;

/**
 * Created by Bharat on 07/08/2016.
 */
public class JobsPageJOBSFragment extends Fragment implements OnReciveServerResponse, OnListItemClickListener, View.OnClickListener {

    private Handler mHandler;
    private ArrayList<JobsModel> jobsArray = new ArrayList<>();
    private Prefs prefs;
    private UserDataModel userData;
    private ProgressBar progressbar;

    // Campus list
    private int campusCounter = 1, campusTotalPage = -1;
    private TextView txt_c_no_list;
    private boolean campusAsyncExcecuting = false;

    private SwipyRefreshLayout swipyrefreshlayout;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerView;
    private JobsInternshipRecyclerViewAdapter rcAdapter;

    public static SlidingLayer sl_more;

    private ImageView img_banner, img_loading;
    private CircleImageView cimg_user;
    private TextView txt_c_name, txt_c_inc, txt_c_address, txt_c_posted_by, txt_c_company_desc;

    private ScrollView scrollView;
    public static TwitterLoginButton loginButton;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.jobs_page_layout, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getActivity());
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        userData = prefs.getUserdata();
        if (jobsArray.size() == 0) {
            callCampusListAsync();
            campusAsyncExcecuting = true;
        } else
            mHandler.sendEmptyMessage(2);
    }

    private void callCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, campusCounter + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.job_type, "1");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_JOBS,
                    mPostArrayList, null);
            if (!campusAsyncExcecuting)
                mHandler.sendEmptyMessage(10);
            campusAsyncExcecuting = true;
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    txt_c_no_list.setVisibility(jobsArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No jobs available");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                }
            }
        };
    }

    private void initView(View view) {
        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
//
        recyclerView.setHasFixedSize(true);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        rcAdapter = new JobsInternshipRecyclerViewAdapter(getActivity(), jobsArray, this);
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (campusTotalPage != 0 && !campusAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 campusAsyncExcecuting = true;
                                 callCampusListAsync();
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + campusTotalPage + ", " + campusAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );


        sl_more = (SlidingLayer) view.findViewById(R.id.sl_more);
        sl_more.setStickTo(SlidingLayer.STICK_TO_BOTTOM);
        sl_more.setLayerTransformer(new AlphaTransformer());
        sl_more.setShadowSize(0);
        sl_more.setShadowDrawable(null);
        sl_more.closeLayer(true);

        img_banner = (ImageView) view.findViewById(R.id.img_banner);
        img_loading = (ImageView) view.findViewById(R.id.img_loading);
        cimg_user = (CircleImageView) view.findViewById(R.id.cimg_user);

        txt_c_name = (TextView) view.findViewById(R.id.txt_c_name);
        txt_c_inc = (TextView) view.findViewById(R.id.txt_c_inc);
        txt_c_address = (TextView) view.findViewById(R.id.txt_c_address);
        txt_c_posted_by = (TextView) view.findViewById(R.id.txt_c_posted_by);
        txt_c_company_desc = (TextView) view.findViewById(R.id.txt_c_company_desc);

        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

//        view.findViewById(R.id.txt_c_share).setOnClickListener(this);
        view.findViewById(R.id.txt_c_apply).setOnClickListener(this);

        view.findViewById(R.id.img_c_fb).setOnClickListener(this);
        view.findViewById(R.id.img_c_twitter).setOnClickListener(this);
        loginButton = (TwitterLoginButton) view.findViewById(R.id.login_button);

    }

    private File capturedFile;
    private Bitmap OriginalPhoto;
    private boolean imageLoaded = false;

    public void writeImageFile(Bitmap OriginalPhoto) {
        FileOutputStream fOut = null;
        try {
            if (capturedFile == null)
                capturedFile = new File(getNewFile());
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            try {
                File capturedFile_3 = new File(directoryFile, "Image_" + System.currentTimeMillis()
                        + ".png");
                if (capturedFile_3.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile_3.getAbsolutePath());
                    return capturedFile_3.getAbsolutePath();
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        callbackManager = null;
        loginButton = null;
        AppDelegate.LogT("ProductDetailFragment onDestroyView called");
    }

    private ShareDialog shareDialog;
    public static CallbackManager callbackManager;
    private boolean isCalledOnce = false;

    public void openFacebook(final JobsModel productModel) {
        FacebookSdk.sdkInitialize(getActivity());
        shareDialog = new ShareDialog(this);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("onSuccess = " + loginResult.getAccessToken());
                        shareFacebook(productModel);
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook(productModel);
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(getActivity());
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook(productModel);
                                }
                            }
                        }
                    }
                });
    }

    private void shareFacebook(JobsModel productModel) {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            String description = "\nPosted by" + productModel.posted_by + "\n" + productModel.description;
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(productModel.title)
//                    .setImageUrl(imageURI)
                    .setImageUrl(Uri.parse(productModel.banner_image))
                    .setContentDescription(description)
                    .setContentUrl(Uri.parse("http://www.stuxmobile.com/"))
                    .build();
            shareDialog.show(linkContent);
        }
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_JOBS)) {
            campusAsyncExcecuting = false;
            swipyrefreshlayout.setRefreshing(false);
            parseCampusListResult(result);
        }
    }

    private void parseCampusListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        JobsModel productModel = new JobsModel();
                        productModel.id = object.getString(Tags.id);
                        productModel.job_type = object.getString(Tags.job_type);
                        productModel.job_timing_type = object.getString(Tags.job_timing_type);
                        productModel.title = object.getString(Tags.title);
                        productModel.description = object.getString(Tags.description);
                        productModel.company_name = object.getString(Tags.company_name);
                        productModel.company_address = object.getString(Tags.company_address);
                        productModel.industry_type = object.getString(Tags.industry_type);
                        productModel.logo_image = object.getString(Tags.logo_image);
                        productModel.banner_image = object.getString(Tags.banner_image);
                        productModel.contact_no = object.getString(Tags.contact_no);

                        productModel.logo_image_thumb = object.getString(Tags.logo_image_thumb);
                        productModel.banner_image_thumb = object.getString(Tags.banner_image_thumb);

                        productModel.posted_by = object.getString(Tags.posted_by);
                        productModel.designation = object.getString(Tags.designation);
                        productModel.email = object.getString(Tags.email);
                        productModel.website_url = object.getString(Tags.website_url);
                        productModel.expiry_date = object.getString(Tags.expiry_date);
                        productModel.status = object.getString(Tags.status);
                        productModel.created = object.getString(Tags.created);
                        productModel.modified = object.getString(Tags.modified);
                        productModel.isApplied = object.getString(Tags.isApplied);
                        productModel.apply_type = object.getString(Tags.apply_type);
                        productModel.external_link = object.getString(Tags.external_link);
                        jobsArray.add(productModel);
                    }
                } else {
                    campusTotalPage = 0;
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                campusTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            campusCounter++;
            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.jobs)) {
            jobsModel = jobsArray.get(position);
            setValues();
            sl_more.openLayer(true);
        }
    }

    JobsModel jobsModel;

    private void setValues() {
        img_loading.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
        frameAnimation.setCallback(img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        imageLoader.displayImage(jobsModel.banner_image, img_banner);
//        Picasso.with(getActivity()).load(jobsModel.banner_image).into(img_banner);
        imageLoader.loadImage(jobsModel.logo_image, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                cimg_user.setImageBitmap(loadedImage);
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
//        Picasso.with(getActivity()).load(jobsModel.logo_image).into(cimg_user, new Callback() {
//            @Override
//            public void onSuccess() {
//                img_loading.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onError() {
//
//            }
//        });
        txt_c_name.setText(jobsModel.title);
        txt_c_inc.setText(jobsModel.company_name);
        txt_c_address.setText(jobsModel.company_address);
        txt_c_posted_by.setText(Html.fromHtml("<p>Posted by <b>" + jobsModel.posted_by + "<b/> (" + jobsModel.designation + ")</p>"));
        txt_c_company_desc.setText(jobsModel.description);

        scrollView.fullScroll(ScrollView.FOCUS_UP);

        loginButton.setCallback(new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                AppDelegate.LogT("Twitter success called");
                if (OriginalPhoto != null) {
                    if (capturedFile == null)
                        writeImageFile(OriginalPhoto);
                    if (capturedFile == null) {
                        AppDelegate.showToast(getActivity(), "Failed to create image for share please try again later.");
                        return;
                    }
                    final TwitterSession session = TwitterCore.getInstance().getSessionManager()
                            .getActiveSession();
                    Card card = new Card.AppCardBuilder(getActivity())
                            .imageUri(Uri.fromFile(capturedFile))
                            .googlePlayId(getActivity().getPackageName())
                            .build();
                    Intent intent = new ComposerActivity.Builder(getActivity())
                            .session(session)
                            .card(card)
                            .hashtags("#" + jobsModel.title, "#" + jobsModel.company_name, "#" + jobsModel.posted_by)
                            .createIntent();
                    startActivity(intent);
                } else {
                    AppDelegate.showToast(getActivity(), "For tweet you have to wait until first image get downloaded.");
                }
            }

            @Override
            public void failure(TwitterException exception) {
                AppDelegate.LogT("Twitter failure called => " + exception);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_fb:
//                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, jobsModel.title);
//                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, jobsModel.description + "\n\nContact Number " + jobsModel.contact_no + "\n\nEmail " + jobsModel.email);
//                startActivity(Intent.createChooser(sharingIntent, "Share Using.."));
                openFacebook(jobsModel);
                break;

            case R.id.img_c_twitter:
                loginButton.performClick();
                break;

            case R.id.txt_c_apply:
                if (!AppDelegate.isValidString(jobsModel.apply_type) || (AppDelegate.isValidString(jobsModel.apply_type) && jobsModel.apply_type.equalsIgnoreCase("1"))) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + jobsModel.email));
                    intent.putExtra(Intent.EXTRA_SUBJECT, jobsModel.title);
                    startActivity(intent);
                } else {
                    AppDelegate.openURL(getActivity(), jobsModel.external_link);
                }
                break;
        }
    }

}
