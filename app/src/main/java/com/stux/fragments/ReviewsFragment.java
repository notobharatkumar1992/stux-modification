package com.stux.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.InstitutionModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ReviewModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DividerItemDecoration;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.wunderlist.slidinglayer.SlidingLayer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.ReviewsRecyclerViewAdapter;

/**
 * Created by Bharat on 07/08/2016.
 */
public class ReviewsFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse, OnListItemClickListener {

    private Handler mHandler;
    private Prefs prefs;
    private UserDataModel dataModel, sellerDataModel;

    private SwipyRefreshLayout swipyrefreshlayout;
    private RecyclerView recyclerView;
    private ReviewsRecyclerViewAdapter rcAdapter;

    private ArrayList<ReviewModel> reviewArray = new ArrayList<>();

    private SlidingLayer mSlidingLayer;

    private ImageView img_star_1, img_star_2, img_star_3, img_star_4, img_star_5;
    private EditText et_description;

    private int campusCounter = 1, campusTotalPage = -1;
    private int preLast, star_position = 0;
    private boolean campusAsyncExcecuting = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reviews_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        sellerDataModel = getArguments().getParcelable(Tags.user);
        AppDelegate.LogT("ReviewsFragment sellerDataModel = " + sellerDataModel.userId + ", dataModel = " + dataModel.userId);
        initView(view);
        setHandler();
        if (reviewArray.size() == 0) {
            callReviewListAsync();
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
//                    progressbar.setVisibility(View.VISIBLE);
//                    txt_c_no_value.setVisibility(View.GONE);
                } else if (msg.what == 13) {
//                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 1) {
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
//                    txt_c_no_value.setVisibility(reviewArray.size() == 0 ? View.VISIBLE : View.GONE);
                }
            }
        };
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText(R.string.Review);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        view.findViewById(R.id.img_c_left).setVisibility(View.VISIBLE);
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });
        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        rcAdapter = new ReviewsRecyclerViewAdapter(getActivity(), reviewArray, this);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getActivity(), R.drawable.bg_divider));
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
        swipyrefreshlayout.setPadding(AppDelegate.dpToPix(getActivity(), 15), 0, AppDelegate.dpToPix(getActivity(), 15), 0);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (campusTotalPage != 0 && !campusAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 callReviewListAsync();
                                 campusAsyncExcecuting = true;
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + campusTotalPage + ", " + campusAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );

        if (sellerDataModel != null && dataModel != null && sellerDataModel.userId.equalsIgnoreCase(dataModel.userId)) {
            view.findViewById(R.id.txt_c_add_review).setVisibility(View.GONE);
        } else
            view.findViewById(R.id.txt_c_add_review).setOnClickListener(this);

        mSlidingLayer = (SlidingLayer) view.findViewById(R.id.slidingLayer1);
        mSlidingLayer.setStickTo(SlidingLayer.STICK_TO_BOTTOM);

        mSlidingLayer.setShadowSize(0);
        mSlidingLayer.setShadowDrawable(null);
//        mSlidingLayer.openLayer(true);
        mSlidingLayer.closeLayer(false);

        img_star_1 = (ImageView) view.findViewById(R.id.img_star_1);
        img_star_1.setOnClickListener(this);
        img_star_2 = (ImageView) view.findViewById(R.id.img_star_2);
        img_star_2.setOnClickListener(this);
        img_star_3 = (ImageView) view.findViewById(R.id.img_star_3);
        img_star_3.setOnClickListener(this);
        img_star_4 = (ImageView) view.findViewById(R.id.img_star_4);
        img_star_4.setOnClickListener(this);
        img_star_5 = (ImageView) view.findViewById(R.id.img_star_5);
        img_star_5.setOnClickListener(this);

        et_description = (EditText) view.findViewById(R.id.et_description);
        et_description.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));


        view.findViewById(R.id.txt_c_submit).setOnClickListener(this);
    }

    private void callReviewListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.reviewer_id, sellerDataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, campusCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_USER_REVIEW,
                    mPostArrayList, null);
            if (campusCounter == 1)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callAddReviewListAsync(String reviewer_id, String str_comment, int review) {
        if (str_comment.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter comment.");
        } else if (review == 0) {
            AppDelegate.showAlert(getActivity(), "Please provide rating to submit review.");
        } else if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.reviewer_id, reviewer_id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.reviews, str_comment);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.rating, review + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.ADD_REVIEW,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                ((MainActivity) getActivity()).toggleSlider();
                break;

            case R.id.txt_c_add_review:
                mSlidingLayer.openLayer(true);
                break;

            case R.id.txt_c_submit:
                callAddReviewListAsync(sellerDataModel.userId, et_description.getText().toString(), star_position);
                break;


            case R.id.img_star_1:
                star_position = 1;
                img_star_1.setImageResource(R.drawable.star_filled);
                img_star_2.setImageResource(R.drawable.star_blank);
                img_star_3.setImageResource(R.drawable.star_blank);
                img_star_4.setImageResource(R.drawable.star_blank);
                img_star_5.setImageResource(R.drawable.star_blank);
                break;

            case R.id.img_star_2:
                star_position = 2;
                img_star_1.setImageResource(R.drawable.star_filled);
                img_star_2.setImageResource(R.drawable.star_filled);
                img_star_3.setImageResource(R.drawable.star_blank);
                img_star_4.setImageResource(R.drawable.star_blank);
                img_star_5.setImageResource(R.drawable.star_blank);
                break;

            case R.id.img_star_3:
                star_position = 3;
                img_star_1.setImageResource(R.drawable.star_filled);
                img_star_2.setImageResource(R.drawable.star_filled);
                img_star_3.setImageResource(R.drawable.star_filled);
                img_star_4.setImageResource(R.drawable.star_blank);
                img_star_5.setImageResource(R.drawable.star_blank);
                break;

            case R.id.img_star_4:
                star_position = 4;
                img_star_1.setImageResource(R.drawable.star_filled);
                img_star_2.setImageResource(R.drawable.star_filled);
                img_star_3.setImageResource(R.drawable.star_filled);
                img_star_4.setImageResource(R.drawable.star_filled);
                img_star_5.setImageResource(R.drawable.star_blank);
                break;

            case R.id.img_star_5:
                star_position = 5;
                img_star_1.setImageResource(R.drawable.star_filled);
                img_star_2.setImageResource(R.drawable.star_filled);
                img_star_3.setImageResource(R.drawable.star_filled);
                img_star_4.setImageResource(R.drawable.star_filled);
                img_star_5.setImageResource(R.drawable.star_filled);
                break;
        }

    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(13);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_USER_REVIEW)) {
            if (campusCounter > 1) {
                campusAsyncExcecuting = false;
            } else if (campusCounter == 1)
                mHandler.sendEmptyMessage(11);
            swipyrefreshlayout.setRefreshing(false);
            parseReviewListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.ADD_REVIEW)) {
            mHandler.sendEmptyMessage(11);
            parseAddReviewResponse(result);
        }
    }

    private void parseAddReviewResponse(String result) {
        try {
            JSONObject object = new JSONObject(result);
            if (!object.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), object.getString(Tags.message));
            } else {
                mSlidingLayer.closeLayer(false);
                et_description.setText("");
                img_star_1.performClick();

                JSONObject jsonObject = object.getJSONObject(Tags.response);
                ReviewModel reviewModel = new ReviewModel();
                reviewModel.id = jsonObject.getString(Tags.id);
                reviewModel.user_id = jsonObject.getString(Tags.logged_in_user_id);
                reviewModel.reviewer_id = jsonObject.getString(Tags.reviewer_id);
                reviewModel.reviews = jsonObject.getString(Tags.reviews);
                reviewModel.rating = jsonObject.getString(Tags.rating);
                reviewModel.created = jsonObject.getString(Tags.created);
                reviewModel.userDataModel = dataModel;
                reviewArray.add(reviewModel);
                AppDelegate.showToast(getActivity(), "Review Submitted Successfully!");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(1);
    }

    private void parseReviewListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                if (jsonObject.has(Tags.response)) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    reviewArray.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject objectReview = jsonArray.getJSONObject(i);
                        ReviewModel reviewModel = new ReviewModel();
                        reviewModel.id = objectReview.getString(Tags.id);
                        reviewModel.user_id = objectReview.getString(Tags.logged_in_user_id);
                        reviewModel.reviewer_id = objectReview.getString(Tags.reviewer_id);
                        reviewModel.reviews = objectReview.getString(Tags.reviews);
                        reviewModel.rating = objectReview.getString(Tags.rating);
                        reviewModel.created = objectReview.getString(Tags.created);

                        if (!reviewModel.user_id.equalsIgnoreCase("0")) {
                            JSONObject object = objectReview.getJSONObject(Tags.user);
                            reviewModel.userDataModel = new UserDataModel();
                            reviewModel.userDataModel.role = object.getString(Tags.role);
                            reviewModel.userDataModel.status = object.getString(Tags.status);
                            reviewModel.userDataModel.first_name = object.getString(Tags.first_name);
                            reviewModel.userDataModel.last_name = object.getString(Tags.last_name);
                            reviewModel.userDataModel.email = object.getString(Tags.email);
                            reviewModel.userDataModel.password = object.getString(Tags.password);
                            reviewModel.userDataModel.role = object.getString(Tags.role);
                            reviewModel.userDataModel.image = object.getString(Tags.image);
                            reviewModel.userDataModel.created = object.getString(Tags.created);
                            reviewModel.userDataModel.gcm_token = object.getString(Tags.gcm_token);
                            reviewModel.userDataModel.facebook_id = object.getString(Tags.social_id);
                            reviewModel.userDataModel.login_type = object.getString(Tags.login_type);
                            reviewModel.userDataModel.is_verified = object.getString(Tags.is_verified);

                            JSONObject studentObject = object.getJSONObject(Tags.student_detail);
                            reviewModel.userDataModel.str_Gender = studentObject.getString(Tags.gender);
                            reviewModel.userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
                            reviewModel.userDataModel.userId = studentObject.getString(Tags.user_id);
                            reviewModel.userDataModel.dob = studentObject.getString(Tags.dob);
                            reviewModel.userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

                            reviewModel.userDataModel.institutionModel = new InstitutionModel();
                            reviewModel.userDataModel.institutionModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                            if (studentObject.has(Tags.institution_state_name))
                                reviewModel.userDataModel.institutionModel.institution_state_name = studentObject.getString(Tags.institution_state_name);
                            if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                                reviewModel.userDataModel.institutionModel.institution_name_id = studentObject.getString(Tags.institution_state_id);
                                reviewModel.userDataModel.institutionModel.institution_name = studentObject.getString(Tags.institution_name);
                            } else {
                                reviewModel.userDataModel.institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
                            }
                            if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                                reviewModel.userDataModel.institutionModel.department_name = studentObject.getString(Tags.department_name);
                            }

                            reviewArray.add(reviewModel);
                        }
                    }
                } else {
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                }
            } else {
                campusTotalPage = 0;
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
            }
            campusCounter++;
            mHandler.sendEmptyMessage(1);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.product)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.user, reviewArray.get(position).userDataModel);
            Fragment fragment = new SellersProfileFragment();
            AppDelegate.LogT("ReviewsFragment count = " + reviewArray.get(position).userDataModel.userId + ", dataModel = " + dataModel.userId);
            if (reviewArray.get(position).userDataModel.userId.equalsIgnoreCase(dataModel.userId)) {
                fragment = new MyProfileFragment();
            } else {
                fragment = new SellersProfileFragment();
            }
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        }
    }
}
