package com.stux.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.DealModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DividerItemDecoration;
import com.stux.Utils.Prefs;
import com.stux.activities.DealDetailsActivity;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.MyDealRecyclerViewAdapter;

/**
 * Created by Bharat on 07/08/2016.
 */
public class MyDealListingFragment extends Fragment implements OnReciveServerResponse, OnListItemClickListener {

    public static ArrayList<DealModel> dealArray = new ArrayList<>();
    private Handler mHandler;
    private Prefs prefs;
    private UserDataModel userData;
    private ProgressBar progressbar;

    private TextView txt_c_no_list;
    public static int dealCounter = 1, dealTotalPage = -1;
    private boolean dealAsyncExcecuting = false;
    private int preLast;

    private SwipyRefreshLayout swipyrefreshlayout;
    private RecyclerView recyclerView;
    private MyDealRecyclerViewAdapter rcAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_event_list_view, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        dealArray.clear();
        dealCounter = 1;
        dealTotalPage = -1;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (dealArray.size() == 0) {
            dealAsyncExcecuting = true;
            callCampusListAsync();
        } else
            mHandler.sendEmptyMessage(2);
    }

    private void callCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, dealCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.MY_COUPON,
                    mPostArrayList, null);
            if (dealCounter == 1)
                mHandler.sendEmptyMessage(12);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                    txt_c_no_list.setVisibility(View.GONE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    AppDelegate.LogT("mEventAdapter notified = " + dealArray.size());
                    txt_c_no_list.setVisibility(dealArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No deal available");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                }
            }
        };
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("My Deals");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlider();
            }
        });
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });
        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        rcAdapter = new MyDealRecyclerViewAdapter(getActivity(), dealArray, this);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getActivity(), R.drawable.bg_divider));
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setPadding(0, 0, 0, 0);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (dealTotalPage != 0 && !dealAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 callCampusListAsync();
                                 dealAsyncExcecuting = true;
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + dealTotalPage + ", " + dealAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(13);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.MY_COUPON)) {
            dealAsyncExcecuting = false;
            swipyrefreshlayout.setRefreshing(false);
            parseEventListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GRAB_COUPON_DELETE)) {
            mHandler.sendEmptyMessage(11);
            parseCouponDeleteResult(result);
        }
    }

    private void parseCouponDeleteResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                dealArray.remove(selected_item);
                mHandler.sendEmptyMessage(2);
            } else {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseEventListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        DealModel dealModel = new DealModel();
                        dealModel.created = object.getString(Tags.created);
                        dealModel.id = object.getString(Tags.id);

                        dealModel.total_deal_views = jsonArray.getJSONObject(i).getString(Tags.total_deal_views);
                        dealModel.logged_user_view_status = jsonArray.getJSONObject(i).getString(Tags.logged_user_view_status);

                        if (object.has(Tags.deal) && AppDelegate.isValidString(object.getString(Tags.deal))) {
                            object = object.getJSONObject(Tags.deal);
                            dealModel.id = object.getString(Tags.id);
                            dealModel.user_id = object.getString(Tags.user_id);
                            dealModel.deal_area = object.getString(Tags.deal_area);
                            dealModel.deal_catid = object.getString(Tags.deal_catid);
                            dealModel.institute_id = object.getString(Tags.institute_id);
                            dealModel.title = object.getString(Tags.title);
                            dealModel.details = object.getString(Tags.details);
                            dealModel.venue = object.getString(Tags.venue);
                            dealModel.price = object.getString(Tags.price);
//                            if (!AppDelegate.isValidInt(dealModel.price))
//                                dealModel.price = "0";
                            dealModel.discount = object.getString(Tags.discount);
//                            if (!AppDelegate.isValidInt(dealModel.discount))
//                                dealModel.discount = "0";
                            dealModel.discount_price = object.getString(Tags.discount_price);
//                            if (!AppDelegate.isValidInt(dealModel.discount_price))
//                                dealModel.discount_price = "0";
                            dealModel.coupon_code = object.getString(Tags.coupon_code);
                            dealModel.coupon_no = object.optString(Tags.coupon_no);
                            dealModel.expiry_date = object.getString(Tags.expiry_date);

                            JSONObject dealObject = object.getJSONObject(Tags.deal_category);
                            dealModel.product_category = dealObject.getString(Tags.title);

                            dealModel.image_1 = object.getString(Tags.image_1);
                            dealModel.image_2 = object.getString(Tags.image_2);
                            dealModel.image_3 = object.getString(Tags.image_3);
                            dealModel.image_4 = object.getString(Tags.image_4);

                            dealModel.image_1_thumb = object.getString(Tags.image_1_thumb);
                            dealModel.image_2_thumb = object.getString(Tags.image_2_thumb);
                            dealModel.image_3_thumb = object.getString(Tags.image_3_thumb);
                            dealModel.image_4_thumb = object.getString(Tags.image_4_thumb);

//                    dealModel.is_grabbed = object.getString(Tags.is_grabbed);
//                    dealModel.sold_status = object.getString(Tags.sold_status);
//                    dealModel.status = object.getString(Tags.status);
//                    dealModel.modified = object.getString(Tags.modified);
                            dealArray.add(dealModel);
                        }
                    }
                } else {
                    dealTotalPage = 0;
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                dealTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            dealCounter++;

            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.deal)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.deal, dealArray.get(position));
            bundle.putString(Tags.FROM, Tags.MY_DEAL);
            Intent intent = new Intent(getActivity(), DealDetailsActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
//            Bundle bundle = new Bundle();
//            bundle.putParcelable(Tags.deal, dealArray.get(position));
//            Fragment fragment = new DealDetailsActivity();
//            fragment.setArguments(bundle);
//            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        } else if (name.equalsIgnoreCase(Tags.LONG_CLICK)) {
            showChatOptionsAlert(position);
        }
    }

    public void showChatOptionsAlert(final int position) {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"  Open", "  Delete", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Tags.deal, dealArray.get(position));
                        bundle.putString(Tags.FROM, Tags.MY_DEAL);
                        Intent intent = new Intent(getActivity(), DealDetailsActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                    case 1:
                        dialog.dismiss();
                        callDeleteDealCoupnAsync(position);
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public int selected_item = 0;

    private void callDeleteDealCoupnAsync(int position) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.coupon_id, dealArray.get(position).id);

            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GRAB_COUPON_DELETE,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            selected_item = position;
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }
}
