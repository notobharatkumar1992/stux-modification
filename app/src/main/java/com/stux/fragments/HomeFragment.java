package com.stux.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.Adapters.PagerLoopAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.DealModel;
import com.stux.Models.InstitutionModel;
import com.stux.Models.MoviesModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.SliderModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.MyViewPager;
import com.stux.Utils.Prefs;
import com.stux.Utils.SpacesItemDecoration;
import com.stux.activities.MainActivity;
import com.stux.activities.MoviesDetailsActivity;
import com.stux.activities.SellItemActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.DealRecyclerViewAdapter;
import inducesmile.com.androidstaggeredgridlayoutmanager.MoviesRecyclerViewAdapter;
import inducesmile.com.androidstaggeredgridlayoutmanager.ProductRecyclerViewAdapter;

/**
 * Created by NOTO on 5/27/2016.
 */
public class HomeFragment extends Fragment implements OnClickListener, OnReciveServerResponse, OnListItemClickListener {

    public static ArrayList<ProductModel> productArray = new ArrayList<>();
    private MyViewPager /*view_pager,*/ view_pager_banner;
    private PagerLoopAdapter /*pagerAdapter,*/ bannerPagerAdapter;

    private LinearLayout ll_c_campus_list, ll_c_top_deals, ll_c_events;
    private ArrayList<Fragment> bannerFragment = new ArrayList<>();
    private ArrayList<SliderModel> sliderArray = new ArrayList<>();
    private RelativeLayout rl_c_banner;
    private Handler mHandler;
    private ProgressBar progressbar/*, progressbar_1*/;
    private android.widget.LinearLayout pager_indicator;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private ImageView[] dots;
    private TextView txt_c_shop_now/*, txt_c_no_list*/;
    private Prefs prefs;
    private UserDataModel dataModel;
    private InstitutionModel institutionModel;

    // Campus list
    private RecyclerView recyclerView;
    private ProductRecyclerViewAdapter rcAdapter;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    public static int campusCounter = 1, campusTotalPage = -1;

    private SpacesItemDecoration campusItemDecoration;
    private SpacesItemDecoration dealItemDecoration;
    private SpacesItemDecoration moviesItemDecoration;

    // Deal list
    public static ArrayList<DealModel> dealArray = new ArrayList<>();
    private DealRecyclerViewAdapter dealAdapter;
    private LinearLayoutManager linearLayoutManager;

    public static int dealCounter = 1, dealTotalPage = -1;

    // Movies list
    private MoviesRecyclerViewAdapter moviesAdapter;
    public static ArrayList<MoviesModel> moviesArray = new ArrayList<>();
    public static int moviesCounter = 1, moviesTotalPage = -1;

    private PostAsync sliderAsync;

    private boolean campusAsyncExcecuting = false, dealAsyncExcecuting = false, moviesAsyncExcecuting = false;

    private SwipyRefreshLayout swipyrefreshlayout;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppDelegate.LogT("HomeFragment onCreateView called");
        return inflater.inflate(R.layout.home, container, false);
    }

    public View convertView;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        convertView = view;
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        AppDelegate.LogT("HomeFragment onViewCreated called");
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        institutionModel = prefs.getInstitutionModel();
        sliderArray.clear();
        setHandler();
        initView(view);
        recyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                recyclerView.smoothScrollToPosition(0);
                updateAdapter(selected_tab);
            }
        }, 200);
        ((MainActivity) getActivity()).callGetNotificationAsync();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                    txt_c_shop_now.setVisibility(View.GONE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 14) {
//                    progressbar_1.setVisibility(View.VISIBLE);
//                    txt_c_no_list.setVisibility(View.GONE);
                } else if (msg.what == 15) {
//                    progressbar_1.setVisibility(View.GONE);
                } else if (msg.what == 1) {
                    AppDelegate.LogT("sliderArray = " + sliderArray.size());
                    bannerFragment.clear();
                    for (int i = 0; i < sliderArray.size(); i++) {
                        Fragment fragment = new BannerHomeFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Tags.slider_id, sliderArray.get(i));
                        fragment.setArguments(bundle);
                        bannerFragment.add(fragment);
                    }

                    bannerPagerAdapter = new PagerLoopAdapter(getChildFragmentManager(), bannerFragment);
                    view_pager_banner.setAdapter(bannerPagerAdapter);

                    setUiPageViewController();
                    if (sliderArray.size() == 0) {
                        txt_c_shop_now.setVisibility(View.VISIBLE);
                        if (selected_tab == 0) {
                            txt_c_shop_now.setText("Top Campus banner are not available.");
                        } else if (selected_tab == 1) {
                            txt_c_shop_now.setText("Top Deals banner are not available.");
                        } else if (selected_tab == 2) {
                            txt_c_shop_now.setText("Top Events banner are not available.");
                        }
                    } else {
                        txt_c_shop_now.setVisibility(View.GONE);
                        rl_c_banner.setVisibility(View.VISIBLE);
                        view_pager_banner.setVisibility(View.VISIBLE);
                    }
                    timeOutLoop();
                } else if (msg.what == 2) {
                    AppDelegate.LogT("gridViewCampusList notified => selected_tab = " + selected_tab);
                    if (selected_tab == 0) {
                        rcAdapter.notifyDataSetChanged();
                        recyclerView.invalidate();
                    }
                } else if (msg.what == 3) {
                    if (selected_tab == 1) {
                        AppDelegate.LogT("mDealAdapter notified = " + dealArray.size() + ",  => selected_tab = " + selected_tab);
                        dealAdapter.notifyDataSetChanged();
                        recyclerView.invalidate();
                    }
                } else if (msg.what == 4) {
                    if (selected_tab == 2) {
                        AppDelegate.LogT("mMoviesAdapter notified = " + moviesArray.size() + ",  => selected_tab = " + selected_tab + ", moviesCounter = " + moviesCounter);
                        moviesAdapter.notifyDataSetChanged();
                        recyclerView.invalidate();
                    }
                    moviesAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                } else if (msg.what == 20) {
                    if (convertView != null)
                        convertView.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
                }
            }
        };
    }

    private void initView(View view) {
        rl_c_banner = (RelativeLayout) view.findViewById(R.id.rl_c_banner);

        android.widget.LinearLayout.LayoutParams layoutParams = (android.widget.LinearLayout.LayoutParams) rl_c_banner.getLayoutParams();
        layoutParams.height = AppDelegate.getDeviceWith(getActivity()) / 2 - AppDelegate.dpToPix(getActivity(), 10);
        rl_c_banner.setLayoutParams(layoutParams);

        view.findViewById(R.id.txt_c_header).setVisibility(View.GONE);
        view.findViewById(R.id.img_c_header).setVisibility(View.VISIBLE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);

        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        ll_c_campus_list = (LinearLayout) view.findViewById(R.id.ll_c_campus_list);
        ll_c_campus_list.setOnClickListener(this);
        ll_c_top_deals = (LinearLayout) view.findViewById(R.id.ll_c_top_deals);
        ll_c_top_deals.setOnClickListener(this);
        ll_c_events = (LinearLayout) view.findViewById(R.id.ll_c_events);
        ll_c_events.setOnClickListener(this);

        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);

        txt_c_shop_now = (TextView) view.findViewById(R.id.txt_c_shop_now);

        pager_indicator = (android.widget.LinearLayout) view.findViewById(R.id.pager_indicator);

        view_pager_banner = (MyViewPager) view.findViewById(R.id.view_pager_banner);
        bannerPagerAdapter = new PagerLoopAdapter(getChildFragmentManager(), bannerFragment);
        view_pager_banner.setAdapter(bannerPagerAdapter);
        view_pager_banner.setVisibility(View.VISIBLE);
        setUiPageViewController();
        view_pager_banner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (bannerFragment.size() > 0)
                    switchBannerPage(position % bannerFragment.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        switchBannerPage(0);

        // Campus list Data init
        campusItemDecoration = new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 5));
        dealItemDecoration = new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 0), true);
        moviesItemDecoration = new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 5));

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        rcAdapter = new ProductRecyclerViewAdapter(getActivity(), productArray, this);
        recyclerView.setAdapter(rcAdapter);

        // Deal list Data init
        linearLayoutManager = new LinearLayoutManager(getActivity());
        dealAdapter = new DealRecyclerViewAdapter(getActivity(), dealArray, this);
        recyclerView.setAdapter(dealAdapter);

        // Movies list Data init
        moviesAdapter = new MoviesRecyclerViewAdapter(getActivity(), moviesArray, this);
        recyclerView.setAdapter(moviesAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (selected_tab == 0) {
                                 if (campusTotalPage != 0 && !campusAsyncExcecuting) {
                                     mHandler.sendEmptyMessage(2);
                                     callCampusListAsync();
                                     campusAsyncExcecuting = true;
                                 } else {
                                     swipyrefreshlayout.setRefreshing(false);
                                     AppDelegate.LogT("selected_tab = 0, " + campusTotalPage + ", " + campusAsyncExcecuting);
                                 }
                             } else if (selected_tab == 1) {
                                 if (dealTotalPage != 0 && !dealAsyncExcecuting) {
                                     mHandler.sendEmptyMessage(2);
                                     callDealsListAsync();
                                     dealAsyncExcecuting = true;
                                 } else {
                                     swipyrefreshlayout.setRefreshing(false);
                                     AppDelegate.LogT("selected_tab = 1, " + dealTotalPage + ", " + dealAsyncExcecuting);
                                 }
                             } else {
                                 if (moviesTotalPage != 0 && !moviesAsyncExcecuting) {
                                     mHandler.sendEmptyMessage(4);
                                     callEventListAsync();
                                     moviesAsyncExcecuting = true;
                                 } else {
                                     swipyrefreshlayout.setRefreshing(false);
                                     AppDelegate.LogT("selected_tab = 2, " + moviesTotalPage + ", " + moviesAsyncExcecuting);
                                 }
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );

    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        int value = view_pager_banner.getCurrentItem();
                        if (sliderArray.size() == 0 || !isAdded()) {
                            return;
                        } else {
                            pager_indicator.setVisibility(View.VISIBLE);
                            if (value == bannerPagerAdapter.getCount() - 1) {
                                value = 0;
                            } else {
                                value++;
                            }
                            if (value < bannerPagerAdapter.getCount())
                                view_pager_banner.setCurrentItem(value, false);
                            if (isAdded()) {
                                mHandler.postDelayed(this, 3000);
                            } else {
                                mHandler.removeCallbacks(this);
                            }
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }

    public static void clearArrayData(int value) {
        AppDelegate.LogCh("clearArrayData called at HomeFragment => " + value);
        switch (value) {
            case 0:
                campusCounter = 1;
                campusTotalPage = -1;
                productArray.clear();
                break;
            case 1:
                dealCounter = 1;
                dealTotalPage = -1;
                dealArray.clear();
                break;
            case 2:
                moviesCounter = 1;
                moviesTotalPage = -1;
                moviesArray.clear();
                break;
            default:
                campusCounter = 1;
                campusTotalPage = -1;
                productArray.clear();

                dealCounter = 1;
                dealTotalPage = -1;
                dealArray.clear();

                moviesCounter = 1;
                moviesTotalPage = -1;
                moviesArray.clear();
                break;
        }
    }

    private void callCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.institution_id, institutionModel.institution_name_id);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.CAMPUS_LIST,
                    mPostArrayList, null);
            if (campusCounter == 1) {
                mHandler.sendEmptyMessage(10);
            }
            AppDelegate.LogT("callCampusListAsync called");
            mPostAsyncObj.execute();
        } else if (isAdded()) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppDelegate.LogE("onDestroy called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppDelegate.LogE("onDestroyView called");
    }

    private void callDealsListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            try {
                if (AppDelegate.isValidString(prefs.getInstitutionModel().institution_name_id))
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.institution_id, prefs.getInstitutionModel().institution_name_id);
                else
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.institution_id, "other");
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }

            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, dealCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.DEALS_LIST,
                    mPostArrayList, null);
            if (dealCounter == 1)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else if (isAdded()) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callEventListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, moviesCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.MOVIES_LIST,
                    mPostArrayList, null);
            if (moviesCounter == 1)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else if (isAdded()) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void switchBannerPage(int position) {
        try {
            if (dotsCount > 0) {
                for (int i = 0; i < dotsCount; i++) {
                    if (dots != null && dots.length > i)
                        dots[i].setImageResource(R.drawable.white_radius_square);
                }
                if (dots != null && dots.length > position)
                    dots[position].setImageResource(R.drawable.orange_radius_square);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        timeOutLoop();
    }

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = bannerFragment.size();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(getActivity());
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20, 20);
                if (getString(R.string.values_folder).equalsIgnoreCase("values-hdpi")) {
                    params = new LinearLayout.LayoutParams(12, 12);
                    params.setMargins(7, 0, 7, 0);
                } else
                    params.setMargins(10, 0, 10, 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.orange_radius_square));
        }
    }

    private void callProductLikesAsync(String product_id, String status) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, product_id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, status);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.PRODUCT_LIKES,
                    mPostArrayList, this);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else if (isAdded()) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callClicksAsync(SliderModel sliderModel, int type) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.slider_id, sliderModel.id);
            if (type == 0)
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.click_status, "1");
            else
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.thumb_status, "1");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.device_id, AppDelegate.getUUID(getActivity()));
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.SLIDERS_CLICKS,
                    mPostArrayList, this);
            mPostAsyncObj.execute();
        } else if (isAdded()) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callSliderAsync(String value) {
        try {
            if (sliderAsync != null) {
                sliderAsync.cancelAsync(false);
                sliderAsync.cancel(true);
            }
            sliderArray.clear();
            bannerFragment.clear();
//            bannerPagerAdapter = new PagerLoopAdapter(getChildFragmentManager(), bannerFragment);
//            view_pager_banner.setAdapter(bannerPagerAdapter);
            mHandler.sendEmptyMessage(1);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.slider_type, value);
            sliderAsync = new PostAsync(getActivity(),
                    this, ServerRequestConstants.SLIDERS_LIST,
                    mPostArrayList, this);
            txt_c_shop_now.setVisibility(View.GONE);
            mHandler.sendEmptyMessage(12);
            sliderAsync.execute();
        } else if (isAdded()) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            mHandler.sendEmptyMessage(11);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.SLIDERS_LIST)) {
            mHandler.sendEmptyMessage(13);
            parseSliderAsync(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.SLIDERS_CLICKS)) {
            mHandler.sendEmptyMessage(13);
            parseSlidersClick(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.CAMPUS_LIST)) {
            if (campusCounter > 1) {
                campusAsyncExcecuting = false;
            } else if (campusCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 0)
                swipyrefreshlayout.setRefreshing(false);
            swipyrefreshlayout.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
            parseCampusListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.DEALS_LIST)) {
            if (dealCounter > 1) {
                dealAsyncExcecuting = false;
            } else if (dealCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 1)
                swipyrefreshlayout.setRefreshing(false);
            parseDealListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.EVENTS_LIST)) {
            if (moviesCounter > 1) {
                moviesAsyncExcecuting = false;
            } else if (moviesCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 2)
                swipyrefreshlayout.setRefreshing(false);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.MOVIES_LIST)) {
            if (moviesCounter > 1) {
                moviesAsyncExcecuting = false;
            } else if (moviesCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 2)
                swipyrefreshlayout.setRefreshing(false);
            parseMoviesListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.PRODUCT_LIKES)) {
            mHandler.sendEmptyMessage(11);
            parseLikesResponse(result);
        }
    }

    private void parseLikesResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                productArray.get(selected_pos).product_like_status = productArray.get(selected_pos).product_like_status == 0 ? 1 : 0;
                SellItemActivity.updateProduct(productArray.get(selected_pos));
            }
            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseDealListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                dealArray.clear();
                if (jsonObject.has(Tags.response) && AppDelegate.isValidString(JSONParser.getString(jsonObject, Tags.response)) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);

                        DealModel dealModel = new DealModel();
                        dealModel.id = JSONParser.getString(object, Tags.id);
                        dealModel.user_id = JSONParser.getString(object, Tags.user_id);
                        dealModel.deal_area = JSONParser.getString(object, Tags.deal_area);
                        dealModel.deal_catid = JSONParser.getString(object, Tags.deal_catid);
                        dealModel.institute_id = JSONParser.getString(object, Tags.institute_id);
                        dealModel.title = JSONParser.getString(object, Tags.title);
                        dealModel.details = JSONParser.getString(object, Tags.details);
                        dealModel.venue = JSONParser.getString(object, Tags.venue);
                        dealModel.price = JSONParser.getString(object, Tags.price);
                        dealModel.discount = JSONParser.getString(object, Tags.discount);
                        dealModel.discount_price = JSONParser.getString(object, Tags.discount_price);
                        dealModel.emailid = JSONParser.getString(object, Tags.emailid);
                        dealModel.coupon_code = JSONParser.getString(object, Tags.coupon_code);
                        dealModel.coupon_no = object.optString(Tags.coupon_no);
                        dealModel.expiry_date = JSONParser.getString(object, Tags.expiry_date);

                        JSONObject dealObject = object.getJSONObject(Tags.deal_category);
                        dealModel.product_category = dealObject.getString(Tags.title);

                        dealModel.image_1 = JSONParser.getString(object, Tags.image_1);
                        dealModel.image_2 = JSONParser.getString(object, Tags.image_2);
                        dealModel.image_3 = JSONParser.getString(object, Tags.image_3);
                        dealModel.image_4 = JSONParser.getString(object, Tags.image_4);

                        dealModel.image_1_thumb = JSONParser.getString(object, Tags.image_1_thumb);
                        dealModel.image_2_thumb = JSONParser.getString(object, Tags.image_2_thumb);
                        dealModel.image_3_thumb = JSONParser.getString(object, Tags.image_3_thumb);
                        dealModel.image_4_thumb = JSONParser.getString(object, Tags.image_4_thumb);

                        dealModel.total_deal_views = JSONParser.getString(object, Tags.total_deal_views);
                        dealModel.logged_user_view_status = JSONParser.getString(object, Tags.logged_user_view_status);

                        dealModel.is_grabbed = JSONParser.getString(object, Tags.is_grabbed);

//                    dealModel.sold_status = JSONParser.getString(object, Tags.sold_status);
//                    dealModel.status = JSONParser.getString(object, Tags.status);
                        dealModel.created = JSONParser.getString(object, Tags.created);
//                    dealModel.modified = JSONParser.getString(object, Tags.modified);

                        dealArray.add(dealModel);
                    }
                } else {
                    dealTotalPage = 0;
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                dealTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            dealCounter++;

            mHandler.sendEmptyMessage(3);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(3);
                }
            }, 2000);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }


    private void parseMoviesListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                moviesArray.clear();
                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        MoviesModel moviesModel = new MoviesModel();
                        moviesModel.id = JSONParser.getString(object, Tags.id);
                        moviesModel.user_id = JSONParser.getString(object, Tags.user_id);
                        moviesModel.title = JSONParser.getString(object, Tags.title);
                        moviesModel.banner_image = JSONParser.getString(object, Tags.banner_image);
                        moviesModel.banner_image_thumb = JSONParser.getString(object, Tags.banner_image_thumb);
                        moviesModel.imdb_rating = JSONParser.getString(object, Tags.imdb_rating);
                        moviesModel.sub_title = JSONParser.getString(object, Tags.sub_title);

                        moviesModel.synopsis = JSONParser.getString(object, Tags.synopsis);
                        moviesModel.run_time = JSONParser.getString(object, Tags.run_time);
                        moviesModel.starring = JSONParser.getString(object, Tags.starring);
                        moviesModel.trailer_link = JSONParser.getString(object, Tags.trailer_link);
                        moviesModel.genre = JSONParser.getString(object, Tags.genre);
                        moviesModel.rating = JSONParser.getString(object, Tags.rating);

                        moviesModel.cinema_type_with_time = JSONParser.getString(object, Tags.cinema_type_with_time);
                        moviesModel.status = JSONParser.getString(object, Tags.status);
                        moviesModel.is_view = JSONParser.getString(object, Tags.is_view);

                        moviesModel.total_movie_views = JSONParser.getString(object, Tags.total_movie_views);
                        moviesModel.logged_user_view_status = JSONParser.getString(object, Tags.logged_user_view_status);

                        moviesModel.created = JSONParser.getString(object, Tags.created);

                        moviesModel.ticket_fee = JSONParser.getString(object, Tags.ticket_fee);
                        moviesModel.directed_by = JSONParser.getString(object, Tags.directed_by);

                        moviesArray.add(moviesModel);
                    }
                } else {
                    moviesTotalPage = 0;
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                moviesTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            moviesCounter++;

            mHandler.sendEmptyMessage(4);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(4);
                }
            }, 2000);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    private void parseCampusListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                productArray.clear();
                if (jsonObject.has(Tags.response)) {
                    if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                        JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            ProductModel productModel = new ProductModel();
                            productModel.id = JSONParser.getString(object, Tags.id);
                            productModel.cat_id = JSONParser.getString(object, Tags.cat_id);
                            productModel.title = JSONParser.getString(object, Tags.title);
                            productModel.description = JSONParser.getString(object, Tags.description);
                            productModel.price = JSONParser.getString(object, Tags.price);
                            productModel.item_condition = JSONParser.getString(object, Tags.item_condition);

                            productModel.image_1 = JSONParser.getString(object, Tags.image_1);
                            productModel.image_2 = JSONParser.getString(object, Tags.image_2);
                            productModel.image_3 = JSONParser.getString(object, Tags.image_3);
                            productModel.image_4 = JSONParser.getString(object, Tags.image_4);

                            productModel.image_1_thumb = JSONParser.getString(object, Tags.image_1_thumb);
                            productModel.image_2_thumb = JSONParser.getString(object, Tags.image_2_thumb);
                            productModel.image_3_thumb = JSONParser.getString(object, Tags.image_3_thumb);
                            productModel.image_4_thumb = JSONParser.getString(object, Tags.image_4_thumb);

                            productModel.total_product_likes = JSONParser.getInt(object, Tags.total_product_likes);
                            productModel.product_like_status = JSONParser.getInt(object, Tags.product_like_status);
                            productModel.total_comments = JSONParser.getInt(object, Tags.total_comments);

                            float floatValue = Float.parseFloat(JSONParser.getString(object, Tags.rating));
//                    AppDelegate.LogT("floatValue = " + floatValue);
                            productModel.rating = (int) floatValue + (floatValue % 1 > 0.50f ? 1 : 0);
//                    AppDelegate.LogT("productModel.rating = " + productModel.rating);

                            productModel.sold_status = JSONParser.getString(object, Tags.sold_status);
                            productModel.status = JSONParser.getString(object, Tags.status);
                            productModel.created = JSONParser.getString(object, Tags.created);
                            productModel.modified = JSONParser.getString(object, Tags.modified);
                            productModel.total_product_views = JSONParser.getString(object, Tags.total_product_views);
                            productModel.logged_user_view_status = JSONParser.getString(object, Tags.logged_user_view_status);

                            if (object.has(Tags.product_category) && AppDelegate.isValidString(object.optJSONObject(Tags.product_category) + "")) {
                                JSONObject productObject = object.getJSONObject(Tags.product_category);
                                productModel.pc_id = productObject.getString(Tags.id);
                                productModel.pc_title = productObject.getString(Tags.cat_name);
                                productModel.pc_status = productObject.getString(Tags.status);
                            }

                            productModel.latitude = JSONParser.getString(object, Tags.latitude);
                            productModel.longitude = JSONParser.getString(object, Tags.longitude);

                            if (AppDelegate.isValidString(JSONParser.getString(object, Tags.user))) {

                                JSONObject userObject = object.getJSONObject(Tags.user);
                                productModel.user_id = userObject.getString(Tags.id);
                                productModel.user_first_name = userObject.getString(Tags.first_name);
                                productModel.user_last_name = userObject.getString(Tags.last_name);
                                productModel.user_email = userObject.getString(Tags.email);
                                productModel.user_role = userObject.getString(Tags.role);
                                productModel.user_image = userObject.getString(Tags.image);
                                productModel.user_social_id = userObject.getString(Tags.social_id);
                                productModel.user_gcm_token = userObject.getString(Tags.gcm_token);

                                productModel.user_following_count = userObject.getInt(Tags.following_count);
                                productModel.user_followers_count = userObject.getInt(Tags.followers_count);
                                productModel.user_total_product = userObject.getInt(Tags.total_product);
                                productModel.user_follow_status = userObject.getInt(Tags.follow_status);

                                JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                                    productModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                                    productModel.user_institution_id = studentObject.getString(Tags.institution_id);
                                    JSONObject instituteObject = studentObject.getJSONObject(Tags.institute);
                                    if (instituteObject.has(Tags.institute_name) && AppDelegate.isValidString(instituteObject.optString(Tags.institute_name))) {
                                        productModel.user_institute_name = instituteObject.getString(Tags.institute_name);
                                    }
                                    if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                                        productModel.user_department_name = studentObject.getString(Tags.department_name);
                                    } else {
                                        productModel.user_department_name = studentObject.getString(Tags.department_name);
                                    }
                                } else {
                                    productModel.user_institute_name = studentObject.getString(Tags.other_ins_name);
                                    productModel.user_department_name = studentObject.getString(Tags.department_name);
                                }

                                productArray.add(productModel);
                            } else {
                                AppDelegate.LogE("User not find of productModel.id => " + productModel.id);
                            }
                        }
                    } else {
                        campusTotalPage = 0;
                        AppDelegate.showToast(getActivity(), "No record found.");
                    }
                } else {
                    campusTotalPage = 0;
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                campusTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            campusCounter++;
            mHandler.sendEmptyMessage(2);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(2);
                }
            }, 2000);
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    private void parseSlidersClick(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
//                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                if (click_type == 1) {
                    sliderArray.get(item_position).thump_clicked = 1;
                } else {
                    sliderArray.get(item_position).single_clicked = 1;
                }
            } else {
                if (click_type == 1) {
                    sliderArray.get(item_position).thump_clicked = 1;
                } else {
                    sliderArray.get(item_position).single_clicked = 1;
                }
            }
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    private void parseSliderAsync(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            sliderArray.clear();
            AppDelegate.LogCh("checkUserStatus called => " + jsonObject.toString());
            if (MainActivity.checkUserStatus(getActivity(), jsonObject)) {
                return;
            }
//            ((MainActivity) getActivity()).updateNotificationCount(JSONParser.getString(jsonObject, Tags.total_notifications));
            mHandler.sendEmptyMessage(20);
            if (AppDelegate.isValidString(JSONParser.getString(jsonObject, Tags.user_online_time_id))) {
                dataModel.user_online_time_id = JSONParser.getString(jsonObject, Tags.user_online_time_id);
                prefs.setUserData(dataModel);
            }
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
//                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    SliderModel sliderModel = new SliderModel();
                    sliderModel.id = JSONParser.getString(object, Tags.id);
                    sliderModel.banner_image = JSONParser.getString(object, Tags.img);
//                    sliderModel.banner_thumb_image = JSONParser.getString(object, Tags.thumb);
                    sliderModel.banner_thumb_image = sliderModel.banner_image;
                    sliderModel.expiry_date = JSONParser.getString(object, Tags.expiry_date);
                    sliderModel.slider_category = JSONParser.getString(object, Tags.slider_category);
                    sliderModel.status = JSONParser.getString(object, Tags.status);
                    sliderModel.created = JSONParser.getString(object, Tags.created);
                    sliderModel.title = JSONParser.getString(object, Tags.title);
                    sliderModel.descriptions = JSONParser.getString(object, Tags.description);
                    sliderModel.url = JSONParser.getString(object, Tags.url);
                    sliderModel.banner_type = object.getInt(Tags.banner_type);
                    sliderModel.type_of_slider = selected_tab;
                    sliderModel.from_page = 0;

                    if (object.has(Tags.click_status) && object.optJSONObject(Tags.click_status) != null) {
                        sliderModel.single_clicked = Integer.parseInt(object.getJSONObject(Tags.click_status).getString(Tags.click_status));
                        sliderModel.thump_clicked = Integer.parseInt(object.getJSONObject(Tags.click_status).getString(Tags.thumb_status));
                    } else {
                        sliderModel.single_clicked = 0;
                        sliderModel.thump_clicked = 0;
                    }
                    sliderArray.add(sliderModel);
                }
            }
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(1);
    }

    @Override
    public void onResume() {
        super.onResume();
        switchPage(selected_tab);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) getActivity()).toggleSlider();
                    }
                }, 300);
                break;

            case R.id.img_c_right:
                if (isAdded())
                    AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new RefineSearchFragment());
                break;

            case R.id.ll_c_campus_list:
                selected_tab = 0;
                switchPage(0);
                break;

            case R.id.ll_c_top_deals:
                selected_tab = 1;
                switchPage(1);
                break;

            case R.id.ll_c_events:
                selected_tab = 2;
                switchPage(2);
                break;

            case R.id.txt_c_shop_now:
                break;

            case R.id.view_background_banner:
                if (sliderArray.size() > 0) {
                    this.item_position = view_pager_banner.getCurrentItem();
                    if (sliderArray.get(item_position).single_clicked == 0) {
                        callClicksAsync(sliderArray.get(item_position), 0);
                    }
                    showBannerDialog(sliderArray.get(item_position));
                }
                break;
        }
    }

    private void updateAdapter(int i) {
        if (!isAdded()) {
            return;
        }
        swipyrefreshlayout.setRefreshing(false);
        ll_c_campus_list.setSelected(false);
        ll_c_top_deals.setSelected(false);
        ll_c_events.setSelected(false);
        AppDelegate.LogT("updateAdapter => " + i);
        callDealsAdapterBeforeGridAdapter();
        switch (i) {
            case 0:
                ll_c_campus_list.setSelected(true);
                rcAdapter = new ProductRecyclerViewAdapter(getActivity(), productArray, this);
                recyclerView.setBackgroundColor(getResources().getColor(R.color.grey_font));
                recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
                recyclerView.setLayoutManager(gaggeredGridLayoutManager);
                recyclerView.addItemDecoration(campusItemDecoration);
                recyclerView.setAdapter(rcAdapter);
                mHandler.sendEmptyMessage(2);
                break;

            case 1:
                ll_c_top_deals.setSelected(true);
                dealAdapter = new DealRecyclerViewAdapter(getActivity(), dealArray, this);
                recyclerView.setBackgroundColor(Color.WHITE);
                recyclerView.setPadding(0, 0, 0, 0);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.addItemDecoration(dealItemDecoration);
                recyclerView.setAdapter(dealAdapter);
                mHandler.sendEmptyMessage(3);
                break;

            case 2:
                ll_c_events.setSelected(true);
                moviesAdapter = new MoviesRecyclerViewAdapter(getActivity(), moviesArray, this);
                recyclerView.setBackgroundColor(getResources().getColor(R.color.grey_font));
                recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
                recyclerView.setLayoutManager(gaggeredGridLayoutManager);
                recyclerView.addItemDecoration(moviesItemDecoration);
                recyclerView.setAdapter(moviesAdapter);
                mHandler.sendEmptyMessage(4);
                break;
        }
    }

    private void switchPage(int i) {
        swipyrefreshlayout.setRefreshing(false);
        ll_c_campus_list.setSelected(false);
        ll_c_top_deals.setSelected(false);
        ll_c_events.setSelected(false);
        callDealsAdapterBeforeGridAdapter();
        switch (i) {
            case 0:
                AppDelegate.LogT("switchPage => 0");
                ll_c_campus_list.setSelected(true);
                rcAdapter = new ProductRecyclerViewAdapter(getActivity(), productArray, this);
                recyclerView.setBackgroundColor(getResources().getColor(R.color.grey_font));
                recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
                recyclerView.setLayoutManager(gaggeredGridLayoutManager);
                recyclerView.addItemDecoration(campusItemDecoration);
                recyclerView.setAdapter(rcAdapter);
//                if (productArray.size() == 0) {
                callCampusListAsync();
//                } else {
//                    mHandler.sendEmptyMessage(2);
//                }
                callSliderAsync("5");
                break;

            case 1:
                ll_c_top_deals.setSelected(true);
                dealAdapter = new DealRecyclerViewAdapter(getActivity(), dealArray, this);
                recyclerView.setBackgroundColor(Color.WHITE);
                recyclerView.setPadding(0, 0, 0, 0);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.addItemDecoration(dealItemDecoration);
                recyclerView.setAdapter(dealAdapter);
//                if (dealArray.size() == 0) {
                callDealsListAsync();
//                } else {
//                    mHandler.sendEmptyMessage(3);
//                }
                callSliderAsync("3");
                break;

            case 2:
                ll_c_events.setSelected(true);
                moviesAdapter = new MoviesRecyclerViewAdapter(getActivity(), moviesArray, this);
                recyclerView.setBackgroundColor(getResources().getColor(R.color.grey_font));
                recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
                recyclerView.setLayoutManager(gaggeredGridLayoutManager);
                recyclerView.addItemDecoration(moviesItemDecoration);
                recyclerView.setAdapter(moviesAdapter);
//                if (moviesArray.size() == 0) {
                callEventListAsync();
//                } else {
//                    mHandler.sendEmptyMessage(4);
//                }
                callSliderAsync("4");
                break;
        }
    }

    private void callDealsAdapterBeforeGridAdapter() {
        // Adapter was not notifying for that reason i used to this function
        recyclerView.removeItemDecoration(campusItemDecoration);
        recyclerView.removeItemDecoration(dealItemDecoration);
        recyclerView.removeItemDecoration(moviesItemDecoration);

        dealAdapter = new DealRecyclerViewAdapter(getActivity(), dealArray, HomeFragment.this);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setPadding(0, 0, 0, 0);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(dealItemDecoration);
        recyclerView.setAdapter(dealAdapter);

        recyclerView.removeItemDecoration(campusItemDecoration);
        recyclerView.removeItemDecoration(dealItemDecoration);
        recyclerView.removeItemDecoration(moviesItemDecoration);
    }

    private void showBannerDialog(final SliderModel sliderModel) {
        Dialog bannerDialog = new Dialog(getActivity(), android.R.style.Theme_Light);
        bannerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bannerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        bannerDialog.setContentView(R.layout.dialog_deal_coupen);
        ImageView img_c_coupon = (ImageView) bannerDialog.findViewById(R.id.img_c_coupon);
        imageLoader.displayImage(sliderModel.banner_image, img_c_coupon);
//        Picasso.with(getActivity()).load(sliderModel.banner_image).into(img_c_coupon);
        TextView txt_c_coupon = (TextView) bannerDialog.findViewById(R.id.txt_c_coupon);
        TextView txt_coupon_time = (TextView) bannerDialog.findViewById(R.id.txt_coupon_time);
        TextView txt_c_save = (TextView) bannerDialog.findViewById(R.id.txt_c_save);
        txt_c_save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sliderModel.single_clicked == 0) {
                    callClicksAsync(sliderModel, 1);
                }
            }
        });
        bannerDialog.show();
    }

    int selected_pos = 0;

    @Override
    public void setOnListItemClickListener(String name, int position) {
        selected_pos = position;
        if (name.equalsIgnoreCase(Tags.product)) {

            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.product, productArray.get(position));
            bundle.putInt(Tags.FROM, AppDelegate.PRODUCT_DASHBOARD);
            Fragment fragment = new ProductDetailFragment();
            fragment.setArguments(bundle);
            if (isAdded())
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);

        } else if (name.equalsIgnoreCase(Tags.LIKES)) {
            callProductLikesAsync(productArray.get(position).id, productArray.get(position).product_like_status == 0 ? "1" : "0");
        } else if (name.equalsIgnoreCase(Tags.deal)) {
//            Bundle bundle = new Bundle();
//            bundle.putParcelable(Tags.deal, dealArray.get(position));
//            Fragment fragment = new DealDetailsActivity();
//            fragment.setArguments(bundle);
//            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        } else if (name.equalsIgnoreCase(Tags.movies)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.movies, moviesArray.get(position));
            Intent intent = new Intent(getActivity(), MoviesDetailsActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }
}
