package com.stux.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.GCMClientManager;
import com.stux.Models.InstitutionModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.Utils.TransitionHelper;
import com.stux.activities.MainActivity;
import com.stux.activities.WebViewDetailActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.Fb_detail_GetSet;
import com.stux.parser.Fb_details;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import carbon.widget.TextView;

/**
 * Created by NOTO on 5/24/2016.
 */
public class SignInFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {

    private EditText et_email, et_password;

    public static CallbackManager callbackManager;
    private String fb_LoginToken;
    private boolean isCalledOnce = false;
    private Handler mHandler;
    private Prefs prefs;
    private ImageView img_password_view;
    private TextView txt_c_tnc;

    private Fb_detail_GetSet fbUserData;

    private CheckBox cb_tnc;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sign_in, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        prefs = new Prefs(getActivity());
        initView(view);
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                }
            }
        };
    }

    private void initView(View view) {
        et_email = (EditText) view.findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_password = (EditText) view.findViewById(R.id.et_password);
        et_password.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));

        img_password_view = (ImageView) view.findViewById(R.id.img_password_view);
        img_password_view.setOnClickListener(this);

        view.findViewById(R.id.txt_c_forgot_password).setOnClickListener(this);
        view.findViewById(R.id.txt_c_sign_up).setOnClickListener(this);
        view.findViewById(R.id.ll_c_sign_in_fb).setOnClickListener(this);
        view.findViewById(R.id.txt_c_sign_in).setOnClickListener(this);

        txt_c_tnc = (TextView) view.findViewById(R.id.txt_c_tnc);
        txt_c_tnc.setOnClickListener(this);
        cb_tnc = (CheckBox) view.findViewById(R.id.cb_tnc);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_sign_in:
                callSignInAsync();
                break;

            case R.id.ll_c_sign_in_fb:
                if (cb_tnc.isChecked()) {
                    if (AppDelegate.haveNetworkConnection(getActivity(), false))
                        openFacebook();
                    else
                        AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
                } else {
                    AppDelegate.showAlert(getActivity(), getString(R.string.alert_terms_conditions));
                }
                break;
            case R.id.txt_c_sign_up:
                new Prefs(getActivity()).clearTempPrefs();
                if (AppDelegate.haveNetworkConnection(getActivity(), false))
                    AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new SignUpInstitutionFragment());
                else
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
                break;

            case R.id.txt_c_forgot_password:
                AppDelegate.loadFragment(getActivity().getSupportFragmentManager(), new ForgotPasswordFragment());
                break;

            case R.id.img_password_view:
                img_password_view.setSelected(!img_password_view.isSelected());
                if (img_password_view.isSelected()) {
                    et_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    et_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                et_password.setSelection(et_password.length());
                break;

            case R.id.txt_c_tnc:
                Bundle bundle = new Bundle();
                bundle.putInt(Tags.FROM, 0);
                Intent intent = new Intent(getActivity(), WebViewDetailActivity.class);
                intent.putExtras(bundle);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false, new Pair<>(txt_c_tnc, "web_view"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;
        }
    }

    private void callSignInAsync() {
        if (et_email.length() == 0) {
            AppDelegate.showAlert(getActivity(), getString(R.string.Alert_Enter_email));
        } else if (!(AppDelegate.CheckEmail(et_email.getText().toString())) /*|| !et_email.getText().toString().substring(et_email.getText().toString().indexOf("@"), et_email.getText().toString().length()).equals("@edu.com")*/) {
            AppDelegate.showAlert(getActivity(), getString(R.string.please_fill_email_address_in_format));
        } else if (et_password.length() == 0) {
            AppDelegate.showAlert(getActivity(), getString(R.string.Please_Enter_password));
        } else if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.login_type, "0");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.email, et_email.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.rec_password, et_password.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gcm_token, GCMClientManager.getRegistrationId(getActivity()));
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, Tags.LOGIN, ServerRequestConstants.LOGIN,
                    mPostArrayList, this);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        callbackManager = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(getActivity());
    }

    public void openFacebook() {
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("login success" + loginResult.getAccessToken());
                        AppDelegate.LogT("onSuccess = " + loginResult.getAccessToken());
                        AppDelegate.showProgressDialog(getActivity());
                        fb_LoginToken = loginResult.getAccessToken().toString();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {

                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        // Application code
                                        AppDelegate.LogFB("Graph response" + response.toString());
                                        AppDelegate.LogFB("Graph obj" + object.toString());
                                        fbUserData = new Fb_detail_GetSet();
                                        fbUserData = new Fb_details().getFacebookDetail(response.getJSONObject().toString());

                                        AppDelegate.hideProgressDialog(getActivity());
                                        callAsyncFacebookVerify(fbUserData);
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "first_name,last_name,email,id,name,gender,birthday,picture");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook();
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(getActivity());
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("Login onActivityResult called");
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void callAsyncFacebookVerify(Fb_detail_GetSet fbUserData) {
        if (fbUserData == null) {
            AppDelegate.showAlert(getActivity(), "Facebook data is incorrect, please try after some time.");
        } else if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.social_id, fbUserData.getId());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.login_type, "1");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gcm_token, GCMClientManager.getRegistrationId(getActivity()));
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.LOGIN,
                    mPostArrayList, this);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.LOGIN)) {
            parseLoginFacebookResult(result);
        } else if (apiName.equalsIgnoreCase(Tags.LOGIN)) {
            parseSignInResult(result);
        }
    }

    private void parseSignInResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);

                UserDataModel userDataModel = new UserDataModel();
                userDataModel.role = object.getString(Tags.role);
                userDataModel.user_online_time_id = object.getString(Tags.user_online_time_id);

                userDataModel.status = object.getString(Tags.status);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.role = object.getString(Tags.role);
                userDataModel.image = object.getString(Tags.image);
                if (!AppDelegate.isValidString(userDataModel.image) && prefs.getTempFacebookData() != null && AppDelegate.isValidString(prefs.getTempFacebookData().image)) {
                    userDataModel.image = prefs.getTempFacebookData().image;
                }
                userDataModel.created = object.getString(Tags.created);
                userDataModel.gcm_token = object.getString(Tags.gcm_token);
                userDataModel.facebook_id = object.getString(Tags.social_id);
                userDataModel.login_type = object.getString(Tags.login_type);
                userDataModel.is_verified = object.getString(Tags.is_verified);

                userDataModel.following_count = object.getInt(Tags.following_count);
                userDataModel.followers_count = object.getInt(Tags.followers_count);
                userDataModel.total_product = object.getInt(Tags.total_product);

                JSONObject studentObject = object.getJSONObject(Tags.student_detail);

                userDataModel.str_Gender = studentObject.getString(Tags.gender);
                userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
                userDataModel.userId = studentObject.getString(Tags.user_id);
                userDataModel.dob = studentObject.getString(Tags.dob);
                userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

                InstitutionModel institutionModel = new InstitutionModel();
                institutionModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                AppDelegate.LogT("institute id => " + studentObject.getString(Tags.institution_id));
                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                    institutionModel.institution_name_id = studentObject.getString(Tags.institution_id);
                    institutionModel.institution_name = studentObject.getString(Tags.institution_name);
                } else {
                    institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
                }
                institutionModel.department_name = studentObject.getString(Tags.department_name);

                prefs.setUserData(userDataModel);
                prefs.setInstitutionModel(institutionModel);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }

    private void parseLoginFacebookResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.role = object.getString(Tags.role);
                userDataModel.user_online_time_id = object.getString(Tags.user_online_time_id);
                userDataModel.status = object.getString(Tags.status);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.role = object.getString(Tags.role);
                if (object.has(Tags.image) && AppDelegate.isValidString(object.optString(Tags.image)))
                    userDataModel.image = object.getString(Tags.image);
                if (!AppDelegate.isValidString(userDataModel.image) && prefs.getTempFacebookData() != null && AppDelegate.isValidString(prefs.getTempFacebookData().image)) {
                    userDataModel.image = prefs.getTempFacebookData().image;
                }
                userDataModel.created = object.getString(Tags.created);
                userDataModel.gcm_token = object.getString(Tags.gcm_token);
                userDataModel.facebook_id = object.getString(Tags.social_id);
                userDataModel.login_type = object.getString(Tags.login_type);
                userDataModel.is_verified = object.getString(Tags.is_verified);

                userDataModel.following_count = object.getInt(Tags.following_count);
                userDataModel.followers_count = object.getInt(Tags.followers_count);
                userDataModel.total_product = object.getInt(Tags.total_product);

                JSONObject studentObject = object.getJSONObject(Tags.student_detail);

                userDataModel.str_Gender = studentObject.getString(Tags.gender);
                userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
                userDataModel.userId = studentObject.getString(Tags.user_id);
                userDataModel.dob = studentObject.getString(Tags.dob);
                userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

                InstitutionModel institutionModel = new InstitutionModel();
                institutionModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                AppDelegate.LogT("institute id => " + studentObject.getString(Tags.institution_id));
                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                    institutionModel.institution_name_id = studentObject.getString(Tags.institution_id);
                    institutionModel.institution_name = studentObject.getString(Tags.institution_name);
                } else {
                    institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
                }
                institutionModel.department_name = studentObject.getString(Tags.department_name);

                prefs.setUserData(userDataModel);
                prefs.setInstitutionModel(institutionModel);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            } else {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null && jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null) {
                    AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                } else {
                    prefs.setTempFacebookData(fbUserData);
                    AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new SignUpInstitutionFragment());
                }
            }
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            AppDelegate.LogE(e);
        }
    }

}
