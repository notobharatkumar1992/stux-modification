package com.stux.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.Utils.SpacesItemDecoration;
import com.stux.activities.MainActivity;
import com.stux.activities.SellItemActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnListItemClickListenerWithHeight;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.ProductRecyclerViewAdapter;

/**
 * Created by Bharat on 07/08/2016.
 */
public class SearchProductFragment extends Fragment implements OnReciveServerResponse, OnListItemClickListener, OnListItemClickListenerWithHeight {

    public static final int FROM_REFINE_SEARCH = 0, FROM_CHOOSE_CATEGORY = 1;
    public static Handler mHandler;
    public ArrayList<ProductModel> filderedArray = new ArrayList<>();
    private Prefs prefs;
    private UserDataModel userData;
    private ProgressBar progressbar;

    // Campus list
    private TextView txt_c_no_list;

    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private RecyclerView recyclerView;
    private ProductRecyclerViewAdapter rcAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sold_list_view, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        if (HomeFragment.productArray.size() == 0) {
            execute_searchProduct();
        } else {
            mHandler.sendEmptyMessage(3);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        filderedArray.clear();
        mHandler = null;
    }

    private void execute_searchProduct() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, userData.userId + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.institution_id, prefs.getInstitutionModel().institution_name_id);

            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    SearchProductFragment.this, ServerRequestConstants.CAMPUS_LIST,
                    mPostArrayList, SearchProductFragment.this);
            if (!campusAsyncExcecuting)
                mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    AppDelegate.LogT("gridViewCampusList notified ");
                    txt_c_no_list.setVisibility(filderedArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No product available");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            rcAdapter.notifyDataSetChanged();
                            recyclerView.invalidate();
                        }
                    }, 1000);
                } else if (msg.what == 3) {
                    String searchText = ((MainActivity) getActivity()).et_search.getText().toString();
                    searchText = searchText.toLowerCase();
                    filderedArray.clear();

                    if (searchText.length() == 0) {
                        filderedArray.addAll(HomeFragment.productArray);
                    } else {
                        AppDelegate.LogT("productArray => " + HomeFragment.productArray.size());
                        for (ProductModel productModel : HomeFragment.productArray) {
                            if (productModel.title.toLowerCase().contains(searchText)) {
                                filderedArray.add(productModel);
                            }
                        }
                    }
                    mHandler.sendEmptyMessage(2);
                }
            }
        };
    }

    private boolean campusAsyncExcecuting = false;
    private int campusCounter = 1, campusTotalPage = -1;

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Search Product");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlider();
            }
        });
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });

        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });


        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
        recyclerView.setHasFixedSize(true);

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);
        recyclerView.addItemDecoration(new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 5)));
        rcAdapter = new ProductRecyclerViewAdapter(getActivity(), filderedArray, this);
        recyclerView.setAdapter(rcAdapter);

        final SwipyRefreshLayout swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (campusTotalPage != 0 && !campusAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 campusAsyncExcecuting = true;
                                 execute_searchProduct();
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + campusTotalPage + ", " + campusAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (apiName.equalsIgnoreCase(ServerRequestConstants.PRODUCT_LIKES)) {
            mHandler.sendEmptyMessage(11);
            parseLikesResponse(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.CAMPUS_LIST)) {
            mHandler.sendEmptyMessage(11);
            if (campusCounter > 1) {
                campusAsyncExcecuting = false;
            }
            parseCampusListResult(result);
        }
    }

    private void parseLikesResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                filderedArray.get(selected_pos).product_like_status = filderedArray.get(selected_pos).product_like_status == 0 ? 1 : 0;
                SellItemActivity.updateProduct(filderedArray.get(selected_pos));
            }
            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseCampusListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                HomeFragment.productArray.clear();
                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        ProductModel productModel = new ProductModel();
                        productModel.id = object.getString(Tags.id);
                        productModel.cat_id = object.getString(Tags.cat_id);
                        productModel.title = object.getString(Tags.title);
                        productModel.description = object.getString(Tags.description);
                        productModel.price = object.getString(Tags.price);
                        productModel.item_condition = object.getString(Tags.item_condition);

                        productModel.image_1 = JSONParser.getString(object, Tags.image_1);
                        productModel.image_2 = JSONParser.getString(object, Tags.image_2);
                        productModel.image_3 = JSONParser.getString(object, Tags.image_3);
                        productModel.image_4 = JSONParser.getString(object, Tags.image_4);

                        productModel.image_1_thumb = JSONParser.getString(object, Tags.image_1_thumb);
                        productModel.image_2_thumb = JSONParser.getString(object, Tags.image_2_thumb);
                        productModel.image_3_thumb = JSONParser.getString(object, Tags.image_3_thumb);
                        productModel.image_4_thumb = JSONParser.getString(object, Tags.image_4_thumb);

                        productModel.total_product_likes = JSONParser.getInt(object, Tags.total_product_likes);
                        productModel.product_like_status = JSONParser.getInt(object, Tags.product_like_status);
                        productModel.total_comments = JSONParser.getInt(object, Tags.total_comments);

                        float floatValue = Float.parseFloat(object.getString(Tags.rating));
                        AppDelegate.LogT("floatValue = " + floatValue);
                        productModel.rating = (int) floatValue + (floatValue % 1 > 0.50f ? 1 : 0);
                        AppDelegate.LogT("productModel.rating = " + productModel.rating);

                        productModel.sold_status = object.getString(Tags.sold_status);
                        productModel.status = object.getString(Tags.status);
                        productModel.created = object.getString(Tags.created);
                        productModel.modified = object.getString(Tags.modified);
                        productModel.total_product_views = object.getString(Tags.total_product_views);
                        productModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);

                        if (object.has(Tags.product_category) && AppDelegate.isValidString(object.optJSONObject(Tags.product_category) + "")) {
                            JSONObject productObject = object.getJSONObject(Tags.product_category);
                            productModel.pc_id = productObject.getString(Tags.id);
                            productModel.pc_title = productObject.getString(Tags.cat_name);
                            productModel.pc_status = productObject.getString(Tags.status);
                        } else {
                            productModel.pc_id = productModel.cat_id;
//                        productModel.pc_title
                        }

                        productModel.latitude = object.getString(Tags.latitude);
                        productModel.longitude = object.getString(Tags.longitude);

                        JSONObject userObject = object.getJSONObject(Tags.user);
                        productModel.user_id = userObject.getString(Tags.id);
                        productModel.user_first_name = userObject.getString(Tags.first_name);
                        productModel.user_last_name = userObject.getString(Tags.last_name);
                        productModel.user_email = userObject.getString(Tags.email);
                        productModel.user_role = userObject.getString(Tags.role);
                        productModel.user_image = userObject.getString(Tags.image);
                        productModel.user_social_id = userObject.getString(Tags.social_id);
                        productModel.user_gcm_token = userObject.getString(Tags.gcm_token);

                        JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                        if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                            productModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                            productModel.user_institution_id = studentObject.getString(Tags.institution_id);
                            JSONObject instituteObject = studentObject.getJSONObject(Tags.institute);
                            if (instituteObject.has(Tags.institute_name) && AppDelegate.isValidString(instituteObject.optString(Tags.institute_name))) {
                                productModel.user_institute_name = instituteObject.getString(Tags.institute_name);
                            }
                            if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                                productModel.user_department_name = studentObject.getString(Tags.department_name);
                            } else {
                                productModel.user_department_name = studentObject.getString(Tags.department_name);
                            }
                        } else {
                            productModel.user_institute_name = studentObject.getString(Tags.other_ins_name);
                            productModel.user_department_name = studentObject.getString(Tags.department_name);
                        }
                        HomeFragment.productArray.add(productModel);
                    }
                    campusCounter++;
                } else {
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            mHandler.sendEmptyMessage(3);
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    int selected_pos = 0;

    @Override
    public void setOnListItemClickListener(String name, int position) {
        selected_pos = position;
        if (name.equalsIgnoreCase(Tags.product)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.product, filderedArray.get(position));
            bundle.putInt(Tags.FROM, AppDelegate.PRODUCT_MY_LISTING);
            Fragment fragment = new ProductDetailFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        } else if (name.equalsIgnoreCase(Tags.LIKES)) {
            callProductLikesAsync(filderedArray.get(position).id, filderedArray.get(position).product_like_status == 0 ? "1" : "0");
        }
    }

    private void callProductLikesAsync(String product_id, String status) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, product_id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, status);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.PRODUCT_LIKES,
                    mPostArrayList, this);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int height) {

    }
}
