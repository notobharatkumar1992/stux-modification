package com.stux.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.stux.Adapters.SpinnerArrayStringAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.InstitutionModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.R;
import com.stux.Utils.FlushedInputStream;
import com.stux.Utils.Prefs;
import com.stux.Utils.TransitionHelper;
import com.stux.activities.WebViewDetailActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.Fb_detail_GetSet;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by NOTO on 5/24/2016.
 */
public class SignUpPersonalFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {

    private TextView txt_c_header, txt_c_tnc;
    private ImageView img_c_left, img_c_password_view, img_c_confirm_password_view;
    private EditText et_first_name, et_last_name, et_email, et_phone, et_password, et_confirm_password;
    private Spinner spn_gender;
    private SpinnerArrayStringAdapter adapterGender;
    public ArrayList<String> arrayStringGender = new ArrayList<>();
    private int selected_gender = -1;

    public InstitutionModel institutionModel;
    private Fb_detail_GetSet fbUserData;
    private CheckBox cb_tnc;

    public Prefs prefs;
    public Handler mHandler;
    public AppDelegate mInstance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sign_up_personal, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mInstance = AppDelegate.getInstance(getActivity());
        prefs = new Prefs(getActivity());
        initView(view);
        setHandler();
        getValues();
    }

    private void getValues() {
        if (getArguments() != null) {
            institutionModel = getArguments().getParcelable(Tags.TEMP_INSTITUTION_MODEL);
        }
        if (prefs.getTempFacebookData() != null && AppDelegate.isValidString(prefs.getTempFacebookData().getId())) {
            fbUserData = prefs.getTempFacebookData();
            et_first_name.setText(fbUserData.getF_name());
            et_last_name.setText(fbUserData.getL_name());
            et_email.setText(fbUserData.getEmail().contains("edu.com") ? fbUserData.getEmail() : "");
            et_phone.setText(fbUserData.phone);
            spn_gender.setSelection(fbUserData.getGender().equalsIgnoreCase(Tags.MALE) ? 1 : 2);
//            if (AppDelegate.isValidString(fbUserData.image)) {
//                AppDelegate.setStictModePermission();
//                Picasso.with(getActivity()).load(fbUserData.image).into(new Target() {
//                    @Override
//                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
//                        AppDelegate.LogT("onBitmapLoaded ");
//                        if (bitmap != null) {
//                            OriginalPhoto = bitmap;
//                        }
//                    }
//
//                    @Override
//                    public void onPrepareLoad(Drawable placeHolderDrawable) {
//                        AppDelegate.LogT("onPrepareLoad ");
//                    }
//
//                    @Override
//                    public void onBitmapFailed(Drawable errorDrawable) {
//                        AppDelegate.LogT("onBitmapFailed ");
//                    }
//                });
//            }
        }
    }

    private static Bitmap downloadBitmap(String stringUrl) {
        URL url = null;
        HttpURLConnection connection = null;
        InputStream inputStream = null;

        try {
            url = new URL(stringUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(true);
            inputStream = connection.getInputStream();
            return BitmapFactory.decodeStream(new FlushedInputStream(inputStream));
        } catch (Exception e) {
            Log.w("test", "Error while retrieving bitmap from " + stringUrl, e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return null;
    }


    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {

                }
            }
        };
    }

    private void initView(View view) {
        txt_c_header = (TextView) view.findViewById(R.id.txt_c_header);
        txt_c_header.setText(getString(R.string.Sign_Up));
        img_c_left = (ImageView) view.findViewById(R.id.img_c_left);
        img_c_left.setImageDrawable(getResources().getDrawable(R.drawable.back));
        img_c_left.setOnClickListener(this);

        img_c_password_view = (ImageView) view.findViewById(R.id.img_c_password_view);
        img_c_password_view.setOnClickListener(this);
        img_c_confirm_password_view = (ImageView) view.findViewById(R.id.img_c_confirm_password_view);
        img_c_confirm_password_view.setOnClickListener(this);

        et_first_name = (EditText) view.findViewById(R.id.et_first_name);
        et_first_name.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_last_name = (EditText) view.findViewById(R.id.et_last_name);
        et_last_name.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_email = (EditText) view.findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_phone = (EditText) view.findViewById(R.id.et_phone);
        et_phone.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_password = (EditText) view.findViewById(R.id.et_password);
        et_password.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_confirm_password = (EditText) view.findViewById(R.id.et_confirm_password);
        et_confirm_password.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));

        spn_gender = (Spinner) view.findViewById(R.id.spn_gender);
        arrayStringGender.add("Select Gender");
        arrayStringGender.add("Male");
        arrayStringGender.add("Female");
        adapterGender = new SpinnerArrayStringAdapter(getActivity(), arrayStringGender);
        spn_gender.setAdapter(adapterGender);
        spn_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_gender = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        view.findViewById(R.id.txt_c_register).setOnClickListener(this);
        txt_c_tnc = (TextView) view.findViewById(R.id.txt_c_tnc);
        txt_c_tnc.setOnClickListener(this);
        cb_tnc = (CheckBox) view.findViewById(R.id.cb_tnc);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(getActivity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                getFragmentManager().popBackStack();
                break;
            case R.id.txt_c_register:
//                if (cb_tnc.isChecked()) {
                execute_signUpAsync();
//                } else {
//                    AppDelegate.showAlert(getActivity(), getString(R.string.alert_terms_conditions));
//                }
//                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new SignInFragment());
                break;

            case R.id.img_c_password_view:
                img_c_password_view.setSelected(!img_c_password_view.isSelected());
                if (img_c_password_view.isSelected()) {
                    et_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    et_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                et_password.setSelection(et_password.length());
                break;

            case R.id.img_c_confirm_password_view:
                img_c_confirm_password_view.setSelected(!img_c_confirm_password_view.isSelected());
                if (img_c_confirm_password_view.isSelected()) {
                    et_confirm_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    et_confirm_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                et_confirm_password.setSelection(et_confirm_password.length());
                break;

            case R.id.txt_c_tnc:
                Bundle bundle = new Bundle();
                bundle.putInt(Tags.FROM, 0);
                Intent intent = new Intent(getActivity(), WebViewDetailActivity.class);
                intent.putExtras(bundle);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false, new Pair<>(txt_c_tnc, "web_view"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;
        }
    }

    private void execute_signUpAsync() {
        if (et_first_name.length() == 0) {
            AppDelegate.showAlert(getActivity(), getActivity().getString(R.string.Alert_First_Name));
        } else if (et_last_name.length() == 0) {
            AppDelegate.showAlert(getActivity(), getActivity().getString(R.string.Alert_Last_name));
        } else if (selected_gender <= 0) {
            AppDelegate.showAlert(getActivity(), getActivity().getString(R.string.Alert_Gender));
        } else if (et_email.length() == 0) {
            AppDelegate.showAlert(getActivity(), getActivity().getString(R.string.Alert_Enter_email));
        } else if (!(AppDelegate.CheckEmail(et_email.getText().toString())) /*|| !et_email.getText().toString().substring(et_email.getText().toString().indexOf("@"), et_email.getText().toString().length()).equals("@edu.com")*/) {
            AppDelegate.showAlert(getActivity(), getString(R.string.please_fill_email_address_in_format));
        } else if (et_phone.length() == 0) {
            AppDelegate.showAlert(getActivity(), getString(R.string.Please_enter_phone_numer));
        } else if (et_phone.length() != 14) {
            AppDelegate.showAlert(getActivity(), getString(R.string.Please_enter_phone_number_length));
        } else if (et_password.length() == 0) {
            AppDelegate.showAlert(getActivity(), getString(R.string.Please_Enter_password));
        } else if (!AppDelegate.isLegalPassword(et_password.getText().toString())) {
            AppDelegate.showToast(getActivity(), getResources().getString(R.string.forlength));
        } else if (et_confirm_password.length() == 0) {
            AppDelegate.showAlert(getActivity(), getString(R.string.Please_Enter_confirm_password));
        } else if (!et_password.getText().toString().equals(et_confirm_password.getText().toString())) {
            AppDelegate.showAlert(getActivity(), mInstance.getString(R.string.Password_Confirm_password_must_Be_same));
        } else if (!cb_tnc.isChecked()) {
            AppDelegate.showAlert(getActivity(), getString(R.string.alert_terms_conditions));
        } else if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            writeImageFile();
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            mInstance.setPostParamsSecond(mPostArrayList, Tags.role, "3");
            mInstance.setPostParamsSecond(mPostArrayList, Tags.status, "2");
            mInstance.setPostParamsSecond(mPostArrayList, Tags.is_verified, "1");

            mInstance.setPostParamsSecond(mPostArrayList, Tags.first_name, et_first_name.getText().toString());
            mInstance.setPostParamsSecond(mPostArrayList, Tags.last_name, et_last_name.getText().toString());
            mInstance.setPostParamsSecond(mPostArrayList, Tags.rec_password, et_password.getText().toString());
            mInstance.setPostParamsSecond(mPostArrayList, Tags.email, et_email.getText().toString());
            // male and female.
            mInstance.setPostParamsSecond(mPostArrayList, Tags.gender_param, selected_gender == 1 ? Tags.Male : Tags.Female + "");
            mInstance.setPostParamsSecond(mPostArrayList, Tags.phone_no, et_phone.getText().toString());

            if (institutionModel != null) {
                mInstance.setPostParamsSecond(mPostArrayList, Tags.institution_state_id, institutionModel.institution_state_id);
                if (institutionModel.institution_name_id.equalsIgnoreCase("other")) {
                    mInstance.setPostParamsSecond(mPostArrayList, Tags.other_ins_name, institutionModel.institution_name);
                    mInstance.setPostParamsSecond(mPostArrayList, Tags.institution_id, "0");
                } else {
                    mInstance.setPostParamsSecond(mPostArrayList, Tags.institution_id, institutionModel.institution_name_id);
                }
//                if (institutionModel.department_name_id.equalsIgnoreCase("other")) {
////                    mInstance.setPostParamsSecond(mPostArrayList, Tags.other_department_name, institutionModel.department_name);
//                    mInstance.setPostParamsSecond(mPostArrayList, Tags.department_name, institutionModel.department_name);
//                } else {
                mInstance.setPostParamsSecond(mPostArrayList, Tags.department_name, institutionModel.department_name);
//                }
            }


            if (prefs.getTempFacebookData() != null && AppDelegate.isValidString(prefs.getTempFacebookData().getId())) {
                mInstance.setPostParamsSecond(mPostArrayList, Tags.social_id, prefs.getTempFacebookData().getId());
                mInstance.setPostParamsSecond(mPostArrayList, Tags.image, prefs.getTempFacebookData().getImage());
            }

//            if (capturedFile != null)
//                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image, capturedFile.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);

            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    SignUpPersonalFragment.this, ServerRequestConstants.REGISTRATION,
                    mPostArrayList, SignUpPersonalFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.REGISTRATION)) {
            parseSignUpAsync(result);
        }
    }

    private void parseSignUpAsync(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {

//                JSONObject object = jsonObject.getJSONObject(Tags.response);
//                UserDataModel userDataModel = new UserDataModel();
//                userDataModel.role = object.getString(Tags.role);
//                userDataModel.status = object.getString(Tags.status);
//                userDataModel.first_name = object.getString(Tags.first_name);
//                userDataModel.last_name = object.getString(Tags.last_name);
//                userDataModel.email = object.getString(Tags.email);
//                userDataModel.password = object.getString(Tags.password);
//                userDataModel.role = object.getString(Tags.role);
//                userDataModel.image = JSONParser.getString(object, Tags.image);
//                userDataModel.created = object.getString(Tags.created);
//                userDataModel.gcm_token = JSONParser.getString(object, Tags.gcm_token);
//                userDataModel.facebook_id = JSONParser.getString(object, Tags.social_id);
//
//                userDataModel.login_type = object.getString(Tags.login_type);
//                userDataModel.is_verified = object.getString(Tags.is_verified);
//
//                userDataModel.following_count = JSONParser.getInt(object, Tags.following_count);
//                userDataModel.followers_count = JSONParser.getInt(object, Tags.followers_count);
//                userDataModel.total_product = JSONParser.getInt(object, Tags.total_product);
//
//                JSONObject studentObject = object.getJSONObject(Tags.student_detail);
//
//                userDataModel.str_Gender = studentObject.getString(Tags.gender);
//                userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
//                userDataModel.userId = studentObject.getString(Tags.user_id);
//                userDataModel.dob = studentObject.getString(Tags.dob);
//                userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);
//
//                InstitutionModel institutionModel = new InstitutionModel();
//                institutionModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
//                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
//                    institutionModel.institution_name_id = studentObject.getString(Tags.institution_state_id);
//                    institutionModel.institution_name = studentObject.getString(Tags.institution_name);
//                } else {
//                    institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
//                }
//                institutionModel.department_name = studentObject.getString(Tags.department_name);

                AppDelegate.showAlert(getActivity(), "We’ve just sent you an activation link in the email address entered during registration. Please follow the link in the email to activate your account.");

                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new SignInFragment());

            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            AppDelegate.LogE(e);
        }
    }

    private Bitmap OriginalPhoto;
    public static File capturedFile;

    public void writeImageFile() {
        if (OriginalPhoto == null)
            return;
        if (capturedFile == null)
            capturedFile = new File(getNewFile());
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }
}
