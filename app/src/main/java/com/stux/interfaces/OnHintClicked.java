package com.stux.interfaces;

/**
 * Created by Marcin on 2015-04-30.
 */
public interface OnHintClicked {
    void onHintClicked(String hint);
}
