package com.stux.interfaces;


import com.stux.Utils.EditTextBackEvent;

/**
 * Created by bharat on 25/4/16.
 */
public interface EditTextImeBackListener {
    public abstract void onImeBack(EditTextBackEvent ctrl, String text);
}
