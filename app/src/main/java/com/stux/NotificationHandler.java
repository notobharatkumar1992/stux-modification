package com.stux;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.stux.activities.MainActivity;
import com.stux.constants.Tags;

/**
 * Created by Bharat on 06/13/2016.
 */
public class NotificationHandler extends FragmentActivity {

    private Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getIntent().getExtras();
        AppDelegate.LogT("NotificationHandler => " + bundle.getString(Tags.FROM));
        if (bundle != null && AppDelegate.isValidString(bundle.getString(Tags.FROM))) {
            try {
                if (MainActivity.mActivity != null)
                    MainActivity.mActivity.finish();
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            if (bundle.getString(Tags.FROM).equalsIgnoreCase(Tags.follow)) {
                Intent intent = new Intent(NotificationHandler.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtras(bundle);
                startActivity(intent);
                AppDelegate.LogT("NotificationHandler startActivity called");
            } else if (bundle.getString(Tags.FROM).equalsIgnoreCase(Tags.deal)) {
                Intent intent = new Intent(NotificationHandler.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtras(bundle);
                startActivity(intent);
                AppDelegate.LogT("NotificationHandler startActivity called");
            } else if (bundle.getString(Tags.FROM).equalsIgnoreCase(Tags.product)) {
                Intent intent = new Intent(NotificationHandler.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtras(bundle);
                startActivity(intent);
                AppDelegate.LogT("NotificationHandler startActivity called");
            } else if (bundle.getString(Tags.FROM).equalsIgnoreCase(Tags.CHAT)) {
                Intent intent = new Intent(NotificationHandler.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtras(bundle);
                startActivity(intent);
                AppDelegate.LogT("NotificationHandler startActivity called");
            } else if (bundle.getString(Tags.FROM).equalsIgnoreCase(Tags.makeOffer)) {
                Intent intent = new Intent(NotificationHandler.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtras(bundle);
                startActivity(intent);
                AppDelegate.LogT("NotificationHandler startActivity called");
            }

        } else {
            AppDelegate.LogE("NotificationHandler, From value or bundle is null.");
        }
        finish();
    }
}
