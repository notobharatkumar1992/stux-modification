package com.stux.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.stux.AppDelegate;
import com.stux.service.ReceiveNotificationService;

/**
 * Created by Bharat on 08/29/2016.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        AppDelegate.LogS("NetworkChangeReceiver onReceive called");
        if (AppDelegate.haveNetworkConnection(context, false)) {
            context.startService(new Intent(context, ReceiveNotificationService.class));
        }
    }
}
