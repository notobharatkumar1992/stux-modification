package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 06/22/2016.
 */
public class SliderModel implements Parcelable {

    public String id, user_id, title, descriptions, url, slider_category, banner_image, banner_thumb_image, clicks, tot_clicks_users, expiry_date, status, created, modified;

    public int single_clicked = 0, thump_clicked = 0, type_of_slider = 0, from_page = 0, banner_type = 0;

    public SliderModel(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        title = in.readString();
        descriptions = in.readString();
        url = in.readString();
        slider_category = in.readString();
        banner_image = in.readString();
        banner_thumb_image = in.readString();
        clicks = in.readString();
        tot_clicks_users = in.readString();
        expiry_date = in.readString();
        status = in.readString();
        created = in.readString();
        modified = in.readString();
        single_clicked = in.readInt();
        thump_clicked = in.readInt();
        type_of_slider = in.readInt();
        from_page = in.readInt();
        banner_type = in.readInt();
    }

    public static final Creator<SliderModel> CREATOR = new Creator<SliderModel>() {
        @Override
        public SliderModel createFromParcel(Parcel in) {
            return new SliderModel(in);
        }

        @Override
        public SliderModel[] newArray(int size) {
            return new SliderModel[size];
        }
    };

    public SliderModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(user_id);
        dest.writeString(title);
        dest.writeString(descriptions);
        dest.writeString(url);
        dest.writeString(slider_category);
        dest.writeString(banner_image);
        dest.writeString(banner_thumb_image);
        dest.writeString(clicks);
        dest.writeString(tot_clicks_users);
        dest.writeString(expiry_date);
        dest.writeString(status);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeInt(single_clicked);
        dest.writeInt(thump_clicked);
        dest.writeInt(type_of_slider);
        dest.writeInt(from_page);
        dest.writeInt(banner_type);
    }
}
