package com.stux.Models;

/**
 * Created by Bharat on 08/30/2016.
 */
public class NotificationModel {

    public String user_id, user_image, user_first_name, user_last_name; //user follow variable
    public String product_id, product_image, product_name, offer_price;//user products variable
    public String deal_id, deal_image, deal_title;//user deal variable
    public String str_message;//user chat variable
    public String id, notification_type, created, is_view;
}
