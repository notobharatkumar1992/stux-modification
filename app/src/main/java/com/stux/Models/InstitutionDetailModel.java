package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 06/08/2016.
 */
public class InstitutionDetailModel implements Parcelable {

    public String id, institute_name, country_id, state_id, city_id, address, postal_code, email, contact_no, ins_head_name, ins_head_contact, department_name, status, created, modified;

    public InstitutionDetailModel(String institute_name) {
        this.institute_name = institute_name;
        this.id = institute_name;
    }

    public InstitutionDetailModel() {
    }

    protected InstitutionDetailModel(Parcel in) {
        id = in.readString();
        institute_name = in.readString();
        country_id = in.readString();
        state_id = in.readString();
        city_id = in.readString();
        address = in.readString();
        postal_code = in.readString();
        email = in.readString();
        contact_no = in.readString();
        ins_head_name = in.readString();
        ins_head_contact = in.readString();
        department_name = in.readString();
        status = in.readString();
        created = in.readString();
        modified = in.readString();
    }

    public static final Creator<InstitutionDetailModel> CREATOR = new Creator<InstitutionDetailModel>() {
        @Override
        public InstitutionDetailModel createFromParcel(Parcel in) {
            return new InstitutionDetailModel(in);
        }

        @Override
        public InstitutionDetailModel[] newArray(int size) {
            return new InstitutionDetailModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(institute_name);
        dest.writeString(country_id);
        dest.writeString(state_id);
        dest.writeString(city_id);
        dest.writeString(address);
        dest.writeString(postal_code);
        dest.writeString(email);
        dest.writeString(contact_no);
        dest.writeString(ins_head_name);
        dest.writeString(ins_head_contact);
        dest.writeString(department_name);
        dest.writeString(status);
        dest.writeString(created);
        dest.writeString(modified);
    }
}
