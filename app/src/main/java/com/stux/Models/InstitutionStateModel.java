package com.stux.Models;

/**
 * Created by Bharat on 06/08/2016.
 */
public class InstitutionStateModel {

    public String id, country_id, region_name, slug, status;

    public InstitutionStateModel(String region_name) {
        this.region_name = region_name;
    }

    public InstitutionStateModel() {
    }
}
