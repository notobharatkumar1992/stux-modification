package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 08/05/2016.
 */
public class RefineSearchModel implements Parcelable {

    public int page = 0;
    public String logged_in_User_id, record, product_name, campus_id, item_condition, cat_id, price, upload, rangeProgress;

    public RefineSearchModel(Parcel in) {
        page = in.readInt();
        logged_in_User_id = in.readString();
        record = in.readString();
        product_name = in.readString();
        campus_id = in.readString();
        item_condition = in.readString();
        cat_id = in.readString();
        price = in.readString();
        upload = in.readString();
        rangeProgress = in.readString();
    }

    public static final Creator<RefineSearchModel> CREATOR = new Creator<RefineSearchModel>() {
        @Override
        public RefineSearchModel createFromParcel(Parcel in) {
            return new RefineSearchModel(in);
        }

        @Override
        public RefineSearchModel[] newArray(int size) {
            return new RefineSearchModel[size];
        }
    };

    public RefineSearchModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(page);
        dest.writeString(logged_in_User_id);
        dest.writeString(record);
        dest.writeString(product_name);
        dest.writeString(campus_id);
        dest.writeString(item_condition);
        dest.writeString(cat_id);
        dest.writeString(price);
        dest.writeString(upload);
        dest.writeString(rangeProgress);
    }
}
