package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 06/27/2016.
 */
public class ProductModel implements Parcelable {

    public String id, cat_id, title, description, price, item_condition, image_1, image_2, image_3, image_4, sold_status, status, created, modified, image_1_thumb, image_2_thumb, image_3_thumb, image_4_thumb,
            latitude, longitude;

    // Product Category
    public String pc_id, pc_title, pc_description, pc_meta_tags, pc_meta_keywords, pc_status, pc_created, pc_lft, pc_rght, pc_parent_id;

    // Product User datail
    public String user_id, user_first_name, user_last_name, user_gender, user_email, user_role, user_image, user_password, user_social_id, user_gcm_token, user_login_type, user_is_verified, user_status, user_is_login, user_is_view, user_created, user_dob;

    public String user_department_name, user_institute_name, user_institution_id, user_institution_state_id, total_product_views, logged_user_view_status, user_institute_lat, user_institute_lng;

    public int height, rating, user_following_count, user_followers_count, user_total_product, user_follow_status, total_product_likes, product_like_status, total_comments;

    protected ProductModel(Parcel in) {
        id = in.readString();
        cat_id = in.readString();
        title = in.readString();
        description = in.readString();
        price = in.readString();
        item_condition = in.readString();
        image_1 = in.readString();
        image_2 = in.readString();
        image_3 = in.readString();
        image_4 = in.readString();
        sold_status = in.readString();
        status = in.readString();
        created = in.readString();
        modified = in.readString();
        image_1_thumb = in.readString();
        image_2_thumb = in.readString();
        image_3_thumb = in.readString();
        image_4_thumb = in.readString();
        latitude = in.readString();
        longitude = in.readString();

        pc_id = in.readString();
        pc_title = in.readString();
        pc_description = in.readString();
        pc_meta_tags = in.readString();
        pc_meta_keywords = in.readString();
        pc_status = in.readString();
        pc_created = in.readString();
        pc_lft = in.readString();
        pc_rght = in.readString();
        pc_parent_id = in.readString();

        user_id = in.readString();
        user_first_name = in.readString();
        user_last_name = in.readString();
        user_gender = in.readString();
        user_email = in.readString();
        user_role = in.readString();
        user_image = in.readString();
        user_password = in.readString();
        user_social_id = in.readString();
        user_gcm_token = in.readString();
        user_login_type = in.readString();
        user_is_verified = in.readString();
        user_status = in.readString();
        user_is_login = in.readString();
        user_is_view = in.readString();
        user_created = in.readString();
        user_dob = in.readString();

        user_department_name = in.readString();
        user_institute_name = in.readString();
        user_institution_id = in.readString();
        user_institution_state_id = in.readString();
        total_product_views = in.readString();
        logged_user_view_status = in.readString();
        user_institute_lat = in.readString();
        user_institute_lng = in.readString();

        height = in.readInt();
        rating = in.readInt();
        user_following_count = in.readInt();
        user_followers_count = in.readInt();
        user_total_product = in.readInt();
        user_follow_status = in.readInt();
        total_product_likes = in.readInt();
        product_like_status = in.readInt();
        total_comments = in.readInt();
    }

    public ProductModel() {
    }

    public static final Creator<ProductModel> CREATOR = new Creator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel in) {
            return new ProductModel(in);
        }

        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(cat_id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(price);
        dest.writeString(item_condition);
        dest.writeString(image_1);
        dest.writeString(image_2);
        dest.writeString(image_3);
        dest.writeString(image_4);
        dest.writeString(sold_status);
        dest.writeString(status);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeString(image_1_thumb);
        dest.writeString(image_2_thumb);
        dest.writeString(image_3_thumb);
        dest.writeString(image_4_thumb);
        dest.writeString(latitude);
        dest.writeString(longitude);

        dest.writeString(pc_id);
        dest.writeString(pc_title);
        dest.writeString(pc_description);
        dest.writeString(pc_meta_tags);
        dest.writeString(pc_meta_keywords);
        dest.writeString(pc_status);
        dest.writeString(pc_created);
        dest.writeString(pc_lft);
        dest.writeString(pc_rght);
        dest.writeString(pc_parent_id);

        dest.writeString(user_id);
        dest.writeString(user_first_name);
        dest.writeString(user_last_name);
        dest.writeString(user_gender);
        dest.writeString(user_email);
        dest.writeString(user_role);
        dest.writeString(user_image);
        dest.writeString(user_password);
        dest.writeString(user_social_id);
        dest.writeString(user_gcm_token);
        dest.writeString(user_login_type);
        dest.writeString(user_is_verified);
        dest.writeString(user_status);
        dest.writeString(user_is_login);
        dest.writeString(user_is_view);
        dest.writeString(user_created);
        dest.writeString(user_dob);

        dest.writeString(user_department_name);
        dest.writeString(user_institute_name);
        dest.writeString(user_institution_id);
        dest.writeString(user_institution_state_id);
        dest.writeString(total_product_views);
        dest.writeString(logged_user_view_status);
        dest.writeString(user_institute_lat);
        dest.writeString(user_institute_lng);

        dest.writeInt(height);
        dest.writeInt(rating);
        dest.writeInt(user_following_count);
        dest.writeInt(user_followers_count);
        dest.writeInt(user_total_product);
        dest.writeInt(user_follow_status);
        dest.writeInt(total_product_likes);
        dest.writeInt(product_like_status);
        dest.writeInt(total_comments);
    }
}
