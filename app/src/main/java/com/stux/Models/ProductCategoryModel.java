package com.stux.Models;

/**
 * Created by Bharat on 06/27/2016.
 */
public class ProductCategoryModel {

    public String id, title, description, cat_name, status, created;
    public int selected = 0;


    public ProductCategoryModel(String cat_name) {
        this.cat_name = cat_name;
    }

    public ProductCategoryModel() {
    }

}
