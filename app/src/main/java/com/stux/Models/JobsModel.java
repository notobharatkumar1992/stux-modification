package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 08/01/2016.
 */
public class JobsModel implements Parcelable {

    public String id, job_type, job_timing_type, title, description, company_name, company_address, industry_type, logo_image, banner_image, contact_no, posted_by, designation, email, website_url, expiry_date, status, created, modified, isApplied, logo_image_thumb, banner_image_thumb, apply_type, external_link;

    public JobsModel(Parcel in) {
        id = in.readString();
        job_type = in.readString();
        job_timing_type = in.readString();
        title = in.readString();
        description = in.readString();
        company_name = in.readString();
        company_address = in.readString();
        industry_type = in.readString();
        logo_image = in.readString();
        banner_image = in.readString();
        contact_no = in.readString();
        posted_by = in.readString();
        designation = in.readString();
        email = in.readString();
        website_url = in.readString();
        expiry_date = in.readString();
        status = in.readString();
        created = in.readString();
        modified = in.readString();
        isApplied = in.readString();
        logo_image_thumb = in.readString();
        banner_image_thumb = in.readString();
        apply_type = in.readString();
        external_link = in.readString();
    }

    public static final Creator<JobsModel> CREATOR = new Creator<JobsModel>() {
        @Override
        public JobsModel createFromParcel(Parcel in) {
            return new JobsModel(in);
        }

        @Override
        public JobsModel[] newArray(int size) {
            return new JobsModel[size];
        }
    };

    public JobsModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(job_type);
        dest.writeString(job_timing_type);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(company_name);
        dest.writeString(company_address);
        dest.writeString(industry_type);
        dest.writeString(logo_image);
        dest.writeString(banner_image);
        dest.writeString(contact_no);
        dest.writeString(posted_by);
        dest.writeString(designation);
        dest.writeString(email);
        dest.writeString(website_url);
        dest.writeString(expiry_date);
        dest.writeString(status);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeString(isApplied);
        dest.writeString(logo_image_thumb);
        dest.writeString(banner_image_thumb);
        dest.writeString(apply_type);
        dest.writeString(external_link);

    }
}
