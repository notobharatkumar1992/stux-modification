package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Message implements Parcelable {

    public static final int TYPE_MESSAGE = 0;
    public static final int TYPE_LOG = 1;
    public static final int TYPE_ACTION = 2;

    public static final int USER_SENDER = 0;
    public static final int USER_RECEIVER = 1;

    public int mType;
    public int user_type = USER_SENDER;
    public int newMessageCount = 0;
    public String id;
    public String product_id, product_name, product_image, offer_price;
    public String mUsername;
    public String mMessage;
    public String created;
    public String room_id, sender_id, receiver_id, status;
    public UserDataModel dataModel;
    public ProductModel productModel;

    public Message() {
    }

    @Override
    public String toString() {
        return "Message{" +
                "mType=" + mType +
                ", user_type=" + user_type +
                ", newMessageCount=" + newMessageCount +
                ", id='" + id + '\'' +
                ", product_id='" + product_id + '\'' +
                ", product_name='" + product_name + '\'' +
                ", product_image='" + product_image + '\'' +
                ", offer_price='" + offer_price + '\'' +
                ", mUsername='" + mUsername + '\'' +
                ", mMessage='" + mMessage + '\'' +
                ", created='" + created + '\'' +
                ", room_id='" + room_id + '\'' +
                ", sender_id='" + sender_id + '\'' +
                ", receiver_id='" + receiver_id + '\'' +
                ", status='" + status + '\'' +
                ", dataModel=" + dataModel +
                ", productModel=" + productModel +
                '}';
    }

    protected Message(Parcel in) {
        mType = in.readInt();
        user_type = in.readInt();
        newMessageCount = in.readInt();
        id = in.readString();
        product_id = in.readString();
        product_name = in.readString();
        product_image = in.readString();
        offer_price = in.readString();
        mUsername = in.readString();
        mMessage = in.readString();
        created = in.readString();
        room_id = in.readString();
        sender_id = in.readString();
        receiver_id = in.readString();
        status = in.readString();
        dataModel = in.readParcelable(UserDataModel.class.getClassLoader());
        productModel = in.readParcelable(ProductModel.class.getClassLoader());
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public int getType() {
        return mType;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getUsername() {
        return mUsername;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mType);
        dest.writeInt(user_type);
        dest.writeInt(newMessageCount);
        dest.writeString(id);
        dest.writeString(product_id);
        dest.writeString(product_name);
        dest.writeString(product_image);
        dest.writeString(offer_price);
        dest.writeString(mUsername);
        dest.writeString(mMessage);
        dest.writeString(created);
        dest.writeString(room_id);
        dest.writeString(sender_id);
        dest.writeString(receiver_id);
        dest.writeString(status);
        dest.writeParcelable(dataModel, flags);
        dest.writeParcelable(productModel, flags);
    }


    public static class Builder {
        private final int mType;
        private String mUsername;
        private String mMessage;

        public Builder(int type) {
            mType = type;
        }

        public Builder username(String username) {
            mUsername = username;
            return this;
        }

        public Builder message(String message) {
            mMessage = message;
            return this;
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "mType=" + mType +
                    ", mUsername='" + mUsername + '\'' +
                    ", mMessage='" + mMessage + '\'' +
                    '}';
        }

        public Message build() {
            Message message = new Message();
            message.mType = mType;
            message.mUsername = mUsername;
            message.mMessage = mMessage;
            return message;
        }
    }
}
