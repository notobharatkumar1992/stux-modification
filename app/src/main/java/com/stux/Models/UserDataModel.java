package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by parul on 15/1/16.
 */
public class UserDataModel implements Parcelable {
    public String userId, first_name, last_name, email, dob, image, fbImageUrl, str_Gender, mobile_number, password, facebook_id, role, gcm_token, login_type, fav_cat_id, status, is_verified, created, student_id, city_name, is_view, social_id, user_online_time_id;
    //    public static String user_online_time_id;
    public String institution_state_id, institution_id, institute_name, department_name;
    public String offer_price, product_id, product_title, product_image;
    public int date, month, year, isGroupOwner = 0, follow_status = 0, total_product = 0, followers_count = 0, following_count = 0;

    public double latitude, longitude;

    public InstitutionModel institutionModel;

    public UserDataModel() {
    }

    @Override
    public String toString() {
        return "UserDataModel{" +
                "userId='" + userId + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", dob='" + dob + '\'' +
                ", image='" + image + '\'' +
                ", fbImageUrl='" + fbImageUrl + '\'' +
                ", str_Gender='" + str_Gender + '\'' +
                ", mobile_number='" + mobile_number + '\'' +
                ", password='" + password + '\'' +
                ", facebook_id='" + facebook_id + '\'' +
                ", role='" + role + '\'' +
                ", gcm_token='" + gcm_token + '\'' +
                ", login_type='" + login_type + '\'' +
                ", fav_cat_id='" + fav_cat_id + '\'' +
                ", status='" + status + '\'' +
                ", is_verified='" + is_verified + '\'' +
                ", created='" + created + '\'' +
                ", student_id='" + student_id + '\'' +
                ", city_name='" + city_name + '\'' +
                ", is_view='" + is_view + '\'' +
                ", social_id='" + social_id + '\'' +
                ", user_online_time_id='" + user_online_time_id + '\'' +
                ", institution_state_id='" + institution_state_id + '\'' +
                ", institution_id='" + institution_id + '\'' +
                ", institute_name='" + institute_name + '\'' +
                ", department_name='" + department_name + '\'' +
                ", offer_price='" + offer_price + '\'' +
                ", product_id='" + product_id + '\'' +
                ", product_title='" + product_title + '\'' +
                ", product_image='" + product_image + '\'' +
                ", date=" + date +
                ", month=" + month +
                ", year=" + year +
                ", isGroupOwner=" + isGroupOwner +
                ", follow_status=" + follow_status +
                ", total_product=" + total_product +
                ", followers_count=" + followers_count +
                ", following_count=" + following_count +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", institutionModel=" + institutionModel +
                '}';
    }

    protected UserDataModel(Parcel in) {
        userId = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        email = in.readString();
        dob = in.readString();
        image = in.readString();
        fbImageUrl = in.readString();
        str_Gender = in.readString();
        mobile_number = in.readString();
        password = in.readString();
        facebook_id = in.readString();
        role = in.readString();
        gcm_token = in.readString();
        login_type = in.readString();
        fav_cat_id = in.readString();
        status = in.readString();
        is_verified = in.readString();
        created = in.readString();
        student_id = in.readString();
        city_name = in.readString();
        is_view = in.readString();
        social_id = in.readString();
        user_online_time_id = in.readString();
        institution_state_id = in.readString();
        institution_id = in.readString();
        institute_name = in.readString();
        department_name = in.readString();
        offer_price = in.readString();
        product_id = in.readString();
        product_title = in.readString();
        product_image = in.readString();
        date = in.readInt();
        month = in.readInt();
        year = in.readInt();
        isGroupOwner = in.readInt();
        follow_status = in.readInt();
        total_product = in.readInt();
        followers_count = in.readInt();
        following_count = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
        institutionModel = in.readParcelable(InstitutionModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(email);
        dest.writeString(dob);
        dest.writeString(image);
        dest.writeString(fbImageUrl);
        dest.writeString(str_Gender);
        dest.writeString(mobile_number);
        dest.writeString(password);
        dest.writeString(facebook_id);
        dest.writeString(role);
        dest.writeString(gcm_token);
        dest.writeString(login_type);
        dest.writeString(fav_cat_id);
        dest.writeString(status);
        dest.writeString(is_verified);
        dest.writeString(created);
        dest.writeString(student_id);
        dest.writeString(city_name);
        dest.writeString(is_view);
        dest.writeString(social_id);
        dest.writeString(user_online_time_id);
        dest.writeString(institution_state_id);
        dest.writeString(institution_id);
        dest.writeString(institute_name);
        dest.writeString(department_name);
        dest.writeString(offer_price);
        dest.writeString(product_id);
        dest.writeString(product_title);
        dest.writeString(product_image);
        dest.writeInt(date);
        dest.writeInt(month);
        dest.writeInt(year);
        dest.writeInt(isGroupOwner);
        dest.writeInt(follow_status);
        dest.writeInt(total_product);
        dest.writeInt(followers_count);
        dest.writeInt(following_count);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeParcelable(institutionModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserDataModel> CREATOR = new Creator<UserDataModel>() {
        @Override
        public UserDataModel createFromParcel(Parcel in) {
            return new UserDataModel(in);
        }

        @Override
        public UserDataModel[] newArray(int size) {
            return new UserDataModel[size];
        }
    };
}
