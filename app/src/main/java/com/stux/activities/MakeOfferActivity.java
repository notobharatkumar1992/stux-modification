package com.stux.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.stux.Adapters.MakeOfferAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 08/03/2016.
 */
public class MakeOfferActivity extends AppCompatActivity implements OnReciveServerResponse, View.OnClickListener, OnListItemClickListener {

    private Handler mHandler;
    private Prefs prefs;
    private UserDataModel dataModel;

    private EditText et_currency;
    private TextView txt_c_offer_text, txt_c_continue;

    private ProductModel productModel;

    private ViewPager view_pager;
    private MakeOfferAdapter mPagerAdapter;
    public ArrayList<String> arrayString = new ArrayList<>();

    public Activity mActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        mActivity = this;
        setContentView(R.layout.make_offer);
        prefs = new Prefs(MakeOfferActivity.this);
        dataModel = prefs.getUserdata();
        productModel = getIntent().getExtras().getParcelable(Tags.product);
        setHandler();
        initView();
    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        int value = view_pager.getCurrentItem();
                        if (value == mPagerAdapter.getCount() - 1) {
                            value = 0;
                        } else {
                            value++;
                        }
                        if (value < mPagerAdapter.getCount())
                            view_pager.setCurrentItem(value, true);
                        if (mActivity != null) {
                            mHandler.postDelayed(this, 3000);
                        } else {
                            mHandler.removeCallbacks(this);
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    private void callMakeOfferAsync() {
        if (AppDelegate.haveNetworkConnection(MakeOfferActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(MakeOfferActivity.this).setPostParamsSecond(mPostArrayList, Tags.buyer_id, dataModel.userId);
            AppDelegate.getInstance(MakeOfferActivity.this).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            AppDelegate.getInstance(MakeOfferActivity.this).setPostParamsSecond(mPostArrayList, Tags.seller_id, productModel.user_id + "");
            AppDelegate.getInstance(MakeOfferActivity.this).setPostParamsSecond(mPostArrayList, Tags.offer_price, et_currency.getText().toString() + "");
            AppDelegate.getInstance(MakeOfferActivity.this).setPostParamsSecond(mPostArrayList, Tags.status, "1");
            PostAsync mPostAsyncObj = new PostAsync(MakeOfferActivity.this,
                    this, ServerRequestConstants.MAKE_OFFER,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            startActivity(new Intent(MakeOfferActivity.this, NoInternetConnectionActivity.class));
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(MakeOfferActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(MakeOfferActivity.this);
                }
            }
        };
    }

    private void initView() {
        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.txt_c_header)).setText("Make Offer");
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(this);
        findViewById(R.id.ll_c_notification_dot).setVisibility(View.GONE);
        findViewById(R.id.img_c_notification).setVisibility(View.GONE);
        findViewById(R.id.img_c_search).setVisibility(View.GONE);

        et_currency = (EditText) findViewById(R.id.et_currency);
        et_currency.setTypeface(Typeface.createFromAsset(MakeOfferActivity.this.getAssets(), getString(R.string.font_roman)));
        et_currency.setText("" + productModel.price);
        et_currency.setSelection(et_currency.length());

        txt_c_offer_text = (TextView) findViewById(R.id.txt_c_offer_text);
        txt_c_continue = (TextView) findViewById(R.id.txt_c_continue);
        txt_c_continue.setOnClickListener(this);

        txt_c_offer_text.setText("Enter your offer for " + productModel.title);


        view_pager = (ViewPager) findViewById(R.id.view_pager);
        if (arrayString.size() == 0)
            arrayString.add(productModel.image_1);

        mPagerAdapter = new MakeOfferAdapter(MakeOfferActivity.this, arrayString, this);
        view_pager.setAdapter(mPagerAdapter);
        timeOutLoop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_continue:
                callMakeOfferAsync();
                break;

            case R.id.img_c_left:
                onBackPressed();
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (mActivity != null)
                startActivity(new Intent(MakeOfferActivity.this, NoInternetConnectionActivity.class));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.MAKE_OFFER)) {
            parseMakeOfferResult(result);
        }
    }

    private void parseMakeOfferResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                AppDelegate.showToast(MakeOfferActivity.this, jsonObject.getString(Tags.message));
                onBackPressed();
            } else {
                AppDelegate.showAlert(MakeOfferActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(MakeOfferActivity.this, "Server Error");
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
    }
}
