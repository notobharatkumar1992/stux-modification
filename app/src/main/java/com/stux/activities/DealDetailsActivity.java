package com.stux.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.google.android.gms.maps.MapsInitializer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.stux.Adapters.ProductPagerAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.DealModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.Utils.TransitionHelper;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.fragments.DealsMoviesFragment;
import com.stux.fragments.HomeFragment;
import com.stux.fragments.MyDealListingFragment;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

/**
 * Created by NOTO on 5/30/2016.
 */
public class DealDetailsActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener, OnReciveServerResponse {

    private Activity mActivity;
    private TextView txt_c_detail_name, txt_c_category, txt_c_viewer, txt_c_area, txt_c_new_price, txt_c_old_price, txt_c_percent, txt_c_address_1, txt_c_address_2, txt_c_contact, txt_c_coupon, txt_c_grab, txt_c_address, txt_c_timer, txt_c_detail;
    private carbon.widget.ImageView img_c_deal_banner;
    private ImageView img_loading;
    public LinearLayout ll_c_coupon_number;
    public TextView txt_c_coupon_no;
    public static DealModel dealModel;
    private Prefs prefs;
    private UserDataModel dataModel;

    public ArrayList<String> arrayString = new ArrayList<>();
    private android.widget.LinearLayout pager_indicator;
    private ViewPager view_pager;
    private ProductPagerAdapter mPagerAdapter;

    private boolean fromMyCoupon = false;
    private boolean asyncExecuting = false;

    private Handler mHandler;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(DealDetailsActivity.this);
        setContentView(R.layout.deals_detail);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        mActivity = this;
        prefs = new Prefs(DealDetailsActivity.this);
        dataModel = prefs.getUserdata();
        AppDelegate.LogT("dealData => " + getIntent().getExtras().getString(Tags.from));
        if (dealModel == null)
            dealModel = getIntent().getExtras().getParcelable(Tags.deal);
        if (AppDelegate.isValidString(getIntent().getExtras().getString(Tags.FROM))) {
            fromMyCoupon = true;
        }
        initView();
        initDialog();
        setValues();
        setHandler();
//        if (dealModel != null) {
//            if (!AppDelegate.isValidString(dealModel.price)) {
        executeDealDetailApi();
//            }
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setValues();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
        dealModel = null;
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(DealDetailsActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(DealDetailsActivity.this);
                        break;
                    case 1:
                        setValues();
                        break;
                }
            }
        };
    }

    private void executeItemViewApi() {
        if (AppDelegate.haveNetworkConnection(DealDetailsActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(DealDetailsActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(DealDetailsActivity.this).setPostParamsSecond(mPostArrayList, Tags.deal_id, dealModel.id);
            PostAsync mPostasyncObj = new PostAsync(DealDetailsActivity.this,
                    DealDetailsActivity.this, ServerRequestConstants.DEALS_VIEW,
                    mPostArrayList, null);
//            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
            asyncExecuting = true;
        } else {
            startActivity(new Intent(DealDetailsActivity.this, NoInternetConnectionActivity.class));
        }
    }

    private void executeDealDetailApi() {
        if (AppDelegate.haveNetworkConnection(DealDetailsActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(DealDetailsActivity.this).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(DealDetailsActivity.this).setPostParamsSecond(mPostArrayList, Tags.deal_id, dealModel.id);
            PostAsync mPostasyncObj = new PostAsync(DealDetailsActivity.this,
                    DealDetailsActivity.this, ServerRequestConstants.DEAL_DETAIL,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
            asyncExecuting = true;
        } else {
            startActivity(new Intent(DealDetailsActivity.this, NoInternetConnectionActivity.class));
        }
    }

    private void setValues() {
        if (dealModel != null) {
            txt_c_detail_name.setText(dealModel.title);
            txt_c_viewer.setText(AppDelegate.isValidString(dealModel.total_deal_views) ? dealModel.total_deal_views : "0");
            txt_c_category.setText("CATEGORY: " + dealModel.product_category);

            txt_c_old_price.setText(" N" + dealModel.price + " ");
            txt_c_old_price.setPaintFlags(txt_c_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            AppDelegate.LogT("Price = " + dealModel.price + ", discount = " + dealModel.discount + ", dis price = " + dealModel.discount_price);
            String price = AppDelegate.isValidString(dealModel.price) ? dealModel.price : "";
            price = price.replaceAll(",", "").replaceAll(" ", "");
            String discount_price = AppDelegate.isValidString(dealModel.discount_price) ? dealModel.discount_price : "";
            discount_price = discount_price.replaceAll(",", "").replaceAll(" ", "");
            if (!AppDelegate.isValidInt(price))
                price = "0";
            if (!AppDelegate.isValidInt(discount_price))
                discount_price = "0";
            int calculatedPrice = Integer.parseInt(price) - Integer.parseInt(discount_price);
            if (calculatedPrice < 0)
                calculatedPrice = 0;
            txt_c_new_price.setText("N" + AppDelegate.getPriceFormatted(calculatedPrice));

            if (AppDelegate.isValidInt(dealModel.discount))
                txt_c_percent.setText(dealModel.discount + "%");
            else
                txt_c_percent.setText("0%");

            txt_c_address_1.setText(dealModel.venue);
            txt_c_address_2.setText(dealModel.venue);


            txt_c_contact.setText("Contact: " + (AppDelegate.isValidString(dealModel.emailid) ? dealModel.emailid : ""));
            txt_c_address.setText("Address: " + dealModel.venue);
            txt_c_detail.setText("Detail: " + dealModel.details);

//            1=for all students , 2 = for particular institutes
            if (AppDelegate.isValidString(dealModel.deal_area))
                txt_c_area.setText("AREA: " + (dealModel.deal_area.equalsIgnoreCase("1") ? "ALL STUDENTS" : "PARTICULAR INSTITUTES"));

            img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
            frameAnimation.setCallback(img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
            img_c_deal_banner.setVisibility(View.GONE);

            if (fromMyCoupon && AppDelegate.isValidString(dealModel.is_grabbed) && dealModel.is_grabbed.equalsIgnoreCase("1")) {
                txt_c_grab.setClickable(false);
                txt_c_grab.setEnabled(false);
                txt_c_grab.setOnClickListener(null);
                txt_c_grab.setText(dealModel.coupon_code + "");
                ll_c_coupon_number.setVisibility(View.VISIBLE);
                txt_c_coupon_no.setText(dealModel.coupon_no);
            } else if (AppDelegate.isValidString(dealModel.is_grabbed) && dealModel.is_grabbed.equalsIgnoreCase("1")) {
                txt_c_grab.setClickable(false);
                txt_c_grab.setEnabled(false);
                txt_c_grab.setOnClickListener(null);
                txt_c_grab.setText(dealModel.coupon_code + "");
                ll_c_coupon_number.setVisibility(View.VISIBLE);
                txt_c_coupon_no.setText(dealModel.coupon_no);
            } else {
                ll_c_coupon_number.setVisibility(View.GONE);
            }

            if (AppDelegate.isValidString(dealModel.logged_user_view_status) && dealModel.logged_user_view_status.equalsIgnoreCase("0") && !asyncExecuting) {
                executeItemViewApi();
            }
            if (AppDelegate.isValidString(dealModel.expiry_date) && countDownTimer == null)
                startTimer();

            if (arrayString.size() == 0)
                if (AppDelegate.isValidString(dealModel.image_4_thumb)) {
                    arrayString.add(dealModel.image_1_thumb);
                    arrayString.add(dealModel.image_2_thumb);
                    arrayString.add(dealModel.image_3_thumb);
                    arrayString.add(dealModel.image_4_thumb);
                } else if (AppDelegate.isValidString(dealModel.image_3_thumb)) {
                    arrayString.add(dealModel.image_1_thumb);
                    arrayString.add(dealModel.image_2_thumb);
                    arrayString.add(dealModel.image_3_thumb);
                } else if (AppDelegate.isValidString(dealModel.image_2_thumb)) {
                    arrayString.add(dealModel.image_1_thumb);
                    arrayString.add(dealModel.image_2_thumb);
                } else if (AppDelegate.isValidString(dealModel.image_1_thumb)) {
                    arrayString.add(dealModel.image_1_thumb);
                } else {
//                arrayString.add("");
                }
            setUiPageViewController();
            mPagerAdapter.notifyDataSetChanged();
            view_pager.invalidate();
        }
    }

    CountDownTimer countDownTimer;
    private boolean couponExpired = false;

    private void startTimer() {
        Calendar calendar = Calendar.getInstance();
        dealModel.expiry_date = dealModel.expiry_date + "000";
        long remaining = Long.parseLong(dealModel.expiry_date) - calendar.getTimeInMillis();
        if (remaining > 0) {
            couponExpired = false;
            calendar.setTimeInMillis(remaining);
            countDownTimer = new CountDownTimer(remaining, 1000) { // adjust the milli seconds here
                public void onTick(long millisUntilFinished) {
                    long seconds = millisUntilFinished / 1000;
                    long minutes = seconds / 60;
                    long hours = minutes / 60;
                    long days = hours / 24;
                    String time = days + "D " + hours % 24 + "H " + minutes % 60 + "M " + seconds % 60 + "S ";
                    txt_c_grab.setBackgroundColor(getResources().getColor(R.color.orange));

                    txt_c_timer.setText(time + "");
                    txt_coupon_time.setText("Coupon will expire in \n" + time + "");
                }

                public void onFinish() {
                    couponExpired = true;
                    txt_c_timer.setText("Coupon expired");
                    txt_coupon_time.setText("Coupon expired");
                    txt_c_grab.setBackgroundColor(getResources().getColor(R.color.TEXT_COLOR_ON_slide_black));

                    txt_c_grab.setClickable(false);
                    txt_c_grab.setEnabled(false);
                    txt_c_grab.setOnClickListener(null);

                    txt_c_save.setClickable(false);
                    txt_c_save.setEnabled(false);
                    txt_c_save.setOnClickListener(null);
                }
            }.start();
        } else {
            couponExpired = true;
            AppDelegate.LogE("Deal expired : remaining => " + remaining);
            txt_c_grab.setBackgroundColor(getResources().getColor(R.color.TEXT_COLOR_ON_slide_black));
            txt_c_timer.setText("Coupon expired");
            txt_coupon_time.setText("Coupon expired");
        }
    }

    private void initView() {
        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.txt_c_header)).setText("Deal Detail");
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(this);
        findViewById(R.id.ll_c_notification_dot).setVisibility(View.GONE);
        findViewById(R.id.img_c_notification).setVisibility(View.GONE);
        findViewById(R.id.img_c_search).setVisibility(View.GONE);

        txt_c_detail_name = (TextView) findViewById(R.id.txt_c_detail_name);
        txt_c_viewer = (TextView) findViewById(R.id.txt_c_viewer);
        txt_c_category = (TextView) findViewById(R.id.txt_c_category);
        txt_c_area = (TextView) findViewById(R.id.txt_c_area);
        txt_c_new_price = (TextView) findViewById(R.id.txt_c_new_price);
        txt_c_old_price = (TextView) findViewById(R.id.txt_c_old_price);
        txt_c_percent = (TextView) findViewById(R.id.txt_c_percent);
        txt_c_address_1 = (TextView) findViewById(R.id.txt_c_address_1);
        txt_c_address_2 = (TextView) findViewById(R.id.txt_c_address_2);
        txt_c_contact = (TextView) findViewById(R.id.txt_c_contact);
        txt_c_coupon = (TextView) findViewById(R.id.txt_c_coupon);
        txt_c_grab = (TextView) findViewById(R.id.txt_c_grab);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);
        txt_c_timer = (TextView) findViewById(R.id.txt_c_timer);
        txt_c_detail = (TextView) findViewById(R.id.txt_c_detail);

        ll_c_coupon_number = (LinearLayout) findViewById(R.id.ll_c_coupon_number);
        txt_c_coupon_no = (TextView) findViewById(R.id.txt_c_coupon_no);

        img_c_deal_banner = (carbon.widget.ImageView) findViewById(R.id.img_c_deal_banner);
        img_c_deal_banner.setOnClickListener(this);
        img_loading = (ImageView) findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);

        findViewById(R.id.txt_c_grab).setOnClickListener(this);

        pager_indicator = (android.widget.LinearLayout) findViewById(R.id.pager_indicator);
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        mPagerAdapter = new ProductPagerAdapter(DealDetailsActivity.this, arrayString, this);
        view_pager.setAdapter(mPagerAdapter);
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void switchBannerPage(int position) {
        try {
            if (dotsCount > 0) {
                for (int i = 0; i < dotsCount; i++) {
                    if (dots != null && dots.length > i)
                        dots[i].setImageResource(R.drawable.white_radius_square);
                }
                if (dots != null && dots.length > position)
                    dots[position].setImageResource(R.drawable.orange_radius_square);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private ImageView[] dots;

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = arrayString.size();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(DealDetailsActivity.this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(25, 25);
                if (getString(R.string.values_folder).equalsIgnoreCase("values-hdpi")) {
                    params = new LinearLayout.LayoutParams(15, 15);
                    params.setMargins(7, 0, 7, 0);
                } else
                    params.setMargins(10, 0, 10, 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.orange_radius_square));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                onBackPressed();
                break;

            case R.id.txt_c_grab:
                if (!couponExpired) {
                    showDialogGrab();
                } else {
                    AppDelegate.showToast(DealDetailsActivity.this, "You can't grab expired coupon.");
                }
                break;

            case R.id.img_c_deal_banner:
                if (AppDelegate.haveNetworkConnection(DealDetailsActivity.this, false)) {
                    Intent intent = new Intent(DealDetailsActivity.this, LargeImageActivity.class);
                    Bundle bundle = new Bundle();
//                    bundle.putString(Tags.image_1, dealModel.banner_image);
//                    bundle.putInt(Tags.count, 1);
                    bundle.putString(Tags.image_1, dealModel.image_1);
                    bundle.putString(Tags.image_2, dealModel.image_2);
                    bundle.putString(Tags.image_3, dealModel.image_3);
                    bundle.putString(Tags.image_4, dealModel.image_4);
                    bundle.putInt(Tags.POSITION, 0);
                    bundle.putInt(Tags.count, 4);
                    intent.putExtras(bundle);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(DealDetailsActivity.this, false, new Pair<>(view_pager, "Image"));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(DealDetailsActivity.this, pairs);
                    startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                } else
                    startActivity(new Intent(DealDetailsActivity.this, NoInternetConnectionActivity.class));
                break;
        }
    }

    private Dialog dialog;
    private TextView txt_coupon_time, txt_c_save;

    private void initDialog() {
        dialog = new Dialog(DealDetailsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_deal_coupen);
        final carbon.widget.ImageView img_c_coupon = (carbon.widget.ImageView) dialog.findViewById(R.id.img_c_coupon);
        imageLoader.loadImage(dealModel.image_1_thumb, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                img_c_coupon.setImageBitmap(loadedImage);
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });

        TextView txt_c_coupon = (TextView) dialog.findViewById(R.id.txt_c_coupon);
        AppDelegate.LogT("dealModel.coupon_code => " + dealModel.coupon_code);
        txt_c_coupon.setText(dealModel.coupon_code);

        txt_coupon_time = (TextView) dialog.findViewById(R.id.txt_coupon_time);

        txt_c_save = (TextView) dialog.findViewById(R.id.txt_c_save);
        txt_c_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callGrabCouponAsync();
            }
        });
    }

    private void showDialogGrab() {
        dialog.show();
    }

    private void callGrabCouponAsync() {
        if (AppDelegate.haveNetworkConnection(DealDetailsActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(DealDetailsActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(DealDetailsActivity.this).setPostParamsSecond(mPostArrayList, Tags.deal_id, dealModel.id);
            AppDelegate.getInstance(DealDetailsActivity.this).setPostParamsSecond(mPostArrayList, Tags.coupon_code, dealModel.coupon_code);
            PostAsync mPostAsyncObj = new PostAsync(DealDetailsActivity.this,
                    this, ServerRequestConstants.GRAB_COUPONS,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            startActivity(new Intent(DealDetailsActivity.this, NoInternetConnectionActivity.class));
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
        this.item_position = position;
        if (name.equalsIgnoreCase(Tags.LIST_ITEM_TRENDING)) {
            if (AppDelegate.haveNetworkConnection(DealDetailsActivity.this, false)) {
                Intent intent = new Intent(DealDetailsActivity.this, LargeImageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.image_1, dealModel.image_1);
                bundle.putString(Tags.image_2, dealModel.image_2);
                bundle.putString(Tags.image_3, dealModel.image_3);
                bundle.putString(Tags.image_4, dealModel.image_4);
                bundle.putInt(Tags.POSITION, position);
                bundle.putInt(Tags.count, 4);
                intent.putExtras(bundle);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(DealDetailsActivity.this, false, new Pair<>(view_pager, "Image"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(DealDetailsActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
            }
        } else {
            startActivity(new Intent(DealDetailsActivity.this, NoInternetConnectionActivity.class));
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (mActivity != null)
                startActivity(new Intent(DealDetailsActivity.this, NoInternetConnectionActivity.class));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GRAB_COUPONS)) {
            mHandler.sendEmptyMessage(11);
            parseGrabResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.DEALS_VIEW)) {
            parseDealsResponse(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.DEAL_DETAIL)) {
            mHandler.sendEmptyMessage(11);
            parseDealDetailResult(result);
        }
    }

    private void parseDealDetailResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONArray(Tags.response).getJSONObject(0);
                dealModel.id = JSONParser.getString(object, Tags.id);
                dealModel.user_id = JSONParser.getString(object, Tags.user_id);
                dealModel.deal_area = JSONParser.getString(object, Tags.deal_area);
                dealModel.deal_catid = JSONParser.getString(object, Tags.deal_catid);
                dealModel.institute_id = JSONParser.getString(object, Tags.institute_id);
                dealModel.title = JSONParser.getString(object, Tags.title);


                dealModel.details = JSONParser.getString(object, Tags.details);
                dealModel.venue = JSONParser.getString(object, Tags.venue);
                dealModel.price = JSONParser.getString(object, Tags.price);
//                if (!AppDelegate.isValidInt(dealModel.price))
//                dealModel.price = "0";
                dealModel.discount = JSONParser.getString(object, Tags.discount);
//                if (!AppDelegate.isValidInt(dealModel.discount))
//                    dealModel.discount = "0";
                dealModel.discount_price = JSONParser.getString(object, Tags.discount_price);
//                if (!AppDelegate.isValidInt(dealModel.discount_price))
//                dealModel.discount_price = "0";

                dealModel.emailid = JSONParser.getString(object, Tags.emailid);
                dealModel.coupon_code = JSONParser.getString(object, Tags.coupon_code);
                dealModel.coupon_no = object.optString(Tags.coupon_no);
                dealModel.expiry_date = JSONParser.getString(object, Tags.expiry_date);

                JSONObject dealObject = object.getJSONObject(Tags.deal_category);
                dealModel.product_category = dealObject.getString(Tags.title);

                dealModel.image_1 = JSONParser.getString(object, Tags.image_1);
                dealModel.image_2 = JSONParser.getString(object, Tags.image_2);
                dealModel.image_3 = JSONParser.getString(object, Tags.image_3);
                dealModel.image_4 = JSONParser.getString(object, Tags.image_4);

                dealModel.image_1_thumb = JSONParser.getString(object, Tags.image_1_thumb);
                dealModel.image_2_thumb = JSONParser.getString(object, Tags.image_2_thumb);
                dealModel.image_3_thumb = JSONParser.getString(object, Tags.image_3_thumb);
                dealModel.image_4_thumb = JSONParser.getString(object, Tags.image_4_thumb);

                dealModel.total_deal_views = JSONParser.getString(object, Tags.total_deal_views);
                dealModel.logged_user_view_status = JSONParser.getString(object, Tags.logged_user_view_status);

                dealModel.is_grabbed = JSONParser.getString(object, Tags.is_grabbed);

//                    dealModel.sold_status = JSONParser.getString(object, Tags.sold_status);
//                    dealModel.status = JSONParser.getString(object, Tags.status);
                dealModel.created = JSONParser.getString(object, Tags.created);
//                    dealModel.modified = JSONParser.getString(object, Tags.modified);
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showAlert(DealDetailsActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(DealDetailsActivity.this, "Server Error");
        }
    }

    private void parseDealsResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                asyncExecuting = false;
            } else {
                try {
                    dealModel.total_deal_views = (AppDelegate.getIntValue(dealModel.total_deal_views) + 1) + "";
                    dealModel.logged_user_view_status = "1";
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                setValues();
                updateDeals(dealModel);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void updateDeals(DealModel dealModel) {
        String total_deal_views, logged_user_view_status;
        total_deal_views = DealDetailsActivity.dealModel.total_deal_views;
        logged_user_view_status = DealDetailsActivity.dealModel.logged_user_view_status;

        DealDetailsActivity.dealModel = dealModel;
        AppDelegate.LogT("DealDetailsActivity.dealModel => " + DealDetailsActivity.dealModel.image_1 + ", " + DealDetailsActivity.dealModel.image_2 + ", " + DealDetailsActivity.dealModel.image_3 + ", " + DealDetailsActivity.dealModel.image_4);
        DealDetailsActivity.dealModel.total_deal_views = total_deal_views;
        DealDetailsActivity.dealModel.logged_user_view_status = logged_user_view_status;

        for (int i = 0; i < HomeFragment.dealArray.size(); i++) {
            if (HomeFragment.dealArray.get(i).id.equalsIgnoreCase(DealDetailsActivity.dealModel.id)) {
                HomeFragment.dealArray.remove(i);
                HomeFragment.dealArray.add(i, DealDetailsActivity.dealModel);
                AppDelegate.LogT("added at HomeFragment at position => " + i);
                break;
            }
        }
        for (int i = 0; i < MyDealListingFragment.dealArray.size(); i++) {
            if (MyDealListingFragment.dealArray.get(i).id.equalsIgnoreCase(DealDetailsActivity.dealModel.id)) {
                MyDealListingFragment.dealArray.remove(i);
                MyDealListingFragment.dealArray.add(i, DealDetailsActivity.dealModel);
                AppDelegate.LogT("added at MyProfileFragment at position => " + i);
                break;
            }
        }
        for (int i = 0; i < DealsMoviesFragment.dealArray.size(); i++) {
            if (DealsMoviesFragment.dealArray.get(i).id.equalsIgnoreCase(DealDetailsActivity.dealModel.id)) {
                DealsMoviesFragment.dealArray.remove(i);
                DealsMoviesFragment.dealArray.add(i, DealDetailsActivity.dealModel);
                AppDelegate.LogT("added at MyProductListFragment at position => " + i);
                break;
            }
        }
    }

    private void parseGrabResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                AppDelegate.showToast(DealDetailsActivity.this, jsonObject.getString(Tags.message));
                dealModel.coupon_no = jsonObject.getJSONObject(Tags.response).getString(Tags.coupon_no);
                dealModel.is_grabbed = "1";
                setValues();
                updateDeals(dealModel);
            } else {
                AppDelegate.showAlert(DealDetailsActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(DealDetailsActivity.this, "Server Error");
        }
    }
}

