package com.stux.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.stux.AppDelegate;
import com.stux.Async.LocationAddress;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnPictureResult;
import com.stux.interfaces.OnReciveServerResponse;
import com.yalantis.ucrop.UCrop;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

/**
 * Created by Bharat on 08/01/2016.
 */
public class AddItemFoundActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnPictureResult, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private Prefs prefs;
    private UserDataModel dataModel;
    private Handler mHandler;

    private EditText et_item_name, et_item_description, et_location_found, et_campus_name, et_contact;
    private LinearLayout ll_c_upload_pic, ll_c_img_layout;
    private RelativeLayout rl_c_pic_0, rl_c_pic_1, rl_c_pic_2, rl_c_pic_3;
    private ImageView img_c_pic_0, img_c_pic_1, img_c_pic_2, img_c_pic_3, img_c_close_0, img_c_close_1, img_c_close_2, img_c_close_3;

    private ArrayList<File> arrayImageFile = new ArrayList<>();

    public static File capturedFile;
    public static Uri imageURI = null;
//    public static OnPictureResult onPictureResult;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.add_item_found);
        mGoogleApiClient = new GoogleApiClient.Builder(AddItemFoundActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        prefs = new Prefs(AddItemFoundActivity.this);
        dataModel = prefs.getUserdata();
        initView();
        setHandler();
        showGPSalert();
    }

    private void showGPSalert() {
        try {
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(AddItemFoundActivity.this)
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this).build();
                mGoogleApiClient.connect();
            }
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    AppDelegate.LogT("state => " + state + ", status = " + status);
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                if (status != null)
                                    status.startResolutionForResult(AddItemFoundActivity.this, 1000);
                                else
                                    AppDelegate.showToast(AddItemFoundActivity.this, "Something wrong with your GPS please try again later");
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }

            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(AddItemFoundActivity.this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void requestForLocationUpdate() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setInterval(100); // Update location every second
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("request for Location update = " + mCurrentLocation);
        if (!findAddressCalled && mCurrentLocation != null) {
            setLatLngAndFindAddress(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 100);
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location != null)
            mCurrentLocation = location;
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        if (!findAddressCalled) {
            setLatLngAndFindAddress(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 100);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        AppDelegate.LogT("onConnected called");
        requestForLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        AppDelegate.LogT("onConnectionSuspended called");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppDelegate.LogT("onConnectionFailed called");
    }

    private CountDownTimer countDownTimer;
    private String city_name = "";
    boolean findAddressCalled = false;

    public void setLatLngAndFindAddress(final LatLng latLng, long countDownTime) {
        AppDelegate.LogT("setLatLngAndFindAddress called");
        findAddressCalled = true;
        countDownTimer = new CountDownTimer(countDownTime, countDownTime) {

            @Override
            public void onTick(long millisUntilFinished) {
                AppDelegate.LogT("timer = " + millisUntilFinished);
            }

            @Override
            public void onFinish() {
                LatLng arg0 = AppDelegate.getRoundedLatLng(latLng);
                mHandler.sendEmptyMessage(10);
                LocationAddress.getAddressFromLocation(
                        arg0.latitude, arg0.longitude,
                        AddItemFoundActivity.this, mHandler);
            }
        };
        countDownTimer.start();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(AddItemFoundActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(AddItemFoundActivity.this);
                        break;
                    case 2:
                        setResultFromGeoCoderApi(msg.getData());
                        break;
                }
            }
        };
    }

    private boolean onceCalled = false;

    public void setResultFromGeoCoderApi(Bundle bundle) {
        if (bundle.getString(Tags.PLACE_NAME) != null) {
            city_name = bundle.getString(Tags.PLACE_ADD);
            et_location_found.setText(city_name);
            mHandler.sendEmptyMessage(11);
        } else if (!onceCalled) {
            onceCalled = true;
            setLatLngAndFindAddress(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 100);
        } else {
            mHandler.sendEmptyMessage(11);
            et_location_found.setEnabled(false);
            AppDelegate.showToast(AddItemFoundActivity.this, "Location not available or something went wrong with server, Please try again later.");
        }
    }

    private void initView() {
        ((TextView) findViewById(R.id.txt_c_header)).setText("Add Item Found");
        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        findViewById(R.id.img_c_right).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.txt_c_right)).setText(" Add ");
        findViewById(R.id.txt_c_right).setVisibility(View.VISIBLE);
        findViewById(R.id.txt_c_right).setOnClickListener(this);

        et_item_name = (EditText) findViewById(R.id.et_item_name);
        et_item_name.setTypeface(Typeface.createFromAsset(AddItemFoundActivity.this.getAssets(), getString(R.string.font_roman)));
        et_item_description = (EditText) findViewById(R.id.et_item_description);
        et_item_description.setTypeface(Typeface.createFromAsset(AddItemFoundActivity.this.getAssets(), getString(R.string.font_roman)));
        et_location_found = (EditText) findViewById(R.id.et_location_found);
        et_location_found.setTypeface(Typeface.createFromAsset(AddItemFoundActivity.this.getAssets(), getString(R.string.font_roman)));
        et_location_found.setEnabled(false);
        et_campus_name = (EditText) findViewById(R.id.et_campus_name);
        et_campus_name.setTypeface(Typeface.createFromAsset(AddItemFoundActivity.this.getAssets(), getString(R.string.font_roman)));
        et_contact = (EditText) findViewById(R.id.et_contact);
        et_contact.setTypeface(Typeface.createFromAsset(AddItemFoundActivity.this.getAssets(), getString(R.string.font_roman)));

        ll_c_img_layout = (LinearLayout) findViewById(R.id.ll_c_img_layout);
        ll_c_img_layout.setVisibility(View.GONE);
        rl_c_pic_0 = (RelativeLayout) findViewById(R.id.rl_c_pic_0);
        rl_c_pic_0.setVisibility(View.INVISIBLE);
        rl_c_pic_1 = (RelativeLayout) findViewById(R.id.rl_c_pic_1);
        rl_c_pic_1.setVisibility(View.INVISIBLE);
        rl_c_pic_2 = (RelativeLayout) findViewById(R.id.rl_c_pic_2);
        rl_c_pic_2.setVisibility(View.INVISIBLE);
        rl_c_pic_3 = (RelativeLayout) findViewById(R.id.rl_c_pic_3);
        rl_c_pic_3.setVisibility(View.INVISIBLE);

        img_c_pic_0 = (ImageView) findViewById(R.id.img_c_pic_0);
        img_c_pic_1 = (ImageView) findViewById(R.id.img_c_pic_1);
        img_c_pic_2 = (ImageView) findViewById(R.id.img_c_pic_2);
        img_c_pic_3 = (ImageView) findViewById(R.id.img_c_pic_3);

        img_c_close_0 = (ImageView) findViewById(R.id.img_c_close_0);
        img_c_close_0.setOnClickListener(this);
        img_c_close_1 = (ImageView) findViewById(R.id.img_c_close_1);
        img_c_close_1.setOnClickListener(this);
        img_c_close_2 = (ImageView) findViewById(R.id.img_c_close_2);
        img_c_close_2.setOnClickListener(this);
        img_c_close_3 = (ImageView) findViewById(R.id.img_c_close_3);
        img_c_close_3.setOnClickListener(this);

        findViewById(R.id.ll_c_upload_pic).setOnClickListener(this);

    }

    private void removePicUpdate(int i) {
        switch (i) {
            case 0:
                if (arrayImageFile.size() > 0) {
                    arrayImageFile.remove(0);
                }
                break;
            case 1:
                if (arrayImageFile.size() > 1) {
                    arrayImageFile.remove(1);
                }
                break;
            case 2:
                if (arrayImageFile.size() > 2) {
                    arrayImageFile.remove(2);
                }
                break;
            case 3:
                if (arrayImageFile.size() > 3) {
                    arrayImageFile.remove(3);
                }
                break;
        }
        updateImageView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                onBackPressed();
                break;

            case R.id.txt_c_right:
                execute_addLostFoundItem();
                break;

            case R.id.ll_c_upload_pic:
                if (arrayImageFile.size() < 4) {
                    showImageSelectorList();
                } else {
                    AppDelegate.showAlert(AddItemFoundActivity.this, "You can't upload more than 4 image.");
                }
                break;

            case R.id.img_c_close_0:
                removePicUpdate(0);
                break;
            case R.id.img_c_close_1:
                removePicUpdate(1);
                break;
            case R.id.img_c_close_2:
                removePicUpdate(2);
                break;
            case R.id.img_c_close_3:
                removePicUpdate(3);
                break;
        }
    }

    private void execute_addLostFoundItem() {
        if (et_item_name.length() == 0) {
            AppDelegate.showAlert(AddItemFoundActivity.this, "Please enter item name.");
        } else if (et_item_description.length() == 0) {
            AppDelegate.showAlert(AddItemFoundActivity.this, "Please enter item description.");
        } else if (et_location_found.length() == 0) {
            AppDelegate.showAlert(AddItemFoundActivity.this, "Please enable GPS to detect your location.");
        } else if (et_campus_name.length() == 0) {
            AppDelegate.showAlert(AddItemFoundActivity.this, "Please enter campus name.");
        } else if (et_contact.length() == 0) {
            AppDelegate.showAlert(AddItemFoundActivity.this, "Please enter your contact detail.");
        } else if (arrayImageFile.size() == 0) {
            AppDelegate.showAlert(AddItemFoundActivity.this, "Please add image.");
        } else if (!AppDelegate.haveNetworkConnection(AddItemFoundActivity.this, false)) {
            startActivity(new Intent(AddItemFoundActivity.this, NoInternetConnectionActivity.class));
        } else {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.item_name, et_item_name.getText().toString());
            AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.item_description, et_item_description.getText().toString());
            AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.location, et_location_found.getText().toString());
            AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.campus_name, et_campus_name.getText().toString());
            AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.contact_no, et_contact.getText().toString() + "");
            AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.latitude, mCurrentLocation.getLatitude() + "");
            AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.longitude, mCurrentLocation.getLongitude() + "");

            if (arrayImageFile.size() == 4) {
                AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.image_1, arrayImageFile.get(0).getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
                AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.image_2, arrayImageFile.get(1).getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
                AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.image_3, arrayImageFile.get(2).getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
                AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.image_4, arrayImageFile.get(3).getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            } else if (arrayImageFile.size() == 3) {
                AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.image_1, arrayImageFile.get(0).getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
                AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.image_2, arrayImageFile.get(1).getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
                AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.image_3, arrayImageFile.get(2).getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            } else if (arrayImageFile.size() == 2) {
                AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.image_1, arrayImageFile.get(0).getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
                AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.image_2, arrayImageFile.get(1).getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            } else if (arrayImageFile.size() == 1) {
                AppDelegate.getInstance(AddItemFoundActivity.this).setPostParamsSecond(mPostArrayList, Tags.image_1, arrayImageFile.get(0).getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            }

            PostAsync mPostAsyncObj = new PostAsync(AddItemFoundActivity.this, this, ServerRequestConstants.ADD_ITEM_FOUND,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            startActivity(new Intent(AddItemFoundActivity.this, NoInternetConnectionActivity.class));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.ADD_ITEM_FOUND)) {
            mHandler.sendEmptyMessage(11);
            parseAddItemResult(result);
        }
    }

    private void parseAddItemResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {

//                JSONObject object = jsonObject.getJSONObject(Tags.response);
//                ItemFoundModel itemModel = new ItemFoundModel();
//                itemModel.id = JSONParser.getString(object, Tags.id);
//                itemModel.user_id = JSONParser.getString(object, Tags.user_id);
//                itemModel.item_name = object.getString(Tags.item_name);
//                itemModel.item_description = object.getString(Tags.item_description);
//                itemModel.location = JSONParser.getString(object, Tags.location);
//                itemModel.campus_name = JSONParser.getString(object, Tags.campus_name);
//                itemModel.contact_no = JSONParser.getString(object, Tags.contact_no);
//                itemModel.image_1 = JSONParser.getString(object, Tags.image_1);
//                itemModel.image_thumb_1 = JSONParser.getString(object, Tags.image_thumb);
//                itemModel.image_2 = JSONParser.getString(object, Tags.image_2);
//                itemModel.image_thumb_2 = JSONParser.getString(object, Tags.image_thumb);
//                itemModel.image_3 = JSONParser.getString(object, Tags.image_3);
//                itemModel.image_thumb_3 = JSONParser.getString(object, Tags.image_thumb);
//                itemModel.image_4 = JSONParser.getString(object, Tags.image_4);
//                itemModel.image_thumb_4 = JSONParser.getString(object, Tags.image_thumb);
//
//                itemModel.status = JSONParser.getString(object, Tags.status);
//                itemModel.is_view = JSONParser.getString(object, Tags.is_view);
//                itemModel.created = JSONParser.getString(object, Tags.created);
//                itemModel.contact_no = JSONParser.getString(object, Tags.contact_no);
//
//                itemModel.lat = JSONParser.getString(object, Tags.latitude);
//                itemModel.lng = JSONParser.getString(object, Tags.longitude);
//
//                itemModel.userModel = prefs.getUserdata();

                if (LostFoundItemActivity.mHandler != null) {
                    LostFoundItemActivity.mHandler.sendEmptyMessage(3);
                }
                AppDelegate.showToast(AddItemFoundActivity.this, jsonObject.getString(Tags.MESSAGE));
                finish();
            } else {
                AppDelegate.showAlert(AddItemFoundActivity.this, jsonObject.getString(Tags.MESSAGE));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(AddItemFoundActivity.this, "Server Error");
        }
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(AddItemFoundActivity.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(AddItemFoundActivity.this);
        ListView modeList = new ListView(AddItemFoundActivity.this);
        String[] stringArray = new String[]{"  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(AddItemFoundActivity.this, R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        AddItemFoundActivity.this.startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), AppDelegate.SELECT_PICTURE);
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(AddItemFoundActivity.this, "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            AddItemFoundActivity.this.startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }

    @Override
    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            try {
                Bitmap OriginalPhoto = MediaStore.Images.Media.getBitmap(AddItemFoundActivity.this.getContentResolver(), picUri);
                capturedFile = new File(getNewFile());
                FileOutputStream fOut = null;
                try {
                    fOut = new FileOutputStream(capturedFile);
                } catch (FileNotFoundException e) {
                    AppDelegate.LogE(e);
                }
                OriginalPhoto.compress(Bitmap.CompressFormat.JPEG, 30, fOut);
                try {
                    fOut.flush();
                } catch (IOException e) {
                    AppDelegate.LogE(e);
                }
                try {
                    fOut.close();
                } catch (IOException e) {
                    AppDelegate.LogE(e);
                }
                arrayImageFile.add(capturedFile);
                updateImageView();
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
    }

    private void setVisiblePic(int value) {
        rl_c_pic_0.setVisibility(View.INVISIBLE);
        rl_c_pic_1.setVisibility(View.INVISIBLE);
        rl_c_pic_2.setVisibility(View.INVISIBLE);
        rl_c_pic_3.setVisibility(View.INVISIBLE);
        switch (value) {
            case 0:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                break;
            case 1:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                rl_c_pic_1.setVisibility(View.VISIBLE);
                break;
            case 2:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                rl_c_pic_1.setVisibility(View.VISIBLE);
                rl_c_pic_2.setVisibility(View.VISIBLE);
                break;
            case 3:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                rl_c_pic_1.setVisibility(View.VISIBLE);
                rl_c_pic_2.setVisibility(View.VISIBLE);
                rl_c_pic_3.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void updateImageView() {
        ll_c_img_layout.setVisibility(View.VISIBLE);
        if (arrayImageFile.size() == 1) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            setVisiblePic(0);
        } else if (arrayImageFile.size() == 2) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            img_c_pic_1.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(1).getAbsolutePath()));
            setVisiblePic(1);
        } else if (arrayImageFile.size() == 3) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            img_c_pic_1.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(1).getAbsolutePath()));
            img_c_pic_2.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(02).getAbsolutePath()));
            setVisiblePic(2);
        } else if (arrayImageFile.size() == 4) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            img_c_pic_1.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(01).getAbsolutePath()));
            img_c_pic_2.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(02).getAbsolutePath()));
            img_c_pic_3.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(03).getAbsolutePath()));
            setVisiblePic(3);
        } else {
            ll_c_img_layout.setVisibility(View.GONE);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = AddItemFoundActivity.this.getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("onActivityResult AddItemFoundActivity");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppDelegate.SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(AddItemFoundActivity.this, data.getData());
                } else {
                    Toast.makeText(AddItemFoundActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            } else if (requestCode == AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE) {
                if (AddItemFoundActivity.imageURI != null) {
                    startCropActivity(AddItemFoundActivity.this, AddItemFoundActivity.imageURI);
                } else {
                    Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            setOnReceivePictureResult(Tags.PICTURE, resultUri);
        } else {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    public static void startCropActivity(FragmentActivity mActivity, Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".png";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(mActivity.getCacheDir(), destinationFileName)));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);
        uCrop.start(mActivity);
    }

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop basisConfig(@NonNull UCrop uCrop) {
        uCrop = uCrop.useSourceImageAspectRatio();
        return uCrop;
    }

    /**
     * Sometimes you want to adjust more options, it's done via {@link com.yalantis.ucrop.UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setCompressionQuality(100);
        options.setHideBottomControls(false);
        options.setFreeStyleCropEnabled(true);
        return uCrop.withOptions(options);
    }
}
