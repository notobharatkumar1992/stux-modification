package com.stux.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.fragments.NoInternetConnectionFragment;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 07/27/2016.
 */
public class ProductReasonOtherActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {

    public Prefs prefs;
    public UserDataModel dataModel;
    public Handler mHandler;
    public Activity mActivity;

    private EditText et_description;
    private ProductModel productModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.reason_other);
        mActivity = this;
        prefs = new Prefs(ProductReasonOtherActivity.this);
        dataModel = prefs.getUserdata();
        productModel = getIntent().getExtras().getParcelable(Tags.product);
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(ProductReasonOtherActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(ProductReasonOtherActivity.this);
                }
            }
        };
    }

    private void initView() {
        ((TextView) findViewById(R.id.txt_c_header)).setText("Other");
        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        findViewById(R.id.img_c_right).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.txt_c_right)).setText("Publish");
        findViewById(R.id.txt_c_right).setVisibility(View.GONE);
        findViewById(R.id.txt_c_right).setOnClickListener(this);


        et_description = (EditText) findViewById(R.id.et_description);
        et_description.setTypeface(Typeface.createFromAsset(ProductReasonOtherActivity.this.getAssets(), getString(R.string.font_roman)));

        findViewById(R.id.txt_c_submit).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                onBackPressed();
                break;

            case R.id.txt_c_submit:
                execute_contactUsAsync();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    private void execute_contactUsAsync() {
        if (et_description.length() == 0) {
            AppDelegate.showAlert(ProductReasonOtherActivity.this, "Please enter description.");
        } else if (!AppDelegate.haveNetworkConnection(ProductReasonOtherActivity.this, false)) {
            AppDelegate.addFragment(ProductReasonOtherActivity.this.getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        } else {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(ProductReasonOtherActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(ProductReasonOtherActivity.this).setPostParamsSecond(mPostArrayList, Tags.username, dataModel.first_name);
            AppDelegate.getInstance(ProductReasonOtherActivity.this).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            AppDelegate.getInstance(ProductReasonOtherActivity.this).setPostParamsSecond(mPostArrayList, Tags.description, et_description.getText().toString());

            PostAsync mPostAsyncObj = new PostAsync(ProductReasonOtherActivity.this, this, ServerRequestConstants.REPORT_PRODUCT,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (mActivity != null)
                startActivity(new Intent(ProductReasonOtherActivity.this, NoInternetConnectionActivity.class));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.REPORT_PRODUCT)) {
            mHandler.sendEmptyMessage(11);
            parseContactUsResult(result);
        }
    }

    private void parseContactUsResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                AppDelegate.showToast(ProductReasonOtherActivity.this, "Thanks for your report.");
                if (ProductReasonActivity.mActivity != null)
                    ProductReasonActivity.mActivity.finish();
                finish();
            } else {
                AppDelegate.showAlert(ProductReasonOtherActivity.this, jsonObject.getString(Tags.MESSAGE));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
}
