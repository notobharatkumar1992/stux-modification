package com.stux.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.stux.Adapters.SpinnerFavCategoryAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.GCMClientManager;
import com.stux.Models.FavCategoryModel;
import com.stux.Models.InstitutionModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DateUtils;
import com.stux.Utils.Prefs;
import com.stux.Utils.TransitionHelper;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.fragments.MyProfileFragment;
import com.stux.interfaces.OnPictureResult;
import com.stux.interfaces.OnReciveServerResponse;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

/**
 * Created by Bharat on 06/17/2016.
 */
public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnPictureResult {

    private Activity mActivity;
    private EditText et_first_name, et_last_name;
    private TextView txt_c_place, txt_c_dob, txt_c_male, txt_c_female, txt_c_change_email;

    private Spinner spn_fav_category;

    private SpinnerFavCategoryAdapter adapterFavCategory;

    public ArrayList<FavCategoryModel> arrayFavCategoryModels = new ArrayList<>();
    private Prefs prefs;
    private UserDataModel dataModel;
    private Handler mHandler;

    private ImageView img_c_profile;

    private Bitmap OriginalPhoto;

    public static File capturedFile;
    public static Uri imageURI = null;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.edit_profile);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        mActivity = this;
        prefs = new Prefs(EditProfileActivity.this);
        dataModel = prefs.getUserdata();
        initView();
        setHandler();
        setValues();
        execute_getFavCategoryApi();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    private void setValues() {
        if (dataModel != null) {
            et_first_name.setText(dataModel.first_name);
            et_last_name.setText(dataModel.last_name);
            if (AppDelegate.isValidString(prefs.getUserdata().image)) {
                imageLoader.loadImage(prefs.getUserdata().image, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                        if (bitmap != null) {
                            img_c_profile.setImageBitmap(bitmap);
                            OriginalPhoto = bitmap;
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });

//                Picasso.with(EditProfileActivity.this).load(prefs.getUserdata().image).into(new Target() {
//                    @Override
//                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
//                        if (bitmap != null) {
//                            img_c_profile.setImageBitmap(bitmap);
//                            OriginalPhoto = bitmap;
//                        }
//                    }
//
//                    @Override
//                    public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                    }
//
//                    @Override
//                    public void onBitmapFailed(Drawable errorDrawable) {
//
//                    }
//                });
            }
            if (dataModel.str_Gender.equals("1") || dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                selected_gender = Tags.Male;
                txt_c_male.setSelected(true);
                txt_c_female.setSelected(false);
                txt_c_male.setTextColor(getResources().getColor(R.color.profile_desc));
                txt_c_female.setTextColor(getResources().getColor(R.color.hint_color));
            } else {
                selected_gender = Tags.Female;
                txt_c_male.setSelected(false);
                txt_c_female.setSelected(true);
                txt_c_male.setTextColor(getResources().getColor(R.color.hint_color));
                txt_c_female.setTextColor(getResources().getColor(R.color.profile_desc));
            }

            try {
                if (AppDelegate.isValidString(dataModel.dob)) {
//                    txt_c_dob.setText(AppDelegate.isValidString(dataModel.dob) ? dataModel.dob : "");
                    txt_c_dob.setText(new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(dataModel.dob)));
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
                txt_c_dob.setText("");
            }

            if (arrayFavCategoryModels.size() > 0 && AppDelegate.isValidString(dataModel.fav_cat_id)) {
                for (int i = 0; i < arrayFavCategoryModels.size(); i++) {
                    FavCategoryModel favCategoryModel = arrayFavCategoryModels.get(i);
                    if (favCategoryModel.id.equalsIgnoreCase(dataModel.fav_cat_id)) {
                        spn_fav_category.setSelection(i);
                        break;
                    }
                }
            }
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(EditProfileActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(EditProfileActivity.this);
                } else if (msg.what == 1) {
                    adapterFavCategory.notifyDataSetChanged();
                    spn_fav_category.invalidate();

                    if (arrayFavCategoryModels.size() > 0 && AppDelegate.isValidString(dataModel.fav_cat_id)) {
                        for (int i = 0; i < arrayFavCategoryModels.size(); i++) {
                            FavCategoryModel favCategoryModel = arrayFavCategoryModels.get(i);
                            if (favCategoryModel.id.equalsIgnoreCase(dataModel.fav_cat_id)) {
                                spn_fav_category.setSelection(i);
                                break;
                            }
                        }
                    }
                }
            }
        };
    }

    private void initView() {
        ((TextView) findViewById(R.id.txt_c_header)).setText(getString(R.string.Edit_Profile));
        findViewById(R.id.img_c_left).setOnClickListener(this);
        ((ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        ((ImageView) findViewById(R.id.img_c_left)).setVisibility(View.VISIBLE);

        et_first_name = (EditText) findViewById(R.id.et_first_name);
        et_first_name.setTypeface(Typeface.createFromAsset(EditProfileActivity.this.getAssets(), getString(R.string.font_HN_bold)));
        et_last_name = (EditText) findViewById(R.id.et_last_name);
        et_last_name.setTypeface(Typeface.createFromAsset(EditProfileActivity.this.getAssets(), getString(R.string.font_HN_bold)));

        txt_c_place = (TextView) findViewById(R.id.txt_c_place);
        txt_c_dob = (TextView) findViewById(R.id.txt_c_dob);
        txt_c_dob.setOnClickListener(this);
        txt_c_male = (TextView) findViewById(R.id.txt_c_male);
        txt_c_male.setOnClickListener(this);
        txt_c_female = (TextView) findViewById(R.id.txt_c_female);
        txt_c_female.setOnClickListener(this);
        findViewById(R.id.ll_c_change_password).setOnClickListener(this);
        txt_c_change_email = (TextView) findViewById(R.id.txt_c_change_email);

        spn_fav_category = (Spinner) findViewById(R.id.spn_fav_category);
        adapterFavCategory = new SpinnerFavCategoryAdapter(EditProfileActivity.this, arrayFavCategoryModels);
        spn_fav_category.setAdapter(adapterFavCategory);
        spn_fav_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_fav_category_position = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        img_c_profile = (ImageView) findViewById(R.id.img_c_profile);
        img_c_profile.setOnClickListener(this);

        findViewById(R.id.txt_c_continue).setOnClickListener(this);
    }

    int selected_fav_category_position = 0;

    private void execute_updateProfileApi() {
        if (et_first_name.length() == 0) {
            AppDelegate.showAlert(EditProfileActivity.this, EditProfileActivity.this.getString(R.string.Alert_First_Name));
        } else if (et_last_name.length() == 0) {
            AppDelegate.showAlert(EditProfileActivity.this, EditProfileActivity.this.getString(R.string.Alert_Last_name));
        } else if (OriginalPhoto == null) {
            AppDelegate.showAlert(EditProfileActivity.this, "Please select image.");
        } else if (txt_c_dob.length() == 0) {
            AppDelegate.showAlert(EditProfileActivity.this, "Please select your Date of Birth.");
        } else if (selected_fav_category_position == 0) {
            AppDelegate.showAlert(EditProfileActivity.this, "Please select Favourite Category.");
        } else if (AppDelegate.haveNetworkConnection(EditProfileActivity.this, false)) {
            writeImageFile();
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.first_name, et_first_name.getText().toString());
            AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.last_name, et_last_name.getText().toString());
            AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.email, dataModel.email);

            if (txt_c_dob.length() > 0) {
                String time = "";
                try {
                    time = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd MMM yyyy").parse(txt_c_dob.getText().toString()));
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.dob, time);
            }
            AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.gender, selected_gender);

//            AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.city_name, "");

            AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.fav_cat_id, arrayFavCategoryModels.get(selected_fav_category_position).id + "");
            AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.image, capturedFile.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.gcm_token, GCMClientManager.getRegistrationId(EditProfileActivity.this));

            PostAsync mPostasyncObj = new PostAsync(EditProfileActivity.this,
                    EditProfileActivity.this, ServerRequestConstants.EDIT_PROFILE,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            startActivity(new Intent(EditProfileActivity.this, NoInternetConnectionActivity.class));
        }
    }

    private void execute_getFavCategoryApi() {
        if (AppDelegate.haveNetworkConnection(EditProfileActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            PostAsync mPostasyncObj = new PostAsync(EditProfileActivity.this,
                    EditProfileActivity.this, ServerRequestConstants.FAV_CATEGORY,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            startActivity(new Intent(EditProfileActivity.this, NoInternetConnectionActivity.class));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                onBackPressed();
                break;

            case R.id.img_c_profile:
                showImageSelectorList();
                break;

            case R.id.txt_c_dob:
                showDatePickerDialog();
                break;

            case R.id.txt_c_continue:
                execute_updateProfileApi();
                break;

            case R.id.txt_c_male:
                selected_gender = Tags.Male;
                txt_c_male.setSelected(true);
                txt_c_female.setSelected(false);
                txt_c_male.setTextColor(getResources().getColor(R.color.profile_desc));
                txt_c_female.setTextColor(getResources().getColor(R.color.hint_color));
                break;

            case R.id.txt_c_female:
                selected_gender = Tags.Female;
                txt_c_male.setSelected(false);
                txt_c_female.setSelected(true);
                txt_c_male.setTextColor(getResources().getColor(R.color.hint_color));
                txt_c_female.setTextColor(getResources().getColor(R.color.profile_desc));
                break;

            case R.id.ll_c_change_password:
                Intent intent = new Intent(EditProfileActivity.this, ChangePasswordActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(EditProfileActivity.this, false, new Pair<>(findViewById(R.id.txt_c_change_password), "title"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(EditProfileActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;


        }
    }

    private String selected_gender = Tags.Male;

    private DatePickerDialog datePickerDialog;
    private int year = -1, month = -1, day = -1;
    private Calendar calendar;

    private void showDatePickerDialog() {
        calendar = Calendar.getInstance();
        if (AppDelegate.isValidString(txt_c_dob.getText().toString())) {
            try {
                calendar.setTime(new SimpleDateFormat("dd MMM yyyy").parse(txt_c_dob.getText().toString()));
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }
        }
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(EditProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                if (DateUtils.isAfterDay(Calendar.getInstance(), calendar)) {
                    EditProfileActivity.this.year = year;
                    EditProfileActivity.this.month = monthOfYear;
                    EditProfileActivity.this.day = dayOfMonth;
                    txt_c_dob.setText(new SimpleDateFormat("dd MMM yyyy").format(calendar.getTime()));
                } else {
                    AppDelegate.showAlert(EditProfileActivity.this, "Date of birth can't be equal to or greater than Today.");
                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(EditProfileActivity.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        ListView modeList = new ListView(EditProfileActivity.this);
        String[] stringArray = new String[]{"  Avatar", "  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(EditProfileActivity.this, R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        showAvatarAlert();
                        break;
                    case 1:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 2:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 3:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    private void showAvatarAlert() {
        try {
            final AlertDialog.Builder mAlert = new AlertDialog.Builder(EditProfileActivity.this);
            mAlert.setCancelable(true);
            mAlert.setTitle("Use image");
            LinearLayout ll_c_img_01, ll_c_img_02;
            ImageView img_01, img_02;
            View view = ((LayoutInflater) EditProfileActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_default_user_image, null, false);

            img_01 = (ImageView) view.findViewById(R.id.img_01);
            img_02 = (ImageView) view.findViewById(R.id.img_02);

            if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                img_01.setImageResource(R.drawable.user_male_1);
                img_02.setImageResource(R.drawable.user_male_2);
            } else {
                img_01.setImageResource(R.drawable.user_female_1);
                img_02.setImageResource(R.drawable.user_female_2);
            }

            ll_c_img_01 = (LinearLayout) view.findViewById(R.id.ll_c_img_01);
            ll_c_img_02 = (LinearLayout) view.findViewById(R.id.ll_c_img_02);
            mAlert.setView(view);
            mAlert.setNegativeButton(
                    Tags.CANCEL,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            final AlertDialog dialog = mAlert.show();

            ll_c_img_01.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    AppDelegate.LogT("ll_c_img_01 dataModel.str_Gender = " + dataModel.str_Gender);

                    if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_1);
                    } else {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_1);
                    }

                    img_c_profile.setImageBitmap(OriginalPhoto);
                }
            });
            ll_c_img_02.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    AppDelegate.LogT("ll_c_img_02 dataModel.str_Gender = " + dataModel.str_Gender);
                    if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_2);
                    } else {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_2);
                    }

                    img_c_profile.setImageBitmap(OriginalPhoto);
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public void writeImageFile() {
        if (capturedFile == null)
            capturedFile = new File(getNewFile());
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        OriginalPhoto.compress(Bitmap.CompressFormat.JPEG, 70, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        EditProfileActivity.this.startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), AppDelegate.SELECT_PICTURE);
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(EditProfileActivity.this, "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            EditProfileActivity.this.startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = EditProfileActivity.this.getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            try {
                OriginalPhoto = MediaStore.Images.Media.getBitmap(EditProfileActivity.this.getContentResolver(), picUri);
                OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 1000, 1000, true);
                img_c_profile.setImageBitmap(OriginalPhoto);
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (mActivity != null)
                startActivity(new Intent(EditProfileActivity.this, NoInternetConnectionActivity.class));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.FAV_CATEGORY)) {
            parseFavCategoryResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.EDIT_PROFILE)) {
            parseUpdateProfileResult(result);
        }
    }

    private void parseUpdateProfileResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.role = object.getString(Tags.role);
                userDataModel.status = object.getString(Tags.status);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.role = object.getString(Tags.role);
                userDataModel.image = object.getString(Tags.image);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.gcm_token = object.getString(Tags.gcm_token);
                userDataModel.facebook_id = object.getString(Tags.social_id);
                userDataModel.login_type = object.getString(Tags.login_type);
                userDataModel.is_verified = object.getString(Tags.is_verified);

                userDataModel.following_count = object.getInt(Tags.following_count);
                userDataModel.followers_count = object.getInt(Tags.followers_count);
                userDataModel.total_product = object.getInt(Tags.total_product);

                JSONObject studentObject = object.getJSONObject(Tags.student_detail);

                userDataModel.str_Gender = studentObject.getString(Tags.gender);
                userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
                userDataModel.userId = studentObject.getString(Tags.user_id);
                userDataModel.dob = studentObject.getString(Tags.dob);
                userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

                InstitutionModel institutionModel = new InstitutionModel();
                institutionModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                    institutionModel.institution_name_id = studentObject.getString(Tags.institution_id);
                    institutionModel.institution_name = studentObject.getString(Tags.institution_name);
                } else {
                    institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
                }
                institutionModel.department_name = studentObject.getString(Tags.department_name);

                prefs.setUserData(userDataModel);
                prefs.setInstitutionModel(institutionModel);

//                if (MainActivity.mHandler != null) {
//                    MainActivity.mHandler.sendEmptyMessage(6);
//                    MainActivity.mHandler.sendEmptyMessage(7);
//                }
                if (MainActivity.onReciveServerResponse != null)
                    MainActivity.onReciveServerResponse.setOnReciveResult(Tags.editProfile, "");
                if (MyProfileFragment.mHandler != null) {
                    MyProfileFragment.mHandler.sendEmptyMessage(3);
                }
                onBackPressed();
            } else {
                AppDelegate.showAlert(EditProfileActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
//            AppDelegate.showAlert(EditProfileActivity.this, "Response is not proper. Please try again later.");
            if (mActivity != null)
                startActivity(new Intent(EditProfileActivity.this, NoInternetConnectionActivity.class));
            AppDelegate.LogE(e);
        }
    }

    private void parseFavCategoryResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                arrayFavCategoryModels.clear();
                arrayFavCategoryModels.add(new FavCategoryModel("Select Favourite Category"));
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    FavCategoryModel model = new FavCategoryModel();
                    model.id = object.getString(Tags.id);
                    model.name = object.getString(Tags.title);
                    arrayFavCategoryModels.add(model);
                }
            } else {
                AppDelegate.showAlert(EditProfileActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            if (mActivity != null)
                startActivity(new Intent(EditProfileActivity.this, NoInternetConnectionActivity.class));
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("onActivityResult AddItemFoundActivity");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppDelegate.SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(EditProfileActivity.this, data.getData());
                } else {
                    Toast.makeText(EditProfileActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            } else if (requestCode == AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE) {
                if (imageURI != null) {
                    startCropActivity(EditProfileActivity.this, imageURI);
                } else {
                    Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            setOnReceivePictureResult(Tags.PICTURE, resultUri);
        } else {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    public static void startCropActivity(FragmentActivity mActivity, Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".png";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(mActivity.getCacheDir(), destinationFileName)));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);
        uCrop.start(mActivity);
    }

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop basisConfig(@NonNull UCrop uCrop) {
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }

    /**
     * Sometimes you want to adjust more options, it's done via {@link com.yalantis.ucrop.UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setCompressionQuality(100);
        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);
        return uCrop.withOptions(options);
    }
}
