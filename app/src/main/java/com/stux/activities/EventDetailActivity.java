package com.stux.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ScrollView;

import com.google.android.gms.maps.MapsInitializer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.EventModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.Utils.Prefs;
import com.stux.Utils.TransitionHelper;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.fragments.EventsCampusBasedFragment;
import com.stux.fragments.MyEventListingFragment;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import carbon.widget.ImageView;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

/**
 * Created by Bharat on 06/03/2016.
 */
public class EventDetailActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {

    private Prefs prefs;
    private UserDataModel dataModel;
    private CircleImageView cimg_event;
    private ImageView img_c_event_banner, img_c_like;
    private android.widget.ImageView img_loading;
    private TextView txt_c_event_name, txt_c_campus, txt_c_contact, txt_c_like_count, txt_c_view_count, txt_c_loc_ph, txt_c_date_ph, txt_c_loc, txt_c_date, txt_c_ticket_type,
            txt_c_ticket_price, txt_c_detail_ph, txt_c_detail, txt_c_theme, txt_c_created_by, txt_c_name, txt_c_more;
    private RelativeLayout rl_c_img_layout;
    private ScrollView scrollView;
    public static Handler mHandler;
    private EventModel eventModel;

    private Bundle bundle;

    private boolean asyncExecuting = false;
    public String tagFrom = "";
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(EventDetailActivity.this);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.event_detail);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        prefs = new Prefs(EventDetailActivity.this);
        dataModel = prefs.getUserdata();
        eventModel = getIntent().getExtras().getParcelable(Tags.EVENT);
        tagFrom = getIntent().getExtras().getString(Tags.FROM);
        AppDelegate.LogT("EventDetailActivity tagFrom = " + tagFrom);
        initView();
        setHandler();
        setValus();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler = null;
    }

    private void executeItemViewApi() {
        if (AppDelegate.haveNetworkConnection(EventDetailActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(EventDetailActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(EventDetailActivity.this).setPostParamsSecond(mPostArrayList, Tags.event_id, eventModel.id);
            PostAsync mPostasyncObj = new PostAsync(EventDetailActivity.this,
                    EventDetailActivity.this, ServerRequestConstants.EVENT_VIEW,
                    mPostArrayList, null);
//            mHandler.sendEmptyMessage(10);
            asyncExecuting = true;
            mPostasyncObj.execute();
        } else {
            startActivity(new Intent(EventDetailActivity.this, NoInternetConnectionActivity.class));
        }
    }

    private void setValus() {
        if (eventModel != null) {
            if (AppDelegate.isValidString(eventModel.user_image)) {
                img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                frameAnimation.setCallback(img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                imageLoader.loadImage(eventModel.user_image, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        img_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                        cimg_event.setImageBitmap(bitmap);
                        img_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });

//                Picasso.with(EventDetailActivity.this).load(eventModel.user_image).into(cimg_event, new Callback() {
//                    @Override
//                    public void onSuccess() {
//                        img_loading.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onError() {
//                        img_loading.setVisibility(View.GONE);
//                    }
//                });
            }
            imageLoader.displayImage(eventModel.banner_image_thumb, img_c_event_banner);
//            Picasso.with(EventDetailActivity.this).load(eventModel.banner_image_thumb).into(img_c_event_banner);
            txt_c_event_name.setText(Html.fromHtml("<b>" + eventModel.event_name + "</b>"));
            if (eventModel.venue.contains("@")) {
                String location = eventModel.venue.substring(0, eventModel.venue.indexOf("@"));
                String venue = eventModel.venue.substring(eventModel.venue.indexOf("@") + 1, eventModel.venue.length());
                txt_c_loc.setText(location + " " + venue);
            } else {
                txt_c_loc.setText(eventModel.location + " " + eventModel.venue);
            }

            img_c_like.setSelected(eventModel.event_like_status.equalsIgnoreCase("1"));
            txt_c_like_count.setText(Html.fromHtml("<b>" + eventModel.total_event_likes + "</b>"));
            txt_c_view_count.setText(Html.fromHtml("<b>" + eventModel.total_event_views + "</b>"));

            if (eventModel.gate_fees_type == 0) {
                txt_c_ticket_type.setText(Html.fromHtml("<b>TICKET:</b>   Free"));
                txt_c_ticket_price.setVisibility(View.GONE);
            } else {
                txt_c_ticket_type.setText(Html.fromHtml("<b>TICKET:</b>   Paid"));
                txt_c_ticket_price.setVisibility(View.VISIBLE);
                txt_c_ticket_price.setText(Html.fromHtml("<b>TICKET PRICE:</b>   " + eventModel.gate_fees));
            }

            txt_c_name.setText(Html.fromHtml(eventModel.user_first_name + " " + eventModel.user_last_name));
            txt_c_created_by.setText(Html.fromHtml("<b>CREATED BY:</b>"));
            txt_c_loc_ph.setText(Html.fromHtml("<b>LOCATION</b>"));
            txt_c_date_ph.setText(Html.fromHtml("<b>DATE</b>"));
            txt_c_detail_ph.setText(Html.fromHtml("<b>EVENT DETAIL</b>"));
            if (AppDelegate.isValidString(eventModel.details)) {
                if (eventModel.details.length() > 50) {
                    txt_c_more.setVisibility(View.VISIBLE);
                    txt_c_detail.setText(Html.fromHtml(eventModel.details.substring(0, 50)));
                } else {
                    txt_c_more.setVisibility(View.GONE);
                    txt_c_detail.setText(Html.fromHtml(eventModel.details));
                }
            } else {
                txt_c_more.setVisibility(View.GONE);
            }
            txt_c_theme.setText(Html.fromHtml("<b>THEME:</b> " + eventModel.theme));

            txt_c_contact.setText(Html.fromHtml("<b>CONTACT NUMBER:</b> " + eventModel.contact_no));
            String time = eventModel.event_start_time.substring(0, eventModel.event_start_time.lastIndexOf("+"));
            String time2 = eventModel.event_end_time.substring(0, eventModel.event_end_time.lastIndexOf("+"));
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time);
                Date date2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time2);
                String thString = AppDelegate.getDayOfMonthSuffix(new SimpleDateFormat("dd").format(date)) + ",";
                time = new SimpleDateFormat("EEE':' MMM dd'" + thString + " ' yyyy" + "'\n'hh:mm aa").format(date);
                time2 = new SimpleDateFormat("hh:mm aa").format(date2);

                txt_c_date.setText(time + " - " + time2);
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }

//            txt_c_ticket_type, txt_c_ticket_price,,txt_c_detail,
            AppDelegate.LogT("Event Detail => id = " + eventModel.event_id + ", view_status = " + eventModel.logged_user_view_status);
            if (eventModel.logged_user_view_status.equalsIgnoreCase("0") && !asyncExecuting) {
                executeItemViewApi();
            }

            if (AppDelegate.isValidString(eventModel.location_type) && Integer.parseInt(eventModel.location_type) == 1) {
                txt_c_campus.setText("On Campus");
            } else {
                txt_c_campus.setText("Off Campus");
            }
        }
    }

    public void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(EventDetailActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(EventDetailActivity.this);
                        break;
                    case 3:
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onBackPressed();
                            }
                        }, 100);
                        break;
                    case 2:
                        setValus();
                        break;
                }
            }
        };
    }

    private void initView() {
        cimg_event = (CircleImageView) findViewById(R.id.cimg_event);
        img_c_event_banner = (ImageView) findViewById(R.id.img_c_event_banner);
        img_c_event_banner.setOnClickListener(this);

        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.txt_c_header)).setText("Event Details");
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(this);
        findViewById(R.id.img_c_right).setOnClickListener(this);

        if (eventModel != null && AppDelegate.isValidString(eventModel.user_id) && eventModel.user_id.equalsIgnoreCase(dataModel.userId)) {
            findViewById(R.id.img_c_right).setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.img_c_right)).setImageResource(R.drawable.edit);
        } else findViewById(R.id.img_c_right).setVisibility(View.GONE);

        txt_c_event_name = (TextView) findViewById(R.id.txt_c_event_name);
        txt_c_campus = (TextView) findViewById(R.id.txt_c_campus);
        txt_c_contact = (TextView) findViewById(R.id.txt_c_contact);

        txt_c_like_count = (TextView) findViewById(R.id.txt_c_like_count);
        txt_c_view_count = (TextView) findViewById(R.id.txt_c_view_count);

        txt_c_loc_ph = (TextView) findViewById(R.id.txt_c_loc_ph);
        txt_c_date_ph = (TextView) findViewById(R.id.txt_c_date_ph);
        txt_c_loc = (TextView) findViewById(R.id.txt_c_loc);
        txt_c_date = (TextView) findViewById(R.id.txt_c_date);
        txt_c_ticket_type = (TextView) findViewById(R.id.txt_c_ticket_type);
        txt_c_ticket_price = (TextView) findViewById(R.id.txt_c_ticket_price);
        txt_c_detail_ph = (TextView) findViewById(R.id.txt_c_detail_ph);
        txt_c_detail = (TextView) findViewById(R.id.txt_c_detail);
        txt_c_theme = (TextView) findViewById(R.id.txt_c_theme);
        txt_c_created_by = (TextView) findViewById(R.id.txt_c_created_by);
        txt_c_name = (TextView) findViewById(R.id.txt_c_name);
        txt_c_more = (TextView) findViewById(R.id.txt_c_more);
        SpannableString content = new SpannableString("More");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        txt_c_more.setText(content);
        txt_c_more.setOnClickListener(this);

        img_c_like = (ImageView) findViewById(R.id.img_c_like);

        rl_c_img_layout = (RelativeLayout) findViewById(R.id.rl_c_img_layout);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        img_loading = (android.widget.ImageView) findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);

        rl_c_img_layout.getLayoutParams().height = AppDelegate.getDeviceWith(EventDetailActivity.this) / 2 + AppDelegate.dpToPix(EventDetailActivity.this, 60);
        rl_c_img_layout.requestLayout();

        findViewById(R.id.ll_c_like).setOnClickListener(this);
        findViewById(R.id.ll_c_flag).setOnClickListener(this);

        findViewById(R.id.txt_c_location).setOnClickListener(this);
    }

    private Intent intent;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                onBackPressed();
                break;

            case R.id.img_c_right:
                intent = new Intent(EventDetailActivity.this, CreateEventActivity.class);
                bundle = new Bundle();
                bundle.putParcelable(Tags.EVENT, eventModel);
                intent.putExtra(Tags.FROM, tagFrom);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.img_c_event_banner:
                if (AppDelegate.haveNetworkConnection(EventDetailActivity.this, false)) {
                    intent = new Intent(EventDetailActivity.this, LargeImageActivity.class);
                    bundle = new Bundle();
                    bundle.putString(Tags.image_1, eventModel.banner_image);
                    bundle.putInt(Tags.count, 1);
                    intent.putExtras(bundle);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(EventDetailActivity.this, false, new Pair<>(img_c_event_banner, "Image"));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(EventDetailActivity.this, pairs);
                    startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                } else {
                    startActivity(new Intent(EventDetailActivity.this, NoInternetConnectionActivity.class));
                }
                break;

            case R.id.ll_c_like:
                callLikeEventAsync(eventModel.id, eventModel.event_like_status.equalsIgnoreCase("0") ? "1" : "0");
                break;

            case R.id.ll_c_flag:
                shareEvent();
                break;

            case R.id.txt_c_location:
                if (!AppDelegate.haveNetworkConnection(EventDetailActivity.this, false)) {
                    startActivity(new Intent(EventDetailActivity.this, NoInternetConnectionActivity.class));
                } else {
                    Intent intent = new Intent(EventDetailActivity.this, MapDetailActivity.class);
                    intent.putExtra(Tags.LAT, eventModel.latitude);
                    intent.putExtra(Tags.LNG, eventModel.longitude);
                    intent.putExtra(Tags.name, txt_c_loc.getText().toString());
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(EventDetailActivity.this, false, new Pair<>(txt_c_loc, "Location"));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(EventDetailActivity.this, pairs);
                    startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                }
                break;

            case R.id.txt_c_more:
                txt_c_detail.setText(Html.fromHtml(eventModel.details));
                txt_c_more.setVisibility(View.GONE);
                break;
        }
    }

    private void shareEvent() {
        try {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, eventModel.event_name);
            String content = "";
            if (eventModel.gate_fees_type == 0) {
                content = eventModel.details + "\nTheme : " + eventModel.theme + "\nContact Number : " + eventModel.contact_no + "\nCreated By : " + eventModel.user_first_name + " " + eventModel.user_last_name + "\nTicket Type : Free";
            } else {
                content = eventModel.details + "\nTheme : " + eventModel.theme + "\nContact Number : " + eventModel.contact_no + "\nCreated By : " + eventModel.user_first_name + " " + eventModel.user_last_name + "\nTicket Type : Paid, Amount : " + eventModel.gate_fees;
            }
            sharingIntent.putExtra(Intent.EXTRA_TEXT, content);
            startActivity(Intent.createChooser(sharingIntent, "Share Using"));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void callLikeEventAsync(String event_id, String event_like_status) {
        if (AppDelegate.haveNetworkConnection(EventDetailActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(EventDetailActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(EventDetailActivity.this).setPostParamsSecond(mPostArrayList, Tags.event_id, event_id);
            AppDelegate.getInstance(EventDetailActivity.this).setPostParamsSecond(mPostArrayList, Tags.status, event_like_status);
            PostAsync mPostAsyncObj = new PostAsync(EventDetailActivity.this,
                    this, ServerRequestConstants.EVENT_LIKES,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            startActivity(new Intent(EventDetailActivity.this, NoInternetConnectionActivity.class));
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (apiName.equalsIgnoreCase(ServerRequestConstants.EVENT_VIEW)) {
            parseEventViewResponse(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.EVENT_LIKES)) {
            mHandler.sendEmptyMessage(11);
            parseEventLikeSatatusResult(result);
        }
    }

    private void parseEventLikeSatatusResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showToast(EventDetailActivity.this, jsonObject.getString(Tags.message));
                if (eventModel.event_like_status.equalsIgnoreCase("1")) {
                    eventModel.event_like_status = "0";
                    try {
                        int value = Integer.parseInt(eventModel.total_event_likes);
                        value -= 1;
                        eventModel.total_event_likes = value + "";
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                } else {
                    eventModel.event_like_status = "1";
                    try {
                        int value = Integer.parseInt(eventModel.total_event_likes);
                        value += 1;
                        eventModel.total_event_likes = value + "";
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
                updateEvents(eventModel);
                mHandler.sendEmptyMessage(2);
            } else {
                AppDelegate.showAlert(EventDetailActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseEventViewResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                asyncExecuting = false;
            } else {
                eventModel.total_event_views = (AppDelegate.getIntValue(eventModel.total_event_views) + 1) + "";
                eventModel.logged_user_view_status = "1";
                setValus();
                updateEvents(eventModel);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void updateEvents(EventModel eventModel) {
        String total_event_views, logged_user_view_status;
        total_event_views = eventModel.total_event_views;
        logged_user_view_status = eventModel.logged_user_view_status;

        this.eventModel = eventModel;
        this.eventModel.total_event_views = total_event_views;
        this.eventModel.logged_user_view_status = logged_user_view_status;

        for (int i = 0; i < EventsCampusBasedFragment.eventOffCampusArray.size(); i++) {
            if (EventsCampusBasedFragment.eventOffCampusArray.get(i).id.equalsIgnoreCase(this.eventModel.id)) {
                EventsCampusBasedFragment.eventOffCampusArray.remove(i);
                EventsCampusBasedFragment.eventOffCampusArray.add(i, this.eventModel);
                AppDelegate.LogT("added at EventsCampusBasedFragment eventOffCampusArray at position => " + i);
                break;
            }
        }
        for (int i = 0; i < EventsCampusBasedFragment.eventOnCampusArray.size(); i++) {
            if (EventsCampusBasedFragment.eventOnCampusArray.get(i).id.equalsIgnoreCase(this.eventModel.id)) {
                EventsCampusBasedFragment.eventOnCampusArray.remove(i);
                EventsCampusBasedFragment.eventOnCampusArray.add(i, this.eventModel);
                AppDelegate.LogT("added at EventsCampusBasedFragment eventOnCampusArray at position => " + i);
                break;
            }
        }
        for (int i = 0; i < MyEventListingFragment.eventArray.size(); i++) {
            if (MyEventListingFragment.eventArray.get(i).id.equalsIgnoreCase(this.eventModel.id)) {
                MyEventListingFragment.eventArray.remove(i);
                MyEventListingFragment.eventArray.add(i, this.eventModel);
                AppDelegate.LogT("added at MyProfileFragment at position => " + i);
                break;
            }
        }
    }
}
