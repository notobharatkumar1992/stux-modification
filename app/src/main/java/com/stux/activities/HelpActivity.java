package com.stux.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.stux.AppDelegate;
import com.stux.R;
import com.stux.Utils.TransitionHelper;
import com.stux.constants.Tags;
import com.stux.parser.Fb_detail_GetSet;
import com.stux.parser.Fb_details;

import org.json.JSONObject;

import java.util.Arrays;

import carbon.widget.TextView;

/**
 * Created by Bharat on 07/25/2016.
 */
public class HelpActivity extends AppCompatActivity implements View.OnClickListener {

    private Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.help);
        facebookSDKInitialize();
        initView();
    }

    private void initView() {
        ((TextView) findViewById(R.id.txt_c_header)).setText("Help");
        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(this);
        findViewById(R.id.img_c_notification).setVisibility(View.GONE);

        findViewById(R.id.ll_c_terms_condition).setOnClickListener(this);
        findViewById(R.id.ll_c_privacy_policy).setOnClickListener(this);
        findViewById(R.id.ll_c_safety_guide).setOnClickListener(this);
        findViewById(R.id.ll_c_rules).setOnClickListener(this);

        findViewById(R.id.txt_c_like_fb).setOnClickListener(this);
        findViewById(R.id.txt_c_like_twitter).setOnClickListener(this);
        findViewById(R.id.txt_c_like_twitter).setVisibility(View.GONE);

        findViewById(R.id.ll_c_faq).setOnClickListener(this);
        findViewById(R.id.ll_c_contact_us).setOnClickListener(this);
        findViewById(R.id.txt_c_software).setOnClickListener(this);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            ((TextView) findViewById(R.id.txt_c_version)).setText(pInfo.versionCode + "");
        } catch (PackageManager.NameNotFoundException e) {
            AppDelegate.LogE(e);
        }
    }

    private Intent intent;
    private Pair<View, String>[] pairs;
    private ActivityOptionsCompat transitionActivityOptions;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                onBackPressed();
                break;

            case R.id.ll_c_terms_condition:
                bundle = new Bundle();
                bundle.putInt(Tags.FROM, 0);
                intent = new Intent(HelpActivity.this, WebViewDetailActivity.class);
                intent.putExtras(bundle);
                pairs = TransitionHelper.createSafeTransitionParticipants(HelpActivity.this, false, new Pair<>(findViewById(R.id.txt_c_terms_condition), "web_view"));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(HelpActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;

            case R.id.ll_c_privacy_policy:
                bundle = new Bundle();
                bundle.putInt(Tags.FROM, 1);
                intent = new Intent(HelpActivity.this, WebViewDetailActivity.class);
                intent.putExtras(bundle);
                pairs = TransitionHelper.createSafeTransitionParticipants(HelpActivity.this, false, new Pair<>(findViewById(R.id.txt_c_privacy_policy), "web_view"));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(HelpActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;

            case R.id.ll_c_safety_guide:
                bundle = new Bundle();
                bundle.putInt(Tags.FROM, 2);
                intent = new Intent(HelpActivity.this, WebViewDetailActivity.class);
                intent.putExtras(bundle);
                pairs = TransitionHelper.createSafeTransitionParticipants(HelpActivity.this, false, new Pair<>(findViewById(R.id.txt_c_safety_guide), "web_view"));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(HelpActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;

            case R.id.ll_c_rules:
                bundle = new Bundle();
                bundle.putInt(Tags.FROM, 3);
                intent = new Intent(HelpActivity.this, WebViewDetailActivity.class);
                intent.putExtras(bundle);
                pairs = TransitionHelper.createSafeTransitionParticipants(HelpActivity.this, false, new Pair<>(findViewById(R.id.txt_c_rules), "web_view"));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(HelpActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;

            case R.id.ll_c_faq:
                bundle = new Bundle();
                bundle.putInt(Tags.FROM, 4);
                intent = new Intent(HelpActivity.this, WebViewDetailActivity.class);
                intent.putExtras(bundle);
                pairs = TransitionHelper.createSafeTransitionParticipants(HelpActivity.this, false, new Pair<>(findViewById(R.id.txt_c_faq), "web_view"));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(HelpActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);

                break;

            case R.id.txt_c_like_fb:
                if (!AppDelegate.haveNetworkConnection(HelpActivity.this, false)) {
                    startActivity(new Intent(HelpActivity.this, NoInternetConnectionActivity.class));
                } else {
                    openFacebook();
                }
                break;

            case R.id.ll_c_contact_us:
                intent = new Intent(HelpActivity.this, ContactUsActivity.class);
                pairs = TransitionHelper.createSafeTransitionParticipants(HelpActivity.this, false, new Pair<>(findViewById(R.id.txt_c_contact_us), "contact_us"));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(HelpActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        callbackManager = null;
    }

    /*Facebook integration for Invite User for app */
    public CallbackManager callbackManager;
    private String fb_LoginToken;
    private boolean isCalledOnce = false;
    private Fb_detail_GetSet fbUserData;

    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(HelpActivity.this);
        callbackManager = CallbackManager.Factory.create();
    }


    public void openFacebook() {
        FacebookSdk.sdkInitialize(HelpActivity.this);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(HelpActivity.this, Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("onSuccess = " + loginResult.getAccessToken());
                        AppDelegate.showProgressDialog(HelpActivity.this);
                        fb_LoginToken = loginResult.getAccessToken().toString();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        // Application code
                                        AppDelegate.LogFB("Graph response" + response.toString());
                                        fbUserData = new Fb_detail_GetSet();
                                        fbUserData = new Fb_details().getFacebookDetail(response.getJSONObject().toString());

                                        AppDelegate.hideProgressDialog(HelpActivity.this);
                                        inviteFbFriends();
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "first_name,last_name,email,id,name,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook();
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(HelpActivity.this);
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("Login onActivityResult called");
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void inviteFbFriends() {
        String appLinkUrl, previewImageUrl;
        appLinkUrl = "app url(create it from facebook)"; //your applink url
        previewImageUrl = "image url";//your image url

        appLinkUrl = "app url(create it from facebook)";
        previewImageUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7U-uGd4v8UJ7Xynkjoi9upDVmEczdlvsVPOZpjvbcUbOczcEcpg";
//        if (AppInviteDialog.canShow()) {
        AppInviteContent content = new AppInviteContent.Builder()
                .setApplinkUrl(appLinkUrl)
                .setPreviewImageUrl(previewImageUrl)
                .build();
        AppInviteDialog.show(HelpActivity.this, content);
//        }
        AppDelegate.LogT("inviteFbFriends called");
    }

}
