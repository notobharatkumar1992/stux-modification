package com.stux.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.stux.AppDelegate;
import com.stux.Async.LocationAddress;
import com.stux.R;
import com.stux.Utils.WorkaroundMapFragment;
import com.stux.constants.Tags;

import carbon.widget.TextView;

/**
 * Created by Bharat on 07/29/2016.
 */
public class MapDetailActivity extends AppCompatActivity {

    private GoogleMap map_business;
    private double lat = 0.0, lng = 0.0;
    private String name = "";
    private Handler mHandler;
    private TextView txt_c_header;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.map_detail_page);
        setHandler();
        txt_c_header = (TextView) findViewById(R.id.txt_c_header);
        try {
            lat = Double.parseDouble(AppDelegate.isValidString(getIntent().getStringExtra(Tags.LAT)) ? getIntent().getStringExtra(Tags.LAT) : "0");
            lng = Double.parseDouble(AppDelegate.isValidString(getIntent().getStringExtra(Tags.LNG)) ? getIntent().getStringExtra(Tags.LNG) : "0");
            name = getIntent().getStringExtra(Tags.name);
            txt_c_header.setText(name);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (AppDelegate.haveNetworkConnection(this) && !AppDelegate.isValidString(name)) {
            setLatLngAndFindAddress(new LatLng(lat, lng), 100);
        }
        map_business = ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).getMap();
        showMap();
        findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setHandler() {
        try {
            mHandler = new Handler() {
                @Override
                public void dispatchMessage(Message msg) {
                    super.dispatchMessage(msg);
                    if (msg.what == 10) {
                        AppDelegate.showProgressDialog(MapDetailActivity.this);
                    } else if (msg.what == 11) {
                        AppDelegate.hideProgressDialog(MapDetailActivity.this);
                    } else if (msg.what == 2) {
                        setResultFromGeoCoderApi(msg.getData());
                    }
                }
            };
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private String place_name = "", place_address = "";

    public void setResultFromGeoCoderApi(Bundle bundle) {
        mHandler.sendEmptyMessage(11);
        if (bundle.getString(Tags.PLACE_NAME) != null) {
            place_name = bundle.getString(Tags.PLACE_NAME);
            place_address = bundle.getString(Tags.PLACE_ADD);
            showMap();
        } else {
            AppDelegate.showToast(MapDetailActivity.this, "Location not available or something went wrong with server, Please try again later.");
        }
    }

    public void setLatLngAndFindAddress(final LatLng latLng, long countDownTime) {
        LatLng arg0 = AppDelegate.getRoundedLatLng(latLng);
        LocationAddress.getAddressFromLocation(arg0.latitude, arg0.longitude, this, mHandler);
    }

    private void showMap() {
        if (map_business == null) {
            return;
        }
        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        map_business.clear();
        if (AppDelegate.isValidString(name)) {
            map_business.addMarker(AppDelegate.getMarkerOptionsWithTitleSnippet(this, new LatLng(lat, lng), R.drawable.map_pin, 100, 100, name));
        } else
            map_business.addMarker(AppDelegate.getMarkerOptions(this, new LatLng(lat, lng), R.drawable.map_pin));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, lng))
                .zoom(14).build();
        //Zoom in and animate the camera.
        map_business.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}
