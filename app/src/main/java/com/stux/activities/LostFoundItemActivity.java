package com.stux.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.ItemFoundModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DividerItemDecoration;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.LostFoundItemRecyclerViewAdapter;

/**
 * Created by Bharat on 07/08/2016.
 */
public class LostFoundItemActivity extends AppCompatActivity implements OnReciveServerResponse, OnListItemClickListener {

    private ArrayList<ItemFoundModel> itemArray = new ArrayList<>();

    public static Handler mHandler;
    private Prefs prefs;
    private UserDataModel userData;

    private ProgressBar progressbar;

    private TextView txt_c_no_list;
    private int itemCounter = 1, itemTotalPage = -1;
    private boolean itemAsyncExcecuting = false;
    private int preLast;

    private SwipyRefreshLayout swipyrefreshlayout;
    private RecyclerView recyclerView;
    private LostFoundItemRecyclerViewAdapter rcAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.my_event_list_view);
        prefs = new Prefs(LostFoundItemActivity.this);
        userData = prefs.getUserdata();
        itemArray.clear();
        setHandler();
        initView();
        if (itemArray.size() == 0) {
            itemCounter = 1;
            itemTotalPage = -1;
            itemAsyncExcecuting = true;
            callFoundItemListAsync();
        } else
            mHandler.sendEmptyMessage(2);
    }

    private void callFoundItemListAsync() {
        if (AppDelegate.haveNetworkConnection(LostFoundItemActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(LostFoundItemActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, userData.userId);
            AppDelegate.getInstance(LostFoundItemActivity.this).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(LostFoundItemActivity.this).setPostParamsSecond(mPostArrayList, Tags.page, itemCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(LostFoundItemActivity.this,
                    this, ServerRequestConstants.GET_FOUND_LIST,
                    mPostArrayList, null);
            if (itemCounter == 1)
                mHandler.sendEmptyMessage(12);
            mPostAsyncObj.execute();
        } else {
            startActivity(new Intent(LostFoundItemActivity.this, NoInternetConnectionActivity.class));
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(LostFoundItemActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(LostFoundItemActivity.this);
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                    txt_c_no_list.setVisibility(View.GONE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    AppDelegate.LogT("mEventAdapter notified = " + itemArray.size());
                    txt_c_no_list.setVisibility(itemArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No item available");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                } else if (msg.what == 3) {
                    itemCounter = 1;
                    itemTotalPage = -1;
                    itemAsyncExcecuting = true;
                    callFoundItemListAsync();
                }
            }
        };
    }

    private void initView() {
        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.txt_c_header)).setText("Lost / Found");
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ((carbon.widget.ImageView) findViewById(R.id.img_c_right)).setImageResource(R.drawable.add);
        findViewById(R.id.img_c_right).setVisibility(View.VISIBLE);
        findViewById(R.id.img_c_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppDelegate.haveNetworkConnection(LostFoundItemActivity.this, false)) {
                    startActivity(new Intent(LostFoundItemActivity.this, NoInternetConnectionActivity.class));
                } else
                    startActivity(new Intent(LostFoundItemActivity.this, AddItemFoundActivity.class));
            }
        });


        txt_c_no_list = (TextView) findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        rcAdapter = new LostFoundItemRecyclerViewAdapter(LostFoundItemActivity.this, itemArray, this, Tags.LOST_FOUND);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setLayoutManager(new LinearLayoutManager(LostFoundItemActivity.this));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(LostFoundItemActivity.this, R.drawable.bg_divider));
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setPadding(AppDelegate.dpToPix(LostFoundItemActivity.this, 15), 0, AppDelegate.dpToPix(LostFoundItemActivity.this, 15), 0);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (itemTotalPage != 0 && !itemAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 callFoundItemListAsync();
                                 itemAsyncExcecuting = true;
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + itemTotalPage + ", " + itemAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler = null;
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (mHandler == null)
            return;
        mHandler.sendEmptyMessage(13);
        if (!AppDelegate.isValidString(result)) {
            startActivity(new Intent(LostFoundItemActivity.this, NoInternetConnectionActivity.class));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_FOUND_LIST)) {
            itemAsyncExcecuting = false;
            swipyrefreshlayout.setRefreshing(false);
            parseEventListResult(result);
        }
    }

    private void parseEventListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                itemArray.clear();
                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        ItemFoundModel itemModel = new ItemFoundModel();
                        itemModel.id = JSONParser.getString(object, Tags.id);
                        itemModel.user_id = JSONParser.getString(object, Tags.user_id);
                        itemModel.item_name = object.getString(Tags.item_name);
                        itemModel.item_description = object.getString(Tags.item_description);
                        itemModel.location = JSONParser.getString(object, Tags.location);
                        itemModel.campus_name = JSONParser.getString(object, Tags.campus_name);
                        itemModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                        itemModel.image_1 = JSONParser.getString(object, Tags.image_1);
                        itemModel.image_thumb_1 = JSONParser.getString(object, Tags.image_1_thumb);
                        itemModel.image_2 = JSONParser.getString(object, Tags.image_2);
                        itemModel.image_thumb_2 = JSONParser.getString(object, Tags.image_2_thumb);
                        itemModel.image_3 = JSONParser.getString(object, Tags.image_3);
                        itemModel.image_thumb_3 = JSONParser.getString(object, Tags.image_3_thumb);
                        itemModel.image_4 = JSONParser.getString(object, Tags.image_4);
                        itemModel.image_thumb_4 = JSONParser.getString(object, Tags.image_4_thumb);

                        itemModel.status = JSONParser.getString(object, Tags.status);
                        itemModel.is_view = JSONParser.getString(object, Tags.is_view);
                        itemModel.created = JSONParser.getString(object, Tags.created);
                        itemModel.contact_no = JSONParser.getString(object, Tags.contact_no);

                        itemModel.lat = JSONParser.getString(object, Tags.latitude);
                        itemModel.lng = JSONParser.getString(object, Tags.longitude);

                        itemModel.userModel = new UserDataModel();
                        if (AppDelegate.isValidString(object.optString("Tags.user"))) {
                            JSONObject userObject = object.getJSONObject(Tags.user);
                            itemModel.userModel.first_name = userObject.getString(Tags.first_name);
                            itemModel.userModel.last_name = userObject.getString(Tags.last_name);
                            itemModel.userModel.image = userObject.getString(Tags.image);

                            JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                            if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                                itemModel.userModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                                itemModel.userModel.institution_id = studentObject.getString(Tags.institution_id);
                                if (studentObject.has(Tags.institute_name) && AppDelegate.isValidString(studentObject.optString(Tags.institute_name))) {
                                    itemModel.userModel.institute_name = studentObject.getString(Tags.institution_name);
                                }
                                if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                                    itemModel.userModel.department_name = studentObject.getString(Tags.department_name);
                                } else {
                                    itemModel.userModel.department_name = studentObject.getString(Tags.department_name);
                                }
                            } else {
                                itemModel.userModel.institute_name = studentObject.getString(Tags.other_ins_name);
                                itemModel.userModel.department_name = studentObject.getString(Tags.department_name);
                            }
                        }
                        itemArray.add(itemModel);
                    }
                } else {
                    itemTotalPage = 0;
                    AppDelegate.showToast(LostFoundItemActivity.this, "No record found.");
                }
            } else {
                itemTotalPage = 0;
                AppDelegate.showToast(LostFoundItemActivity.this, "No record found.");
            }
            itemCounter++;

            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            startActivity(new Intent(LostFoundItemActivity.this, NoInternetConnectionActivity.class));
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.LOST_FOUND)) {
//            Bundle bundle = new Bundle();
//            bundle.putParcelable(Tags.FOUND, itemArray.get(position));
//            Fragment fragment = new LostFoundDetailActivity();
//            fragment.setArguments(bundle);
//            AppDelegate.showFragmentAnimation(LostFoundItemActivity.this.getSupportFragmentManager(), fragment);
        }
    }

}
