package com.stux.activities;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bulletnoid.android.widget.StaggeredGridViewDemo.STGVImageView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.SliderModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.fragments.NoInternetConnectionFragment;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by Bharat on 07/18/2016.
 */
public class BannerDetailActivity extends FragmentActivity implements View.OnClickListener, OnReciveServerResponse, YouTubePlayer.OnInitializedListener {

    private TextView txt_c_action, txt_c_title, txt_c_description;
    private ImageView img_c_large;
    private ProgressBar progressbar;
    private STGVImageView img_content;
    private YouTubePlayerFragment youTubePlayerFragment;
    private Fragment youtube_fragment;
    private RelativeLayout rl_image, rl_video;

    private SliderModel sliderModel;
    private Prefs prefs;
    private UserDataModel dataModel;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.banner_image_screen);
        prefs = new Prefs(this);
        dataModel = prefs.getUserdata();
        sliderModel = getIntent().getExtras().getParcelable(Tags.slider_id);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        initView();
        setValues();
    }

    private void setValues() {
        if (AppDelegate.isValidString(sliderModel.title))
            txt_c_title.setText(sliderModel.title);
        else txt_c_title.setVisibility(View.GONE);

        if (AppDelegate.isValidString(sliderModel.descriptions))
            txt_c_description.setText(sliderModel.descriptions);
        else txt_c_description.setVisibility(View.GONE);

        if (sliderModel.banner_type == 1) {
            txt_c_action.setText("Learn More");
        } else if (sliderModel.banner_type == 2) {
            txt_c_action.setText("Open");
        } else if (sliderModel.banner_type == 3) {
            txt_c_action.setText("Shop");
        }
        if (AppDelegate.isValidString(sliderModel.banner_image)) {

            if (sliderModel.from_page == 0 && sliderModel.type_of_slider == 2) {
                youTubePlayerFragment.initialize(AppDelegate.DEVELOPER_KEY, this);
                rl_video.setVisibility(View.VISIBLE);
                rl_image.setVisibility(View.GONE);
            } else {
                rl_image.setVisibility(View.VISIBLE);
                rl_video.setVisibility(View.GONE);
                progressbar.setVisibility(View.VISIBLE);
                imageLoader.loadImage(sliderModel.banner_image, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                        img_content.mWidth = bitmap.getWidth();
                        img_content.mHeight = bitmap.getHeight();
                        img_content.setImageBitmap(bitmap);
                        progressbar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });
//                Picasso.with(this).load(sliderModel.banner_image).into(new Target() {
//                    @Override
//                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                        img_content.mWidth = bitmap.getWidth();
//                        img_content.mHeight = bitmap.getHeight();
//                        img_content.setImageBitmap(bitmap);
//                        progressbar.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onBitmapFailed(Drawable errorDrawable) {
//                    }
//
//                    @Override
//                    public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    }
//                });
            }
        }
    }

    private void initView() {
        txt_c_action = (TextView) findViewById(R.id.txt_c_action);
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        txt_c_description = (TextView) findViewById(R.id.txt_c_description);

        img_c_large = (ImageView) findViewById(R.id.img_c_large);
        img_content = (STGVImageView) findViewById(R.id.img_content);

        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        progressbar.setVisibility(View.GONE);

        findViewById(R.id.ll_c_left).setOnClickListener(this);
        if (AppDelegate.isValidString(sliderModel.url))
            findViewById(R.id.ll_c_right).setOnClickListener(this);
        else findViewById(R.id.ll_c_right).setVisibility(View.GONE);

        rl_image = (RelativeLayout) findViewById(R.id.rl_image);
        rl_video = (RelativeLayout) findViewById(R.id.rl_video);

        youTubePlayerFragment = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_c_left:
                finish();
                break;
            case R.id.ll_c_right:
                if (sliderModel.thump_clicked == 0) {
                    callClicksAsync(sliderModel, 1);
                }
                AppDelegate.openURL(this, sliderModel.url);
                break;
        }
    }

    private void callClicksAsync(SliderModel sliderModel, int type) {
        if (AppDelegate.haveNetworkConnection(BannerDetailActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(BannerDetailActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(BannerDetailActivity.this).setPostParamsSecond(mPostArrayList, Tags.slider_id, sliderModel.id);
            if (type == 0)
                AppDelegate.getInstance(BannerDetailActivity.this).setPostParamsSecond(mPostArrayList, Tags.click_status, "1");
            else
                AppDelegate.getInstance(BannerDetailActivity.this).setPostParamsSecond(mPostArrayList, Tags.thumb_status, "1");
            AppDelegate.getInstance(BannerDetailActivity.this).setPostParamsSecond(mPostArrayList, Tags.device_id, AppDelegate.getUUID(BannerDetailActivity.this));
            PostAsync mPostAsyncObj = new PostAsync(BannerDetailActivity.this,
                    this, ServerRequestConstants.SLIDERS_CLICKS,
                    mPostArrayList, null);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(BannerDetailActivity.this.getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (apiName.equalsIgnoreCase(ServerRequestConstants.SLIDERS_CLICKS)) {
            parseSlidersClick(result);
        }
    }

    private void parseSlidersClick(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
//                AppDelegate.showAlert(BannerDetailActivity.this, jsonObject.getString(Tags.message));
                sliderModel.single_clicked = 1;
            } else {
                sliderModel.single_clicked = 1;
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored) {
            AppDelegate.LogT("onInitializationSuccess called.");
            youTubePlayer.cueVideo("hks_TtC5uC8");
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        String errorMessage = String.format(getString(R.string.error_player), youTubeInitializationResult.toString());
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }
}


