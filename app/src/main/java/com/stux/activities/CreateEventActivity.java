package com.stux.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.stux.Adapters.LocationMapAdapter;
import com.stux.Adapters.SpinnerArrayStringAdapter;
import com.stux.AppDelegate;
import com.stux.Async.GetLatLongFromPlaceIdAsync;
import com.stux.Async.PlacesService;
import com.stux.Async.PostAsync;
import com.stux.Models.EventCategoryModel;
import com.stux.Models.EventModel;
import com.stux.Models.Place;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DateUtils;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.fragments.EventsCampusBasedFragment;
import com.stux.fragments.NoInternetConnectionFragment;
import com.stux.interfaces.GetLocationResult;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnPictureResult;
import com.stux.interfaces.OnReciveServerResponse;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

/**
 * Created by Bharat on 06/02/2016.
 */
public class CreateEventActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, GetLocationResult, OnListItemClickListener, OnPictureResult {

    private Prefs prefs;
    private UserDataModel dataModel;

    private Activity mActivity;

    public static List<Place> mapPlacesArray = new ArrayList<>();

    private Spinner spn_type_event, spn_location, spn_theme, spn_gate_fee, spn_ticket;
    private SpinnerArrayStringAdapter adapterEventType, adapterLocationType, adapterTheme, adapterGateFee, adapterTicket;
    public ArrayList<EventCategoryModel> eventTypeArray = new ArrayList<>();
    public ArrayList<String> arrayStringTypeEvent = new ArrayList<>();
    public ArrayList<String> arrayStringLoaction = new ArrayList<>();
    public ArrayList<String> arrayStringTheme = new ArrayList<>();
    public ArrayList<String> arrayStringGateFee = new ArrayList<>();
    public ArrayList<String> arrayStringTicket = new ArrayList<>();

    private EditText et_event_name, et_location, et_venue, et_theme, et_contact_number, et_email, et_fees, et_description;
    private LinearLayout ll_c_upload_pic, ll_c_img_layout;
    private RelativeLayout rl_c_pic_0, rl_c_pic_1, rl_c_pic_2, rl_c_pic_3;
    private ImageView img_c_pic_0, img_c_pic_1, img_c_pic_2, img_c_pic_3, img_c_close_0, img_c_close_1, img_c_close_2, img_c_close_3;
    private TextView txt_c_from_date, txt_c_to_date, txt_c_from_time, txt_c_to_time, no_location_text;

    private ListView search_list;
    private ProgressBar pb_location_progressbar;
    public Thread mThreadOnSearch;
    public LocationMapAdapter adapter;

    private int selected_event_type = 0, selected_location = 0, selected_fees = 0, selected_ticket = 0;

    private ArrayList<File> arrayImageFile = new ArrayList<>();

    private ExpandableRelativeLayout exp_layout;
    private boolean canSearch = true;
    private String placeKeyword = "";
    private Place placeDetail;
    private int selected_item_position = 0;
    private LatLng latLng;

    private EventModel eventModel;
    private boolean fromEdit = false;
    private Handler mHandler;

    private Bitmap OriginalPhoto;
    public static File capturedFile;
    public static Uri imageURI = null;
//    public static OnPictureResult onPictureResult;

    private ScrollView scrollView;
    private String tagFrom = "";
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        mActivity = this;
        setContentView(R.layout.create_event);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        prefs = new Prefs(CreateEventActivity.this);
        dataModel = prefs.getUserdata();
        eventModel = new EventModel();
        initView();
        setHandler();
        executeEventApi();
        tagFrom = getIntent().getStringExtra(Tags.FROM);
        if (getIntent().getExtras() != null && getIntent().getExtras().getParcelable(Tags.EVENT) != null) {
            ((TextView) findViewById(R.id.txt_c_header)).setText("Edit Event");
            ((TextView) findViewById(R.id.txt_c_right)).setText("Update");
            fromEdit = true;
            eventModel = getIntent().getExtras().getParcelable(Tags.EVENT);
            setValues();
        }
    }

    public void expandableView(final ExpandableRelativeLayout exp_layout, final int value) {
        exp_layout.post(new Runnable() {
            @Override
            public void run() {
                if (value == MainActivity.COLLAPSE) {
                    exp_layout.collapse();
                } else {
                    exp_layout.expand();
                }
            }
        });
    }

    private void setValues() {
        if (eventTypeArray.size() > 0) {
            for (int i = 0; i < eventTypeArray.size(); i++) {
                AppDelegate.LogT("event => " + eventTypeArray.get(i).id + " " + eventModel.event_type);
                if (eventTypeArray.get(i) != null && AppDelegate.isValidString(eventTypeArray.get(i).id) && eventTypeArray.get(i).id.equalsIgnoreCase(eventModel.event_type)) {
                    spn_type_event.setSelection(i);
                    break;
                }
            }
        }

        et_event_name.setText(eventModel.event_name);
        try {
            if (arrayStringLoaction.size() > Integer.parseInt(eventModel.location_type))
                spn_location.setSelection(Integer.parseInt(eventModel.location_type));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        canSearch = false;
        et_venue.setText(eventModel.venue);
        try {
            if (placeDetail == null) {
                placeDetail = new Place();
                placeDetail.latitude = Double.parseDouble(eventModel.latitude);
                placeDetail.longitude = Double.parseDouble(eventModel.longitude);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        canSearch = true;
        latLng = new LatLng(Double.parseDouble(eventModel.latitude), Double.parseDouble(eventModel.longitude));

        et_theme.setText(eventModel.theme);
        et_contact_number.setText(eventModel.contact_no);
        et_email.setText(eventModel.email_address);

        try {
            if ((eventModel.gate_fees_type + 1) < arrayStringGateFee.size())
                spn_gate_fee.setSelection(eventModel.gate_fees_type + 1);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        et_fees.setText(eventModel.gate_fees + "");

        try {
            if (arrayStringTicket.size() > Integer.parseInt(eventModel.tickets + 1))
                spn_ticket.setSelection(Integer.parseInt(eventModel.tickets + 1));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        et_description.setText(eventModel.details);

        String date = "", time = "", dateTime = "";
        dateTime = eventModel.event_start_time.substring(0, eventModel.event_start_time.lastIndexOf("+"));

        try {
            date = new SimpleDateFormat(DATE_FORMAT).format(new SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT).parse(dateTime));
            time = new SimpleDateFormat(TIME_FORMAT_12_HOUR).format(new SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT).parse(dateTime));
            txt_c_from_date.setText(date);
            txt_c_from_time.setText(time);
        } catch (ParseException e) {
            AppDelegate.LogE(e);
        }
        dateTime = eventModel.event_end_time.substring(0, eventModel.event_end_time.lastIndexOf("+"));
        try {
            date = new SimpleDateFormat(DATE_FORMAT).format(new SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT).parse(dateTime));
            time = new SimpleDateFormat(TIME_FORMAT_12_HOUR).format(new SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT).parse(dateTime));
            txt_c_to_date.setText(date);
            txt_c_to_time.setText(time);
        } catch (ParseException e) {
            AppDelegate.LogE(e);
        }

        if (AppDelegate.isValidString(eventModel.banner_image)) {
            ll_c_img_layout.setVisibility(View.VISIBLE);
            imageLoader.displayImage(eventModel.banner_image_thumb,img_c_pic_0);
//            Picasso.with(CreateEventActivity.this).load(eventModel.banner_image_thumb).into(img_c_pic_0);
            setVisiblePic(0);
        }
    }

    private void executeEventApi() {
        if (AppDelegate.haveNetworkConnection(CreateEventActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
//            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
//            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            PostAsync mPostasyncObj = new PostAsync(CreateEventActivity.this,
                    CreateEventActivity.this, ServerRequestConstants.GET_EVENT_CATEGORY,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(CreateEventActivity.this.getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(CreateEventActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(CreateEventActivity.this);
                        break;

                    case 1:
                        adapterEventType.notifyDataSetChanged();
                        spn_type_event.invalidate();
                        break;

                    case 2:
                        if (mapPlacesArray != null && mapPlacesArray.size() > 0) {
                            AppDelegate.LogT("handler 1");
                            adapter.notifyDataSetChanged();
                            search_list.invalidate();
                            search_list.setVisibility(View.VISIBLE);
                        } else {
                            search_list.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult) {
                                no_location_text
                                        .setText(getResources()
                                                .getString(
                                                        R.string.no_location_available_for_this_address));
                            } else {
                                no_location_text.setText(getResources().getString(
                                        R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text.setVisibility(View.VISIBLE);
                            pb_location_progressbar.setVisibility(View.GONE);
                        }
                        setListViewHeight(CreateEventActivity.this, exp_layout, adapter);
                        break;
                    case 6:
                        no_location_text.setText(getResources().getString(
                                R.string.no_internet_connection));
                        pb_location_progressbar.setVisibility(View.INVISIBLE);
                        no_location_text.setVisibility(View.VISIBLE);
                        search_list.setVisibility(View.GONE);
                        mHandler.sendEmptyMessage(22);
                        setListViewHeight(CreateEventActivity.this, exp_layout, adapter);
                        break;
                    case 7:
                        pb_location_progressbar.setVisibility(View.VISIBLE);
                        no_location_text.setVisibility(View.GONE);
                        break;
                    case 8:
                        pb_location_progressbar.setVisibility(View.INVISIBLE);
                        break;

                    case 9:
                        scrollView.fullScroll(View.FOCUS_DOWN);
                        break;

                    case 21:
                        expandableView(exp_layout, MainActivity.COLLAPSE);
                        break;

                    case 22:
                        expandableView(exp_layout, MainActivity.EXPAND);
                        break;
                }
            }
        };
    }

    public void setListViewHeight(Context mContext, ExpandableRelativeLayout listView, ListAdapter gridAdapter) {
        try {
            if (gridAdapter == null) {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = AppDelegate.dpToPix(mContext, 180);
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;
            int itemCount = 5;

            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                    View.MeasureSpec.AT_MOST);
            if (itemCount == 0) {
                totalHeight = AppDelegate.dpToPix(mContext, 180);
            } else {
                for (int i = 0; i < itemCount; i++) {
                    View listItem = gridAdapter.getView(0, null, listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += listItem.getMeasuredHeight();
                }
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initView() {
        ((TextView) findViewById(R.id.txt_c_header)).setText("Create Event");
        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(this);
        ((ImageView) findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        findViewById(R.id.img_c_right).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.txt_c_right)).setText("Create");
        findViewById(R.id.txt_c_right).setVisibility(View.VISIBLE);
        findViewById(R.id.txt_c_right).setOnClickListener(this);

        txt_c_from_date = (TextView) findViewById(R.id.txt_c_from_date);
        txt_c_to_date = (TextView) findViewById(R.id.txt_c_to_date);
        txt_c_from_time = (TextView) findViewById(R.id.txt_c_from_time);
        txt_c_to_time = (TextView) findViewById(R.id.txt_c_to_time);

        et_event_name = (EditText) findViewById(R.id.et_event_name);
        et_event_name.setTypeface(Typeface.createFromAsset(CreateEventActivity.this.getAssets(), getString(R.string.font_roman)));
        et_location = (EditText) findViewById(R.id.et_location);
        et_location.setTypeface(Typeface.createFromAsset(CreateEventActivity.this.getAssets(), getString(R.string.font_roman)));
        et_venue = (EditText) findViewById(R.id.et_venue);
        et_venue.setTypeface(Typeface.createFromAsset(CreateEventActivity.this.getAssets(), getString(R.string.font_roman)));
//        et_venue.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                placeKeyword = et_venue.getText().toString();
//                if (canSearch) {
//                    if (et_venue.length() != 0) {
//                        mHandler.sendEmptyMessage(22);
//                        stopStartThread(mThreadOnSearch);
//                        no_location_text.setVisibility(View.GONE);
//                    } else {
//                        mHandler.sendEmptyMessage(21);
//                    }
//                } else {
//                    mHandler.sendEmptyMessage(21);
//                }
//            }
//
//        });

        et_theme = (EditText) findViewById(R.id.et_theme);
        et_theme.setTypeface(Typeface.createFromAsset(CreateEventActivity.this.getAssets(), getString(R.string.font_roman)));
        et_contact_number = (EditText) findViewById(R.id.et_contact_number);
        et_contact_number.setTypeface(Typeface.createFromAsset(CreateEventActivity.this.getAssets(), getString(R.string.font_roman)));
        et_email = (EditText) findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(CreateEventActivity.this.getAssets(), getString(R.string.font_roman)));
        et_fees = (EditText) findViewById(R.id.et_fees);
        et_fees.setTypeface(Typeface.createFromAsset(CreateEventActivity.this.getAssets(), getString(R.string.font_roman)));
        et_description = (EditText) findViewById(R.id.et_description);
        et_description.setTypeface(Typeface.createFromAsset(CreateEventActivity.this.getAssets(), getString(R.string.font_roman)));

        arrayStringTypeEvent.add("Type of Event");
        eventTypeArray.add(new EventCategoryModel("Type of Event"));

        arrayStringLoaction.add("Location Type");
        arrayStringLoaction.add("On Campus");
        arrayStringLoaction.add("Off Campus");

        arrayStringTheme.add("Theme");
        arrayStringTheme.add("Theme");
        arrayStringTheme.add("Theme");

        arrayStringGateFee.add("Gate Fees Type");
        arrayStringGateFee.add("Free");
        arrayStringGateFee.add("Paid");

        arrayStringTicket.add("Ticket Type");
        arrayStringTicket.add("All Ticket Sold Out");
        arrayStringTicket.add("Ticket Available");

        spn_type_event = (Spinner) findViewById(R.id.spn_type_event);
        adapterEventType = new SpinnerArrayStringAdapter(CreateEventActivity.this, arrayStringTypeEvent);
        spn_type_event.setAdapter(adapterEventType);
        spn_type_event.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_event_type = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spn_location = (Spinner) findViewById(R.id.spn_location);
        adapterLocationType = new SpinnerArrayStringAdapter(CreateEventActivity.this, arrayStringLoaction);
        spn_location.setAdapter(adapterLocationType);
        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_location = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spn_theme = (Spinner) findViewById(R.id.spn_theme);
        adapterTheme = new SpinnerArrayStringAdapter(CreateEventActivity.this, arrayStringTheme);
        spn_theme.setAdapter(adapterTheme);

        spn_gate_fee = (Spinner) findViewById(R.id.spn_gate_fee);
        adapterGateFee = new SpinnerArrayStringAdapter(CreateEventActivity.this, arrayStringGateFee);
        spn_gate_fee.setAdapter(adapterGateFee);
        spn_gate_fee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_fees = position;
                spn_ticket.setEnabled(true);
                switch (position) {
                    case 0:
                        et_fees.setVisibility(View.GONE);
                        break;
                    case 1:
                        et_fees.setVisibility(View.GONE);
                        spn_ticket.setSelection(2);
                        spn_ticket.setEnabled(false);
                        break;
                    case 2:
                        et_fees.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spn_ticket = (Spinner) findViewById(R.id.spn_ticket);
        adapterTicket = new SpinnerArrayStringAdapter(CreateEventActivity.this, arrayStringTicket);
        spn_ticket.setAdapter(adapterTicket);
        spn_ticket.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_ticket = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ll_c_img_layout = (LinearLayout) findViewById(R.id.ll_c_img_layout);
        ll_c_img_layout.setVisibility(View.GONE);
        rl_c_pic_0 = (RelativeLayout) findViewById(R.id.rl_c_pic_0);
        rl_c_pic_0.setVisibility(View.INVISIBLE);
        rl_c_pic_1 = (RelativeLayout) findViewById(R.id.rl_c_pic_1);
        rl_c_pic_1.setVisibility(View.INVISIBLE);
        rl_c_pic_2 = (RelativeLayout) findViewById(R.id.rl_c_pic_2);
        rl_c_pic_2.setVisibility(View.INVISIBLE);
        rl_c_pic_3 = (RelativeLayout) findViewById(R.id.rl_c_pic_3);
        rl_c_pic_3.setVisibility(View.INVISIBLE);

        img_c_pic_0 = (ImageView) findViewById(R.id.img_c_pic_0);
        img_c_pic_1 = (ImageView) findViewById(R.id.img_c_pic_1);
        img_c_pic_2 = (ImageView) findViewById(R.id.img_c_pic_2);
        img_c_pic_3 = (ImageView) findViewById(R.id.img_c_pic_3);

        img_c_close_0 = (ImageView) findViewById(R.id.img_c_close_0);
        img_c_close_0.setOnClickListener(this);
        img_c_close_1 = (ImageView) findViewById(R.id.img_c_close_1);
        img_c_close_1.setOnClickListener(this);
        img_c_close_2 = (ImageView) findViewById(R.id.img_c_close_2);
        img_c_close_2.setOnClickListener(this);
        img_c_close_3 = (ImageView) findViewById(R.id.img_c_close_3);
        img_c_close_3.setOnClickListener(this);

        findViewById(R.id.ll_c_upload_pic).setOnClickListener(this);
        findViewById(R.id.rl_c_from_date).setOnClickListener(this);
        findViewById(R.id.rl_c_to_date).setOnClickListener(this);
        findViewById(R.id.rl_c_from_time).setOnClickListener(this);
        findViewById(R.id.rl_c_to_time).setOnClickListener(this);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);

        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        expandableView(exp_layout, MainActivity.COLLAPSE);

        no_location_text = (TextView) findViewById(R.id.no_location_text);
        search_list = (ListView) findViewById(R.id.search_list);
        adapter = new LocationMapAdapter(CreateEventActivity.this, mapPlacesArray, this);
        search_list.setAdapter(adapter);

        pb_location_progressbar = (ProgressBar) findViewById(R.id.pb_location_progressbar);
        pb_location_progressbar.setVisibility(View.GONE);


        ll_c_img_layout = (LinearLayout) findViewById(R.id.ll_c_img_layout);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) ll_c_img_layout.getLayoutParams();
        layoutParams.height = (AppDelegate.getDeviceWith(CreateEventActivity.this) - AppDelegate.dpToPix(CreateEventActivity.this, 55)) / 4;
        ll_c_img_layout.setLayoutParams(layoutParams);
        ll_c_img_layout.invalidate();

        scrollView = (ScrollView) findViewById(R.id.scrollView);

    }

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    private void stopStartThread(Thread theThread) {
        if (theThread != null)
            onlyStopThread(theThread);
        if (!AppDelegate.haveNetworkConnection(CreateEventActivity.this, false)) {
            mHandler.sendEmptyMessage(6);
        } else {
            theThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    canSearch = false;
                    mHandler.sendEmptyMessage(7);
                    mapPlacesArray.clear();
                    mapPlacesArray.addAll(PlacesService.autocompleteForMap(placeKeyword));
                    mHandler.sendEmptyMessage(8);
                    mHandler.sendEmptyMessage(2);
                    canSearch = true;
                }
            });
            theThread.start();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                finish();
                break;
            case R.id.ll_c_upload_pic:
                if (arrayImageFile.size() < 1) {
                    showImageSelectorList();
                } else {
                    AppDelegate.showAlert(CreateEventActivity.this, "You can't upload more than 1 image.");
                }
                break;
            case R.id.rl_c_from_date:
                showDateDialog(0);
                break;
            case R.id.rl_c_to_date:
                showDateDialog(1);
                break;
            case R.id.rl_c_from_time:
                showTimeDialog(0);
                break;
            case R.id.rl_c_to_time:
                showTimeDialog(1);
                break;
            case R.id.txt_c_right:
                callCreateEventAsync();
                break;

            case R.id.img_c_close_0:
                removePicUpdate(0);
                break;
            case R.id.img_c_close_1:
                removePicUpdate(1);
                break;
            case R.id.img_c_close_2:
                removePicUpdate(2);
                break;
            case R.id.img_c_close_3:
                removePicUpdate(3);
                break;
        }
    }

    private void callCreateEventAsync() {
        if (selected_event_type == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please select event type.");
        } else if (et_event_name.length() == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please enter event name.");
        } else if (selected_location == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please select location type.");
//        } else if (et_location.length() == 0) {
//            AppDelegate.showAlert(CreateEventActivity.this, "Please enter event location.");
        } else if (et_venue.length() == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please enter event venue.");
//        } else if (placeDetail == null) {
//            AppDelegate.showAlert(CreateEventActivity.this, "Please select address from bellow list of venue.");
        } else if (et_theme.length() == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please enter event theme.");
        } else if (et_contact_number.length() == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please enter contact number.");
        } else if (et_email.length() == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please enter email address.");
        } else if (!(AppDelegate.CheckEmail(et_email.getText().toString())) /*|| !et_email.getText().toString().substring(et_email.getText().toString().indexOf("@"), et_email.getText().toString().length()).equals("@edu.com")*/) {
            AppDelegate.showAlert(CreateEventActivity.this, getString(R.string.please_fill_email_address_in_format));
        } else if (txt_c_from_date.length() == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please select from date of event.");
        } else if (txt_c_to_date.length() == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please select to date of event.");
        } else if (txt_c_from_time.length() == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please select from time of event.");
        } else if (txt_c_to_time.length() == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please select to time of event.");
        } else if (selected_fees == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please select fees type.");
        } else if (selected_fees == 2 && et_fees.length() == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please enter event fees.");
        } else if (selected_ticket == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please select ticket type.");
        } else if (et_description.length() == 0) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please enter description.");
        } else if (!fromEdit && capturedFile == null) {
            AppDelegate.showAlert(CreateEventActivity.this, "Please select image file.");
        } else if (!AppDelegate.haveNetworkConnection(CreateEventActivity.this, false)) {
            AppDelegate.addFragment(CreateEventActivity.this.getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        } else {

            /**
             *  user_id,location_type(1=oncampus , 2= off campus),event_type,event_name, banner_image,theme,details, venue,location, gate_fees_type(0= free , 1= paid),
             *  gate_fees,tickets(0 = sold out , 1 = available),contact_no,event_start_time, event_end_time, latitude, longitude,status=1
             *
             *  2016-06-13 10:38:00
             ***/

            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.event_type, eventTypeArray.get(selected_event_type).id + "");
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.event_name, et_event_name.getText().toString());
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.location_type, selected_location + "");
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.venue, et_venue.getText().toString());
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.theme, et_theme.getText().toString() + "");
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.contact_no, et_contact_number.getText().toString() + "");
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.email, et_email.getText().toString() + "");
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.tickets, (selected_ticket - 1) + "");
            String start_time = "";
            try {
                start_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("dd-MM-yyyy hh:mm aa").parse(txt_c_from_date.getText().toString() + " " + txt_c_from_time.getText().toString()));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.event_start_time, start_time + "");
            String end_time = "";
            try {
                end_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("dd-MM-yyyy hh:mm aa").parse(txt_c_to_date.getText().toString() + " " + txt_c_to_time.getText().toString()));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.event_end_time, end_time + "");

            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.gate_fees_type, selected_fees == 1 ? 0 : 1 + "");
            if (selected_fees == 2)
                AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.gate_fees, et_fees.getText().toString() + "");

            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.details, et_description.getText().toString() + "");
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.status, "2");

            if (capturedFile != null) {
                AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.banner_image, capturedFile.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            }

            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.latitude, "0.0");
            AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.longitude, "0.0");
            PostAsync mPostAsyncObj = null;
            if (fromEdit) {
                AppDelegate.getInstance(CreateEventActivity.this).setPostParamsSecond(mPostArrayList, Tags.event_id, eventModel.id + "");
                mPostAsyncObj = new PostAsync(CreateEventActivity.this, this, ServerRequestConstants.EDIT_EVENT,
                        mPostArrayList, null);
            } else {
                mPostAsyncObj = new PostAsync(CreateEventActivity.this, this, ServerRequestConstants.CREATE_EVENT,
                        mPostArrayList, null);
            }
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }


    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.hideKeyBoard(CreateEventActivity.this);
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(CreateEventActivity.this)) {
            if (mapPlacesArray.get(position).postal_code != null) {
                placeDetail = mapPlacesArray.get(position);
                canSearch = false;
                if (AppDelegate.isValidString(mapPlacesArray.get(position).name))
                    et_venue.setText(mapPlacesArray.get(position).name);
                else
                    et_venue.setText(mapPlacesArray.get(position).vicinity);
                et_venue.setSelection(et_venue.length());
                canSearch = true;
                selected_item_position = position;
                mHandler.sendEmptyMessage(21);
                this.latLng = new LatLng(mapPlacesArray.get(position).latitude, mapPlacesArray.get(position).longitude);
                mHandler.sendEmptyMessage(8);
                mHandler.sendEmptyMessage(11);
            } else {
                placeDetail = mapPlacesArray.get(position);
                canSearch = false;
                if (AppDelegate.isValidString(mapPlacesArray.get(position).name))
                    et_venue.setText(mapPlacesArray.get(position).name);
                else
                    et_venue.setText(mapPlacesArray.get(position).vicinity);
                et_venue.setSelection(et_venue.length());
                selected_item_position = position;
                mHandler.sendEmptyMessage(21);
                canSearch = true;
                mHandler.sendEmptyMessage(10);
                new GetLatLongFromPlaceIdAsync(CreateEventActivity.this,
                        this, Tags.LAT_LNG_COLLECT,
                        placeDetail.getId()).execute();
            }
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (mActivity != null)
                startActivity(new Intent(CreateEventActivity.this, NoInternetConnectionActivity.class));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_EVENT_CATEGORY)) {
            mHandler.sendEmptyMessage(11);
            parseEventResult(result);
            if (fromEdit)
                setValues();
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.CREATE_EVENT) || apiName.equalsIgnoreCase(ServerRequestConstants.EDIT_EVENT)) {
            mHandler.sendEmptyMessage(11);
            parseCreateEventResult(result);
        }
    }

    private void parseCreateEventResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);

//                EventModel eventModel = new EventModel();
//                eventModel.id = object.getString(Tags.id);
//                eventModel.location_type = object.getString(Tags.location_type);
//                eventModel.event_type = object.getString(Tags.event_type);
//                eventModel.event_name = object.getString(Tags.event_name);
//                eventModel.banner_image = object.getString(Tags.banner_image);
//
//                eventModel.total_event_views = object.getString(Tags.total_event_views);
//                eventModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);
//                eventModel.email_address = object.getString(Tags.email);
//                eventModel.theme = object.getString(Tags.theme);
//                eventModel.details = object.getString(Tags.details);
//                eventModel.venue = object.getString(Tags.venue);
//                eventModel.location = JSONParser.getString(object, Tags.location);
//                eventModel.tickets = object.getString(Tags.tickets);
//
//                eventModel.gate_fees_type = object.getInt(Tags.gate_fees_type);
//                eventModel.gate_fees = object.getString(Tags.gate_fees);
//                eventModel.contact_no = object.getString(Tags.contact_no);
//                eventModel.event_start_time = object.getString(Tags.event_start_time);
//
//                eventModel.event_end_time = object.getString(Tags.event_end_time);
//                eventModel.created = object.getString(Tags.created);
////                    eventModel.modified = object.getString(Tags.modified);
//                eventModel.banner_image_thumb = object.getString(Tags.banner_image_thumb);
//
//                eventModel.latitude = object.getString(Tags.latitude);
//                eventModel.longitude = object.getString(Tags.longitude);
//
//
//                JSONObject userObject = object.getJSONObject(Tags.user);
//                eventModel.user_id = JSONParser.getString(userObject, Tags.id);
//                eventModel.user_first_name = userObject.getString(Tags.first_name);
//                eventModel.user_last_name = userObject.getString(Tags.last_name);
//                eventModel.user_image = userObject.getString(Tags.image);
//
//                JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
//                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
//                    eventModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
//                    eventModel.user_institution_id = studentObject.getString(Tags.institution_id);
//                    if (studentObject.has(Tags.institute_name) && AppDelegate.isValidString(studentObject.optString(Tags.institute_name))) {
//                        eventModel.user_institution_name = studentObject.getString(Tags.institution_name);
//                    }
//                    if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
//                        eventModel.user_department_name = studentObject.getString(Tags.department_name);
//                    } else {
//                        eventModel.user_department_name = studentObject.getString(Tags.department_name);
//                    }
//                } else {
//                    eventModel.user_institution_name = studentObject.getString(Tags.other_ins_name);
//                    eventModel.user_department_name = studentObject.getString(Tags.department_name);
//                }

                onBackPressed();
                if (fromEdit && EventDetailActivity.mHandler != null) {
                    EventDetailActivity.mHandler.sendEmptyMessage(3);
                }
                if (AppDelegate.isValidString(tagFrom)) {
                    if (tagFrom.equalsIgnoreCase(Tags.MY_EVENT)) {
//                        if (MyEventListingFragment.mHandler != null) {
//                            MyEventListingFragment.mHandler.sendEmptyMessage(4);
//                        } else {
//                            AppDelegate.LogE("My Event handler is null");
//                        }
                    } else if (tagFrom.equalsIgnoreCase(Tags.eventOffCampus)) {
                        if (EventsCampusBasedFragment.mHandler != null) {
                            EventsCampusBasedFragment.mHandler.sendEmptyMessage(21);
                        } else {
                            AppDelegate.LogE("EventCampusBased for Off Campus handler is null");
                        }
                    } else if (tagFrom.equalsIgnoreCase(Tags.eventOnCampus)) {
                        if (EventsCampusBasedFragment.mHandler != null) {
                            EventsCampusBasedFragment.mHandler.sendEmptyMessage(22);
                        } else {
                            AppDelegate.LogE("EventCampusBased for on Campus handler is null");
                        }
                    }
                } else {
                    AppDelegate.LogE("CreateEventActivity => tagFrom is null");
                }
            } else {
                AppDelegate.showAlert(CreateEventActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            if (mActivity != null)
                startActivity(new Intent(CreateEventActivity.this, NoInternetConnectionActivity.class));
        }
    }

    private void parseEventResult(String result) {
        try {
            JSONObject object = new JSONObject(result);
            if (object.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                JSONArray jsonArray = object.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    EventCategoryModel model = new EventCategoryModel();
                    model.id = jsonObject.getString(Tags.id);
                    model.cat_name = jsonObject.getString(Tags.cat_name);
                    model.status = jsonObject.getString(Tags.status);
                    model.created = jsonObject.getString(Tags.created);
                    model.modified = jsonObject.getString(Tags.modified);
                    eventTypeArray.add(model);
                    arrayStringTypeEvent.add(jsonObject.getString(Tags.cat_name));
                }
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showAlert(CreateEventActivity.this, object.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void removePicUpdate(int i) {
        switch (i) {
            case 0:
                if (arrayImageFile.size() > 0) {
                    arrayImageFile.remove(0);
                }
                break;
            case 1:
                if (arrayImageFile.size() > 1) {
                    arrayImageFile.remove(1);
                }
                break;
            case 2:
                if (arrayImageFile.size() > 2) {
                    arrayImageFile.remove(2);
                }
                break;
            case 3:
                if (arrayImageFile.size() > 3) {
                    arrayImageFile.remove(3);
                }
                break;
        }
        updateImageView();
    }

    private int mYear, mMonth, mDay, mHour, mMinute;

    private void showDateDialog(final int value) {
        final Calendar c = Calendar.getInstance();
        if (value == 0) {
            if (AppDelegate.isValidString(eventModel.from_date)) {
                c.setTime(getDateObject(eventModel.from_date, DATE_FORMAT));
            }
        } else {
            if (AppDelegate.isValidString(eventModel.to_date)) {
                c.setTime(getDateObject(eventModel.to_date, DATE_FORMAT));
            }
        }
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(CreateEventActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // Display Selected date in textbox
                        if (value == 0) {
                            txt_c_from_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            eventModel.from_date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                            eventModel.to_date = "";
                            txt_c_to_date.setText("To");
                        } else {
                            if (AppDelegate.isValidString(eventModel.from_date)) {
                                String selected_to_date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                if (DateUtils.isAfterDay(getDateObject(selected_to_date, DATE_FORMAT), getDateObject(eventModel.from_date, DATE_FORMAT)) || DateUtils.isSameDay(getDateObject(selected_to_date, DATE_FORMAT), getDateObject(eventModel.from_date, DATE_FORMAT))) {
                                    txt_c_to_date.setText(selected_to_date);
                                    eventModel.to_date = selected_to_date;
                                } else {
                                    AppDelegate.showAlert(CreateEventActivity.this, "To date must be greater than from date.");
                                }
                            } else {
                                AppDelegate.showAlert(CreateEventActivity.this, "Please select from date.");
                            }

                        }
                    }
                }, mYear, mMonth, mDay);
        dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 10000);
        dpd.show();
    }

    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String TIME_FORMAT_12_HOUR = "hh:mm aa";
    public static final String TIME_FORMAT_24_HOUR = "HH:mm";

    public static Date getDateObject(String date, String date_format) {
        try {
            return new SimpleDateFormat(date_format).parse(date);
        } catch (ParseException e) {
            AppDelegate.LogE(e);
        }
        return Calendar.getInstance().getTime();
    }

    public void showTimeDialog(final int value) {
        final Calendar c = Calendar.getInstance();
        if (value == 0) {
            if (AppDelegate.isValidString(eventModel.from_date)) {
                c.setTime(getDateObject(eventModel.from_date, DATE_FORMAT));
            }
        } else {
            if (AppDelegate.isValidString(eventModel.to_date)) {
                c.setTime(getDateObject(eventModel.to_date, DATE_FORMAT));
            }
        }
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog tpd = new TimePickerDialog(CreateEventActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        // Display Selected time in text box
                        // txtTime.setText(hourOfDay + ":" + minute);
                        if (AppDelegate.isValidString(eventModel.to_date)) {
                            try {
                                String selected_time = new SimpleDateFormat(TIME_FORMAT_12_HOUR).format(new SimpleDateFormat(TIME_FORMAT_24_HOUR).parse(hourOfDay + ":" + minute));
                                if (value == 0) {
                                    txt_c_from_time.setText(selected_time);
                                    eventModel.from_time = selected_time;
                                    txt_c_to_time.setText("");
                                    eventModel.to_time = "";
                                } else {

                                    if (AppDelegate.isValidString(eventModel.from_time)) {
                                        Date fromdate = new SimpleDateFormat(TIME_FORMAT_12_HOUR).parse(eventModel.from_time);
                                        Date todate = new SimpleDateFormat(TIME_FORMAT_12_HOUR).parse(selected_time);
                                        if (!DateUtils.isSameDay(getDateObject(eventModel.to_date, DATE_FORMAT), getDateObject(eventModel.from_date, DATE_FORMAT))) {
                                            txt_c_to_time.setText(selected_time);
                                            eventModel.to_time = selected_time;
                                        } else {
                                            long diff_time = todate.getTime() - fromdate.getTime();
                                            if (fromdate.before(todate)) {
                                                if (diff_time >= 1800000) {
                                                    AppDelegate.LogT("in diff " + diff_time);
                                                    txt_c_to_time.setText(selected_time);
                                                    eventModel.to_time = selected_time;
                                                } else {
                                                    AppDelegate.showAlert(CreateEventActivity.this, "Difference between Start Time and End Time should be minimum of 30 minutes.");
                                                }
                                            } else {
                                                AppDelegate.showAlert(CreateEventActivity.this, "Please to time must be greater than from time.");
                                            }
                                        }
                                    } else {
                                        AppDelegate.showAlert(CreateEventActivity.this, "Please select from time.");
                                    }
                                }
                            } catch (ParseException e) {
                                AppDelegate.LogE(e);
                            }
                        } else

                        {
                            AppDelegate.showAlert(CreateEventActivity.this, "Please select from and to date.");
                        }
                    }
                }

                , mHour, mMinute, false);
        tpd.show();
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(CreateEventActivity.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateEventActivity.this);
        ListView modeList = new ListView(CreateEventActivity.this);
        String[] stringArray = new String[]{"  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(CreateEventActivity.this, R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        CreateEventActivity.this.startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), AppDelegate.SELECT_PICTURE);
    }


    @Override
    public void onReciveApiResult(String ApiName, String ActionName, LatLng latLng) {
        if (mActivity != null) {
            AppDelegate
                    .LogE("LocationMapFrag is deattached but still called at onReciveApiResult for ApiName = "
                            + ApiName + ", ActionName = " + ActionName);
        } else if (ApiName.equalsIgnoreCase(Tags.LAT_LNG_COLLECT)
                && ActionName.equalsIgnoreCase(Tags.SUCCESS) && latLng != null) {
            this.latLng = latLng;
            mHandler.sendEmptyMessage(8);
            mHandler.sendEmptyMessage(11);
        }
    }

    @Override
    public void onReciveApiResult(String ApiName, String ActionName, Place placeDetails) {
        if (mActivity == null) {
            AppDelegate
                    .LogE("LocationMapFrag is deattached but still called at onReciveApiResult for ApiName = "
                            + ApiName + ", ActionName = " + ActionName);
        } else if (ApiName.equalsIgnoreCase(Tags.LAT_LNG_COLLECT)
                && ActionName.equalsIgnoreCase(Tags.SUCCESS) && placeDetails != null) {
            mHandler.sendEmptyMessage(11);
            this.latLng = new LatLng(placeDetails.latitude, placeDetails.longitude);
            mHandler.sendEmptyMessage(8);

//            this.placeDetail.name = placeDetails.name;
//            this.placeDetail.vicinity = placeDetails.vicinity;
            this.placeDetail.icon = placeDetails.icon;
            this.placeDetail.city = placeDetails.city;
            this.placeDetail.country = placeDetails.country;
            this.placeDetail.latitude = placeDetails.latitude;
            this.placeDetail.longitude = placeDetails.longitude;
            this.placeDetail.postal_code = placeDetails.postal_code;
            this.placeDetail.formatted_phone_number = placeDetails.formatted_phone_number;
//            canSearch = false;
//            et_venue.setText(placeDetail.name + ", " + placeDetail.vicinity);
//            canSearch = true;
        }
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(CreateEventActivity.this, "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            CreateEventActivity.this.startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }

    }

    @Override
    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            try {
                OriginalPhoto = MediaStore.Images.Media.getBitmap(CreateEventActivity.this.getContentResolver(), picUri);
//                OriginalPhoto = AppDelegate.getResizedBitmap(OriginalPhoto, 300);
                capturedFile = new File(getNewFile());
                FileOutputStream fOut = null;
                try {
                    fOut = new FileOutputStream(capturedFile);
                } catch (FileNotFoundException e) {
                    AppDelegate.LogE(e);
                }
                OriginalPhoto.compress(Bitmap.CompressFormat.JPEG, 50, fOut);
                try {
                    fOut.flush();
                } catch (IOException e) {
                    AppDelegate.LogE(e);
                }
                try {
                    fOut.close();
                } catch (IOException e) {
                    AppDelegate.LogE(e);
                }
                arrayImageFile.add(capturedFile);
                updateImageView();
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        mHandler.sendEmptyMessage(9);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    private void setVisiblePic(int value) {
        rl_c_pic_0.setVisibility(View.INVISIBLE);
        rl_c_pic_1.setVisibility(View.INVISIBLE);
        rl_c_pic_2.setVisibility(View.INVISIBLE);
        rl_c_pic_3.setVisibility(View.INVISIBLE);
        switch (value) {
            case 0:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                break;
            case 1:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                rl_c_pic_1.setVisibility(View.VISIBLE);
                break;
            case 2:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                rl_c_pic_1.setVisibility(View.VISIBLE);
                rl_c_pic_2.setVisibility(View.VISIBLE);
                break;
            case 3:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                rl_c_pic_1.setVisibility(View.VISIBLE);
                rl_c_pic_2.setVisibility(View.VISIBLE);
                rl_c_pic_3.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void updateImageView() {
        ll_c_img_layout.setVisibility(View.VISIBLE);
        if (arrayImageFile.size() == 1) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            setVisiblePic(0);
        } else if (arrayImageFile.size() == 2) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            img_c_pic_1.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(1).getAbsolutePath()));
            setVisiblePic(1);
        } else if (arrayImageFile.size() == 3) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            img_c_pic_1.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(1).getAbsolutePath()));
            img_c_pic_2.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(02).getAbsolutePath()));
            setVisiblePic(2);
        } else if (arrayImageFile.size() == 4) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            img_c_pic_1.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(01).getAbsolutePath()));
            img_c_pic_2.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(02).getAbsolutePath()));
            img_c_pic_3.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(03).getAbsolutePath()));
            setVisiblePic(3);
        } else {
            ll_c_img_layout.setVisibility(View.GONE);
        }

    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = CreateEventActivity.this.getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("onActivityResult AddItemFoundActivity");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppDelegate.SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(CreateEventActivity.this, data.getData());
                } else {
                    Toast.makeText(CreateEventActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            } else if (requestCode == AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE) {
                if (imageURI != null) {
                    startCropActivity(CreateEventActivity.this, imageURI);
                } else {
                    Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            setOnReceivePictureResult(Tags.PICTURE, resultUri);
        } else {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    public static void startCropActivity(FragmentActivity mActivity, Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".png";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(mActivity.getCacheDir(), destinationFileName)));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);
        uCrop.start(mActivity);
    }

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop basisConfig(@NonNull UCrop uCrop) {
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }

    /**
     * Sometimes you want to adjust more options, it's done via {@link UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setCompressionQuality(100);
        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);
        return uCrop.withOptions(options);
    }

}
