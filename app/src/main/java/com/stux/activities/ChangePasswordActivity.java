package com.stux.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;

import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by Bharat on 07/04/2016.
 */
public class ChangePasswordActivity extends AppCompatActivity implements OnReciveServerResponse, View.OnClickListener {

    private EditText et_c_old_password, et_c_new_password, et_c_confirm_password;
    private Prefs prefs;
    private UserDataModel dataModel;
    public Activity mActivity;

    private Handler mHandler;
    private android.widget.ImageView img_old_password, img_new_password, img_confirm_new_password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.change_password);
        mActivity = this;
        prefs = new Prefs(ChangePasswordActivity.this);
        dataModel = prefs.getUserdata();
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(ChangePasswordActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(ChangePasswordActivity.this);
                } else if (msg.what == 1) {

                }
            }
        };
    }

    private void initView() {
        ((TextView) findViewById(R.id.txt_c_header)).setText("Change Password");
        findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ((ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        ((ImageView) findViewById(R.id.img_c_left)).setVisibility(View.VISIBLE);

        et_c_old_password = (EditText) findViewById(R.id.et_c_old_password);
        et_c_old_password.setTypeface(Typeface.createFromAsset(ChangePasswordActivity.this.getAssets(), getString(R.string.font_HN_bold)));
        et_c_new_password = (EditText) findViewById(R.id.et_c_new_password);
        et_c_new_password.setTypeface(Typeface.createFromAsset(ChangePasswordActivity.this.getAssets(), getString(R.string.font_HN_bold)));
        et_c_confirm_password = (EditText) findViewById(R.id.et_c_confirm_password);
        et_c_confirm_password.setTypeface(Typeface.createFromAsset(ChangePasswordActivity.this.getAssets(), getString(R.string.font_HN_bold)));

        img_old_password = (android.widget.ImageView) findViewById(R.id.img_old_password);
        img_old_password.setOnClickListener(this);
        img_new_password = (android.widget.ImageView) findViewById(R.id.img_new_password);
        img_new_password.setOnClickListener(this);
        img_confirm_new_password = (android.widget.ImageView) findViewById(R.id.img_confirm_new_password);
        img_confirm_new_password.setOnClickListener(this);

        findViewById(R.id.txt_c_continue).setOnClickListener(this);
    }

    private void callChangePasswordAsync() {
        if (et_c_old_password.length() == 0) {
            AppDelegate.showAlert(ChangePasswordActivity.this, "Please enter your Old Password.");
        } else if (et_c_new_password.length() == 0) {
            AppDelegate.showAlert(ChangePasswordActivity.this, "Please enter your New Password.");
        } else if (et_c_confirm_password.length() == 0) {
            AppDelegate.showAlert(ChangePasswordActivity.this, "Please enter your Confirm New Password.");
        } else if (!et_c_new_password.getText().toString().equals(et_c_confirm_password.getText().toString())) {
            AppDelegate.showAlert(ChangePasswordActivity.this, "New Password and Confirm New Passwrord must be same, Please enter valid New and Confirm Password.");
        } else if (AppDelegate.haveNetworkConnection(ChangePasswordActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();

            AppDelegate.getInstance(ChangePasswordActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(ChangePasswordActivity.this).setPostParamsSecond(mPostArrayList, Tags.old_password, et_c_old_password.getText().toString());
            AppDelegate.getInstance(ChangePasswordActivity.this).setPostParamsSecond(mPostArrayList, Tags.new_password, et_c_new_password.getText().toString());

            PostAsync mPostasyncObj = new PostAsync(ChangePasswordActivity.this,
                    ChangePasswordActivity.this, ServerRequestConstants.CHANGE_PASSWORD,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            startActivity(new Intent(ChangePasswordActivity.this, NoInternetConnectionActivity.class));
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (mActivity != null)
                startActivity(new Intent(ChangePasswordActivity.this, NoInternetConnectionActivity.class));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.CHANGE_PASSWORD)) {
            parseFavCategoryResult(result);
        }
    }

    private void parseFavCategoryResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(ChangePasswordActivity.this, "Password changed successfully!");
                onBackPressed();
            } else {
                AppDelegate.showAlert(ChangePasswordActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            if (mActivity != null)
                startActivity(new Intent(ChangePasswordActivity.this, NoInternetConnectionActivity.class));

            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_old_password:
                img_old_password.setSelected(!img_old_password.isSelected());
                if (img_old_password.isSelected()) {
                    et_c_old_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    et_c_old_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                et_c_old_password.setSelection(et_c_old_password.length());
                break;

            case R.id.img_new_password:
                img_new_password.setSelected(!img_new_password.isSelected());
                if (img_new_password.isSelected()) {
                    et_c_new_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    et_c_new_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                et_c_new_password.setSelection(et_c_new_password.length());
                break;

            case R.id.img_confirm_new_password:
                img_confirm_new_password.setSelected(!img_confirm_new_password.isSelected());
                if (img_confirm_new_password.isSelected()) {
                    et_c_confirm_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    et_c_confirm_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                et_c_confirm_password.setSelection(et_c_confirm_password.length());
                break;

            case R.id.txt_c_continue:
                callChangePasswordAsync();
                break;
        }
    }

}
