package com.stux.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.stux.AppDelegate;
import com.stux.R;

import carbon.widget.TextView;

/**
 * Created by Bharat on 06/28/2016.
 */
public class NoInternetConnectionActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txt_c_wake_up, txt_c_internet, txt_c_try_again;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.no_internet_connected);
        initView();
    }

    private void initView() {
        findViewById(R.id.img_c_header).setVisibility(View.VISIBLE);
        findViewById(R.id.txt_c_header).setVisibility(View.GONE);
//        if (getIntent().getExtras().getString(Tags.FROM).equalsIgnoreCase(Tags.LOGIN)) {
//            findViewById(R.id.img_c_left).setVisibility(View.GONE);
//        } else {
//            findViewById(R.id.img_c_left).setVisibility(View.GONE);
//        }

        findViewById(R.id.img_c_right).setVisibility(View.GONE);

        txt_c_wake_up = (TextView) findViewById(R.id.txt_c_wake_up);
        txt_c_internet = (TextView) findViewById(R.id.txt_c_internet);
        txt_c_try_again = (TextView) findViewById(R.id.txt_c_try_again);
        txt_c_try_again.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_try_again:
                if (AppDelegate.haveNetworkConnection(NoInternetConnectionActivity.this, false)) {
                    finish();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }
}
