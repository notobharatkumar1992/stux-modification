package com.stux.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
import com.stux.Adapters.ChooseCategoryListAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.DealModel;
import com.stux.Models.NotificationModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductCategoryModel;
import com.stux.Models.ProductModel;
import com.stux.Models.RefineSearchModel;
import com.stux.Models.UserDataModel;
import com.stux.PushNotificationService;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.Utils.DividerItemDecoration;
import com.stux.Utils.Prefs;
import com.stux.constants.Constants;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.fragments.ChatFragment;
import com.stux.fragments.DealsMoviesFragment;
import com.stux.fragments.EventsCampusBasedFragment;
import com.stux.fragments.FollowingListFragment;
import com.stux.fragments.HomeFragment;
import com.stux.fragments.JobsInternshipFragment;
import com.stux.fragments.JobsPageInternshipFragment;
import com.stux.fragments.JobsPageJOBSFragment;
import com.stux.fragments.MyChatListingFragment;
import com.stux.fragments.MyDealListingFragment;
import com.stux.fragments.MyEventListingFragment;
import com.stux.fragments.MyProductListFragment;
import com.stux.fragments.MyProfileFragment;
import com.stux.fragments.NoInternetConnectionFragment;
import com.stux.fragments.ProductDetailFragment;
import com.stux.fragments.ProfileLandingFragment;
import com.stux.fragments.RefineProductFragment;
import com.stux.fragments.RefineSearchFragment;
import com.stux.fragments.SearchProductFragment;
import com.stux.fragments.SellersProfileFragment;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnPictureResult;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.service.ChatService;
import com.stux.service.ServiceToDetectAppKilled;
import com.wunderlist.slidinglayer.SlidingLayer;
import com.wunderlist.slidinglayer.transformer.AlphaTransformer;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import SlidingPaneLayout.SlidingPaneLayout1;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.NotificationRecyclerViewAdapter;

/**
 * Created by NOTO on 5/24/2016.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener, OnReciveServerResponse, EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener, OnPictureResult {
    public static OnReciveServerResponse onReciveServerResponse;
    public static Activity mActivity;
    public static final int COLLAPSE = 0, EXPAND = 1;
    public Handler mHandler;
    public static SlidingPaneLayout1 mSlidingPaneLayout;
    public static final int PANEL_PROFILE = 20, PANEL_DASHBOARD = 0, PANEL_FOLLOWING = 1, PANEL_MY_LIST = 2, PANEL_LIST_ITEM = 3, PANEL_MY_COUPON = 4, PANEL_DEALS_MOVIES = 5, PANEL_EVENTS = 6, PANEL_NOTIFICATION = 7, PANEL_N_MESSAGE = 71, PANEL_N_DEALS = 72, PANEL_N_COUPONS = 73, PANEL_INTERNSHIP_JOBS = 8, PANEL_LOST_FOUND = 9, PANEL_HELP = 10, PANEL_LOGOUT = 11, PANEL_MY_EVENTS = 12, PANEL_MY_DEALS = 13, PANEL_MY_CHAT = 14, PANEL_CAT_PRODUCT = 15;

    public static RelativeLayout user_name_layout, panel_dashboard_layout, panel_my_listing_layout,
            panel_list_item_layout, panel_my_lists_layout, panel_deals_movies_layout, panel_events_layout, panel_notifications_layout,
            panel_intership_jobs_layout, panel_lost_found_layout, panel_help_layout, panel_logout_layout, panel_my_coupon_layout,
            panel_messages_alert_layout, panel_deals_alert_layout, panel_coupons_alert_layout, panel_following_layout, panel_my_event_layout, panel_my_deals_layout, panel_chat_layout, panel_cat_product_layout;
    public static TextView txt_c_user_name, txt_c_address;
    public SlideMenuClickListener menuClickListener;
    //  public TextView panel_home_txt, panel_find_ride_txt, panel_booked_ride_txt,
//            panel_completed_ride_txt, panel_see_family_txt, panel_profile_txt,
//            panel_logout_txt, panel_login_txt;
    public ImageView panel_home_img, panel_find_ride_imge,
            panel_booked_ride_img, panel_completed_ride_img,
            panel_see_family_img, panel_profile_img, panel_logout_img,
            panel_login_img, img_loading, panel_notifications_arrow_img, panel_deals_arrow_img, panel_events_arrow_img, panel_cate_arrow_img;
    public CircleImageView cimg_user;
    public LinearLayout side_panel;
    public boolean isSlideOpen = false;
    public int ratio, ride_cancel_time = 30;
    public float init = 0.0f;

    public SlidingLayer sl_more;

    private RecyclerView recyclerView;
    private NotificationRecyclerViewAdapter rcAdapter;
    public ArrayList<NotificationModel> notificationArray = new ArrayList<>();
    private ProgressBar progressbar;
    private TextView txt_c_no_list;

    public Prefs prefs;
    public ExpandableRelativeLayout exp_layout, exp_deals_movies_layout, exp_events_layout, expandable_search/*, exp_category*/;
    private UserDataModel dataModel;

    public carbon.widget.ImageView img_c_clear;
    public EditText et_search;
    public LinearLayout ll_search_hint;

    // For category async.
    private boolean isCategoryAsyncCalled = false;
    private ListView list_category;
    private ProgressBar progress_category;
    private TextView txt_c_no_cate_retry;
    private ChooseCategoryListAdapter categoryListAdapter;
    public ArrayList<ProductCategoryModel> arrayProductCategory = new ArrayList<>();
    public RefineSearchModel refineSearchModel;
    public RelativeLayout rl_c_category_list;

    // Picture Interface
    public static OnPictureResult onPictureResult;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    public static void isMapNotContain(boolean enable) {
        if (mSlidingPaneLayout != null) {
            mSlidingPaneLayout.setEnable(enable);
            mSlidingPaneLayout.setEnabled(enable);
            Constants.isMapScreen = !enable;
        }
    }

    public static void setEnablePanel(boolean enable) {
        if (mSlidingPaneLayout != null) {
            mSlidingPaneLayout.setEnable(enable);
            mSlidingPaneLayout.setEnabled(enable);
            Constants.isMapScreen = enable;
        }
    }

    @SuppressWarnings("deprecation")
    public static void setInitailSideBar(Context mContext, int value) {
        if (mContext == null)
            return;
        panel_dashboard_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_my_listing_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_list_item_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_my_lists_layout.setBackgroundDrawable(mContext
                .getResources().getDrawable(
                        R.drawable.sl_bg_panel));
        panel_deals_movies_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_events_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_notifications_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_intership_jobs_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
//        switch (value) {
//            case 0:
//                panel_dashboard_layout.setBackgroundDrawable(mContext.getResources()
//                        .getDrawable(R.drawable.selector_slide_background_purple));
//                break;
//            case 1:
//                panel_my_listing_layout.setBackgroundDrawable(mContext
//                        .getResources().getDrawable(
//                                R.drawable.selector_slide_background_purple));
//                break;
//            case 2:
//                panel_list_item_layout.setBackgroundDrawable(mContext
//                        .getResources().getDrawable(
//                                R.drawable.selector_slide_background_purple));
//                break;
//            case 3:
//                panel_my_lists_layout.setBackgroundDrawable(mContext
//                        .getResources().getDrawable(
//                                R.drawable.selector_slide_background_purple));
//                break;
//            case 4:
//                panel_deals_movies_layout.setBackgroundDrawable(mContext
//                        .getResources().getDrawable(
//                                R.drawable.selector_slide_background_purple));
//                break;
//            case 5:
//                panel_events_layout.setBackgroundDrawable(mContext.getResources()
//                        .getDrawable(R.drawable.selector_slide_background_purple));
//                break;
//            default:
//                break;
//        }
    }

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value) {
        expLayout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout.collapse();
                } else {
                    expLayout.expand();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppDelegate.LogT("onPause called at MainActivity");
    }

    @Override
    protected void onDestroy() {
//        ChatService.callLogoutAsync(this, dataModel.userId, prefs.getUserdata().user_online_time_id);
//        if (prefs.getUserdata() != null)
//            ChatService.startApiOnce(prefs.getUserdata().userId, prefs.getUserdata().user_online_time_id);
        AppDelegate.LogT("onDestroy called at MainActivity");
        super.onDestroy();
        mActivity = null;
        onReciveServerResponse = null;
        if (HomeFragment.productArray != null)
            HomeFragment.productArray.clear();
        if (RefineProductFragment.productArray != null)
            RefineProductFragment.productArray.clear();
        if (MyProfileFragment.productArray != null)
            MyProfileFragment.productArray.clear();
        if (MyProductListFragment.productArray != null)
            MyProductListFragment.productArray.clear();
    }

    private void callLogoutAsync() {
        try {
            if (AppDelegate.haveNetworkConnection(this, false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.is_login, "0");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_online_time_id, prefs.getUserdata().user_online_time_id);
                PostAsync mPostAsyncObj = new PostAsync(this,
                        this, ServerRequestConstants.LOGOUT,
                        mPostArrayList, null);
//                AppDelegate.LogT("callLogoutAsync called at MainActivity");
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.activity_main);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        Constants.isMapScreen = true;
        mActivity = this;
        onReciveServerResponse = this;
        prefs = new Prefs(this);
        dataModel = prefs.getUserdata();
        onPictureResult = this;
        showFragment();
        initView();
        setHandler();
        updateUserDetail();
        startService(new Intent(this, ServiceToDetectAppKilled.class));
        startService(new Intent(this, ChatService.class));


//        for (int i = 0; i > 5; i++) {
//            AppDelegate.LogT("i => " + i);
//        }
    }

    private void showFragment() {
        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle.getString(Tags.FROM).equalsIgnoreCase(Tags.follow)) {
                AppDelegate.loadFragment(getSupportFragmentManager(), new HomeFragment());
                Fragment fragment = new SellersProfileFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment);
            } else if (bundle.getString(Tags.FROM).equalsIgnoreCase(Tags.deal)) {
                AppDelegate.loadFragment(getSupportFragmentManager(), new HomeFragment());

                Intent intent = new Intent(MainActivity.this, DealDetailsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            } else if (bundle.getString(Tags.FROM).equalsIgnoreCase(Tags.product)) {
                AppDelegate.loadFragment(getSupportFragmentManager(), new HomeFragment());
                Fragment fragment = new ProductDetailFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment);
            } else if (bundle.getString(Tags.FROM).equalsIgnoreCase(Tags.CHAT)) {
                AppDelegate.loadFragment(getSupportFragmentManager(), new HomeFragment());
                Fragment fragment = new ChatFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment);
            } else if (bundle.getString(Tags.FROM).equalsIgnoreCase(Tags.makeOffer)) {
                AppDelegate.loadFragment(getSupportFragmentManager(), new HomeFragment());
                Fragment fragment = new SellersProfileFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment);
            }
        } else if (dataModel != null && AppDelegate.isValidString(dataModel.image)) {
            AppDelegate.loadFragment(getSupportFragmentManager(), new HomeFragment());
        } else {
            AppDelegate.loadFragment(getSupportFragmentManager(), new ProfileLandingFragment());
        }
    }

    private void initView() {
        side_panel = (LinearLayout) findViewById(R.id.side_panel);
        mSlidingPaneLayout = (SlidingPaneLayout1) findViewById(R.id.sliding_pane);
        mSlidingPaneLayout.setSliderFadeColor(getResources().getColor(android.R.color.transparent));
        mSlidingPaneLayout.setPanelSlideListener(new SliderListener());

        user_name_layout = (RelativeLayout) findViewById(R.id.user_name_layout);
        user_name_layout.setOnClickListener(this);

        panel_dashboard_layout = (RelativeLayout) findViewById(R.id.panel_dashboard_layout);
        panel_dashboard_layout.setOnClickListener(this);
        panel_my_listing_layout = (RelativeLayout) findViewById(R.id.panel_my_listing_layout);
        panel_my_listing_layout.setOnClickListener(this);
        panel_list_item_layout = (RelativeLayout) findViewById(R.id.panel_list_item_layout);
        panel_list_item_layout.setOnClickListener(this);
        panel_my_lists_layout = (RelativeLayout) findViewById(R.id.panel_my_lists_layout);
        panel_my_lists_layout.setOnClickListener(this);
        panel_deals_movies_layout = (RelativeLayout) findViewById(R.id.panel_deals_movies_layout);
        panel_deals_movies_layout.setOnClickListener(this);
        panel_events_layout = (RelativeLayout) findViewById(R.id.panel_events_layout);
        panel_events_layout.setOnClickListener(this);
        panel_notifications_layout = (RelativeLayout) findViewById(R.id.panel_notifications_layout);
        panel_notifications_layout.setOnClickListener(this);
        panel_intership_jobs_layout = (RelativeLayout) findViewById(R.id.panel_intership_jobs_layout);
        panel_intership_jobs_layout.setOnClickListener(this);
        panel_lost_found_layout = (RelativeLayout) findViewById(R.id.panel_lost_found_layout);
        panel_lost_found_layout.setOnClickListener(this);
        panel_help_layout = (RelativeLayout) findViewById(R.id.panel_help_layout);
        panel_help_layout.setOnClickListener(this);
        panel_logout_layout = (RelativeLayout) findViewById(R.id.panel_logout_layout);
        panel_logout_layout.setOnClickListener(this);
        panel_my_coupon_layout = (RelativeLayout) findViewById(R.id.panel_my_coupon_layout);
        panel_my_coupon_layout.setOnClickListener(this);

        panel_messages_alert_layout = (RelativeLayout) findViewById(R.id.panel_messages_alert_layout);
        panel_messages_alert_layout.setOnClickListener(this);
        panel_deals_alert_layout = (RelativeLayout) findViewById(R.id.panel_deals_alert_layout);
        panel_deals_alert_layout.setOnClickListener(this);
        panel_coupons_alert_layout = (RelativeLayout) findViewById(R.id.panel_coupons_alert_layout);
        panel_coupons_alert_layout.setOnClickListener(this);
        panel_following_layout = (RelativeLayout) findViewById(R.id.panel_following_layout);
        panel_following_layout.setOnClickListener(this);

        panel_my_event_layout = (RelativeLayout) findViewById(R.id.panel_my_event_layout);
        panel_my_event_layout.setOnClickListener(this);
        panel_my_deals_layout = (RelativeLayout) findViewById(R.id.panel_my_deals_layout);
        panel_my_deals_layout.setOnClickListener(this);
        panel_chat_layout = (RelativeLayout) findViewById(R.id.panel_chat_layout);
        panel_chat_layout.setOnClickListener(this);
        panel_cat_product_layout = (RelativeLayout) findViewById(R.id.panel_cat_product_layout);
        panel_cat_product_layout.setOnClickListener(this);

        findViewById(R.id.view_deals_movies_arrow).setOnClickListener(this);
        findViewById(R.id.view_events_arrow).setOnClickListener(this);


        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        expandableView(exp_layout, COLLAPSE);

        exp_deals_movies_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_deals_movies_layout);
        expandableView(exp_deals_movies_layout, COLLAPSE);

        exp_events_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_events_layout);
        expandableView(exp_events_layout, COLLAPSE);

        expandable_search = (ExpandableRelativeLayout) findViewById(R.id.expandable_search);
        expandableView(expandable_search, COLLAPSE);

       /* exp_category = (ExpandableRelativeLayout) findViewById(R.id.exp_category);
        expandableView(exp_category, COLLAPSE);*/

        findViewById(R.id.img_c_search).setOnClickListener(this);
        ll_search_hint = (LinearLayout) findViewById(R.id.ll_search_hint);

        img_c_clear = (carbon.widget.ImageView) findViewById(R.id.img_c_clear);
        img_c_clear.setOnClickListener(this);
        img_c_clear.setVisibility(View.GONE);

        et_search = (EditText) findViewById(R.id.et_search);
        et_search.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_roman)));
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_search.length() == 0) {
                    ll_search_hint.setVisibility(View.VISIBLE);
                    img_c_clear.setVisibility(View.GONE);
                } else {
                    ll_search_hint.setVisibility(View.GONE);
                    img_c_clear.setVisibility(View.VISIBLE);
                }
            }
        });
        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(android.widget.TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });

        txt_c_user_name = (TextView) findViewById(R.id.txt_c_user_name);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);


        panel_notifications_arrow_img = (ImageView) findViewById(R.id.panel_notifications_arrow_img);
        panel_notifications_arrow_img.setSelected(true);
        panel_deals_arrow_img = (ImageView) findViewById(R.id.panel_deals_arrow_img);
        panel_deals_arrow_img.setSelected(true);
        panel_events_arrow_img = (ImageView) findViewById(R.id.panel_events_arrow_img);
        panel_events_arrow_img.setSelected(true);
        panel_cate_arrow_img = (ImageView) findViewById(R.id.panel_cate_arrow_img);
        panel_cate_arrow_img.setSelected(true);

        img_loading = (ImageView) findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);

        findViewById(R.id.rl_notification_layout).setOnClickListener(this);

        sl_more = (SlidingLayer) findViewById(R.id.sl_more);
        sl_more.setStickTo(SlidingLayer.STICK_TO_RIGHT);
        sl_more.setLayerTransformer(new AlphaTransformer());
        sl_more.setShadowSize(0);
        sl_more.setShadowDrawable(null);
        sl_more.setSlidingEnabled(false);
        sl_more.setOnInteractListener(new SlidingLayer.OnInteractListener() {

            @Override
            public void onOpen() {
            }

            @Override
            public void onShowPreview() {
            }

            @Override
            public void onClose() {
            }

            @Override
            public void onOpened() {
            }

            @Override
            public void onPreviewShowed() {
            }

            @Override
            public void onClosed() {
            }
        });

        txt_c_no_list = (TextView) findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        progressbar.setVisibility(View.GONE);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        rcAdapter = new NotificationRecyclerViewAdapter(this, notificationArray, this);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, R.drawable.bg_divider));
        recyclerView.setAdapter(rcAdapter);
        recyclerView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });


        progress_category = (ProgressBar) findViewById(R.id.progress_category);
        progress_category.setVisibility(View.GONE);
        txt_c_no_cate_retry = (TextView) findViewById(R.id.txt_c_no_cate_retry);
        txt_c_no_cate_retry.setOnClickListener(this);
        list_category = (ListView) findViewById(R.id.list_category);
        categoryListAdapter = new ChooseCategoryListAdapter(MainActivity.this, arrayProductCategory, this);
        list_category.setAdapter(categoryListAdapter);
        list_category.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        rl_c_category_list = (RelativeLayout) findViewById(R.id.rl_c_category_list);
        rl_c_category_list.setVisibility(View.GONE);
    }

    public boolean newNotificationStatus = false;
    public int notificationValue = 0;

    public void updateNotificationCount(String value) {
        try {
            AppDelegate.LogT("updateNotificationCount => " + value);
            if (AppDelegate.isValidString(value)) {
                newNotificationStatus = Integer.parseInt(value) > 0;
                notificationValue = Integer.parseInt(value);
                getSupportFragmentManager().findFragmentById(R.id.main_content);
            } else {
                newNotificationStatus = false;
                notificationValue = 0;
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            newNotificationStatus = false;
            notificationValue = 0;
        }
    }

    private void performSearch() {
        AppDelegate.hideKeyBoard(MainActivity.this);
        expandableView(expandable_search, COLLAPSE);
        if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof SearchProductFragment) {
            if (SearchProductFragment.mHandler != null) {
                SearchProductFragment.mHandler.sendEmptyMessage(3);
            } else {
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new SearchProductFragment());
            }
        } else {
            AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new SearchProductFragment());
        }
    }

    public void toggleSlidingLayer() {
        AppDelegate.LogT("toggleSlidingLayer => ");
        AppDelegate.hideKeyBoard(this);
        if (sl_more.isOpened()) {
            sl_more.closeLayer(true);
            mHandler.sendEmptyMessage(5);
        } else {
            sl_more.openLayer(true);
            expandableView(expandable_search, COLLAPSE);
            mHandler.sendEmptyMessage(5);
            callGetNotificationAsync();
        }
    }

    public void callGetNotificationAsync() {
        if (AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            PostAsync mPostAsyncObj = new PostAsync(MainActivity.this,
                    this, ServerRequestConstants.GET_NOTIFICATION,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(12);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(MainActivity.this.getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    public String VIEW_NOTIFICATION = "VIEW_NOTIFICATION";

    private void callNotificationViewAsyncAsync(String notifyId) {
        if (AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.notifyId, notifyId);
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.is_view, "1");

            PostAsync mPostAsyncObj = new PostAsync(MainActivity.this,
                    this, VIEW_NOTIFICATION, ServerRequestConstants.GET_NOTIFICATION,
                    mPostArrayList, null);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(MainActivity.this.getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    public void updateUserDetail() {
        if (prefs.getUserdata() != null) {
            prefs.getInstitutionModel();
            txt_c_user_name.setText(prefs.getUserdata().first_name + " " + prefs.getUserdata().last_name);
            txt_c_address.setText(prefs.getInstitutionModel().institution_name + " - " + prefs.getInstitutionModel().department_name);
            if (AppDelegate.isValidString(prefs.getUserdata().image)) {
                img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                frameAnimation.setCallback(img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                imageLoader.loadImage(prefs.getUserdata().image, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        cimg_user.setImageBitmap(loadedImage);
                        img_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });
//                Picasso.with(this).load(prefs.getUserdata().image).into(cimg_user, new Callback() {
//                    @Override
//                    public void onSuccess() {
//                        img_loading.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onError() {
//
//                    }
//                });
            } else if (prefs.getUserdata().str_Gender.equalsIgnoreCase(Tags.MALE)) {
                cimg_user.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.user_male_1));
            } else {
                cimg_user.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.user_female_1));
            }
        }
    }

    @SuppressLint("HandlerLeak")
    private void setHandler() {
        menuClickListener = new SlideMenuClickListener();
        mHandler = new Handler() {

            @Override
            public void dispatchMessage(Message msg) {
                if (msg.what == 0) {
                    toggleSlider();
                } else if (msg.what == 1) {
                    AppDelegate.showProgressDialog(MainActivity.this);
                } else if (msg.what == 2) {
//                    setResultFromGeoCoderApi(msg.getData());
                } else if (msg.what == 3) {
                    AppDelegate.hideProgressDialog(MainActivity.this);
                } else if (msg.what == 5) {
                    progressbar.setVisibility(View.GONE);
                    txt_c_no_list.setVisibility(notificationArray.size() == 0 ? View.VISIBLE : View.GONE);
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                } else if (msg.what == 6) {
                    updateUserDetail();
                } else if (msg.what == 7) {
                    AppDelegate.showFragmentAnimationOppose(getSupportFragmentManager(), new HomeFragment());
                } else if (msg.what == 8) {
                } else if (msg.what == 9) {
                }
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(MainActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(MainActivity.this);
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                    txt_c_no_list.setVisibility(View.GONE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 14) {
                    progress_category.setVisibility(View.VISIBLE);
                } else if (msg.what == 15) {
                    progress_category.setVisibility(View.GONE);
                } else if (msg.what == 16) {
                    txt_c_no_cate_retry.setText("Try again.");
                    txt_c_no_cate_retry.setVisibility(arrayProductCategory.size() == 0 ? View.VISIBLE : View.GONE);
                    categoryListAdapter.notifyDataSetChanged();
                    list_category.invalidate();
                    setListViewHeightBasedOnChildren(list_category);
                } else if (msg.what == 17) {
                    rl_c_category_list.setVisibility(View.GONE);
                } else if (msg.what == 18) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            rl_c_category_list.setVisibility(View.VISIBLE);
                        }
                    }, 300);
                }
            }
        };
    }

    private int heightCategory = 0;

    public void setListViewHeightBasedOnChildren(final ListView listView) {
        final ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }

        heightCategory = totalHeight;

        AppDelegate.LogT("setListViewHeightBasedOnChildren => " + heightCategory);

        rl_c_category_list.post(new Runnable() {
            @Override
            public void run() {
                rl_c_category_list.getLayoutParams().height = heightCategory + AppDelegate.dpToPix(MainActivity.this, 3) + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
                rl_c_category_list.invalidate();
            }
        });

        final int finalTotalHeight = totalHeight;
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.getLayoutParams().height = finalTotalHeight + AppDelegate.dpToPix(MainActivity.this, 3) + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
                listView.invalidate();
            }
        });
    }


    public void setListViewHeight(Context mContext, final ListView listView, ListAdapter gridAdapter) {
        try {
            if (gridAdapter == null) {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = 80;
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;
            int itemCount = gridAdapter.getCount();

            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            AppDelegate.LogT("itemCount = " + itemCount);
            for (int i = 0; i < itemCount; i++) {
                View listItem = gridAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
                AppDelegate.LogT("totalHeight = " + totalHeight);
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            listView.setLayoutParams(params);
            listView.post(new Runnable() {
                @Override
                public void run() {
                    listView.requestLayout();
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onBackPressed() {
        if (mSlidingPaneLayout.isOpen()) {
            mSlidingPaneLayout.closePane();
        } else if (expandable_search.isExpanded()) {
            expandableView(expandable_search, COLLAPSE);
        } else if (sl_more != null && sl_more.isOpened()) {
            sl_more.closeLayer(true);
        } else if (JobsPageJOBSFragment.sl_more != null && JobsPageJOBSFragment.sl_more.isOpened()) {
            JobsPageJOBSFragment.sl_more.closeLayer(true);
        } else if (JobsPageInternshipFragment.sl_more != null && JobsPageInternshipFragment.sl_more.isOpened()) {
            JobsPageInternshipFragment.sl_more.closeLayer(true);
        } else if (MyEventListingFragment.sl_more != null && MyEventListingFragment.sl_more.isOpened()) {
            MyEventListingFragment.sl_more.closeLayer(true);
        } else if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof NoInternetConnectionFragment) {
            if (AppDelegate.haveNetworkConnection(MainActivity.this))
                getFragmentManager().popBackStack();
        } else if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof ProductDetailFragment) {
            super.onBackPressed();
            getFragmentManager().popBackStack();
        } else if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof HomeFragment || getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof ProfileLandingFragment) {
            finish();
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                AppDelegate.showFragmentAnimationOppose(getSupportFragmentManager(), new HomeFragment());
            } else {
                try {
                    super.onBackPressed();
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        }
    }

    public void toggleSearchLayout() {
        if (expandable_search.isExpanded()) {
            expandableView(expandable_search, COLLAPSE);
            AppDelegate.hideKeyBoard(MainActivity.this);
        } else {
            expandableView(expandable_search, EXPAND);
            sl_more.closeLayer(true);
        }
    }

    @Override
    public void onAttachFragment(android.app.Fragment fragment) {
        super.onAttachFragment(fragment);
        AppDelegate.LogT("onAttachFragment called");
        expandableView(expandable_search, COLLAPSE);
        et_search.setText("");
        AppDelegate.hideKeyBoard(MainActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_notification_layout:
                toggleSlidingLayer();
                break;
            case R.id.img_c_search:
                sl_more.closeLayer(true);
                expandableView(expandable_search, COLLAPSE);
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new RefineSearchFragment());
                break;
            case R.id.img_c_clear:
                et_search.setText("");
                break;

            case R.id.user_name_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof MyProfileFragment))
                    if (!AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
                        AppDelegate.addFragment(getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                    } else {
                        menuClickListener.onItemClick(null, v, PANEL_PROFILE, PANEL_PROFILE);
                    }
                break;

            case R.id.panel_dashboard_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof HomeFragment))
                    if (!AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
                        AppDelegate.addFragment(getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                    } else
                        menuClickListener.onItemClick(null, v, PANEL_DASHBOARD, PANEL_DASHBOARD);
                break;

            case R.id.panel_following_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof FollowingListFragment))
                    if (!AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
                        AppDelegate.addFragment(getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                    } else {
                        menuClickListener.onItemClick(null, v, PANEL_FOLLOWING, PANEL_FOLLOWING);

                    }
                break;

            case R.id.panel_my_listing_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof MyProductListFragment))
                    if (!AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
                        AppDelegate.addFragment(getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                    } else {
                        menuClickListener.onItemClick(null, v, PANEL_MY_LIST, PANEL_MY_LIST);

                    }
                break;

            case R.id.panel_list_item_layout:
                mHandler.sendEmptyMessage(0);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(MainActivity.this, SellItemActivity.class));
                    }
                }, 600);
                break;
            case R.id.panel_chat_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof MyChatListingFragment)) {
                    if (AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
                        menuClickListener.onItemClick(null, v, PANEL_MY_CHAT, PANEL_MY_CHAT);
                    } else {
                        AppDelegate.addFragment(getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                    }
                }
                break;
            case R.id.panel_cat_product_layout:
                boolean viewShouldExpand = rl_c_category_list.getVisibility() == View.VISIBLE;
                rl_c_category_list.setVisibility(viewShouldExpand ? View.GONE : View.VISIBLE);

                panel_cate_arrow_img.setSelected(viewShouldExpand);
                AppDelegate.LogT("condition => " + isCategoryAsyncCalled + ", " + (arrayProductCategory.size() == 0) + ", " + viewShouldExpand);
                if (!isCategoryAsyncCalled && arrayProductCategory.size() == 0 && !viewShouldExpand)
                    callGetProductTypeAsync();
                else {
                    mHandler.sendEmptyMessage(16);
                }

//                mHandler.sendEmptyMessage(0);
//                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof SellItemActivity)) {
//                    if (AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
//                        menuClickListener.onItemClick(null, v, PANEL_CAT_PRODUCT, PANEL_CAT_PRODUCT);
//                    } else {
//                        AppDelegate.addFragment(getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
//                    }
//                }
                break;

            case R.id.txt_c_no_cate_retry:
                callGetProductTypeAsync();
                break;

            case R.id.panel_my_event_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof MyEventListingFragment)) {
                    if (!AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
                        AppDelegate.addFragment(getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                    } else
                        menuClickListener.onItemClick(null, v, PANEL_MY_EVENTS, PANEL_MY_EVENTS);
                }
                break;

            case R.id.panel_my_deals_layout:
                mHandler.sendEmptyMessage(0);
                AppDelegate.LogT("panel_my_deals_layout called");
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof MyDealListingFragment)) {
                    if (!AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
                        AppDelegate.addFragment(getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                    } else
                        menuClickListener.onItemClick(null, v, PANEL_MY_DEALS, PANEL_MY_DEALS);
                }
                break;

            case R.id.panel_deals_movies_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof DealsMoviesFragment))
                    if (!AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
                        AppDelegate.addFragment(getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                    } else
                        menuClickListener.onItemClick(null, v, PANEL_DEALS_MOVIES, PANEL_DEALS_MOVIES);
                break;

            case R.id.panel_events_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof EventsCampusBasedFragment))
                    if (!AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
                        AppDelegate.addFragment(getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                    } else
                        menuClickListener.onItemClick(null, v, PANEL_EVENTS, PANEL_EVENTS);
                break;

            case R.id.panel_notifications_layout:
//                mHandler.sendEmptyMessage(0);
                expandableView(exp_layout, exp_layout.isExpanded() ? COLLAPSE : EXPAND);
                panel_notifications_arrow_img.setSelected(exp_layout.isExpanded());
                break;

            case R.id.view_deals_movies_arrow:
                expandableView(exp_deals_movies_layout, exp_deals_movies_layout.isExpanded() ? COLLAPSE : EXPAND);
                panel_deals_arrow_img.setSelected(exp_deals_movies_layout.isExpanded());
                break;

            case R.id.view_events_arrow:
                expandableView(exp_events_layout, exp_events_layout.isExpanded() ? COLLAPSE : EXPAND);
                panel_events_arrow_img.setSelected(exp_events_layout.isExpanded());
                break;

            case R.id.panel_intership_jobs_layout:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_INTERNSHIP_JOBS, PANEL_INTERNSHIP_JOBS);
                break;

            case R.id.panel_lost_found_layout:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_LOST_FOUND, PANEL_LOST_FOUND);
                break;

            case R.id.panel_help_layout:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_HELP, PANEL_HELP);
                break;

            case R.id.panel_logout_layout:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_LOGOUT, PANEL_LOGOUT);
                break;
        }
    }

    private void callGetProductTypeAsync() {
        if (!AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
            AppDelegate.addFragment(MainActivity.this.getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        } else {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            PostAsync mPostAsyncObj = new PostAsync(MainActivity.this,
                    this, ServerRequestConstants.GET_PRODUCT_CATEGORY,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(14);
            isCategoryAsyncCalled = true;
            mPostAsyncObj.execute();
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.notification)) {
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    toggleSlidingLayer();
                }
            }, 700);
            if (notificationValue == 1) {
                notificationValue = 0;
                newNotificationStatus = false;
            } else {
                notificationValue -= 1;
            }
            if (notificationArray.get(position).notification_type.equalsIgnoreCase(Tags.deal)) {
                DealModel dealModel = new DealModel();
                dealModel.id = notificationArray.get(position).deal_id;
                dealModel.title = notificationArray.get(position).deal_title;
                dealModel.image_1_thumb = notificationArray.get(position).deal_image;

                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.deal, dealModel);
                Intent intent = new Intent(MainActivity.this, DealDetailsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            } else if (notificationArray.get(position).notification_type.equalsIgnoreCase(Tags.follow)) {
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.userId = notificationArray.get(position).user_id;
                userDataModel.first_name = notificationArray.get(position).user_first_name;
                userDataModel.last_name = notificationArray.get(position).user_last_name;
                userDataModel.image = notificationArray.get(position).user_image;

                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.user, userDataModel);
                Fragment fragment = new SellersProfileFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment);
            } else if (notificationArray.get(position).notification_type.equalsIgnoreCase(Tags.product)) {
                ProductModel productModel = new ProductModel();
                productModel.id = notificationArray.get(position).product_id;
                productModel.title = notificationArray.get(position).product_name;
                productModel.image_1_thumb = notificationArray.get(position).product_image;

                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.product, productModel);
                Fragment fragment = new ProductDetailFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment);
            } else if (notificationArray.get(position).notification_type.equalsIgnoreCase(Tags.Makeoffer)) {
//                UserDataModel userDataModel = new UserDataModel();
//                userDataModel.userId = notificationArray.get(position).user_id;
//                userDataModel.first_name = notificationArray.get(position).user_first_name;
//                userDataModel.last_name = notificationArray.get(position).user_last_name;
//                userDataModel.image = notificationArray.get(position).user_image;

                com.stux.Models.Message message = new com.stux.Models.Message();
                message.product_id = notificationArray.get(position).product_id;
                message.receiver_id = dataModel.userId;
                message.sender_id = notificationArray.get(position).user_id;
                message.status = "1";
                message.created = notificationArray.get(position).created;
                message.mType = com.stux.Models.Message.TYPE_MESSAGE;
                message.user_type = com.stux.Models.Message.USER_RECEIVER;

                message.product_name = notificationArray.get(position).product_name;
                message.product_image = notificationArray.get(position).product_image;
                message.productModel = new ProductModel();
                message.productModel.id = message.product_id;
                message.productModel.title = message.product_name;
                message.productModel.image_1 = message.product_image;

                message.dataModel = new UserDataModel();
                message.dataModel.userId = notificationArray.get(position).user_id;
                message.dataModel.first_name = notificationArray.get(position).user_first_name;
                message.dataModel.last_name = notificationArray.get(position).user_last_name;
                message.dataModel.image = notificationArray.get(position).user_image;

                message.mMessage = notificationArray.get(position).str_message;

                Bundle bundle = new Bundle();
                bundle.putString(Tags.FROM, Tags.CHAT);
                bundle.putInt(Tags.PAGE, ChatFragment.FROM_CHAT);
                bundle.putParcelable(Tags.message, message);
                Fragment fragment = new ChatFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment);

            } else if (notificationArray.get(position).notification_type.equalsIgnoreCase(Tags.CHAT)) {
//                UserDataModel userDataModel = new UserDataModel();
//                userDataModel.userId = notificationArray.get(position).user_id;
//                userDataModel.first_name = notificationArray.get(position).user_first_name;
//                userDataModel.last_name = notificationArray.get(position).user_last_name;
//                userDataModel.image = notificationArray.get(position).user_image;

                com.stux.Models.Message message = new com.stux.Models.Message();
                message.product_id = notificationArray.get(position).product_id;
                message.receiver_id = dataModel.userId;
                message.sender_id = notificationArray.get(position).user_id;
                message.status = "1";
                message.created = notificationArray.get(position).created;
                message.mType = com.stux.Models.Message.TYPE_MESSAGE;
                message.user_type = com.stux.Models.Message.USER_RECEIVER;

                message.product_name = notificationArray.get(position).product_name;
                message.product_image = notificationArray.get(position).product_image;
                message.productModel = new ProductModel();
                message.productModel.id = message.product_id;
                message.productModel.title = message.product_name;
                message.productModel.image_1 = message.product_image;

                message.dataModel = new UserDataModel();
                message.dataModel.userId = notificationArray.get(position).user_id;
                message.dataModel.first_name = notificationArray.get(position).user_first_name;
                message.dataModel.last_name = notificationArray.get(position).user_last_name;
                message.dataModel.image = notificationArray.get(position).user_image;

                message.mMessage = notificationArray.get(position).str_message;

                Bundle bundle = new Bundle();
                bundle.putString(Tags.FROM, Tags.CHAT);
                bundle.putInt(Tags.PAGE, ChatFragment.FROM_CHAT);
                bundle.putParcelable(Tags.message, message);
                Fragment fragment = new ChatFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment);
            }

            callNotificationViewAsyncAsync(notificationArray.get(position).id);
            notificationArray.remove(position);
        } else if (name.equalsIgnoreCase(Tags.category)) {
            execute_searchProduct(arrayProductCategory.get(position).id + "");
        }
    }

    private void parseRefineProduct(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                if (jsonObject.has(Tags.response)) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    if (jsonArray.length() > 0) {
                        Bundle bundle = new Bundle();
                        bundle.putString(Tags.product, result);
                        bundle.putParcelable(Tags.refine, refineSearchModel);
                        Fragment fragment = new RefineProductFragment();
                        fragment.setArguments(bundle);
                        AppDelegate.showFragmentAnimation(MainActivity.this.getSupportFragmentManager(), fragment);
                    } else {
                        AppDelegate.showToast(MainActivity.this, "No record found.");
                    }
                } else {
                    AppDelegate.showToast(MainActivity.this, "No record found.");
                }
            } else {
                AppDelegate.showAlert(MainActivity.this, jsonObject.getString(Tags.MESSAGE));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(MainActivity.this, "Server Error");
        }
    }


    private void execute_searchProduct(String category_id) {
        if (AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId + "");
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.page, "1");
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.product_name, "");
            try {
                AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.campus_id, "");
            } catch (Exception e) {
                AppDelegate.LogE(e);
                AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.campus_id, "");
            }
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.item_condition, "");
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.cat_id, category_id + "");
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.price, "asc");
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.upload, "asc");
            AppDelegate.getInstance(MainActivity.this).setPostParamsSecond(mPostArrayList, Tags.range, "5000");

            refineSearchModel = new RefineSearchModel();
            refineSearchModel.logged_in_User_id = dataModel.userId;
            refineSearchModel.record = "10";
            refineSearchModel.page = 1;
            refineSearchModel.product_name = "";
            refineSearchModel.campus_id = "";
            refineSearchModel.item_condition = "";
            refineSearchModel.cat_id = "" + category_id;
            refineSearchModel.price = "asc";
            refineSearchModel.upload = "asc";
            refineSearchModel.rangeProgress = "5000";

            PostAsync mPostasyncObj = new PostAsync(MainActivity.this,
                    MainActivity.this, ServerRequestConstants.REFINE_PRODUCTS,
                    mPostArrayList, null);
            toggleSlider();
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(MainActivity.this.getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    public void toggleSlider() {
        // close notification layout if open.
//        sl_more.closeLayer(true);
//        mHandler.sendEmptyMessage(5);
        if (sl_more != null && mHandler != null & sl_more.isOpened()) {
            sl_more.closeLayer(true);
            mHandler.sendEmptyMessage(5);
        }
        if (expandable_search != null && expandable_search.isExpanded()) {
            expandableView(expandable_search, COLLAPSE);
            AppDelegate.hideKeyBoard(MainActivity.this);
        }
        if (mSlidingPaneLayout != null)
            if (!mSlidingPaneLayout.isOpen()) {
                mSlidingPaneLayout.openPane();
            } else {
                mSlidingPaneLayout.closePane();
            }
    }

    public void whileSlide(int view) {
        int newalfa = 255 - view;
        float subvalue = newalfa / 2.55f;
        float f = (100f - subvalue) * 0.01f;
        Animation alphaAnimation = new AlphaAnimation(init, f);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        init = f;
        side_panel.startAnimation(alphaAnimation);
    }

    private Bundle bundle;

    /**
     * Displaying fragment view for selected navigation drawer list item
     */
    private void displayView(int position) {
        AppDelegate.hideKeyBoard(MainActivity.this);
        if (position != 6)
            setInitailSideBar(MainActivity.this, position);
        Fragment fragment = null;
        switch (position) {
            case PANEL_PROFILE:
                fragment = new MyProfileFragment();
                break;

            case PANEL_DASHBOARD:
                HomeFragment.clearArrayData(4);
                fragment = new HomeFragment();
                break;

            case PANEL_FOLLOWING:
                fragment = new FollowingListFragment();
                break;

            case PANEL_MY_LIST:
                bundle = new Bundle();
                bundle.putParcelable(Tags.user, dataModel);
                fragment = new MyProductListFragment();
                fragment.setArguments(bundle);
                break;

            case PANEL_MY_CHAT:
                fragment = new MyChatListingFragment();
                break;

            case PANEL_CAT_PRODUCT:
//                fragment = new ChooseCategoryFragment();
                break;

//            case PANEL_LIST_ITEM:
//                fragment = new SellItemActivity();
//                break;

            case PANEL_DEALS_MOVIES:
                DealsMoviesFragment.clearArrayData(4);
                fragment = new DealsMoviesFragment();
                break;

            case PANEL_MY_EVENTS:
                fragment = new MyEventListingFragment();
                break;

            case PANEL_MY_DEALS:
                fragment = new MyDealListingFragment();
                break;

            case PANEL_EVENTS:
                fragment = new EventsCampusBasedFragment();
                break;

            case PANEL_NOTIFICATION:
                expandableView(exp_layout, exp_layout.isExpanded() ? COLLAPSE : EXPAND);
                break;

            case PANEL_INTERNSHIP_JOBS:
                fragment = new JobsInternshipFragment();
                break;

            case PANEL_LOST_FOUND:
                startActivity(new Intent(MainActivity.this, LostFoundItemActivity.class));
//                fragment = new ChatFragment();
                break;

            case PANEL_HELP:
                startActivity(new Intent(MainActivity.this, HelpActivity.class));
                break;

            case PANEL_LOGOUT:
//                ChatService.callLogoutAsync(this, dataModel.userId, prefs.getUserdata().user_online_time_id);
                callLogoutAsync();
                prefs.clearTempPrefs();
                prefs.clearSharedPreference();
                if (HomeFragment.productArray != null)
                    HomeFragment.productArray.clear();
                if (RefineProductFragment.productArray != null)
                    RefineProductFragment.productArray.clear();
                if (MyProfileFragment.productArray != null)
                    MyProfileFragment.productArray.clear();
                if (MyProductListFragment.productArray != null)
                    MyProductListFragment.productArray.clear();
                Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                bundle = new Bundle();
                bundle.putString(Tags.IS_SPLASH, Tags.FALSE);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
                break;

            default:
                break;
        }

        if (fragment != null) {
            AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment);
        } else if (position != 6) {
            AppDelegate.LogE("Error in creating fragment");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("onActivityResult MainActivity");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppDelegate.SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(MainActivity.this, data.getData());
                } else {
                    Toast.makeText(MainActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            } else if (requestCode == AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE) {
                if (ProfileLandingFragment.onPictureResult != null && ProfileLandingFragment.imageURI != null) {
                    startCropActivity(MainActivity.this, ProfileLandingFragment.imageURI);
                } else if (MyProfileFragment.onPictureResult != null && MyProfileFragment.imageURI != null) {
                    startCropActivity(MainActivity.this, MyProfileFragment.imageURI);
                } else {
                    Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }

        if (ProductDetailFragment.callbackManager != null)
            ProductDetailFragment.callbackManager.onActivityResult(requestCode, resultCode, data);
        if (JobsPageJOBSFragment.callbackManager != null)
            JobsPageJOBSFragment.callbackManager.onActivityResult(requestCode, resultCode, data);
        if (JobsPageInternshipFragment.callbackManager != null)
            JobsPageInternshipFragment.callbackManager.onActivityResult(requestCode, resultCode, data);

        if (ProductDetailFragment.loginButton != null)
            ProductDetailFragment.loginButton.onActivityResult(requestCode, resultCode, data);
        if (JobsPageJOBSFragment.loginButton != null)
            JobsPageJOBSFragment.loginButton.onActivityResult(requestCode, resultCode, data);
        if (JobsPageInternshipFragment.loginButton != null)
            JobsPageInternshipFragment.loginButton.onActivityResult(requestCode, resultCode, data);
    }

    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            if (ProfileLandingFragment.onPictureResult != null) {
                startCropActivity(MainActivity.this, picUri);
            } else if (MyProfileFragment.onPictureResult != null) {
                startCropActivity(MainActivity.this, picUri);
            } else {
                Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            if (ProfileLandingFragment.onPictureResult != null) {
                ProfileLandingFragment.onPictureResult.setOnReceivePictureResult(Tags.PICTURE, resultUri);
            } else if (MyProfileFragment.onPictureResult != null) {
                MyProfileFragment.onPictureResult.setOnReceivePictureResult(Tags.PICTURE, resultUri);
            }
        } else {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    public static void startCropActivity(FragmentActivity mActivity, Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".png";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(mActivity.getCacheDir(), destinationFileName)));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);
        uCrop.start(mActivity);
    }

    public static boolean imageUriIsValid(FragmentActivity mActivity, Uri contentUri) {
        ContentResolver cr = mActivity.getContentResolver();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cur = cr.query(contentUri, projection, null, null, null);
        if (cur != null) {
            if (cur.moveToFirst()) {
                String filePath = cur.getString(0);
                if (new File(filePath).exists()) {
                    // do something if it exists
                    cur.close();
                    return true;
                } else {
                    // File was not found
                    cur.close();
                    AppDelegate.showToast(mActivity, "File not found please try again later.");
                    return false;
                }
            } else {
                // Uri was ok but no entry found.
                AppDelegate.showToast(mActivity, "No image found please try again later.");
                return false;
            }
        } else {
            // content Uri was invalid or some other error occurred
            AppDelegate.showToast(mActivity, "Image path was invalid some exception occured at your device.");
            return false;
        }
    }

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop basisConfig(@NonNull UCrop uCrop) {
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }

    /**
     * Sometimes you want to adjust more options, it's done via {@link com.yalantis.ucrop.UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setCompressionQuality(100);

        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);

        return uCrop.withOptions(options);
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (apiName.equalsIgnoreCase(ServerRequestConstants.LOGOUT)) {
            parseLogoutResult(result);
        } else if (apiName.equalsIgnoreCase(VIEW_NOTIFICATION)) {
            parseViewNotificationResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_NOTIFICATION)) {
            mHandler.sendEmptyMessage(13);
            parseGetNotificationResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_PRODUCT_CATEGORY)) {
            isCategoryAsyncCalled = false;
            mHandler.sendEmptyMessage(15);
            parseProductCategoryResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.REFINE_PRODUCTS)) {
            mHandler.sendEmptyMessage(11);
            parseRefineProduct(result);
        } else if (apiName.equalsIgnoreCase(Tags.editProfile)) {
            AppDelegate.LogT("editProfile called");
            if (mHandler != null) {
                AppDelegate.LogT("editProfile2 called");
                mHandler.sendEmptyMessage(6);
                mHandler.sendEmptyMessage(7);
            }
        }
    }

    private void parseProductCategoryResult(String result) {
        try {
            JSONObject object = new JSONObject(result);
            if (object.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = object.getJSONArray(Tags.response);
                arrayProductCategory.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ProductCategoryModel productCategoryModel = new ProductCategoryModel();
                    productCategoryModel.id = jsonObject.getString(Tags.id);
                    productCategoryModel.cat_name = jsonObject.getString(Tags.cat_name);
                    productCategoryModel.status = jsonObject.getString(Tags.id);
                    arrayProductCategory.add(productCategoryModel);
                }
                mHandler.sendEmptyMessage(17);
                mHandler.sendEmptyMessage(16);
                mHandler.sendEmptyMessage(18);
            } else {
                AppDelegate.showAlert(MainActivity.this, object.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(MainActivity.this, "Server Error");
        }
    }

    private void parseViewNotificationResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {

            } else {

            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseGetNotificationResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            notificationArray.clear();
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);

                    NotificationModel notificationModel = new NotificationModel();
                    notificationModel.id = object.getString(Tags.ID);
                    notificationModel.notification_type = object.getString(Tags.type);
                    notificationModel.created = object.getString(Tags.created);
                    notificationModel.is_view = object.getString(Tags.is_view);


                    if (notificationModel.notification_type.equalsIgnoreCase(Tags.deal) && AppDelegate.isValidString(object.optString(Tags.deal))) {

                        notificationModel.deal_id = object.getJSONObject(Tags.deal).getString(Tags.id);
                        notificationModel.deal_title = object.getJSONObject(Tags.deal).getString(Tags.title);
                        notificationModel.deal_image = object.getJSONObject(Tags.deal).getString(Tags.image_1_thumb);
                        notificationArray.add(notificationModel);

                    } else if (notificationModel.notification_type.equalsIgnoreCase(PushNotificationService.TYPE_PRODUCT) && AppDelegate.isValidJSONObject(object.optString(Tags.product))) {

                        notificationModel.product_id = object.getJSONObject(Tags.product).getString(Tags.id);
                        notificationModel.product_name = object.getJSONObject(Tags.product).getString(Tags.title);
                        notificationModel.product_image = object.getJSONObject(Tags.product).getString(Tags.image_1_thumb);
                        notificationArray.add(notificationModel);

                    } else if (notificationModel.notification_type.equalsIgnoreCase(Tags.follow) && AppDelegate.isValidJSONObject(object.optString(Tags.followUser))) {

                        notificationModel.user_id = object.getJSONObject(Tags.followUser).getString(Tags.id);
                        notificationModel.user_first_name = object.getJSONObject(Tags.followUser).getString(Tags.first_name);
                        notificationModel.user_last_name = object.getJSONObject(Tags.followUser).getString(Tags.last_name);
                        notificationModel.user_image = object.getJSONObject(Tags.followUser).getString(Tags.image);
                        notificationArray.add(notificationModel);

                    } else if (notificationModel.notification_type.equalsIgnoreCase(Tags.makeOffer) && AppDelegate.isValidJSONObject(object.optString(Tags.user))) {

                        notificationModel.str_message = object.getString(Tags.message);
                        notificationModel.created = object.getString(Tags.created);

                        notificationModel.user_id = object.getJSONObject(Tags.user).getString(Tags.id);
                        notificationModel.user_first_name = object.getJSONObject(Tags.user).getString(Tags.first_name);
                        notificationModel.user_last_name = object.getJSONObject(Tags.user).getString(Tags.last_name);
                        notificationModel.user_image = object.getJSONObject(Tags.user).getString(Tags.image);
                        notificationModel.offer_price = object.getJSONObject(Tags.user).getString(Tags.offer_price);

                        notificationModel.product_id = object.getJSONObject(Tags.product).getString(Tags.id);
                        notificationModel.product_name = object.getJSONObject(Tags.product).getString(Tags.title);
                        notificationModel.product_image = object.getJSONObject(Tags.product).getString(Tags.image_1_thumb);
                        notificationArray.add(notificationModel);

                    } else if (notificationModel.notification_type.equalsIgnoreCase(Tags.CHAT) && AppDelegate.isValidJSONObject(object.optString(Tags.sender_data)) && AppDelegate.isValidJSONObject(object.optString(Tags.product))) {

                        notificationModel.str_message = object.getString(Tags.message);
                        notificationModel.created = object.getString(Tags.created);

                        notificationModel.user_id = object.getJSONObject(Tags.sender_data).getString(Tags.id);
                        notificationModel.user_first_name = object.getJSONObject(Tags.sender_data).getString(Tags.first_name);
                        notificationModel.user_last_name = object.getJSONObject(Tags.sender_data).getString(Tags.last_name);
                        notificationModel.user_image = object.getJSONObject(Tags.sender_data).getString(Tags.image);

                        if (object.has(Tags.product) && AppDelegate.isValidString(object.getString(Tags.product))) {
                            notificationModel.product_id = object.getJSONObject(Tags.product).getString(Tags.id);
                            notificationModel.product_name = object.getJSONObject(Tags.product).getString(Tags.title);
                            notificationModel.product_image = object.getJSONObject(Tags.product).getString(Tags.image_1_thumb);
                        } else {
                            AppDelegate.LogE("Product data not available.");
                            break;
                        }
                        notificationArray.add(notificationModel);

                    }
                }
            } else {
                txt_c_no_list.setText(jsonObject.getString(Tags.message));
            }
            if (notificationArray != null)
                updateNotificationCount(notificationArray.size() + "");
            mHandler.sendEmptyMessage(5);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseLogoutResult(String result) {
        try {
//            JSONObject jsonObject = new JSONObject(result);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }


    /**
     * Slide menu item click listener
     **/
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view,
                                final int position, long id) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    displayView(position);
                }
            }, 600);
        }
    }

    class SliderListener extends SlidingPaneLayout1.SimplePanelSlideListener {

        @Override
        public void onPanelOpened(View panel) {
            if (!isSlideOpen) {
                isSlideOpen = true;
            }
            AppDelegate.hideKeyBoard(MainActivity.this);
        }

        @Override
        public void onPanelClosed(View panel) {
            if (isSlideOpen) {
                isSlideOpen = false;
            }
        }

        @Override
        public void onPanelSlide(View view, float slideOffset) {
            ratio = (int) -(0 - (slideOffset) * 255);
            whileSlide(ratio);
        }
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        if (ChatFragment.editEmojicon != null)
            EmojiconsFragment.input(ChatFragment.editEmojicon, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        if (ChatFragment.editEmojicon != null)
            EmojiconsFragment.backspace(ChatFragment.editEmojicon);
    }

    public static boolean checkUserStatus(Activity mActivity, JSONObject jsonObject) {
        try {
            if (AppDelegate.isValidString(jsonObject.getString(Tags.user_status))) {
                if (jsonObject.getString(Tags.user_status).equalsIgnoreCase("0")) {
                    AppDelegate.LogCh("User not exists in server db.");
                    AppDelegate.showToast(mActivity, "Your account not available anymore please contact StuxMobile team for any query.", 1);
                    new Prefs(mActivity).clearTempPrefs();
                    new Prefs(mActivity).clearSharedPreference();
                    Intent intent = new Intent(mActivity, SplashActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Tags.IS_SPLASH, Tags.FALSE);
                    intent.putExtras(bundle);
                    mActivity.startActivity(intent);
                    if (MainActivity.mActivity != null) {
                        MainActivity.mActivity.finish();
                    }
                    mActivity.finish();
                    return true;
                } else {
                    AppDelegate.LogCh("User exists in server db.");
                }
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return false;
    }
}
