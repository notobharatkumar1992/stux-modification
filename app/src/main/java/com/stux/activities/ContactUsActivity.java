package com.stux.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 07/27/2016.
 */
public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {

    public Prefs prefs;
    public UserDataModel dataModel;
    public Handler mHandler;

    private EditText et_title, et_email, et_description;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.contact_us);
        prefs = new Prefs(ContactUsActivity.this);
        dataModel = prefs.getUserdata();
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(ContactUsActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(ContactUsActivity.this);
                }
            }
        };
    }

    private void initView() {
        ((TextView) findViewById(R.id.txt_c_header)).setText("Contact Us");
        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(this);

        et_title = (EditText) findViewById(R.id.et_title);
        et_title.setTypeface(Typeface.createFromAsset(ContactUsActivity.this.getAssets(), getString(R.string.font_roman)));
        et_email = (EditText) findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(ContactUsActivity.this.getAssets(), getString(R.string.font_roman)));
        et_description = (EditText) findViewById(R.id.et_description);
        et_description.setTypeface(Typeface.createFromAsset(ContactUsActivity.this.getAssets(), getString(R.string.font_roman)));

        findViewById(R.id.txt_c_submit).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                onBackPressed();
                break;

            case R.id.txt_c_submit:
                execute_contactUsAsync();
                break;
        }
    }

    private void execute_contactUsAsync() {
        if (et_title.length() == 0) {
            AppDelegate.showAlert(ContactUsActivity.this, "Please enter title.");
        } else if (et_email.length() == 0) {
            AppDelegate.showAlert(ContactUsActivity.this, ContactUsActivity.this.getString(R.string.Alert_Enter_email));
        } else if (!(AppDelegate.CheckEmail(et_email.getText().toString())) /*|| !et_email.getText().toString().substring(et_email.getText().toString().indexOf("@"), et_email.getText().toString().length()).equals("@edu.com")*/) {
            AppDelegate.showAlert(ContactUsActivity.this, getString(R.string.please_fill_email_address_in_format));
        } else if (et_description.length() == 0) {
            AppDelegate.showAlert(ContactUsActivity.this, "Please enter description.");
        } else if (!AppDelegate.haveNetworkConnection(ContactUsActivity.this, false)) {
            startActivity(new Intent(ContactUsActivity.this, NoInternetConnectionActivity.class));
        } else {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(ContactUsActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(ContactUsActivity.this).setPostParamsSecond(mPostArrayList, Tags.username, dataModel.first_name);
            AppDelegate.getInstance(ContactUsActivity.this).setPostParamsSecond(mPostArrayList, Tags.title, et_title.getText().toString());
            AppDelegate.getInstance(ContactUsActivity.this).setPostParamsSecond(mPostArrayList, Tags.email, et_email.getText().toString());
            AppDelegate.getInstance(ContactUsActivity.this).setPostParamsSecond(mPostArrayList, Tags.description, et_description.getText().toString());

            PostAsync mPostAsyncObj = new PostAsync(ContactUsActivity.this, this, ServerRequestConstants.FEEDBACK,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            startActivity(new Intent(ContactUsActivity.this, NoInternetConnectionActivity.class));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.FEEDBACK)) {
            mHandler.sendEmptyMessage(11);
            parseContactUsResult(result);
        }
    }

    private void parseContactUsResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                AppDelegate.showToast(ContactUsActivity.this, jsonObject.getString(Tags.MESSAGE));
                onBackPressed();
            } else {
                AppDelegate.showAlert(ContactUsActivity.this, jsonObject.getString(Tags.MESSAGE));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
}
