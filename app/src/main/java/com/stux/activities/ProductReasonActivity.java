package com.stux.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.ReasonModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.Utils.SpacesItemDecoration;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.ReasonRecyclerViewAdapter;

/**
 * Created by Bharat on 07/29/2016.
 */
public class ProductReasonActivity extends AppCompatActivity implements OnReciveServerResponse, OnListItemClickListener {

    private Prefs prefs;
    private UserDataModel dataModel;
    public static Activity mActivity;

    private Handler mHandler;

    private ArrayList<ReasonModel> arrayReasonList = new ArrayList<>();
    private GridLayoutManager gaggeredGridLayoutManager;
    private RecyclerView recyclerView;
    private ReasonRecyclerViewAdapter rcAdapter;
    private ProductModel productModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.reason_page);
        mActivity = this;
        prefs = new Prefs(ProductReasonActivity.this);
        dataModel = prefs.getUserdata();
        productModel = getIntent().getExtras().getParcelable(Tags.product);
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(ProductReasonActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(ProductReasonActivity.this);
                } else if (msg.what == 12) {
                } else if (msg.what == 13) {
                } else if (msg.what == 2) {
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                }
            }
        };
    }

    private void initView() {
        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.txt_c_header)).setText("Reason");
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        findViewById(R.id.ll_c_notification_dot).setVisibility(View.GONE);
        findViewById(R.id.img_c_notification).setVisibility(View.GONE);
        findViewById(R.id.img_c_search).setVisibility(View.GONE);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setPadding(AppDelegate.dpToPix(ProductReasonActivity.this, 5), AppDelegate.dpToPix(ProductReasonActivity.this, 5), AppDelegate.dpToPix(ProductReasonActivity.this, 5), AppDelegate.dpToPix(ProductReasonActivity.this, 5));
        recyclerView.setHasFixedSize(true);

        fillArray();
        gaggeredGridLayoutManager = new GridLayoutManager(ProductReasonActivity.this, 3);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);
        recyclerView.addItemDecoration(new SpacesItemDecoration(AppDelegate.dpToPix(ProductReasonActivity.this, 5), 3));
        rcAdapter = new ReasonRecyclerViewAdapter(ProductReasonActivity.this, arrayReasonList, this);
        recyclerView.setAdapter(rcAdapter);

    }

    private void fillArray() {
        if (arrayReasonList.size() == 0) {
            arrayReasonList.add(new ReasonModel("2", "Wrong category", R.drawable.reason_1));
            arrayReasonList.add(new ReasonModel("4", "Fake Item", R.drawable.reason_2));
            arrayReasonList.add(new ReasonModel("5", "Photo doesn't Match", R.drawable.reason_3));
            arrayReasonList.add(new ReasonModel("6", "Food", R.drawable.reason_4));
            arrayReasonList.add(new ReasonModel("7", "Drugs or Medicine", R.drawable.reason_5));
            arrayReasonList.add(new ReasonModel("8", "People or Animals", R.drawable.reason_6));
            arrayReasonList.add(new ReasonModel("9", "Offensive Content", R.drawable.reason_7));
            arrayReasonList.add(new ReasonModel("10", "Joke", R.drawable.reason_8));
            arrayReasonList.add(new ReasonModel("11", "This item should not be for sale because", R.drawable.reason_9));
        }
    }

    private void callReportProductAsync(ReasonModel reasonModel) {
        if (AppDelegate.haveNetworkConnection(ProductReasonActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
//            user_id=5&product_id=1&reason_id=3&reason_desc=xrtcfgvybu
            AppDelegate.getInstance(ProductReasonActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(ProductReasonActivity.this).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            AppDelegate.getInstance(ProductReasonActivity.this).setPostParamsSecond(mPostArrayList, Tags.reason_id, reasonModel.id);
            AppDelegate.getInstance(ProductReasonActivity.this).setPostParamsSecond(mPostArrayList, Tags.reason_desc, reasonModel.name);
            PostAsync mPostAsyncObj = new PostAsync(ProductReasonActivity.this,
                    this, ServerRequestConstants.REPORT_PRODUCT,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            startActivity(new Intent(ProductReasonActivity.this, NoInternetConnectionActivity.class));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (mActivity != null)
                startActivity(new Intent(ProductReasonActivity.this, NoInternetConnectionActivity.class));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.REPORT_PRODUCT)) {
            parseReportProductResult(result);
        }

    }

    private void parseReportProductResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                AppDelegate.showToast(ProductReasonActivity.this, "Thanks for your report.");
                finish();
            } else {
                AppDelegate.showAlert(ProductReasonActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.reason_id) && position == 8) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.product, productModel);
            Intent intent = new Intent(ProductReasonActivity.this, ProductReasonOtherActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (name.equalsIgnoreCase(Tags.reason_id)) {
            callReportProductAsync(arrayReasonList.get(position));
        }
    }
}
