package com.stux.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.stux.R;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;

import carbon.widget.TextView;

/**
 * Created by Bharat on 07/26/2016.
 */
public class WebViewDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressBar progressbar;
    private int value = 0;
    private TextView txt_c_header;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.web_view_page);
        value = getIntent().getExtras().getInt(Tags.FROM);
        initView();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    private void initView() {
        txt_c_header = (TextView) findViewById(R.id.txt_c_header);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        progressbar.setVisibility(View.GONE);

        findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        WebView webview = (WebView) findViewById(R.id.webview);

        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        progressbar.setVisibility(View.VISIBLE);
        webview.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressbar.setVisibility(View.VISIBLE);
            }

            public void onPageFinished(WebView view, String url) {
                if (progressbar.getVisibility() == View.VISIBLE) {
                    progressbar.setVisibility(View.GONE);
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(WebViewDetailActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
                if (progressbar.getVisibility() == View.VISIBLE) {
                    progressbar.setVisibility(View.GONE);
                }
            }
        });

        switch (value) {
            case 0:
                webview.loadUrl(ServerRequestConstants.HELP_TERMS_AND_CONDITIONS);
                txt_c_header.setText(getString(R.string.terms_amp_conditions));
                break;
            case 1:
                webview.loadUrl(ServerRequestConstants.HELP_PRIVACY_POLICY);
                txt_c_header.setText(getString(R.string.privacy_policy));
                break;
            case 2:
                webview.loadUrl(ServerRequestConstants.HELP_SAFETY_GUIDELINE);
                txt_c_header.setText(getString(R.string.safety_guidelines));
                break;
            case 3:
                webview.loadUrl(ServerRequestConstants.HELP_STUX_RULES);
                txt_c_header.setText(getString(R.string.stux_rules));
                break;
            case 4:
                webview.loadUrl(ServerRequestConstants.HELP_FAQ);
                txt_c_header.setText(getString(R.string.faq));
                break;
        }

    }

    @Override
    public void onClick(View v) {
    }

}
