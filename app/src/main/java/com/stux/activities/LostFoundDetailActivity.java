package com.stux.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.stux.Adapters.ProductPagerAdapter;
import com.stux.AppDelegate;
import com.stux.Models.ItemFoundModel;
import com.stux.R;
import com.stux.Utils.TransitionHelper;
import com.stux.constants.Tags;
import com.stux.fragments.NoInternetConnectionFragment;
import com.stux.interfaces.OnListItemClickListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 06/03/2016.
 */
public class LostFoundDetailActivity extends AppCompatActivity implements OnListItemClickListener {

    public ArrayList<String> arrayString = new ArrayList<>();
    private ImageView img_product;
    private TextView txt_c_name, txt_c_condition, txt_c_image_count, txt_c_address, txt_c_contact, txt_c_description, txt_c_date;
    private ItemFoundModel foundModel;

    private ViewPager view_pager;
    private ProductPagerAdapter mPagerAdapter;


    private GoogleMap map_business;
    private SupportMapFragment fragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(LostFoundDetailActivity.this);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            Drawable background = this.getResources().getDrawable(R.drawable.bg_app_theme);
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
//            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
//            window.setBackgroundDrawable(background);
//        }
        setContentView(R.layout.lost_found_detail_page);
        foundModel = getIntent().getExtras().getParcelable(Tags.FOUND);
        initView();
        setValues();
        initMap();
    }

    private void initMap() {
        fragment = SupportMapFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.map_frame, fragment, "MAP1").addToBackStack(null)
                .commit();
        fragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                showMap();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        super.onBackPressed();
    }

    private void setValues() {
        txt_c_name.setText(foundModel.item_name);
        txt_c_address.setText(foundModel.location);
        txt_c_contact.setText(foundModel.contact_no);
        txt_c_description.setText(Html.fromHtml("<b>Description : </b>" + foundModel.item_description));
        if (AppDelegate.isValidString(foundModel.created)) {
            try {
                txt_c_date.setText(Html.fromHtml("<b>Date : </b>" + new SimpleDateFormat("dd MMM yyyy, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(foundModel.created))));
            } catch (Exception e) {
                AppDelegate.LogE(e);
                txt_c_date.setText(Html.fromHtml("<b>Date : </b>" + foundModel.created + ""));
            }
        } else {
            txt_c_date.setVisibility(View.GONE);
        }
    }

    private void initView() {
        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.txt_c_header)).setText("Lost / Found");
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ((carbon.widget.ImageView) findViewById(R.id.img_c_right)).setImageResource(R.drawable.edit);
        findViewById(R.id.img_c_right).setVisibility(View.GONE);

        img_product = (ImageView) findViewById(R.id.img_product);

        txt_c_name = (TextView) findViewById(R.id.txt_c_name);


        txt_c_condition = (TextView) findViewById(R.id.txt_c_condition);
        txt_c_image_count = (TextView) findViewById(R.id.txt_c_image_count);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);
        txt_c_contact = (TextView) findViewById(R.id.txt_c_contact);
        txt_c_description = (TextView) findViewById(R.id.txt_c_description);
        txt_c_date = (TextView) findViewById(R.id.txt_c_date);

        if (arrayString.size() == 0) {
            if (AppDelegate.isValidString(foundModel.image_thumb_4)) {
                AppDelegate.LogT("LostFound => 4");
                txt_c_image_count.setText("4");
                arrayString.add(foundModel.image_thumb_1);
                arrayString.add(foundModel.image_thumb_2);
                arrayString.add(foundModel.image_thumb_3);
                arrayString.add(foundModel.image_thumb_4);
            } else if (AppDelegate.isValidString(foundModel.image_thumb_3)) {
                txt_c_image_count.setText("3");
                arrayString.add(foundModel.image_thumb_1);
                arrayString.add(foundModel.image_thumb_2);
                arrayString.add(foundModel.image_thumb_3);
            } else if (AppDelegate.isValidString(foundModel.image_thumb_2)) {
                txt_c_image_count.setText("2");
                arrayString.add(foundModel.image_thumb_1);
                arrayString.add(foundModel.image_thumb_2);
            } else if (AppDelegate.isValidString(foundModel.image_thumb_1)) {
                txt_c_image_count.setText("1");
                arrayString.add(foundModel.image_thumb_1);
            } else {
                txt_c_image_count.setText("0");
            }
        }

        mPagerAdapter = new ProductPagerAdapter(LostFoundDetailActivity.this, arrayString, this);
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        view_pager.setAdapter(mPagerAdapter);
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        findViewById(R.id.view_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppDelegate.haveNetworkConnection(LostFoundDetailActivity.this, false)) {
                    startActivity(new Intent(LostFoundDetailActivity.this, NoInternetConnectionActivity.class));
                } else {
                    Intent intent = new Intent(LostFoundDetailActivity.this, MapDetailActivity.class);
                    intent.putExtra(Tags.LAT, foundModel.lat);
                    intent.putExtra(Tags.LNG, foundModel.lng);
                    intent.putExtra(Tags.name, txt_c_address.getText().toString());
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(LostFoundDetailActivity.this, false, new Pair<>(txt_c_address, "Location"));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(LostFoundDetailActivity.this, pairs);
                    startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                }
            }
        });
    }

    private void showMap() {
        map_business = fragment.getMap();
        if (map_business == null) {
            return;
        }

        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        AppDelegate.setMyLocationBottomRightButton(LostFoundDetailActivity.this, fragment);
        if (foundModel != null && AppDelegate.isValidString(foundModel.lat) && AppDelegate.isValidString(foundModel.lng)) {
            AppDelegate.LogT("location showing => " + foundModel.lat + "," + foundModel.lng);
            map_business.addMarker(AppDelegate.getMarkerOptions(LostFoundDetailActivity.this, new LatLng(Double.parseDouble(foundModel.lat), Double.parseDouble(foundModel.lng)), R.drawable.map_pin));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(foundModel.lat), Double.parseDouble(foundModel.lng)))
                    .zoom(14).build();
            //Zoom in and animate the camera.
            map_business.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    private int item_position = 0;

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
        this.item_position = position;
        if (name.equalsIgnoreCase(Tags.LIST_ITEM_TRENDING)) {
            if (AppDelegate.haveNetworkConnection(LostFoundDetailActivity.this, false)) {
                Intent intent = new Intent(LostFoundDetailActivity.this, LargeImageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.image_1, foundModel.image_thumb_1);
                bundle.putString(Tags.image_2, foundModel.image_thumb_2);
                bundle.putString(Tags.image_3, foundModel.image_thumb_3);
                bundle.putString(Tags.image_4, foundModel.image_thumb_4);
                bundle.putInt(Tags.POSITION, position);
                bundle.putInt(Tags.count, 4);
                intent.putExtras(bundle);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(LostFoundDetailActivity.this, false, new Pair<>(view_pager, "Image"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(LostFoundDetailActivity.this, pairs);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
            }
        } else {
            AppDelegate.addFragment(LostFoundDetailActivity.this.getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }
}
