package com.stux.parser;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by parul on 26/12/15.
 */
public class Fb_detail_GetSet implements Parcelable {

    public Fb_detail_GetSet() {

    }

    public String name, f_name, l_name, dob, gender, id, email, phone, image;

    protected Fb_detail_GetSet(Parcel in) {
        name = in.readString();
        f_name = in.readString();
        l_name = in.readString();
        dob = in.readString();
        gender = in.readString();
        id = in.readString();
        email = in.readString();
        phone = in.readString();
        image = in.readString();
    }

    public static final Creator<Fb_detail_GetSet> CREATOR = new Creator<Fb_detail_GetSet>() {
        @Override
        public Fb_detail_GetSet createFromParcel(Parcel in) {
            return new Fb_detail_GetSet(in);
        }

        @Override
        public Fb_detail_GetSet[] newArray(int size) {
            return new Fb_detail_GetSet[size];
        }
    };


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(f_name);
        dest.writeString(l_name);
        dest.writeString(dob);
        dest.writeString(gender);
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(image);
    }
}
