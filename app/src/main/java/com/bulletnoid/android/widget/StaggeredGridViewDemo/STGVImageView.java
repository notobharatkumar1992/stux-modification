package com.bulletnoid.android.widget.StaggeredGridViewDemo;

import android.content.Context;
import android.graphics.ColorMatrixColorFilter;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import com.stux.AppDelegate;

public class STGVImageView extends ImageView {

    public int mWidth = 10;
    public int mHeight = 10, heightC = 10;

    private static final float Trans = -25f;

    private final static float[] BT_SELECTED = new float[]{
            1, 0, 0, 0, Trans,
            0, 1, 0, 0, Trans,
            0, 0, 1, 0, Trans,
            0, 0, 0, 1, 0};

    private final static float[] BT_NOT_SELECTED = new float[]{
            1, 0, 0, 0, 0,
            0, 1, 0, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 0, 1, 0};

    private ColorMatrixColorFilter mPressFilter;
    private ColorMatrixColorFilter mNormalFilter;

    private Context context;

    public STGVImageView(Context context) {
        super(context);
        this.context = context;
    }

    public STGVImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public STGVImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (getDrawable() != null) {
                    if (mPressFilter == null) {
                        mPressFilter = new ColorMatrixColorFilter(BT_SELECTED);
                    }
                    getDrawable().setColorFilter(mPressFilter);
                }
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (getDrawable() != null) {
                    if (mNormalFilter == null) {
                        mNormalFilter = new ColorMatrixColorFilter(BT_NOT_SELECTED);
                    }
                    getDrawable().setColorFilter(mNormalFilter);
                }
                break;
            default:
                break;
        }
        return super.onTouchEvent(event);
    }

    public final int MAX_HEIGHT_EXCLUDE_WIDTH = 150;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        int widthMode = View.MeasureSpec.getMode(widthMeasureSpec);
//        int heightMode = View.MeasureSpec.getMode(heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        try {
            heightC = width * mHeight / mWidth;
            if (heightC > (AppDelegate.getDeviceWith(context) - AppDelegate.dpToPix(context, MAX_HEIGHT_EXCLUDE_WIDTH))) {
                heightC = AppDelegate.getDeviceWith(context) - AppDelegate.dpToPix(context, MAX_HEIGHT_EXCLUDE_WIDTH);
                AppDelegate.LogT("bitmap => " + mWidth + ", " + mHeight + ", height => " + heightC);
            } else {
                AppDelegate.LogT("bitmap => " + mWidth + ", " + mHeight);
            }
            setMeasuredDimension(width, heightC);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
}
